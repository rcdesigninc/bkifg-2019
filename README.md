# bkifg-2019
Check app/Settings/Database_settings.php - use same users and database name on local
Download database from RCMS.IO database area
Import database - create user on database same as settings
Add the following to the end of httpd.conf to increase the Apache stack size to 8MB.
<IfModule mpm_winnt_module>
   ThreadStackSize 8388608
</IfModule>