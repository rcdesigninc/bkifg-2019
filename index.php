<?php
if ($_SERVER['SERVER_NAME']!="bkifg.com"){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

require_once('_config.php');

include_once("m/Mobile_Detect.php");
$detect = new Mobile_Detect();
if($detect->isMobile() && !$_SESSION['mobile-override']) {
	$_SESSION['mobile'] = true;
	header("location: m/index.php?section=".PAGE_TAG);
}

$_SESSION['page_id'] = PAGE_ID;

$System->addInclude('<script type="text/javascript" src="' . TEMP_JS . 'Main.js"></script>');

?>
<!-- Hotjar Tracking Code for http://bkifg.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:605272,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<?php

/*if(PAGE_TAG=='home'){
	$System->addJquery('
	$("#contest-dialog").bPopup(bPopupDefaults);
	');
}*/

if (file_exists(PAGE_TAG . ".php")) include_once(PAGE_TAG . ".php");

else

	$System->renderDefaultPage();



/*

 * EOF

 */