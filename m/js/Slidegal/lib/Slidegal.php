<?

include(dirname(__FILE__) . '/Slidegal_data.php');

class Slidegal extends Plugin {
	public $data = null;

	/**
	 * @param Slidegal_data $data
	 */
	public function __construct(Slidegal_data $data) {
		$this->data = $data;
		$this->process();

		//die(printArray($this));
	}

	public function process() {
		System::$instance->addJquery("$('#{$this->data->id}').RC_Slidegal(" . $this->data->setJson() . ");");

		//die(printArray($this));

		$str = '';
		if ($this->data->show_nav)
			$str .= sprintf('
				<div id="%1$s-controls" class="slidegal-controls hidden">
					<div id="%1$s-next" class="next"></div>
					<div id="%1$s-prev" class="prev"></div>
				</div>', $this->data->id);

		$str .= "<div id='{$this->data->id}'>";

		$str .= $this->data->entries;

		$str .= "</div>";

		if ($this->data->show_nav) {
			if ($this->data->navigation) {
				$str .= sprintf('
				<div id="%1$s-nav" class="slidegal-nav hidden">
					%2$s
				</div>', $this->data->id, $this->data->navigation);
			}
			else {
				$str .= sprintf('
				<div id="%1$s-nav" class="slidegal-nav hidden">
					<div class="slidegal-inner-nav" id="%1$s-inner-nav">
					</div>
				</div>', $this->data->id);
			}
		}

		$this->data->output = $str;
	}

	/**
	 * @return string
	 */
	public function render() {
		return $this->data->output;
	}


	public  static  function ResizeImage($file, $target_width, $target_height) {
		if (is_file($file)) {
			list($image_width, $image_height, $image_type, $image_attr) = getimagesize($file);

			$ratio = 1;
			if ($image_height > $image_width && $image_height > $target_height)
				$ratio = ($target_height / $image_height);
			else if ($image_width > $image_height && $image_width > $target_width)
				$ratio = ($target_width / $image_width);
			$thumb_width = round($image_width * $ratio);
			$thumb_height = round($image_height * $ratio);

			if ($image_type == 1) {
				// First, load the image into memory.
				$image_path = ImageCreateFromGIF($file);
				// next, create a canvas for the new photo.
				$thumb = ImageCreateTrueColor($thumb_width, $thumb_height);
				ImageCopyResampled($thumb, $image_path, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width,
					$image_height);
				ImageGIF($thumb, $file);
				ImageDestroy($image_path);
				ImageDestroy($thumb);
				return true;
			}
			if ($image_type == 2) {
				// First, load the image into memory.
				$image_path = ImageCreateFromJPEG($file);
				// next, create a canvas for the new photo.
				$thumb = ImageCreateTrueColor($thumb_width, $thumb_height);
				ImageCopyResampled($thumb, $image_path, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width,
					$image_height);
				ImageJPEG($thumb, $file);
				ImageDestroy($image_path);
				ImageDestroy($thumb);
				return true;
			}
			if ($image_type == 3) {
				// First, load the image into memory.
				$image_path = ImageCreateFromPNG($file);
				// next, create a canvas for the new photo.
				$thumb = ImageCreateTrueColor($thumb_width, $thumb_height);

				ImageCopyResampled($thumb, $image_path, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width,
					$image_height);

				$alphabg = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
				imagefill($thumb, 0, 0, $alphabg);
				imagealphablending($thumb, true);
				imagesavealpha($thumb, true);

				ImagePNG($thumb, $file);
				ImageDestroy($image_path);
				ImageDestroy($thumb);
				return true;
			}
			return false;
		}
	}
}

/*
 * EOF
 */