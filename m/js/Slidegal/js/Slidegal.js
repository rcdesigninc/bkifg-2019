//RC_SLIDEGAL_AJAX = PLUGIN_PATH + 'Slidegal/ajax/Slidegal.php';
(function($) {
	$.fn.RC_Slidegal = function(options) {
		return this.each(function() {

			//debug("Slidegal::run - " + JSON.stringify(options));

			$container = $(this);
			$controls = $container.prev();
			$nav = $container.next();

			var id = $container.attr('id');

			if ($container.attr("data-cycle-id"))
				return;

			if (!id || id == 'undefined')
				id = $.Guid.New();

			$container.attr("data-cycle-id", id);

			if (options.speed == 'undefined')
				options.speed = 2000;

			if (options.fx == 'undefined')
				options.fx = 'fade';

			easing = null;
			if (options.ease != 'undefined')
				easing = options.ease;

			if (options.transitionSpeed == 'undefined')
				options.transitionSpeed = 2000;

			$container.css('width', options.width);
			$container.css('height', options.height);

			// Set the control buttons to be on top of the slidegal
			$controls.css('z-index', $container.css('z-index') + 1);
			var top = options.height / 2 - $controls.find('.next').height() / 2;
			var left = options.width - $controls.find('.next').width();
			$controls.find('.prev, .next').css('top', top);
			$controls.find('.prev').css('left', '0');
			$controls.find('.next').css('left', left);

			$nav.css('top', parseInt($nav.css('top')) + options.height);

			//.before("" + "<div class='slidegal-controls'>" + "	<div id='" + id + "-next' class='next'><img src='web/media/images/common/arrow-right.png' width='28' height='28' alt='Next Image'/></div>" + "	<div id='" + id + "-prev' class='prev'><img src='web/media/images/common/arrow-left.png' width='28' height='28' alt='Prev Image'/></div>" + "	<p id='" + id + "-caption' class='caption'></p>" + "</div>")
			// .after("<div class='nav' id='" + id + "-nav'><div class='inner-nav' id='" + id + "-inner-nav'></div></div>")

			//debug("run cycle on: " + id);

			if (options.show_nav == 1) {
				$controls.show();
				$nav.show();
			} else if (options.show_nav == 2) {
				$controls.show();
			} else if (options.show_nav == 3) {
				$nav.show();
			}

			var pagerAnchorBuilder = null;
			var updateActivePagerLink = null;

			if (!options.custom_pager) {
				pagerAnchorBuilder = function(idx, slide) {
					return '<div id="' + id + '-pager' + idx + '" class="' + id + '-pager pager"></div>';
				}
				updateActivePagerLink = function(pager, idx) {
					////debug("updating, #" + id + "-pager" + idx);
					$("#" + id + "-inner-nav").css('width', ($("." + id + "-pager").length * ($("." + id + "-pager:first").width() + $("." + id + "-pager:first").css('margin-right') + $("." + id + "-pager:first").css('margin-left'))) + "px");
					$("." + id + "-pager").removeClass('activeSlide');
					$("#" + id + "-pager" + idx).addClass('activeSlide');
				}
			}

			$container.cycle({
				containerResize: 1,   // resize container to fit largest slide
				delay:         0,     // additional delay (in ms) for first transition (hint: can be negative)
				easing:        easing,  // easing method for both in and out transitions
				fit:           0,     // force slides to fit container
				fx:            options.animate,// name of transition effect (or comma separated names, ex: 'fade,scrollUp,shuffle')
				fxFN: function(currSlideElement, nextSlideElement, options, afterCalback, forwardFlag)  {
					setBlocks($(nextSlideElement), 0, 75, 0);
					diagAnimate($img, {opacity:1}, false);
				},
				height:        'auto',// container height (if the 'fit' option is true, the slides will be set to this height as well)
				manualTrump:   true,  // causes manual transition to stop an active transition instead of being ignored
				next:          null,  // element, jQuery object, or jQuery selector string for the element to use as event trigger for next slide
				nowrap:        0,     // true to prevent slidegal from wrapping
				pause:         0,     // true to enable \"pause on hover\"
				prev:          null,  // element, jQuery object, or jQuery selector string for the element to use as event trigger for previous slide
				random:        0,     // true for random, false for sequence (not applicable to shuffle fx)
				speed:         options.transitionSpeed,  // speed of the transition (any valid fx speed value)
				startingSlide: 0,     // zero-based index of the first slide to be displayed
				timeout:       options.speed,  // milliseconds between slide transitions (0 to disable auto advance)
				timeoutFn:     null,  // callback for determining per-slide timeout value:  function(currSlideElement, nextSlideElement, options, forwardFlag)
				width:         null   // container width (if the 'fit' option is true, the slides will be set to this width as well)
				,pager:  "#" + id + "-inner-nav"
				,pagerAnchorBuilder: pagerAnchorBuilder
				,updateActivePagerLink: updateActivePagerLink
				,before:     function() {
					/*
					 if($(this).attr('alt') != '')
					 $container.prev().find('.caption').slideUp();
					 else
					 $container.prev().find('.caption').hide();
					 */
				},
				after:     function() {
					/*
					 if($(this).attr('alt') != '')
					 $container.prev().find('.caption').html($(this).attr('alt')).slideDown();
					 else
					 $container.prev().find('.caption').hide();
					 */
				}
			});

			//set blocks
			var setBlocks = function($img, opacity, size, tPos) {
				var tOffset = 0;
				var lOffset = 0;
				if (rotator.autoCenter()) {
					tOffset = (areaHeight - $img.height())/2;
					lOffset = (areaWidth - $img.width())/2;
				}
				var imgSrc = $img.attr("src");
				for (var i = 0; i < numRows; i++) {
					for (var j = 0; j < numCols; j++) {
						var tVal = ((-i * blockSize) + tOffset);
						var lVal = ((-j * blockSize) + lOffset);
						$blockArr[i][j].css({background:bgColor + " url('"+ imgSrc +"') no-repeat", backgroundPosition:lVal + "px " + tVal + "px",
											 opacity:opacity, top:(i * blockSize) + tPos, left:(j * blockSize), width:size, height:size, "z-index":3});
					}
				}
			}

			//diagonal effect
			var diagAnimate = function($img, props, rev) {
				var $array = new Array(total);
				var start, end, incr, lastId;
				var diagSpan = (numRows - 1) + (numCols - 1);
				if (rev) {
					start = diagSpan;
					end = -1;
					incr = -1;
					lastId = $blocks.data("tlId");
				}
				else {
					start = 0;
					end = diagSpan + 1;
					incr = 1;
					lastId = $blocks.data("brId");
				}

				var count = 0;
				while (start != end) {
					i = Math.min(numRows - 1, start);
					while(i >= 0) {
						j = Math.abs(i - start);
						if (j >= numCols) {
							break;
						}
						$array[count++] = $blockArr[i][j];
						i--;
					}
					start+=incr;
				}

				count = 0;
				intervalId = setInterval(
					function() {
						$array[count++].animate(props, duration, rotator.easing(),
								function() {
									if ($(this).attr("id") == lastId) {
										rotator.setComplete($img);
									}
								});
						if (count == total) {
							clearInterval(intervalId);
						}
					}, delay);
			}
		});
	};
	$('.slidegal-controls .next').live('click', function() {
		$(this).parent().next().cycle('next');
	});
	$('.slidegal-controls .prev').live('click', function() {
		$(this).parent().next().cycle('prev');
	});
})
(jQuery);