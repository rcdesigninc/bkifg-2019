<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<title>Benson Kearley IFG Mobile Site</title>

	<meta name="viewport" content="width = 640"/>

    <script src="//use.typekit.net/acw8upo.js"></script>

    <script>try{Typekit.load();}catch(e){}</script>

	<link href="css/_UniversalReset.css" rel="stylesheet"/>

	<link href="css/ChapparalPro/stylesheet.css" rel="stylesheet"/>

	<link href="css/positioning.css?v=1.0.0" rel="stylesheet"/>



	<link href="css/magnific.css" rel="stylesheet" />

	<style>

	#my-popup.my-driving-discount{height:auto !important;}

	</style>

</head>

<body>



<!-- Popup itself -->
 
<?php if(!isset($_GET['test'])) : ?>



    <!-- Popup itself -->
    
     <div id="my-popup" class="white-popup mfp-with-anim mfp-hide my-driving-discount">

    		<a href="https://www.snowbirdmediquote.com/clientportal/loginpage.aspx?brokerrefererid=84491&DownlineBrokerCompany=442" >

                    <img src="/app/Templates/images/Mobile_Modal.jpg" style="width:100%;">

    		</a>

		

    </div>

   
<?php else : ?>

  <div id="my-popup" class="white-popup mfp-with-anim mfp-hide my-driving-discount">



    		<a href="http://drivensave.ca" target="_blank">

    		<img src="http://drivensave.ca/wp-content/themes/intactlanding-dev/images/mobile-modal.png" style="width:100%;">

    		</a>

		

    </div>


    <!-- Popup itself -->

<!--    <div id="my-popup" class="white-popup mfp-with-anim mfp-hide">

        <img src="../app/Templates/images/header-logo-mobile.png" alt="Logo">

        <hr />



        <div class="logo-contain float-right">

            <img src="../app/Templates/images/modal-profit2014.png" style="position:relative; padding-bottom:10px;" alt="Logo2014">

            <img src="../app/Templates/images/modal-profit.png" alt="Logo">

        </div>



        <p style="line-height: 45px;">



        <span class="italic">For the second<br/>

        consecutive year

        </span>



        <span class="red subhead">

              Benson Kearley IFG </span> <br/>

            has been named to <br/>



        <span class="sans-serif">

            Profit Magazine Top 500 Fastest-Growing Companies in <br/> Canada!

        </span>

        </p>

    </div>-->



<?php endif; ?>





<div class="wrapper-div">

	<div class="center-div">

		<div>
<!-- 
			<img src="img/logo.png" width="640" height="135" /> -->

                    <div class="header-logo-background-mobile"></div>

		</div>

		<div>

			<div class="Slidegal_data-controls" contenteditable="Slidegal_data-controls"></div>

			<div style="width: 640px; height: 220px;" id="Slidegal_data" class="Slidegal">

				<div>

					<img src="img/gallery/header3.jpg" width="640" height="220" />

				</div>

				<div>

					<img src="img/gallery/header1.jpg" width="640" height="220" />

				</div>

				<div>

					<img src="img/gallery/header2.jpg" width="640" height="220" />

				</div>

			</div>

			<div class="Slidegal_data-inner-nav" contenteditable="Slidegal_data-inner-nav">

				<div class="Slidegal_data-pager" contenteditable="Slidegal_data-pager"></div>

			</div>

		</div>

		<div>

			<img src="img/body.png" width="640" height="388" />

		</div>

		<div class="direct-contact">

			<div class="float-left">

				<img src="img/text-direct-contact.png" width="341" height="140" />

			</div>

			<div class="float-right map">

				<a href="http://maps.google.ca/maps?q=Benson%20Kearley%20IFG,%2017705%20Leslie%20St.%20Suite%20101,%20Newmarket%20ON%20L3Y%203E3&hl=en&ll=44.070991,-79.432515&spn=0.0072,0.016512&sll=43.967433,-79.405214&sspn=0.473444,1.056747&vpsrc=6&hq=Benson%20Kearley%20IFG,&hnear=17705%20Leslie%20St,%20Newmarket,%20Ontario%20L3Y%203E3&t=h&z=17&iwloc=A"><img src="img/icon-map.png" width="104" height="128" /></a>

			</div>

		</div>

		<div class="details">

			<p><span>Phone:</span> 905.898.3815</p>



			<p><span>Toll Free:</span> 1.800.463.6503</p>



			<p><span>Email:</span> <a href="mailto:info@bensonkearleyIFG.com">info@bensonkearleyIFG.com</a></p>

		</div>

		<div class="video">

			<iframe src="http://player.vimeo.com/video/37256927" width="599" height="331" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

		</div>

		<div style="height: 73px; overflow: hidden;">

			<img src="img/text-get-a-quote.png" width="640" height="73" />

		</div>

		<div>

			<div class="float-left home-insur insur">

				<div>

					<a href="https://www2.compu-quote.com/ezleadsplus/introhab.asp?ORGSITE=ezLeadsplus&amp;BRKRCDE=BKAI" target="_blank" class="home-quote"> For Home<br />Insurance

					</a>

				</div>

			</div>

			<div class="float-right auto-insur insur">

				<div>

					<a href="https://www2.compu-quote.com/ezleadsplus/intro.asp?ORGSITE=ezLeadsplus&amp;BRKRCDE=BKAI" target="_blank" class="home-quote"> For Auto<br />Insurance

					</a>

				</div>

			</div>

		</div>

		<div class="clear-float-both">

			<img src="img/footer-spacer.png" width="640" height="30" />

		</div>

		<div style="height: 256px; overflow: hidden;">

			<a href="non-mobile.php?section=<?=$_GET['section']?>"><img src="img/footer.png" width="640" height="256" /></a>

		</div>

	</div>

</div>

</body>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>





<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>



	<script type="text/javascript">

	$.magnificPopup.open({

	  items: {

	      src: '#my-popup',

	      type: 'inline'

	  }

	}, 0);

	</script>





<script type="text/javascript" src="js/Slidegal/js/jquery.cycle.all.min.js"></script>

<script type="text/javascript" src="js/Slidegal/js/Slidegal.js"></script>

<script type="text/javascript">

	jQuery(document).ready(function () {

		$('#Slidegal_data').RC_Slidegal({"id" : "Slidegal_data", "width" : 640, "height" : 220, "speed" : 5000, "animate" : "fade", "show_nav" : 0, "customer_pager" : false});

	});

</script>



</html>

