<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">

        <title>Benson Kearley IFG Mobile Site</title>

        <meta name="viewport" content="width = 640"/>

        <script src="//use.typekit.net/acw8upo.js"></script>

        <script>try {
                Typekit.load();
            } catch (e) {
            }</script>


        <script type="text/javascript" src="http://use.typekit.com/clg5rpo.js"></script>
        <style type="text/css">.tk-chaparral-pro{font-family:"chaparral-pro",sans-serif;}.tk-adelle{font-family:"adelle",serif;}</style><link rel="stylesheet" href="http://use.typekit.com/c/8f8c39/1w;adelle,2,XK9:F:n4,XKF:F:n7;chaparral-pro,2,XXw:F:i4,XY8:F:n4,XY9:F:n6/d?3bb2a6e53c9684ffdc9a9bff1b5b2a629811bf9003a7728c0cd239af52f456c92852b61e4f634132c8b9b576ebdd529f4df9767acda8c560469b56832797bb66d6f7c72fcc1fc8d93de9b49ad5272a2f9891dc94df601f2dee09583e40fed44897ce08e36a3d4d695fce796fd68e8cc326985c20c423b2c27e6fc1f47a02ed64f18622b1aac84cd2aeff4b89fba71ba2644b030be9482ba542e290808e1b99dc181e43fc0e6eb8638d8cfef30992f18abe6613f17561e2fe41f250d93f45a7"><script type="text/javascript">try {
                Typekit.load();
            } catch (e) {
            }</script>


        <script src="//use.typekit.net/acw8upo.js"></script>
        <style type="text/css">.tk-proxima-nova{font-family:"proxima-nova",sans-serif;}</style><link rel="stylesheet" href="https://use.typekit.net/c/620093/1w;proxima-nova,7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191,cdh:T:i4,cdf:T:i6,cdX:T:i7,cdg:T:n4,cdd:T:n6,g8x:T:n7/l?3bb2a6e53c9684ffdc9a9bfe195b2a6289048524135a8fa786c246a9f0e51dc0949543be8352e2ec281c6d337245e7bd038725cf8f2737fc84e7c2bd16d2446322a5dc0de59b43f88d60a0f9c76cead1fbf76d63c2f31f92b4e74500a24d627a579404042f6b8ba56e7eb066a48df80e33bfd664e49474fd8c19c9c8844f7302023217916c09ffa4582fef73c2c5845903eaba3f07ca7995453b79f50fb0866e497240290e3f27093ccb93380b99edc14ba5387cffcd56" media="all"><script>try {
                Typekit.load();
            } catch (e) {
            }</script>

        <link type="text/css" rel="stylesheet" href="../app/Templates/css/System/font-face/Chaparral/Chaparral.css">




        <link href="css/_UniversalReset.css" rel="stylesheet"/>

        <link href="css/ChapparalPro/stylesheet.css" rel="stylesheet"/>

        <link href="css/positioning.css" rel="stylesheet"/>



        <link href="css/magnific.css" rel="stylesheet" />

        <style>

            #my-popup.my-driving-discount{height:auto !important;}
            .video {
                left: 0px;
            }
            .content-heading {
                line-height: 1em;
                font-size: 35px !important;
                font-weight: normal;
                color: #8c0c04;
                margin-bottom: 0;
                line-height: 40px !important;
            }
            .container {
                padding: 40px;
                margin-top: 30px;

            }
            p {
                margin: 0 0 1.5em;

            }
            .content-body {
                margin-top: 20px;

            }
            .content-body first{
                font-family: 'chaparral-pro',Georgia,"Times New Roman",Times,serif;
            }
            .content-body p{
                font-size: 22px ;
            }
            a[href$=".pdf"] img {
                display: none;
            }
            .core-link {
                font-weight: 600;
                color: #3d3d3d;
                font-size: 22px !important;
            }
            a {
                font-size: 22px !important;
            }
            .financial-p {
                font-size: 30px;
                font-family: 'chaparral-pro',Georgia,"Times New Roman",Times,serif;
            }
        </style>
        <link type="text/css" rel="stylesheet" href="../app/Minify/?g=main-css">

    </head>

    <body>






        <div class="wrapper-div">

            <div class="center-div">

                <div>

                    <img src="img/logo.png" width="640" height="135" />

                </div>

                <div>

                    <div class="Slidegal_data-controls" contenteditable="Slidegal_data-controls"></div>

                    <div style="width: 640px; height: 570px;">

                        <div>

                            <img src="img/Attention_Banner.png" width="640" height="570" />

                        </div>

                    </div>

                    <div class="Slidegal_data-inner-nav" contenteditable="Slidegal_data-inner-nav">

                        <div class="Slidegal_data-pager" contenteditable="Slidegal_data-pager"></div>

                    </div>

                </div>

                <div class="container">
                    <div class="content-section ">
                        <h2 class="content-heading"></h2>
                        <div class="content-body first"><p class="content-heading" style="font-size: 28px; font-weight: 600; color: #8c0c04; margin-bottom: 10px;">It is important you understand these changes.</p>
                            <p class="content-heading" style="font-weight: normal; color: #8c0c04; margin-bottom: 0;">If you have any questions,&nbsp;<strong>please call our office&nbsp;</strong>at 905.898.3815 or toll-free at 800.463.6503</p>
                            <p class="content-heading" style="font-weight: normal; color: #8c0c04; margin-bottom: 0;">&nbsp;</p>
                            <p><span class="financial-p">The Financial Services Commission of Ontario has mandated changes to the automobile insurance system with the intended goal of reducing premiums in Ontario. &nbsp;You will have more choice and control over your insurance and premium.</span></p>
                            <p>&nbsp;</p>
                            <iframe src="https://www.youtube.com/embed/IhpS7dzJsBM" frameborder="0" width="560" height="340"></iframe>
                        </div>
                    </div>
                    <div class="content-section ">
                        <h2 class="content-heading">
                            There are THREE major changes to the Standard Accident Benefits:
                        </h2>
                        <div class="content-body"><p>1. A <strong>reduction</strong> in the non-catastrophic benefit limits and the duration that benefits are available;</p>
                            <p>2. A major <strong>reduction</strong> in the scope of non-earner benefits; and</p>
                            <p>3. A <strong>reduction</strong> in the catastrophic impairment benefit limits.</p>
                            <p><strong>Regardless of who is at-fault</strong>, many of the changes affect the statutory Accident Benefits you may receive should you be injured in an automobile accident.</p>
                            <p><strong>Your coverage, on renewal, will default to the new lower standard benefits shown below if you do not contact our office to increase your benefits or make changes. </strong><strong>If you have previously chosen to purchase these optional benefits, check your policy – they may have changed to reflect amounts available in new options. Complete details of these changes will be included with your renewal. You may make changes any time after June 1, 2016.</strong></p>

                        </div>
                    </div>
                    <div class="content-section ">
                        <h2 class="content-heading">Most significant changes:</h2>
                        <div class="content-body"><img src="../app/Uploads/site_page_sections/heading372.png"><p><strong><span><br></span></strong></p>
                            <p><strong><span>Some important things to consider about these choices:</span></strong></p>
                            <ul>
                                <li><span>
                                        <p>Medical, Rehabilitation and Attendant Care benefits for minor injuries are fixed at a maximum limit of $3,500</p>
                                    </span></li>
                                <li><span>
                                        <p>If clients purchase both additional Medical, Rehabilitation and Attendant Care benefit for catastrophic injuries <br> for all injuries, the total eligible benefit amount for catastrophic impairment is $3,000,000.</p>
                                    </span></li>
                            </ul>
                            <p><span><strong>* Considerations for updating your coverage</strong></span><span>: * You are a primary caregiver of young children, elderly parents or disabled family members * You earn more than $30,000 a year * You are a snowbird * You are responsible for housekeeping or home maintenance * You have a pre-existing injury that requires regular treatments like physiotherapy or massage * Your auto insurance policy is your only form of coverage (i.e. you do not have a group plan through an employer or spouse)</span></p>
                        </div>
                    </div>
                    <div class="content-section ">
                        <h2 class="content-heading">Additional changes:</h2>
                        <div class="content-body"><p><strong>Minor Accident</strong></p>
                            <p><span>Insurers can no longer use a minor at-fault accident that occurs on or after June 1, 2016 (meeting certain criteria) to increase your premiums. &nbsp;Criteria include that no payment has been made by any insurer, that there are no injuries and that damages to each car and property were less than $2,000 per car and were paid for by the at-fault driver. This provision is limited to one minor accident every three years.</span></p>
                            <p><span><strong>Interest Rate for Monthly Payment Plans &nbsp;</strong></span></p>
                            <p><span>The maximum interest rate that insurers can charge if you make monthly premium payments has been </span><span><strong>lowered</strong></span><span> from 3% to 1.3% for one year policies, with corresponding reductions for shorter terms.</span></p>
                            <p><span><strong>Comprehensive Deductible</strong></span></p>
                            <p><span>The standard deductible for Comprehensive coverage has been </span><span><strong>increased</strong></span><span> from $300 to $500.&nbsp;</span></p>
                            <p><span><strong>Non-Earner benefit&nbsp;</strong></span></p>
                            <p><span>The six-month waiting period for people who are not working to receive benefits has been </span><span><strong>reduced</strong></span><span> to four weeks. Conversely, benefits can now only be received for up to two years after the accident.</span></p>
                            <p><span><strong>Duration of Medical, Rehabilitation and Attendant Care benefits&nbsp;</strong></span></p>
                            <p><span>For all claimants except children, the amount of time that you can receive this standard benefit is now </span><span><strong>five years</strong></span><span> for non-catastrophic injuries, and it will be paid only as long as you remain medically eligible.</span></p>
                            <p><span><strong>Umbrella insurance</strong></span><span> is an option which could save you money and provide additional coverage.</span></p>
                            <p><span>This document contains a summary of the amendments only. Please contact our office if you do not understand how these changes will affect you. For a complete list of all amendments, please refer to:&nbsp;</span><a href="http://www.fsco.gov.on.ca/" target="_blank">www.fsco.gov.on.ca</a></p>
                            <p>We are committed to providing you with expert advice on insurance products and services, specific to your needs.</p>
                            <p><span>Thank you for the privilege of allowing us to service your insurance needs.</span></p>
                            <p><span><a href="http://bensonkearleyifg.com/app/Templates/images/Auto Reform Insert - April 12th.pdf" target="_blank"><img style="vertical-align: middle; display: inline-block;" src="http://bensonkearleyifg.com/app/Templates/images/Page-DownloadPDF.png" alt=""></a>
                                    &nbsp; &nbsp;<span style="text-decoration: underline;"><a href="http://bensonkearleyifg.com/app/Templates/images/Auto Reform Insert - April 12th.pdf" target="_blank" class="no-underline"><img width="25" height="25" class="core-icon valign-middle" src="app/System/images/icons/pdf.png" alt="pdf"><span class="core-link">DOWNLOAD AS PDF</span></a></span></span></p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clear-float-both">

                <img src="img/footer-spacer.png" width="640" height="30" />

            </div>

            <div style="height: 256px; overflow: hidden;" class="footer-auto">

                <a href="non-mobile.php?section=auto-attention"><img src="img/footer.png" width="640" height="256" /></a>

            </div>

        </div>

    </div>

</body>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>






<script type="text/javascript" src="js/Slidegal/js/jquery.cycle.all.min.js"></script>

<script type="text/javascript" src="js/Slidegal/js/Slidegal.js"></script>

<script type="text/javascript">

            jQuery(document).ready(function () {

                $('#Slidegal_data').RC_Slidegal({"id": "Slidegal_data", "width": 640, "height": 220, "speed": 5000, "animate": "fade", "show_nav": 0, "customer_pager": false});

            });

</script>



</html>

