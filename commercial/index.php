<?
require_once('_config.php');
$_SESSION['page_id'] = PAGE_ID;

$System->addInclude('<link type="text/css" rel="stylesheet" href="' . TEMP_CSS . 'Commercial.css" />');

if (!Site_page::getIdByTag(PAGE_TAG)) {
	$orig_page_id = Site_page::getIdByTag(PAGE_TAG, Sites::Main);
	$System->reloadSections($orig_page_id);

	if (file_exists("../" . PAGE_TAG . ".php"))
		include_once("../" . PAGE_TAG . ".php");
	else
		include("../_strip-quote.php");
}
else if (file_exists(PAGE_TAG . ".php"))
	include_once(PAGE_TAG . ".php");
else
	include("../_strip-quote.php");

/*
 * EOF
 */