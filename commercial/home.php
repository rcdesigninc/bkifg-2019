<?php
$System->addInclude('<link type="text/css" rel="stylesheet" href="'.TEMP_CSS.'Homepage.css" />');

$callout = new Site_page_sections();

$callout->LoadWhere('page_id = ' . Site_page::getIdByTag('home') . ' AND template_section_id = ' . System_columns::Content);

$video = <<< EOB
<!--iframe src="https://player.vimeo.com/video/37259139?title=0&amp;byline=0&amp;portrait=0" width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe-->
EOB;
$System->addJquery(<<< EOB
$(".billboard-button:contains('Video')").click(function(){
	$("#video-box").RC_Dialog().open();
});
if(!$.browser.mozilla){
	$(".billboard-button:contains('Video')").css({
		top: '176px',
		right: 0
	});
}
EOB
);
$System->addCSS("
#video-box {
	border: 0;
}
#video-box .heading {
	display: none;
}
#video-box .content {
	padding: 0;
}
.billboard-button {
  background: none repeat scroll 0 0 #FFFFFF;
  bottom: 0;
  cursor: pointer;
  font-family: Verdana,Geneva,sans-serif;
  font-size: 13px;
  font-weight: bold;
  height: 30px;
  line-height: 30px;
  position: absolute;
  right: 0;
  text-align: center;
  text-transform: uppercase;
  top: 176px;
  width: 165px;
}
");
System_core::Dialog('video-box',$video, '','',array('appendTo'=>'#header'));//,'position'=>"new Array(10,0)"

$billboard = $System->renderBillboard();

$System->addCSS(<<< EOB

#billboard-container {

	height: 490px;

}

#home-box-container {

  position: absolute;

  top: 320px;

}

#contentRight .content-section {

	border-color: #d05f14;

}

#home-box-container {
  bottom: 190px;
  height: 154px;
  left: -18px;
  width: 1008px;
  z-index: 4;
}

#home-box-container div {
  float: right;
  height: 97px;
  padding-left: 22.5px;
}

EOB

);



$billboard .= <<< EOB

<div id="home-box-container" class="clear-float">

	<div><a href="index.php?section=staffing"><img src="{TEMP_IMGS}/home/commercial-box4.png"/><img src="{TEMP_IMGS}/home/commercial-box4-over.png" class="hidden"/></a></div>

	<div><a href="index.php?section=information-technology"><img src="{TEMP_IMGS}/home/commercial-box3.png"/><img src="{TEMP_IMGS}/home/commercial-box3-over.png" class="hidden"/></a></div>

	<div><a href="index.php?section=equine"><img src="{TEMP_IMGS}/home/commercial-box2.png"/><img src="{TEMP_IMGS}/home/commercial-box2-over.png" class="hidden"/></a></div>

	<div><a href="index.php?section=plastics-protect"><img src="{TEMP_IMGS}/home/commercial-box1.png"/><img src="{TEMP_IMGS}/home/commercial-box1-over.png" class="hidden"/></a></div>

</div>

<div class='clear-float'></div>

EOB;



$billboard = str_replace('{TEMP_IMGS}', TEMP_IMGS, $billboard);



$toggle = false;

$i = 0;

foreach ($System->sections as $s) {

	/** @var $s Site_page_sections */

	$float = 'clear-float';

	$i++;

	if ($i == 2 || $i == 3)

		$float = 'float-box-' . ($i % 2 == 0 ? 'left' : 'right');



	$content .= $s->getContent('', $float);



}


include("../_strip-quote.php");
//$System->renderDefaultPage($content, '', $billboard);