<?php

LoadModel('Customer_request');
LoadModel('Locations');
$customer_request = new Customer_request();
$l = new Locations();

if(sizeof($_POST)) {
	//die(printArray($_POST));
	if($customer_request->Insert()) {
		//die('inserted');
		$System->Error("<p>Thank you for your message!</p>","Your message has been received");
	}
	else
		$System->Error("<p>There was an error attempting to submit your request, please try again.</p>");
	//die('wtf');
}

$ps = new Site_page_sections();

$System->addCSS('
.short-title {
	margin-top: 20px;
}

');


// Get the default left of this page, to place before the contact form

$content = $System->renderDefaultLeft();

$content .= $customer_request->renderForm();

$icon = TEMP_IMGS.'map-icon.png';

$System->addInclude(<<<EOB
	<script type="text/javascript">

jQuery(".map-box-container *").css({width: "auto"});
</script>
EOB
);

$System->addJquery(<<<EOB
setTimeout(function() {
	jQuery("#location-1").click();
}, 1000);
EOB
);

System::$instance->LoadPlugin('MapBox');

$mapbox_class  = new MapBox();
$mapbox_render = $mapbox_class->render("", "", "", "17705 Leslie st, Newmarket, Ontario");

// Insert Google Map billboard

$locations = $l->Select();

$locationString .= "<a style='visibility:hidden;' href='' class='lol-linky'></a>";

foreach($locations as $location) {
	$locationString .= "<div id='location-".$location->id."' class='location-wrap'>";
	$locationString .= "<span class='full-address'>".$location->full_address."</span>";
	$locationString .= "<span class='link-url'>".$location->url."</span>";
	$locationString .= "<p class='subheading'>".$location->address."</p>";
	$locationString .= "<p>".$location->tagline."</p>";
	$locationString .= "<span class='left-arrow'></span>";
	$locationString .= "</div>";
}

$billboard = <<< EOB

<div class="">

{$mapbox_render}

	<div style="background-color: #8c0c04" class="billboard-content-container contact">
				<div class="billboard-content">
						<div class="billboard-title short-title">{$System->billboard[0]->heading}</div>
						<div class="billboard-copy">{$locationString}</div>
						<div class="billboard-copy bottom">{$System->billboard[0]->content}</div>
				</div>

            </div>



</div>

EOB;

/*

$billboard = <<< EOB

{$mapbox_render}

<div class="">

	<div id="map-box" class="map-box-container" data-name="" data-address="71 Main Street South, Newmarket, Ontario" data-icon="" data-lat="" data-lon=""></div>

	<div style="background-color: #8c0c04" class="billboard-content-container contact">
				<div class="billboard-content">
						<div class="billboard-title short-title">{$System->billboard[0]->heading}</div>
						<div class="billboard-copy">{$locationString}</div>
						<div class="billboard-copy bottom">{$System->billboard[0]->content}</div>
				</div>

            </div>



</div>

EOB;

*/

if(ACTIVE_SITE > 3)
	include("_strip-quote.php");
else
	$System->renderDefaultPage($content,'',$billboard);