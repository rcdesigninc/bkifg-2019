<?


LoadModel('Customer_request');
$customer_request = new Customer_request();

if(sizeof($_POST)) {
	//die(printArray($_POST));
	if($customer_request->Insert()) {
		//die('inserted');
		$System->Error("<p>Thank you for your message!</p>","Your message has been received");
	}
	else
		$System->Error("<p>There was an error attempting to submit your request, please try again.</p>");
	//die('wtf');
}

System::Dialog('popup',$customer_request->renderForm(),'Please fill in the fields below to send your message.<br/>
<span class="name"></span> will get back to you as soon as possible.','',array("scrollBar"=>1,"follow"=>"[!0, !0]"));


$teamVideo = <<< EOB
<iframe id="home-team-vid" src="https://player.vimeo.com/video/78854760?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=home-team-vid" width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
EOB;



$System->addCSS('

.dialog-fix {
	left: 5px;
	top: 107px;
}

.billboard-button{
    bottom: -36px;
    right: -32px;
}

#team-video-box {
	border: 0;
	left : 5px !important;
	top: 108px !important;
}
#team-video-box .heading {
	display: none;
}
#team-video-box .content {

	padding: 0;
}

');


$System->addJquery(<<< EOB
$("a[href^=mailto]").each(function(){

	var email = $(this).attr('href').split(':');
	$(this).attr('data-email',email[1]);
	$(this).attr('href','#');

	$(this).click(function(){
		validCapthca = false;
		Recaptcha.reload();
		$("#popup .name").text($(this).text());
		$("#popup input[name=send-to]").val($(this).attr('data-email'));
		
		$("#popup").RC_Dialog().open();

		return false;
	});
});

$(".billboard-button:contains('Watch This Video')").click(function(){
	$("#team-video-box").RC_Dialog().open();
});



EOB
);


System_core::Dialog('team-video-box',$teamVideo, '','',array('appendTo'=>'#header'));//,'position'=>"new Array(10,0)"

// If commercial or financial, remove sidebar quote links
if(ACTIVE_SITE > 3)
	include("_strip-quote.php");
else
	$System->renderDefaultPage();

/*
 * EOF
 */