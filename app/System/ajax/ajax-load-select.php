<?
define('PAGE_ID', 1);
require_once('_config.php');

$class = ucwords($_POST['table']);
$field = $_POST['show'];
$p_override = $_POST['p_override'] == 'undefined' ? false : true;
$where = $_POST['filter'] == 'undefined' || $_POST['filter'] == '' ? 1 : $_POST['filter'];
$selected_id = $_POST['selected'];

if (LoadModel($class)) {
	/** @var Model $c */
	$c = new $class();

	$label = $c->_humanName;

	//if($System->user->is_root())
	$results = "<option>Select a $label</option>";

	if ($c->hasParent() && !$p_override) {
		$entries = $c->Select('id, parent_id, ' . $field, $where . ' AND parent_id = "0"', $field);
		foreach ($entries as $entry) {
			$results .= '<option value="' . $entry->id . '" ' . ($entry->id == $selected_id ? 'selected="selected"'
					: '') . '>' . $entry->$field . '</option>';

			$subentries = $c->Select('id, ' . $field, $where . ' AND parent_id = "' . $entry->id . '"', $field);
			//print_r($subentries);
			foreach ($subentries as $sub) {
				$results .= '<option value="' . $sub->id . '" ' . ($sub->id == $selected_id ? 'selected="selected"'
						: '') . '>' . $entry->$field . ' -&gt; ' . $sub->$field . '</option>';

				$subsubentries = $c->Select('id, ' . $field, $where . ' AND parent_id = "' . $sub->id . '"', $field);
				foreach ($subsubentries as $x) $results .= '<option value="' . $x->id . '" ' . ($x->id == $selected_id
						? 'selected="selected"'
						: '') . '>' . $entry->$field . ' -&gt; ' . $sub->$field . ' -&gt; ' . $x->$field . '</option>';
			}
		}
	}
	else {
		$entries = $c->Select('id, ' . $field, $where, $field);

		foreach ($entries as $entry) $results .= '<option value="' . $entry->id . '" ' . ($entry->id == $selected_id
				? 'selected="selected"' : '') . '>' . $entry->$field . '</option>';
	}

	print $results;
}

