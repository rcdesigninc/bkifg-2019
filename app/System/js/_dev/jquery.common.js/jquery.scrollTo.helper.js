var autoScroll = false;

jQuery(document).ready(function($) {
	// Update any scroll links to use scroll
	$(".scroll-link").click(function() {
		startScroll($($(this).attr('href')));
		return false;
	});
});

function startScroll($scrollTo, speed, easing, offset) {
	(function($) {
		// Ensure that we are not already scrolling away, and we actually have a valid element to scroll too
		if (!autoScroll && $scrollTo) {
			if ($scrollTo.length) { // Must be a single element found to scroll to it
				var scrollOffset = 0;
				var moveBy = 1000;
				var easingBy = 'easeOutCubic';

				if (offset)
					scrollOffset = offset;

				if (speed)
					moveBy = speed;

				if (easing)
					easingBy = easing;

				debug("startScroll:running  scrollTo = " + $scrollTo + ", speed = " + speed + ", moveBy = " + moveBy);
				autoScroll = true;

				var $to = $scrollTo;
				if ($.browser.msie && $.browser.version == '7.0')
					$to = $to.prev();

				// Temporarily disable the mousewheel, to ensure users don't get jolted while autoScrolling
				// Use a specific function vs global event, so it can individually be removed after.
				var disableMousewheel = function() {
					return false;
				};
				$(window).bind("mousewheel", disableMousewheel);

				$.scrollTo($to, moveBy, {axis: 'y', easing: easingBy, offset: scrollOffset,
					onAfter:function(whereTo) {
						autoScroll = false;

						// Remove the mousewheel disable
						$(window).unbind("mousewheel", disableMousewheel);

						return $(document).scrollTop();
					}});
			}
		}
	})(jQuery);
}