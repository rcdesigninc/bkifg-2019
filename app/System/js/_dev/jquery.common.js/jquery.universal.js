/* jquery.universal.js */
jQuery(document).ready(function($) {
	$.fn.exists = function() {
		return $(this).length > 0;
	};

	$.fn.loadCss = function(css) {
		if (document.createStyleSheet) // To handle IE intigration
			document.createStyleSheet('style.css');
		else
			$("<link>", {rel:'stylesheet',type:'text/css',href: css}).appendTo("head");
	};

	$.fn.loadJs = function(js, sync, callback) {
		if (sync == "undefined") // if no sync specified, default to true
			sync = true;
		if (callback == "undefined") // if no callback specified, set null
			callback = null;

		$.ajax({
			url: js,
			dataType: "script",
			async: sync,
			success: callback
		});
	};

	// Ensure all anchors to outside urls or pdf are targeted to new windows!
	$("a[href^=http], a[href$=pdf]").attr('target', '_blank');

	// Process adding icons to various links within an element
	$.fn.addLinkIcons = function(icons) {
		return this.each(function() {
			$this = $(this);
			// Search for all anchor tags within this element to process
			$.each(icons, function(i, link) {
				// If there was no 'icon' specified, assume the filter is the icon/file type
				if (!link.icon)
					link.icon = link.filter;

				$this.find('a[href$=' + link.filter + ']:not(:has(img))').addClass('no-underline').wrapInner('<span class="core-link"></span>').prepend('<img width="25" height="25" class="core-icon valign-middle" src="' + CORE_IMGS + 'icons/' + link.icon + '.png" alt="' + link.icon + '"/>');
			});
		});
	};

	// Initially, run the function on all globally defined icons
	if(linkIcons != 'undefined')
		$(document).addLinkIcons(linkIcons);
	
	/*************************************************************************
	 *             FONT FACE FIX
	 *             If firefox 3.5+, hide content till load (or 3 seconds) to prevent FOUC
	 *************************************************************************/

	var d = document, e = d.documentElement, s = d.createElement('style');
	if ($.browser.mozilla && e.style.MozTransform === '') // gecko 1.9.1 inference
	{
		s.textContent = 'body{visibility:hidden}';
		e.firstChild.appendChild(s);
		function f() {
			s.parentNode && s.parentNode.removeChild(s);
		}

		addEventListener('load', f, false);
		setTimeout(f, 1);
		setTimeout(f, 50);
		setTimeout(f, 100);
		setTimeout(f, 250);
		setTimeout(f, 500);
	}

	/************************************************************************/


	$.fn.emptySelect = function(optionsDataArray) {
		return this.each(function() {
			if (this.tagName == 'SELECT') this.options.length = 0;
		});
	};

	$.fn.loadSelect = function(optionsDataArray) {
		return this.emptySelect().each(function() {
			if (this.tagName == "SELECT") {
				var selectElement = this;
				$.each(optionsDataArray, function(index, optionData) {
					if (optionData) {
						var option = new Option(optionData.caption, optionData.value, optionData.selected);
						if ($.browser.msie)
							selectElement.add(option);
						else
							selectElement.add(option, null);
					}
				});

			}
		});
	};

	$("input, textarea").live('focus',
	function() {
		if ($(this).val() == $(this).attr('title'))
			$(this).val('');
	}).live('blur', function() {
		if ($(this).val() == '')
			$(this).val($(this).attr('title'));
	});

});

function debug($msg) {
	if (window.console && window.console.log)
		window.console.log($msg);
}