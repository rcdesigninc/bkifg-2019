// JavaScript Document

jQuery(document).ready(function($) {
	$.extend($.expr[':'], {
		'containsi': function(elem, i, match, array) {
			return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});

	$("input[type=checkbox]").css({width:'auto','border':'0'});
	$("input[type=hidden]").css({display:'none',width:'0'});
//
//	$(".enlarge").live('mouseover mouseout',function(){
//		$(this).find('img').toggle();
//	}).live('click',function(){
//		$c = $(this).next().clone(true).insertAfter(this);
//		$(this).next().openPopup();
//	});

	$('.ajaxSelect').each(function(i){
		$(this).load(SYS_AJAX+'ajax-load-select.php', {
			table: $(this).attr('table'),
			show: $(this).attr('show'),
			filter: $(this).attr('filter'),
			selected: $(this).attr('data-selected'),
			p_override: $(this).attr('p_override')
		});
	});

	var $locations = $('.contact div[id*=location]');

	$locations.filter(":first").addClass('selected');

	$locations.click(function() {

		var $mapBoxes = $( '.map-box-container' );

		var $thisDiv = $( this );

		$locations.removeClass( 'selected' );

		$thisDiv.addClass( 'selected' );

		$mapBoxes.each( function() {
			var $this = $( this );
			var lat = $this.attr( 'data-lat' );
			var lon = $this.attr( 'data-lon' );
			//var address = $this.attr('data-address');
			var address = $thisDiv.find( '.full-address' ).html();
			var $linkUrl = $thisDiv.find( '.link-url' ).html();

			//alert(address);

			var latlng;

			$this.attr( 'id', $.Guid.New() );

			$this.find( "*" ).css( 'width', 'auto' );

			if( lat != "" && lon != "" ) {
				latlng = new google.maps.LatLng( lat, lon );
				render_map( latlng, $this );
			} else {
				var data_string = "address=" + address;
				$.ajax( {
					type    : "POST",
					url     : RC_MapBox_AJAX,
					data    : data_string,
					success : function( return_data ) {
						var latlng_array = return_data.split( "@" );
						lat = latlng_array[0];
						lon = latlng_array[1];
						latlng = new google.maps.LatLng( lat, lon );
						render_map( latlng, $this );
					}
				} );
			}

			function render_map( latlng_para, map_obj ) {
				var myOptions = {
					zoom              : 15,
					center            : latlng_para,
					mapTypeControl    : false,
					mapTypeId         : google.maps.MapTypeId.ROADMAP,
					keyboardShortcuts : false,
					scrollwheel       : false,
					navigationControl : false,
					scaleControl      : false
				};
				var map = new google.maps.Map( document.getElementById( map_obj.attr( 'id' ) ), myOptions );
				var marker = new google.maps.Marker( {
					position  : latlng_para,
					map       : map,
					animation : google.maps.Animation.DROP,
					title     : map_obj.attr( 'data-name' ),
					icon      : 'https://www.bkifg.com/app/Templates/images/map-icon.png',
					url       : $linkUrl
				} );

				google.maps.event.addListener(marker, 'click', function() {
					window.open(marker.url, '_blank');
				});
			}

		} )

	});

});