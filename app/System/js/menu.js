// JavaScript Document
var hoveringOn = '';

jQuery(document).ready(function($) {
	$('.menutop').hover(
			function() {
				$(this).addClass('menu1Open');
				$('.submenu').hide();
				$("#" + this.id + "-Submenu").show();
				/*$(this).parent().css('background-image','url('+COMMON_IMGS+'background-menu.png)');*/
			},
			function() {
				if (!$(this).hasClass('selected'))
					$(this).parent().css('background-image', '');
				setTimeout("hideSubmenu('" + this.id + "')", 250);
			}).mousemove(function() {
				$(this).addClass('menu1Open');
				$('.submenu').hide();
				$("#" + this.id + "-Submenu").show();
			});
	$('.menutop').click(function() {
		window.location = $(this).find('a:first').attr('href');
	});

	/*
	 $('.menutop').mouseout(function(){
	 setTimeout("hideSubmenu('"+this.id+"')",250);
	 }).live('mouseover mousemove',function(){
	 $(this).addClass('menu1Open');
	 $('.submenu').hide();
	 $("#"+this.id+"-Submenu").show();
	 });

	 */

	$submenuTop = 0;
	$('.submenu').hover(function() {
		hoveringOn = "#" + this.id;
		//debug('submenuHover::hoveringOn = ' + hoveringOn);
		$('.submenu').hide();
		$(this).show();
		$(this).prev().addClass('subhover');
	}, function() {
		setTimeout("unhoverSubmenu('#" + this.id + "')", 1);
	});

	$('.menu2').hover(function() {
		$(this).find('.tertiaryMenu').show();
	}, function() {
		$(this).find('.tertiaryMenu').hide();
	});

	$(".submenu, .menu2").hover(function(){
		$(this).prev(".menu1").addClass("subhover");
	});

	/*.each(function(i){
	 $(this).css('top',$submenuTop+'px');
	 $submenuTop += $(this).prev().height();
	 });
	 */

	//$(".content-section:containts(&bull;)").
	if ($.browser.msie && $.browser.version < 8) {
		$('.submenu').each(function() {
			if ($(this).children().length == 0)
				$(this).remove();
		});
	}

	if ($.browser.mozilla) {
		setTimeout("mozzMenuFix()", 1);
		setTimeout("mozzMenuFix()", 50);
		setTimeout("mozzMenuFix()", 250);
		setTimeout("mozzMenuFix()", 500);
	}

	/*$("#menu-container div.selected").parent().css('background-image','url('+COMMON_IMGS+'background-menu.png)');*/


	//$(".content-section:containts(&bull;)").
	if ($.browser.msie && $.browser.version < 8) {
		$('.submenu').each(function() {
			if ($(this).children().length == 0)
				$(this).remove();
		});
	}
});

function mozzMenuFix() {
	//debug("mozzMenuFix()::running");
	jQuery(".menu1").each(function(i) {
		jQuery(this).find('a').each(function(i) {
			if (jQuery(this).html() == '')
				jQuery(this).remove();
		});
		/*jQuery("#menu-container div.selected").parent().css('background-image','url('+COMMON_IMGS+'background-menu.png)');*/
	});
}

function hideSubmenu(menuId) {
	submenuId = "#" + menuId + "-Submenu";
	//debug('hideSubmenu::hoveringOn = ' + hoveringOn + ', submenuId = ' + submenuId + ', $(submenuId).css("display") = ' + jQuery(submenuId).css('display'));
	if (hoveringOn == '' && hoveringOn != submenuId && jQuery(submenuId).css('display') == 'block') {
		jQuery("#" + menuId).removeClass('menu1Open');
		jQuery(submenuId).hide();

	}
}

function unhoverSubmenu(submenuId) {
	/*

	 if(hoveringOn == '' && hoveringOn != submenuId && jQuery(submenuId).css('display') == 'block') {
	 */
	//debug('unhoverSubmenu::hoveringOn = ' + hoveringOn + ', submenuId = ' + submenuId + ', $(submenuId).css("display") = ' + jQuery(submenuId).css('display'));
	//	if(!jQuery(submenuId).prev().hasClass('subhover')) {
	hoveringOn = '';
	jQuery(submenuId).hide();
	jQuery(submenuId).prev().removeClass('subhover');
	jQuery(submenuId).find('.tertiaryMenu').hide();
	//	}
	/*
	 }
	 else
	 setTimeout("unhoverSubmenu('"+submenuId+"')",50);
	 */
}