<?

class ModelList {
	private $entries =
	array
	();

	public function __construct() {
	}

	public function Add(Model $o, $label = '', $override = true) {
		if ($label) {
			if (key_exists($label, $this->entries)) {
				if ($override)
					$this->entries[$label] = $o;
			}
			else
				$this->entries[$label] = $o;

		}
		else
			$this->entries[] = $o;
	}

	/**
	 * @return array
	 */
	public function Get() {
		return $this->entries;
	}
}