<?

class_exists('Model_field_data') || require_once(dirname(__FILE__) . '/' . 'Model_field_data.php');

class Model_field {
	/** @var Model_field_data $data */
	private $data = null;

	public function __construct(Model_field_data $data = null) {
		$this->data = $data;
	}

	public function gimme(&$var) {
		$var =& $this;
	}

	public function set($value) {
		$this->data->value = $value;
	}

	public function val() {
		return $this->data->value;
	}

	public function label($wrapper = false) {
		return $this->data->label;
	}

	public function __set($name, $value) {
		if ($this->data->$name)
			$this->data->$name = $value;
		else
			$this->data->value = $value;
	}

	public function __get($name) {
		if ($this->data->$name)
			return $this->data->$name;

		return $this->data->value;
	}

	public function __toString() {
		$return = '';
		if (!($this->data->value instanceof __PHP_Incomplete_Class))
			$return = (string)$this->data->value;
		return $return;
	}

	public static function buildInput($name, $label, $input, $size = '') {
		$instance = new self();
		$instance->data->name = $name;
		$instance->data->label = $label;
		$instance->data->input = $input;
		$instance->data->size = $size;
		return $instance->renderLabeledInput();
	}

	public function SetField($label, $input, $attrs = '', $forceRender = false) {
		$this->data->label = $label;
		$this->data->input = $input;
		$this->data->attrs = $attrs;
		if ($forceRender)
			$this->renderLabeledInput();
	}

	public function renderInput($render_as, $supplement_value) {
		return $this->data->renderInput($render_as, $supplement_value);

	}

	public function renderLabeledInput($render_as = System_defaults::Model_field_render, $supplement_value = '') {
		return $this->renderInput($render_as, $supplement_value);
	}

	public function renderFakeInput($supplement_value = '') {
		return $this->renderInput($supplement_value, Model_field_render::Remove_input_show_value);
	}

}


?>