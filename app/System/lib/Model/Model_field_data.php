<?

class Model_field_data {
	public $name = '';
	public $type = '';
	public $size = '';
	public $null = false;
	public $primary_key = false;
	public $unique = false;
	public $default = null;
	public $auto_increment = false;

	public $input = '';
	public $attrs = '';

	public $label = '';
	/** @var mixed $value */
	public $value = '';

	public $rendered = '';

	public function __construct($name, $type, $size, $null, $primary_key, $unique, $default, $auto_increment) {
		// todo: pull the field creation strait down to this constructor, where it should be handled.
		/*$type = preg_split("/[()]/", $data['type']);

		'name' => $name,
			'type' => $type[0],
			'size' => $type[1],
			'null' => $null == 'NO' ? false : true,
			'primary_key' => $key_type == 'PRI' ? true : false,
			'unique' => $key_type == 'PRI' || $key_type == 'UNI' ? true : false,
			'default' => $default == '' || $default == 'NULL' ? NULL : $default,
			'auto_increment' => $extras == 'auto_increment' ? true : false)*/

		$this->name = $name;
		$this->type = $type;
		$this->size = $size;
		$this->null = $null;
		$this->primary_key = $primary_key;
		$this->unique = $unique;
		$this->default = $default;
		$this->auto_increment = $auto_increment;

		$this->value = '';
	}

	public function __set($name, $value) {
		if ($this->$name)
			$this->$name = $value;
		else
			$this->value = $value;
	}

	public function renderInput($render_as, $supplement_value) {
		$str = '';
		switch ($render_as) {
			case Model_field_render::Remove_input_show_value:
				if ($this->input == Model_field_type::Date)
					$value = date("M j, Y", strtotime($this->value));
				else if ($this->input != Model_field_type::Bool)
					$value = $this->value ? $this->value : $value = $this->default;

				if ($supplement_value == '')
					$supplement_value = $value;

				$str = '<div class="entry-field"> ';
				$str .= "<label id=\"$this->name-label\" for=\"" . $this->name . "\">{$this->label}:</label>\n";
				$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
				$str .= '</div>';

				break;
			case Model_field_render::Outside_label:
				$value = $this->value;
				$required = $this->null ? '' : 'required';

				if ($this->input != Model_field_type::Bool)
					$value = $this->value ? $this->value : $value = $this->default;

				if ($supplement_value == '')
					$supplement_value = $value;

				$str = "<div class='entry-field " . ($this->input == Model_field_type::Hidden ? 'hidden' : '') . "'>";

				$str .= "<div class='label-wrapper'><label id='$this->name-label' for='" . $this->name . "'>{$this->label}:</label></div>\n";

				switch ($this->input) {
					case Model_field_type::Bool:
						$checked = $value ? 'checked="checked"' : '';
						$str .= "<input name='$this->name' type='checkbox' value='1' class='input-field $required' $checked $this->attrs />\n";
						break;
					case Model_field_type::Text:

						$class = 'mceLight';
						//$str .= "<div class='spacer'></div>";
						$str .= "<textarea name='$this->name' class='input-field $class $required' $this->attrs >" . htmlentities($value,
							ENT_QUOTES) . "</textarea><br />\n";
						break;
					case Model_field_type::Textarea:
						$str .= "<div class='scroll-box'>";
						$str .= "<textarea name='$this->name' type='$this->input' class='input-field $required' $this->attrs >" . htmlentities($value,
							ENT_QUOTES) . "</textarea><br />\n";
						$str .= "</div>";
						break;
					case Model_field_type::Text_limited:

						$class = Model_field_type::Text_limited;
						//$str .= "<div class='spacer'></div>";
						$str .= "<textarea name='$this->name' class='input-field $class $required' $this->attrs >" . htmlentities($value,
							ENT_QUOTES) . "</textarea><br />\n";
						break;
					case Model_field_type::Html:
						$str .= "<textarea name='$this->name' type='$this->input' class='input-field mceCoder $required' $this->attrs >" . nl2br(mysql_escape_string($value)) . "</textarea>\n";
						break;
					case Model_field_type::Select:
						$str .= "
							<select name='$this->name' class='input-field $required ajaxSelect' $this->attrs data-selected='$value'>
								<option>Select An Option</option>
							</select>\n";
						break;
					case Model_field_type::Date:
						if ($value == 'CURRENT_TIMESTAMP' || $value == '' && $this->name == "date")
							$value = date("Y-m-d");
						else date("Y-m-d", $value);
						$str .= "<input name='$this->name' type='text' value='$value' maxlength='$this->size' class='input-field $required datefield' $this->attrs />\n";
						break;
					case Model_field_type::Hidden:
						// Override hidden, the entire cell instead is hidden in case the field needs to be later modified.
						$str .= "<input name='$this->name' type='text' value='$value' $this->attrs />\n";
						break;
					case Model_field_type::File:
						$str .= "<input name='$this->name' type='file' class='input-field $required' $this->attrs />\n";
						break;
					case Model_field_type::Password:
						$str .= "<input name='$this->name' type='password' value='$value' maxlength='$this->size' class='input-field $required' $this->attrs />\n";
						break;
					case Model_field_type::Email:
						// Override, do not use the new HTML5 email type, it doesn't always work as well as jq validation...
						$str .= "<input name='$this->name' type='text' value='$value' maxlength='$this->size' class='input-field email $required' $this->attrs />\n";
						break;
					case Model_field_type::Dropdown:
						$str .= SmoothDropdown::renderDropdown($this->name, $this->label, $this->attrs);
						break;
					default:
						$str .= "<input name='$this->name' type='$this->input' value='$value' maxlength='$this->size' class='input-field $required' $this->attrs />\n";
				}
				$str .= '</div>';

				break;
			case Model_field_render::Inside_label:
				$value = $this->value;
				$required = $this->null ? '' : 'required';

				if ($this->input != Model_field_type::Bool)
					$value = $this->value ? $this->value : $value = $this->default;

				if ($supplement_value == '')
					$supplement_value = $value;

				$str = "<div class='entry-field " . ($this->input == Model_field_type::Hidden ? 'hidden' : '') . "'>";

				switch ($this->input) {
					case Model_field_type::Bool:
						$checked = $value ? 'checked="checked"' : '';
						$str .= "<div class='label-wrapper'><label id='$this->name-label' for='" . $this->name . "'>{$this->label}:</label></div>\n";
						$str .= "<input name='$this->name' type='checkbox' value='1' class='input-field $required' $checked $this->attrs />\n";
						break;
					case Model_field_type::Text:

						$class = 'mceLight';
						//$str .= "<div class='spacer'></div>";
						$str .= "<textarea name='$this->name' title='$this->label' class='input-field $class $required' $this->attrs >" . ($value
								? htmlentities($value, ENT_QUOTES) : $this->label) . "</textarea><br />\n";
						break;
					case Model_field_type::Textarea:
						$str .= "<div class='scroll-box'>";
						$str .= "<textarea name='$this->name' title='$this->label' type='$this->input' class='input-field $required' $this->attrs >" . ($value
								? htmlentities($value, ENT_QUOTES) : $this->label) . "</textarea><br />\n";
						$str .= "</div>";
						break;
					case Model_field_type::Text_limited:

						$class = Model_field_type::Text_limited;
						//$str .= "<div class='spacer'></div>";
						$str .= "<textarea name='$this->name' title='$this->label' class='input-field $class $required' $this->attrs >" . ($value
								? htmlentities($value, ENT_QUOTES) : $this->label) . "</textarea><br />\n";
						break;
					case Model_field_type::Html:
						$str .= "<textarea name='$this->name' title='$this->label' type='$this->input' class='input-field mceCoder $required' $this->attrs >" . ($value
								? nl2br(mysql_real_escape_string($value)) : $this->label) . "</textarea>\n";
						break;
					case Model_field_type::Select:
						$str .= "
							<select name='$this->name' class='input-field $required ajaxSelect' $this->attrs data-selected='$value'>
								<option>Select An Option</option>
							</select>\n";
						break;
					case Model_field_type::Date:
						if ($value == 'CURRENT_TIMESTAMP' || $value == '' && $this->name == "date")
							$value = date("Y-m-d");
						$str .= "<input name='$this->name' title='$this->label' type='text' value='" . ($value ? $value
								: $this->label) . "' class='input-field $required datefield' $this->attrs />\n";
						break;
					case Model_field_type::Hidden:
						// Override hidden, the entire cell instead is hidden in case the field needs to be later modified.
						$str .= "<input name='$this->name' type='text' title='$this->label' value='" . ($value ? $value
								: $this->label) . "' $this->attrs />\n";
						break;
					case Model_field_type::File:
						$str .= "<input name='$this->name' title='$this->label' type='file' class='input-field $required' $this->attrs />\n";
						break;
					case Model_field_type::Password:
						$str .= "<input name='$this->name' title='$this->label' type='password' value='" . ($value
								? $value
								: $this->label) . "' maxlength='$this->size' class='input-field $required' $this->attrs />\n";
						break;
					case Model_field_type::Email:
						// Override, do not use the new HTML5 email type, it doesn't always work as well as jq validation...
						$str .= "<input name='$this->name' title='$this->label' type='text' value='" . ($value ? $value
								: $this->label) . "' maxlength='$this->size' class='input-field email $required' $this->attrs />\n";
						break;
					case Model_field_type::Dropdown:
						$str .= SmoothDropdown::renderDropdown($this->name, $this->label, $this->attrs);
						break;
					default:
						$str .= "<input name='$this->name' title='$this->label' type='$this->input' value='" . ($value
								? $value
								: $this->label) . "' maxlength='$this->size' class='input-field $required' $this->attrs />\n";
				}
				$str .= '</div>';

				break;
			case Model_field_render::Hide_input_show_value:

				$value = $this->value;
				$required = $this->null ? '' : 'required';

				if ($this->input != Model_field_type::Bool)
					$value = $this->value ? $this->value : $value = $this->default;

				if ($supplement_value == '')
					$supplement_value = $value;

				$hidden = '';
				if ($this->input == Model_field_type::Hidden)
					$hidden = ' hidden';
				$str = "<div class='entry-field $hidden'>";


				$required .= " hidden";
				$str .= "<label id='$this->name-label' for='" . $this->name . "'>{$this->label}:</label>\n";

				switch ($this->input) {
					case Model_field_type::Bool:
						$checked = $value ? 'checked="checked"' : '';
						$str .= "<input name='$this->name' type='checkbox' value='1' class='input-field $required' $checked $this->attrs />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Text:
						$class = 'mceLight';
						$str .= "<textarea name='$this->name' class='input-field $class $required' $this->attrs >" . htmlentities($value,
							ENT_QUOTES) . "</textarea><br />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Textarea:
						$str .= "<div class='scroll-box'>";
						$str .= "<textarea name='$this->name' type='$this->input' class='input-field $required' $this->attrs >" . htmlentities($value,
							ENT_QUOTES) . "</textarea><br />\n";
						$str .= "<span class=' input-value visible'>" . htmlentities($value, ENT_QUOTES) . "</span>";
						$str .= "</div>";
						break;
					case Model_field_type::Text_limited:
						$class = Model_field_type::Text_limited;
						$str .= "<textarea name='$this->name' class='input-field $class $required' $this->attrs >" . htmlentities($value,
							ENT_QUOTES) . "</textarea><br />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Html:
						$str .= "<textarea name='$this->name' type='$this->input' class='input-field mceCoder $required' $this->attrs >" . nl2br(mysql_escape_string($value)) . "</textarea>\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Select:
						$str .= "
							<select name='$this->name' class='input-field $required ajaxSelect' $this->attrs data-selected='$value'>
								<option>Select An Option</option>
							</select>\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Date:
						if ($value == 'CURRENT_TIMESTAMP' || $value == '' && $this->name == "date")
							$value = date("Y-m-d");
						$str .= "<input name='$this->name' type='text' value='$value' maxlength='$this->size' class='input-field $required datefield' $this->attrs />\n";
						$str .= "<span class='input-value visible'>" . date("M j, Y",
							strtotime($value)) . "</span><br/>";
						break;
					case Model_field_type::Hidden:
						// Override hidden, the entire cell instead is hidden in case the field needs to be later modified.
						$str .= "<input name='$this->name' type='text' value='$value' $this->attrs />\n";
						break;
					case Model_field_type::File:
						$str .= "<input name='$this->name' type='file' class='input-field $required' $this->attrs />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Password:
						$str .= "<input name='$this->name' type='password' value='$value' maxlength='$this->size' class='input-field $required' $this->attrs />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Email:
						// Override, do not use the new HTML5 email type, it doesn't always work as well as jq validation...
						$str .= "<input name='$this->name' type='text' value='$value' maxlength='$this->size' class='input-field email $required' $this->attrs />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
						break;
					case Model_field_type::Dropdown:
						$str .= SmoothDropdown::renderDropdown($this->name, $this->label, $this->attrs);
						break;
					default:
						$str .= "<input name='$this->name' type='$this->input' value='$value' maxlength='$this->size' class='input-field $required' $this->attrs />\n";
						$str .= "<span class='input-value visible'>$supplement_value</span><br/>";
				}
				$str .= '</div>';

				break;
		}


		return $str;

	}


}