<?

class_exists('Model_field') || require_once(dirname(__FILE__) . '/' . 'Model_field.php');

abstract class Model_base {
	protected $_fields =
	array
	();
	private $_hadParent = false;
	private $_hasOrderBy = false;

	public function __construct() {
	}

	protected function _funcAddField($col, $type, $null, $key_type, $default, $extras) {
		// todo: push the field creation strait down to the Model_field constructor, where it should be handled.
		$type = preg_split("/[()]/", $type);

		$this->_fields[$col] = new Model_field(new Model_field_data($col, $type[0], $type[1], $null == 'NO' ? false
						: true, $key_type == 'PRI' ? true : false, $key_type == 'PRI' || $key_type == 'UNI' ? true
						: false, $default == '' || $default == 'NULL' ? NULL : $default, $extras == 'auto_increment'
						? true : false));

		//$this->_fields[$col]->set($this->$col);
		//$this->_fields[$col]->gimme($this->$col);

		if ($col == 'parent_id')
			$this->_hadParent = true;
		else if ($col == 'order_by')
			$this->_hasOrderBy = true;
	}

	public function hasParent() {
		return $this->_hadParent;
	}

	public function hasOrderBy() {
		return $this->_hasOrderBy;
	}


	protected function _funcSetField($field, $input_label, $input_type, $attrs = '') {
		if ($this->_fields[$field])
			$this->_fields[$field]->SetField($input_label, $input_type, $attrs);

		//print "<PRE>".print_r($this->_arrFields[$field],true)."</PRE>";
	}

	public function _funcPopulateFields($arrData = NULL) {
		/*print print_array($arrData);*/
		if ($arrData && is_array($arrData))
			foreach ($arrData as $col => $value)
				/** @var Model_field $col */
				$this->__set($col, $value);
	}

	public function _funcExtractFields() {
		$arrData =
				array
				();
		foreach ($this->_fields as $col => $field)
			/** @var Model_field $field */
			$arrData[$col] = $field->val();
		return $arrData;
	}

	public function _funcGetColumns() {
		$arrData =
				array
				();
		foreach ($this->_fields as $col => $field)
			/** @var Model_field $field */
			$arrData[$col] = $field->label();
		return $arrData;
	}

	public function _funcGetDbFields() {
		return array_keys($this->_fields);
		/*$arrData =
				array
				();

		foreach ($this->_fields as $col => $field)
			$arrData[] = $col;
		return $arrData;*/
	}

	protected $maxsize = 10000000; // 10 mbs

	protected function Upload($file, $destination, $new_file_name = "") {
		//die("Uploading: $file >> $new_file_name");
		$source = $file["tmp_name"];
		if (!$new_file_name) {
			$filename = $file["name"];
		}
		else {
			$filename = $new_file_name;
		}
		if ($file["size"] > 0) {
			if (!move_uploaded_file($source, $destination . $filename)) {
				return false;
			}
			return true;
		}
	}

	public function  getLabel($col) {
		if (array_key_exists($col, $this->_fields))
			return $this->_fields[$col]->label;

		return null;
	}

	public function getInput($col, $render_as = System_defaults::Model_field_render, $supplement_value = '') {
		if (array_key_exists($col, $this->_fields))
			return $this->_fields[$col]->renderLabeledInput($render_as, $supplement_value);
	}

	public function getFakeInput($col, $supplement_value = '') {
		if (array_key_exists($col, $this->_fields))
			return $this->_fields[$col]->renderFakeInput($supplement_value);
	}

	/* 
	* STANDARD OVERRIDES
	*/
	public function __set($col, $value) {
		if (array_key_exists($col, $this->_fields)) {
			//$this->$col = $value;
			$this->_fields[$col]->set($value);
		}
	}

	public function __get($col) {
		if (array_key_exists($col, $this->_fields))
			return $this->_fields[$col]->val();

		return null;
	}

	/**
	 * @param $col
	 * @return bool
	 */
	public function __isset($col) {
		if (array_key_exists($col, $this->_fields))
			return isset($this->$col);
		return false;
	}

	/**
	 * @param $col
	 * @return void
	 */
	public function __unset($col) {
		if (array_key_exists($col, $this->_fields))
			unset($this->$col);
	}

	/*
	public function __toString()
	{
		$str = '';
		foreach($this->_arrFields as $field)
			$str .= "$field\n";
		return $str;
	}*/

}

?>