<?

class_exists('Model_base') || require_once(dirname(__FILE__) . '/Model/' . 'Model_base.php');

abstract class Model extends Model_base {
	private static $loaded =
	array
	();

	protected $_class_prefix = "";

	protected $_uploadDir = '';
	protected $_strCalledClass = '';
	public $_className = '';
	public $_humanName = '';
	private $_tableName = '';

	public function __construct($table) {
		parent::__construct();
		$this->_tableName = $table;
		$this->_className = get_class($this);
		$this->_class_prefix = $this->_className; //get_called_class();
		$this->_uploadDir = UPLOAD_PATH;
	}

	public function getSession() {

		$this->_funcPopulateFields($_SESSION[$this->_class_prefix]);
		/*$columns = array_keys($this->_funcGetColumns());
		foreach ($columns as $col)
			if ($_SESSION[$this->_class_prefix][$col])
				$this->$col = $_SESSION[$this->_class_prefix][$col];*/
	}

	public function setSession($id) {
		$this->Load($id);
		foreach ($this->_fields as $col => $field)
			$_SESSION[$this->_class_prefix][$col] = $field->val();
	}

	/**
	 * @param $id
	 * @return null|Model
	 */
	public function Load($id) {
		// todo: see about adding efficiencies for calling the same table and or queries over and over...
		// todo: Will also need a way to see about this aspect only running while the system is loading,
		// todo: or after when it's rendering the page live
		/*$o = null;
		if(Model::$loaded[$this->_className][$id]) {// instanceof Model)
			$o = Model::$loaded[$this->_className][$id];
			die('found same object'.print_array($o));
		}
		else
		{
			$o = $this->LoadWhere("id = '$id'");
			//Model::$loaded[$this->_className][$id] = $o;
		}
		return $o;*/
		return $this->LoadWhere("id = '$id'");
	}

	/**
	 * @param string $where
	 * @param string $order_by
	 * @return Model
	 */
	public function LoadWhere($where = '', $order_by = '') {
		return $this->Select('*', $where, $order_by, '', Database_query_result::Model);
	}


	/**
	 * @param string $where
	 * @return int
	 */
	public function Count($where = '') {
		return System::$DB->Count($this->_tableName, $where);
	}

	/**
	 * @param string $columns
	 * @param string $where
	 * @param string $order_by
	 * @param string $limit
	 * @param int $returns
	 * @return array[Model]|Model
	 */
	public function Select($columns = '*', $where = '', $order_by = '', $limit = '', $returns = Database_query_result::Model_array) {
		return System::$DB->Select($columns, $this->_tableName, $where,
			(!$order_by && $this->hasOrderBy() ? "`order_by`" : $order_by), $limit, $returns, $this);
	}

	/**
	 * @param $entry
	 * @return Model
	 */
	protected function fillModel($entry) {
		foreach ($entry as $field => $value)
			$this->$field = $value;
		return $this;
	}

	/**
	 * @param $entries
	 * @return array[Model]
	 */
	protected function fillModels($entries) {
		$list =
				array
				();
		while ($entry = mysql_fetch_assoc($entries))
			$list[] = new $this->_className($entry);
		return $list;
	}

	private function prepQueryData($data = null) {
		$columns = '`' . implode('`,`', $this->_funcGetDbFields()) . '`';

		$values = $this->_funcExtractFields();
		if (is_array($data)) {
			$cols = $this->_funcGetDbFields();
			foreach ($cols as $field)
				$values[$field] = $data[$field];
		}
		//		die(print_array($values));
		foreach ($values as $field => $val)
			$values[$field] = scrubDbData($val);

		$data = '"' . implode('","', $values) . '"';
	}


	/**
	 * @param null $data
	 * @return int
	 */
	public function Insert($data = NULL) {
		$columns = '`' . implode('`,`', $this->_funcGetDbFields()) . '`';

		$values = $this->_funcExtractFields();
		if (is_array($data)) {
			$cols = $this->_funcGetDbFields();
			foreach ($cols as $field)
				$values[$field] = $data[$field];
		}
		//		die(print_array($values));
		foreach ($values as $field => $val)
			$values[$field] = scrubDbData($val);

		$data = '"' . implode('","', $values) . '"';

		if ($id = System::$DB->Insert($this->_tableName, $columns, $data))
			$this->Load($id);

		return $id;
	}

	public function Update($data = NULL) {
		$entries =
				array
				();
		$columns = $this->_funcGetDbFields();

		$values = $this->_funcExtractFields();
		if (is_array($data)) {
			$cols = $this->_funcGetColumns();
			foreach ($cols as $field => $label)
				$values[$field] = $data[$field];
		}
		//die("data = $data ".print_array($columns).print_array($values));
		foreach ($values as $field => $val)
			$values[$field] = scrubDbData($val);

		foreach ($columns as $field) {
			if ($field == 'id')
				continue;
			$entries[] = "`$field` = \"$values[$field]\"";
		}
		$entries = implode(" , ", $entries);
		$id = $data['id'] ? $data['id'] : $this->id;

		return System::$DB->Update($this->_tableName, $entries, "`id` = '$id'");
	}

	public function Delete($id = NULL) {
		if (!$id)
			$id = $this->id;
		return System::$DB->Delete($this->_tableName, "`id` = '$id'");
	}

}

function scrubDbData($value) {
	$value = str_replace("\\", "", $value);
	//$value = str_replace("\"","&quot;",$value);
	$value = mysql_real_escape_string($value);
	$value = str_replace('\\\'', "&#039;", $value);
	//$value = str_replace('"','\"',$value);	
	return $value;
}

?>