<?

class_exists('System_core') || require_once(dirname(__FILE__) . '/System/' . 'System_core.php');
final class System extends System_core {
	public $customer = NULL;

	public function __construct() {
		parent::__construct($this);
		self::$instance =& $this; // must be called from child
		$ps = new Site_page_sections();
		// Check if the current page has right sections, if not, load the homepage's right sections
		if (!count($this->sectionsRight))
			$this->sectionsRight = $ps->Select("*",
			                                   "`page_id` = '" . Site_page::getIdByTag(System_defaults::Page) . "' AND template_section_id = '" . System_columns::Content_right . "'");
		// If no billboards are found for this page, load the default one used on the homepage
		if (!count($this->billboard))
			$this->billboard = $ps->Select("*",
			                               "`page_id` = '" . Site_page::getIdByTag(System_defaults::Page) . "' AND template_section_id = '" . System_columns::Billboard . "'");
		// Attempt to load the session customer
		LoadModel('Customer');
		LoadModel('Customer_log');
		Customer_log::Access();
		$this->customer = new Customer();
		$this->customer->getSession();

		$p = System_defaults::Root_web_path;
		$this->addJquery(<<< EOB
		$(".subsite-selector").hover(function(){
			$(".subsite-options").show();
		},function(){
			$(".subsite-options").hide();
		});
		$(".subsite-options div").click(function(){
			window.location = "{$p}"+$(this).attr('data-target');
		});
EOB
);
		$this->renderHomeVideo();

		$this->addJs('
		function dialogFix(id) {
			debug("fixing id of.."+id);
			jQuery("#"+id).css({"left":"5px"});
		}
		');
	}

	public function renderSubsiteSelector() {
		$arrowImg = "<img src='" . TEMP_IMGS . "header/subsite-down-arrow.png'/>";
		if (ACTIVE_SITE == Sites::Personal_insurance) {
			print <<< EOB
		<div class="subsite-selector">
			<div class="subsite-dropdown">
				<div class="subsite-label">Personal</div>
				$arrowImg
			</div>
			<div class="subsite-options">
				<div data-target="commercial">Commercial Insurance</div>
				<div data-target="financial">Financial Services</div>
			</div>
		</div>
EOB;
		}
		else if (ACTIVE_SITE == Sites::Commercial_insurance) {
			print <<< EOB
		<div class="subsite-selector">
			<div class="subsite-dropdown">
				<div class="subsite-label">Commercial</div>
				$arrowImg
			</div>
			<div class="subsite-options">
				<div data-target="personal">Personal Insurance</div>
				<div data-target="financial">Financial Services</div>
			</div>
		</div>
EOB;
		}
		else if (ACTIVE_SITE == Sites::Financial_services) {
			print <<< EOB
		<div class="subsite-selector">
			<div class="subsite-dropdown">
				<div class="subsite-label">Financial</div>
				$arrowImg
			</div>
			<div class="subsite-options">
				<div data-target="personal">Personal Insurance</div>
				<div data-target="commercial">Commercial Insurance</div>
			</div>
		</div>
EOB;
		}
	}

	function renderHomeVideo() {
		if(PAGE_TAG == 'team')
		{
			$this->addJquery('
			$("#contentRight .content-section:eq(1)").hide();
			');
			return;
		}

		$homeVideo = '<iframe id="home-vid" src="https://player.vimeo.com/video/37256927?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=home-vid" width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		$this->addJquery('
		var daHomeVid = null;
		$("#watch-video").click(function(){
			$("#home-video-box").RC_Dialog().open();
			setTimeout("dialogFix(\'home-video-box\')",1);
			setTimeout("dialogFix(\'home-video-box\')",200);
			setTimeout("dialogFix(\'home-video-box\')",500);

			$(".close").click(function() {
				if(!daHomeVid)
					daHomeVid = $f("home-vid");
				daHomeVid.api("unload");
			});
		});');
		$this->addCSS("
		#home-video-box {
			border: 0;
		}
		#home-video-box .heading {
			display: none;
		}
		#home-video-box .content {
			padding: 0;
		}
		");
		System_core::Dialog('home-video-box',$homeVideo, '','',array('appendTo'=>'#header'));//,'position'=>"new Array(10,0)"
	}

}

/*
 * EOF
 */
