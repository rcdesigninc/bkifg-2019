<?

require_once(SETTINGS . 'Database_settings.php');
class_exists('Database_query') || require_once(dirname(__FILE__) . '/Database/' . 'Database_query.php');

final class Database extends Database_settings {
	private $queries =
	array
	();

	public function __construct() {
		$db_connect = mysql_connect(Database::server, Database::user,
			Database::pass) or die ("Could not connect to database");
		$selected_db = mysql_select_db(Database::schema) or die ("Could not select database!");
	}

	/**
	 * @return string
	 */
	public function lastQuery() {
		return (string)end($this->queries);
	}

	/**
	 * @static
	 * @param $error
	 * @param int $user_id
	 * @return void
	 */
	public static function Error($error, $user_id = 0) {
		if (strpos($error, 'Site_log') === false && mysql_error()) {
			LoadModel('Site_log');
			$log = new Site_log();

			if (!$user_id)
				$user_id = System::$instance->user->id;

			$error = str_replace("'", "", ($error . "<br />Error: " . mysql_error()));

			$log->Insert($error, false, $user_id);
		}
	}

	/**
	 * @param $table
	 * @param string $where
	 * @return int
	 */
	public function Count($table, $where = '') {
		$this->queries[] = $query = new Database_query('count(*)', $table, Database_query_type::Select, $where, '', '', Database_query_result::Int);
		return $query->run();
	}

	/**
	 * @param $columns
	 * @param $table
	 * @param $where
	 * @param $order_by
	 * @param $limit
	 * @param $returns
	 * @param Model $model
	 * @return array[Model]|Model
	 */
	public function Select($columns, $table, $where, $order_by, $limit, $returns, Model &$model) {
		$this->queries[] = $query = new Database_query($columns, $table, Database_query_type::Select, $where, $order_by, $limit, $returns, $model);
		return $query->run();
	}

	/**
	 * @param $table
	 * @param $columns
	 * @param $values
	 * @return int
	 */
	public function Insert($table, $columns, $values) {
		$id = 0;

		$sql = "INSERT INTO`$table`($columns)
		VALUES($values)";

		$result = mysql_query($sql);

		if ($result)
			$id = mysql_insert_id();
		else
			self::Error($sql);

		$this->queries[] = $sql;
		return $id;
	}

	/**
	 * @param $table
	 * @param $columns
	 * @param $where
	 * @return bool
	 */
	public function Update($table, $columns, $where) {
		$sql = "UPDATE `$table` SET $columns
		WHERE $where";

		$result = mysql_query($sql);

		if (!$result) {
			self::Error($sql);
			$result = false;
		}
		else
			$result = true;

		$this->queries[] = $sql;
		return $result;
	}

	/**
	 * @param $table
	 * @param $where
	 * @return bool
	 */
	public function Delete($table, $where) {
		$sql = "DELETE FROM `$table` WHERE $where";

		$result = mysql_query($sql);

		if (!$result) {
			self::Error($sql);
			$result = false;
		}
		else
			$result = true;

		$this->queries[] = $sql;
		return $result;
	}


}

/*
 * EOF
 */