<?

class_exists('Database_query_type') || require_once(dirname(__FILE__) . '/' . 'Database_query_type.php');
class_exists('Database_query_result') || require_once(dirname(__FILE__) . '/' . 'Database_query_result.php');

final class Database_query {
	public $model = null;
	public $columns = '';
	public $table = '';
	public $action = 1;
	public $where = '';
	public $order_by = '';
	public $limit = '';
	public $returns = 1;

	private $query = '';

	private $results = '';

	public function __construct($columns = '', $table = '', $action = Database_query_type::Select, $where = '', $order_by = '', $limit = '', $returns = Database_query_result::Model_array, Model &$model = null) {
		$this->columns = $columns;
		$this->table = $table;
		$this->action = $action;
		$this->where = $where;
		$this->order_by = $order_by;
		$this->limit = $limit;

		$this->returns = $returns;

		$this->model = $model;

		$this->buildQuery();
	}

	private function buildQuery() {
		if (is_array($this->columns))
			$this->columns = '`' . implode('`,`', $this->columns) . '`';
		$this->query = "SELECT $this->columns FROM `$this->table`";
		$this->query .= " ";

		if ($this->where != '')
			$this->query .= " WHERE " . $this->where;
		$this->query .= " ";

		if ($this->order_by) // && strpos(strtolower($this->order_by), 'order by') === false
			$this->query .= " ORDER BY $this->order_by";
		$this->query .= " ";

		if ($this->limit != '')
			$this->query .= " LIMIT " . $this->limit;
	}

	public function run() {
		$result = mysql_query($this->query);

		if (!$result) {
			$result = $this->model;
			Database::Error($this->query . $this->results);
		}
		else {
			if (strpos(strtolower($this->query), 'insert') === false
			)
				$this->results = "<br />Results: " . $result . ', ' . mysql_num_rows($result);

			switch ($this->returns) {
				case Database_query_result::Int:
					$return = mysql_fetch_array($result);
					$result = $return[0];
					break;
				case Database_query_result::Model:
					if ($entry = mysql_fetch_assoc($result))
						$this->model->_funcPopulateFields($entry);
					$result = $this->model;
					break;
				case Database_query_result::Model_array:
					// todo: looking into using some kind of cloning to create the objects instead
					//$o = clone $model($data);

					$entries =
							array
							();
					while ($entry = mysql_fetch_assoc($result))
						$entries[] = new $this->model->_className($entry);
					$result = $entries;
					break;
				default:
			}
		}

		return $result;
	}

	public function __toString() {
		return $this->query . $this->results;
	}
}

/*
 * EOF
 */