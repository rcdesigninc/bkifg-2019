<?
class_exists('Database') || require_once(dirname(__FILE__) . '/../' . 'Database.php');

define("BASE_CLASS", "Model");

$DB = new Database();

$tables =
		array
		();
$sql = "SHOW TABLES;";
$q = mysql_query($sql);
while ($table_name = mysql_fetch_array($q))
	$tables[] = $table_name[0];

foreach ($tables as $table_name) {
	$data =
			array
			();
	$data['classname'] = $table_name;
	$data['final_classname'] = ucwords(array_pop(preg_split("/_/", $table_name, 2)));
	$data['data_table'] = $table_name;
	$data['baseclass'] = BASE_CLASS;

	$data['fields'] = $data['sql_insert_list'] = $data['sql_update_list'] =
			array
			();
	$sql = "DESCRIBE $table_name";
	$q = mysql_query($sql);
	while ($column = mysql_fetch_array($q)) {
		list($field, $type, $null, $key, $default, $extra) = $column;
		$data['fields'][$field] = $column;
		$data['sql_select_list'][] = $field;
		$data['sql_insert_list'][] = $field;
		$data['sql_update_list'][] = $field;
	}

	$tables[$table_name] = $data;
}

$built =
		array
		();

$bypassed =
		array
		();

foreach ($tables as $table) {
	if ($table[classname] == 't')
		continue;

	$class = "<?

require_once(SYS_LIB . '/' .'Model.php');

class $table[classname] extends $table[baseclass]
{";
	foreach ($table['fields'] as $field) {
		$class .= "
	/** @var Model_field \$$field[Field] */
	//public \$$field[Field];";
	}
	$class .= "

	public function __construct(\$arrData = NULL) {
		parent::__construct('$table[data_table]');";
	foreach ($table['fields'] as $field) {
		$class .= "
		\$this->_funcAddField('$field[Field]','$field[Type]','$field[Null]','$field[Key]','$field[Default]','$field[Extra]');";
	}
	$class .= "


		\$this->_funcPopulateFields(\$arrData);
		
		\$this->_funcInit();
	}
}

/*
 * EOF
 */";

	/*
	if(!is_dir("Model"))
		mkdir("Model", 0775);
	if(!is_dir("Model/_base"))
		mkdir("Model/_base", 0775);
	*/
	//'Models/_base/'.
	//if ($table['classname'] != 'tbl_site_user' || ($table['classname'] == 'tbl_site_user' && !is_file(MODEL_PATH . '_base/' . $table['classname'] . ".php")))
	{
		$handle = fopen(MODEL_PATH . '_base/' . $table['classname'] . ".php", "w");
		fwrite($handle, $class);
		fclose($handle);
		$built[] = '' . $table['classname'] . ".php - file created/rebuilt<br />";
	}
	/*else
		$bypassed[] = '' . $table['classname'] . ".php - file already exists; bypassed<br />";*/


	$class = "<?

class_exists('$table[classname]') || require_once(MODEL_PATH.'_base/'.'$table[classname]'.'.php');

class $table[final_classname] extends $table[classname] {
	protected function _funcInit() {";

	foreach ($table['fields'] as $field) {
		if($field['Field'] == 'id')
			$class .= "
			\$this->_funcSetField('id', 'Options', Model_field_type::Hidden);";
		else
			$class .= "
			\$this->_funcSetField('$field[Field]', '" . ucwords($field['Field']) . "', Model_field_type::Input); ";
		$class .= "// dbtype: $field[Type]";
	}
	$class .= "
	}
}

/*
 * EOF
 */";
	//'Models/'.

	if (!is_file(MODEL_PATH . '' . $table['final_classname'] . ".php")) {
		$handle = fopen(MODEL_PATH . '' . $table['final_classname'] . ".php", "x");
		if ($handle) {
			fwrite($handle, $class);
			fclose($handle);
			$built[] = '' . $table['final_classname'] . ".php - file created/rebuilt<br />";
		}
	}
	else {
		$bypassed[] = '' . $table['final_classname'] . ".php - file already exists; bypassed<br />";
	}

	//print "<textarea style='width:100%;height:100%'>".$class."</textarea>";
}

print "<h1>Classes Built</h1>";
asort($built);
foreach ($built as $b)
	print $b;

print "<h1>Classes Bypassed</h1>";
asort($bypassed);
foreach ($bypassed as $b)
	print $b;
?>

