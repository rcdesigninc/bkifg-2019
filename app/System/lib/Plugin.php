<?

require_once(SETTINGS . 'Plugin_defaults.php');
class_exists('Plugin_data') || require_once(dirname(__FILE__) . '/Plugin/' . 'Plugin_data.php');
class_exists('Json') || require_once(dirname(__FILE__) . '/Plugin/' . 'Json.php');

interface iPlugin {
	function process();

	function render();
}

abstract class Plugin implements iPlugin {
	/*public $data = null;

	public function __construct(Plugin_data $data = null)
	{
		$this->data = $data;
		if (!$data) $this->data = new Plugin_data();
	}*/
}