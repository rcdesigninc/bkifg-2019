<?

interface iJson {
	public function Add($label, $data);

	public function Encode();

	public function Get();

	public function Val($label);
}

class Json implements iJson {
	private $data =
	array
	();

	public function Add($label, $o) {
		$this->data[$label] = $o;
	}

	public function AddArray($array) {
		$this->data = array_merge($this->data, $array);
	}

	public function Encode() {
		return json_encode($this->data);
	}

	public function Get() {
		return $this->data;
	}

	public function Val($label) {
		return $this->data[$label];
	}
}