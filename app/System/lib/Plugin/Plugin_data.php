<?

interface iPlugin_data {
}

abstract class Plugin_data implements iPlugin_data {
	public $id = '';

	public $entries = null;
	public $json = null;

	public $output = '';

	public function __construct() {
		$this->json = new Json();
		//$this->entries = new ModelList();

		if (!$this->id) // Assigned generic ID, if no id was selected
			$this->id = uniqid() . '-'.get_class($this);
	}

	/**
	 * @abstract
	 * @return string
	 */
	public abstract function setJson();
}