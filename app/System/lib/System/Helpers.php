<?

function LoadClass($cname, $filename) {
	if (is_file($filename))
		class_exists($cname) || require_once($filename);
	return class_exists($cname);
}

function LoadPluginClass($cname) {
	$file = PLUGIN_PATH . $cname . '/lib/' . $cname . '.php';
	return LoadClass($cname, $file);
}

function LoadModel($cname) {
	return LoadClass($cname, MODEL_PATH . $cname . '.php');
}

function printArray($arr) {
	return "<pre>" . print_r($arr, true) . "</pre>";
}

function getTimestamp() {
	//return date("Y-m-d h:m:s");
	return time();
}

function printTimestamp($date) {
	if ($date instanceof Model_field)
		return date("Y-m-d h:m:s", $date->value);
	return date("Y-m-d h:m:s", $date);
}

function printHtml($maxLength, $html) {
	$completed = '';
	$printedLength = 0;
	$position = 0;
	$tags =
			array
			();

	while ($printedLength < $maxLength && preg_match('{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', $html, $match,
		PREG_OFFSET_CAPTURE, $position)) {
		list($tag, $tagPosition) = $match[0];

		// Print text leading up to the tag.
		$str = substr($html, $position, $tagPosition - $position);
		if ($printedLength + strlen($str) > $maxLength) {
			$completed .= substr($str, 0, $maxLength - $printedLength);
			$printedLength = $maxLength;
			break;
		}

		$completed .= $str;
		$printedLength += strlen($str);

		if ($tag[0] == '&') {
			// Handle the entity.
			$completed .= $tag;
			$printedLength++;
		}
		else {
			// Handle the tag.
			$tagName = $match[1][0];
			if ($tag[1] == '/') {
				// This is a closing tag.

				$openingTag = array_pop($tags);
				assert($openingTag == $tagName); // check that tags are properly nested.

				$completed .= $tag;
			}
			else if ($tag[strlen($tag) - 2] == '/') {
				// Self-closing tag.
				$completed .= $tag;
			}
			else {
				// Opening tag.
				$completed .= $tag;
				$tags[] = $tagName;
			}
		}

		// Continue after the tag.
		$position = $tagPosition + strlen($tag);
	}

	// Print any remaining text.
	if ($printedLength < $maxLength && $position < strlen($html))
		$completed .= substr($html, $position, $maxLength - $printedLength);

	if (strlen($completed) >= $maxLength)
		$completed .= '...';

	// Close any open tags.
	while (!empty($tags))
		$completed .= sprintf('</%s>', array_pop($tags));
	return $completed;
}

function hyphonate($str) {
	$str = strtolower(str_replace(" ", "-", $str));
	preg_replace('/[:\.\?\&\;\!\#\/\\],\(\)\']/', "", $str);
	return $str;
}

function unhyphonate($str) {
	//preg_replace('/[:\.\?\&\;\!\#\/\\],\(\)]/',"",$str);
	return ucwords(str_replace("-", " ", $str));
}

function SendMail ($to_address, $from_name, $from_address, $subject, $message)
{
	$headers = "From: ".$from_name." <".$from_address.">\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
	$header = '<html><head></head><body>';
	$footer = "</body></html>";
	$message = $header.$message.$footer;
	return mail($to_address, $subject, $message, $headers);
}

/*
 * EOF
 */