<?



require_once(dirname(__FILE__) . '/' . "Helpers.php");

require_once(dirname(__FILE__) . '/' . 'System_settings.php');

class_exists('Database') || require_once(SYS_LIB . '/' . 'Database.php');

class_exists('Mod') || require_once(SYS_LIB . '/' . 'Plugin.php');



abstract class System_core {

	/** @var Database $DB

	 * A static global reference to the database object/class

	 */

	public static $DB = null;



	/** @var System $instance

	 * A static global reference this object

	 */

	public static $instance = null;



	/** @var Site_user $user

	 * A object of the current user who is logged in, automatically session loaded for ease

	 * */

	public $user = NULL;



	/** @var Site_page $page

	 * A object of the current page which is being view, either by tag or id if specified as an override.

	 * Can still be manipulated before the page is rendered if required, such as realigning pages to match

	 * a common ancestor if they rely on reusable content.

	 * */

	public $page = NULL;



	/** @var Site $site

	 * A object of the current site which is being access.  This is specified and coded in the config files

	 * before any call to the system, and should most typically not change yet is useful to reference.

	 * */

	public $site = NULL;



	/** @var Site_template $template

	 * A object of the current template to load based on the current page.  This can either be controlled

	 * within the CMS to modified which template a page should load, which default template a new page for

	 * a site will load, but as a last resort it is open to be manipulated within the system itself.

	 * */

	public $template = NULL;



	/** @var Site_menu $menu

	 * A object of the current selected menu for this page.  Not a directly used object, namely to aid in

	 * specifying if the current page is a 'root' menu or a submenu item and if so to find it's root for

	 * highlighting styling.

	 * */

	public $menu = NULL;



	/** @var Site_settings $settings

	 * A object of the current site's settings which have some more front-end based fields such as google id,

	 * twitter tag and such, to be used.

	 * */

	public $settings = NULL;



	/** @var string $content

	 * A string of the active 'main' content to be outputted to the template

	 * */

	public $content = '';

	/** @var string $contentRight

	 * A string of the active 'sidebar' content to be outputted to the template

	 * */

	public $contentRight = '';



	/** @var null|array[Site_page_sections] $sections

	 * An array of page sections that make up the 'main' content section, to be rendered on page load

	 * */

	public $sections = NULL;

	/** @var null|array[Site_page_sections] $sectionsRight

	 * An array of page sections that make up the 'sidebar' content section, to be rendered on page load

	 * */

	public $sectionsRight = NULL;

	/** @var null|array[Site_page_sections] $billboard

	 * An array of page sections that make up the 'billboard' section, to be rendered on page load

	 * */

	public $billboard = null;



	/** @var bool $disableMenu

	 * A flag whether or not the site's menu should be visible or not. Handy, namely in the CMS when a

	 * user is trying to login but has not yet or not been successful, ensuring the menu will not render.

	 * */

	public $disableMenu = false;



	/** @var string $css

	 * A string of the generated CSS that's accumulated during loading the system and processing needed

	 * functions, to be outputted upon page load in a controlled manor within the template.

	 * */

	protected $css = '';

	/** @var string $js

	 * A string of the generated JS that's accumulated during loading the system and processing needed

	 * functions, to be outputted upon page load in a controlled manor within the template.

	 * */

	protected $js = '';

	/** @var string $jquery

	 * A string of the generated jQuery that's accumulated during loading the system and processing needed

	 * functions, to be outputted upon page load in a controlled manor within the template.  Unlike the

	 * JS storage, the jquery within the template will wrap inside a jquery section to ensure it runs.

	 * */

	protected $jquery = '';

	/** @var string $includes

	 * A string of include files to be added, JS, CSS lines that need to be ran after the global file calls,

	 * and before any generated js/jquery is ran so that it can make use of these libraries/mods.

	 * */

	protected $includes = '';



	public $dialogs = '';



	/**

	 * @var array $plugins

	 * An array list of the all the currently loaded mods, so that we ensure the same mod and files are not

	 * loaded onto the system / page load twice.

	 */

	private $plugins = array();



	/**

	 * The System_core constructor serves to:

	 * - Set instancing of the Database object;

	 * - Load any core dependant mods;

	 * - Load any system wide models that will be used;

	 * - Attempt to session load the current user;

	 * - Determine the current selected page;

	 * - Load the default system objects in sequence;

	 */

	public function __construct($child) {

		self::$instance =& $child; // must be called from child



		// Fire up the session

		session_start();



		// Fire up the Database

		self::$DB = new Database();



		// Auto-load any required default system wide mods

		$mods = explode(',', System_defaults::Plugins);

		foreach ($mods as $plugin)

			$this->LoadPlugin(trim($plugin));



		// Auto-load any required default system wide models

		$models = explode(',', System_defaults::Models);

		foreach ($models as $model)

			LoadModel(trim($model));



		// If the user is logged in, will automatically pull data from session

		$this->user = new Site_user();

		$this->user->getSession();



		// Determine the current page to be viewed, else default

		$section = System_defaults::Page;

		if ($_GET['section'])

			$section = $_GET['section'];

		define('PAGE_TAG', $section);



		// Load the page

		$this->page = new Site_page();

		if (PAGE_ID != "PAGE_ID")

			$this->page->Load(PAGE_ID);

		else {

			$this->page->LoadWhere("`tag` = '" . PAGE_TAG . "' AND `site_id` = '" . ACTIVE_SITE . "'");

			define('PAGE_ID', $this->page->id);

		}



		$this->setupPage(PAGE_ID);

	}



	final private function setupPage() {

		// Load the selected template

		$this->template = new Site_template();

		$this->template->Load($this->page->template_id);

		//die(print_array($this->template));



		// Load the selected site

		$this->site = new Site();

		$this->site->Load($this->page->site_id);

		define('SITE_ID', $this->site->id);

		//die(print_array($this->site));



		// Load the site settings

		$this->settings = new Site_settings();

		$this->settings->LoadWhere("`site_id` = '" . $this->site->id . "'");

		//die(print_array($this->settings));



		$this->menu = new Site_menu();

		$this->menu->LoadWhere("`site_id` = '" . $this->site->id . "' AND `page_id` = '" . $this->page->id . "'",

		                       "`parent_id` DESC");

		define('MENU_ID', $this->menu->parent_id ? $this->menu->parent_id : $this->menu->id);



		$menus = $this->menu->Select("*",

		                             "`site_id` = '" . $this->site->id . "' AND `page_id` = '" . $this->page->id . "'",

		                             "`parent_id` DESC");

		//die(self::$DB->lastQuery().printArray($menus));

		

		foreach ($menus as $m) {

			$this->addJquery("\n" . '$("#menu-' . $m->page_id . '").addClass("selected");');

			$this->addJquery("\n" . '$("#footer-menu #menu-' . $m->page_id . '").addClass("selected");');

				$this->addJquery("\n" . '$("#footer-sub-menu #menu-' . $m->page_id . '").addClass("selected");');

			if($m->parent_id)

			{

				$p = new Site_menu();

				$p->Load($m->parent_id);

				$this->addJquery("\n" . '$("#menu-' . $p->page_id . '").addClass("selected");');

				$this->addJquery("\n" . '$("#footer-menu #menu-' . $p->page_id . '").addClass("selected");');

				$this->addJquery("\n" . '$("#footer-sub-menu #menu-' . $p->page_id . '").addClass("selected");');

			}

		}





		// Load the pages default sections

		$ps = new Site_page_sections();

		$this->sections = $ps->Select("*",

		                              "`page_id` = '" . $this->page->id . "' AND template_section_id = '" . System_columns::Content_left . "'");

		$this->fillCommonSections($this->sections, System_columns::Content_left);

		$this->sectionsRight = $ps->Select("*",

		                                   "`page_id` = '" . $this->page->id . "' AND template_section_id = '" . System_columns::Content_right . "'");

		$this->fillCommonSections($this->sectionsRight, System_columns::Content_right);



		// Load the pages billboard(s)

		$this->billboard = $ps->Select("*",

		                               "`page_id` = '" . $this->page->id . "' AND template_section_id = '" . System_columns::Billboard . "'");



	}



	final private function fillCommonSections(&$type, $section) {

		for($i = 0; $i < count($type); $i++) {

			if(!strlen($type[$i]->content)) {

				$r = new Site_page_sections();

				$r->LoadWhere("page_id = 56 AND template_section_id= '" . $section . "' AND heading = '{$type[$i]->heading}'");

				if($r->id) {

					$type[$i]->content = $r->content;

				}

			}

		}

	}



	final public function reloadSections($page_id) {

		$this->page->Load($page_id);



		$this->setupPage();

		//		$ps = new Site_page_sections();

		//		$this->sections = $ps->Select("*",

		//				"`page_id` = '" . $page_id . "' AND template_section_id = '" . System_columns::Content_left . "'",

		//			'order_by');

		//		$this->sectionsRight = $ps->Select("*",

		//				"`page_id` = '" . $page_id . "' AND template_section_id = '" . System_columns::Content_right . "'",

		//			'order_by');

		//

		//		//if (!count($this->billboard))

		//		$this->billboard = $ps->LoadWhere("`page_id` = '" . $page_id . "' AND template_section_id = '" . System_columns::Billboard . "'");

	}



	final public function addJs($script) {

		$this->js .= $script;

	}



	final public function addJquery($script) {

		$this->jquery .= $script;

	}



	final public function addCSS($css) {

		$this->css .= $css;

	}



	final public function addInclude($data) {

		$this->includes .= $data . "\n";

	}



	final public function includeJs($data) {

		$this->includes .= $data . "\n";

	}



	final public function includeCss($data) {

		$this->includes .= $data . "\n";

	}



	final public function printPluginIncludes() {

		$minCss = $minJs = '';

		foreach ($this->plugins as $plugin) {

			$minCss .= $plugin['css'];

			$minJs .= $plugin['js'];

		}



		print '<link type="text/css" rel="stylesheet" href="' . APP_PATH . 'Minify/?b=' . System_defaults::Minify_run_path . '&amp;f=' . rtrim($minCss,

		                                                                                                                                       ",") . '" />' . "\n";

		print '<script type="text/javascript" src="' . APP_PATH . '/Minify/?b=' . System_defaults::Minify_run_path . '&amp;f=' . rtrim($minJs,

		                                                                                                                               ",") . '"></script>' . "\n";



		// Place here for late binding

		$this->addJquery('

		buildRelated();

		');

	}



	final public function LoadPlugin($plugin_name, $loadAllJs = true) {

		// Ensure we are only loading system wide plugins ONCE! :D

		if (!$this->plugins[$plugin_name]['added']) {

			$this->plugins[$plugin_name]['added'] = true;

			LoadPluginClass($plugin_name);



			System_defaults::Debugging ? $this->LoadPluginIncludes($plugin_name, $loadAllJs)

					: $this->LoadPluginMinIncludes($plugin_name, $loadAllJs);

		}

	}



	final private function LoadPluginMinIncludes($plugin_name, $loadAllJs = true) {

		$plugin_path = PLUGIN_PATH . $plugin_name . '/';

		$plugin_css = $plugin_path . 'css/';

		$plugin_js = $plugin_path . 'js/';



		LoadPluginClass($plugin_name);



		if (is_dir($plugin_css)) {

			$dh = opendir($plugin_css);

			while (($file = readdir($dh)) !== false) { // Ensure we are only capturing valid CSS files!

				if (preg_match('/\.css$/i', $file))

					$this->plugins[$plugin_name]['css'] .= "Plugins/$plugin_name/css/$file,";

			}

			closedir($dh);

		}



		if ($loadAllJs) {

			if (is_dir($plugin_js)) {

				$dh = opendir($plugin_js);

				while (($file = readdir($dh)) !== false) { // Ensure we are only capturing valid JS files!

					if (preg_match('/\.js$/i', $file))

						$this->plugins[$plugin_name]['js'] .= "Plugins/$plugin_name/js/$file,";

				}

				closedir($dh);

			}

		}

		else

			$this->plugins[$plugin_name]['js'][] = System_defaults::Minify_groupConfig_path . "Plugins/$plugin_name/js/$plugin_name" . '.js';

	}



	final private function LoadPluginIncludes($plugin_name, $loadAllJs = true) {

		$plugin_path = PLUGIN_PATH . $plugin_name . '/';

		$plugin_css = $plugin_path . 'css/';

		$plugin_js = $plugin_path . 'js/';



		LoadPluginClass($plugin_name);



		if (is_dir($plugin_css)) {

			$dh = opendir($plugin_css);

			while (($file = readdir($dh)) !== false) { // Ensure we are only capturing valid CSS files!

				if (preg_match('/\.css$/i', $file))

					$this->addInclude("<link rel='stylesheet' type='text/css' href='$plugin_css$file'/>");

			}

			closedir($dh);

		}



		if ($loadAllJs) {

			if (is_dir($plugin_js)) {

				$dh = opendir($plugin_js);

				while (($file = readdir($dh)) !== false) { // Ensure we are only capturing valid JS files!

					if (preg_match('/\.js$/i', $file))

						$this->addInclude("<script type='text/javascript' src='$plugin_js$file'></script>");

				}

				closedir($dh);

			}

		}

		else

			$this->addInclude("<script type='text/javascript' src='$plugin_js$plugin_name.js'></script>");

	}



	final public function prependToSectionRight($content) {

		array_unshift($this->sectionsRight, $content);

	}



	final public function appendToSectionRight($content) {

		array_push($this->sectionsRight, $content);

	}



	final public function renderContent() {

		print $this->content;

	}



	final public function renderContentRight() {

		$ps = new Site_page_sections();
                $travel_insurance = "";
                $ts = $_GET['section'];
                if($ts == "travel-insurance") {
                    $travel_insurance = '
                        <div class="content-section travel-sidebar" id="" style="margin-bottom: 20px;">
                            <div><a href="https://www.snowbirdmediquote.com/clientportal/loginpage.aspx?brokerrefererid=84491&DownlineBrokerCompany=442" target="_blank"><img src="' . TEMP_IMGS . '/home/Desktop_Personal_Block_Normal.png"/><img src="' . TEMP_IMGS . '/home/Desktop_Personal_Block_Hover.png" class="hidden"/></a></div>
			</div>
                    </div>';
                }
                
		$videoSidebar = '
		<div class="content-section " id="save-and-combine" style="">
			<h2 class="content-heading">Watch a message from our President...</h2>
			<div class="content-body">
				<p>We are independent insurance agency leaders at the top of our game, with our feet planted firmly in our community. Click on the icon below to watch a video from our President, Stephen Kearley.</p>
				<div id="watch-video" style=""></div>
			</div>
		</div>';
//		if(!$_GET['testing'])
//			$videoSidebar = '';

		print $ps->getContent('','hidden related-links').$travel_insurance.$videoSidebar.$this->contentRight;


	}



	final function renderDefaultLeft($content = '') {

		if ($content == '')

			foreach ($this->sections as $section)

				$content .= $section->getContent();



		return $content;

	}



	final function renderDefaultRight() {

		$contentRight = '';

		foreach ($this->sectionsRight as $section)

			$contentRight .= $section->getContent();

		return $contentRight;

	}



	function renderDefaultPage($content = '', $contentRight = '', $billboard = '') {

		if ($content == '')

			$content = $this->renderDefaultLeft();

		if ($contentRight == '')

			$contentRight = $this->renderDefaultRight();



		$this->renderPage($content, $contentRight, $billboard);

	}



	public function renderPage($content = '', $contentRight = '', $billboard = '') {

		global $menu_id;

		$this->content .= $content;

		$this->contentRight .= $contentRight;



		// Was an override billboard provided?  If not, use the default one for the associated page.

		if ($billboard != '')

			$this->billboard = $billboard;

		else

			$this->billboard = $this->renderBillboard();



		$this->content .= $this->dialogs;



		include(TEMP_PATH . $this->template->path);

	}



	public function renderBillboard() {

		if (count($this->billboard) > 1) {

			$entries = '';

			foreach ($this->billboard as $b) {

				$entries .= $b->renderBillboard('hidden'); //'slide hidden');

			}



			/*

			// Custom Call

			$sd = new Slidegal_data();

			$sd->id = 'billboard-slideshow';

			$sd->entries = $entries;

			$sd->animation = Slidegal_animation::fadeZoom;

			$slideshow = new Slidegal($sd);

			*/



			// Lazy Call

			// Takes defaults set within the class, can only be for standard specific use that doesn't change!

			$slideshow = new Slidegal(new Slidegal_data($entries));

			return $slideshow->render();

		}

		else if ($this->billboard[0])

			return $this->billboard[0]->renderBillboard();

		else if ($this->billboard)

			return $this->billboard->renderBillboard();



		return "";

	}



	public function renderMenu($label = '', $prefix = '', $site_id = 0) {

		if ($this->disableMenu)

			return false;



		$menu = array();

		$m = new Site_menu();



		if ($site_id) {

			$parentId = Site_menu::getIdByLabel($label, $site_id);

		}

		else if ($label)

			$parentId = Site_menu::getIdByLabelActive($label);

		else

			$parentId = 0;



		$menu_list = array();

		//		if($site_level != '') {

		//			$menu_list = $m->Select("*", "site_id = $site_level AND `parent_id` = '$parentId'");

		////			print "$label, $prefix, $site_level";



		////			die(print_array($menu_list));

		//		}

		//		else

		$menu_list = $m->Select("*", "`parent_id` = '$parentId'");

		//print self::$DB->lastQuery();



		foreach ($menu_list as $entry)

			$menu[$entry->id] = $entry;



		//die("$label = '', $prefix = ''".$this->lastQuery.print_array($menu));



		$str = '';



		foreach ($menu as $entry) {

			/** @var Site_menu $entry */



			if ($label == 'Top Menu' && $this->site->name == $entry->label)

				continue;



			$sub_id = $mid = $entry->getClassId();


			if($label == 'Main Menu' && ACTIVE_SITE == 2)
				$mid = "main-$mid";


			$submenus = $m->Select("*", "`parent_id` = {$entry->id}");



			//die("entry id = ".$entry->id.print_array($submenus));



			if (!count($submenus)) { // doesn't have a submenu

				$str .= "

					<div id='menu-$mid-Container' class='menu-container'>

						<div id='menu-$mid' class='menu1 menutop'>

							<a href='$prefix" . $entry->getLink() . "'>$entry->label</a>

						</div>

						<div id='menu-$mid-Submenu' class='submenu'>

						</div>

					</div>";

			}

			else {

				$str .= "

					<div id='menu-$mid-Container' class='menu-container'>

						<div id='menu-$mid' class='menu1 menutop'>

							<a href='$prefix" . $entry->getLink() . "'>$entry->label</a>

						</div>

						<div id='menu-$mid-Submenu' class='submenu'>";

				foreach ($submenus as $sub) {

					$str .= "

							<div id='menu-$sub->page_id' class='menu2'><div><a href='" . $sub->getLink() . "'>$sub->label</a></div></div>";

				}

				$str .= "

						</div>

					</div>";

			}

		}

		return $str;

	}



	public function renderFooterCopyright() {

		$ps = new Site_page_sections();

		$copyright = $ps->Select("content",

		                         "`page_id` = '" . Site_page::getIdByTag('home') . "' AND `template_section_id` = '13'",

		                         'order_by', "", false);

		print "

		<div id='copyright'>

		&copy; " . date("Y") . " " . $copyright->content . " <a href='http://www.rcdesign.com' target='_blank'><img src='" . SYS_IMGS . "icons/rc-default.png' width='18' height='18' /></a>

		</div>

		";

	}



	final public function renderGoogle() {

		if ($this->settings->google_analytics != '') {

			print "<script type='text/javascript'>

				var _gaq = _gaq || [];

				_gaq.push(['_setAccount', '" . $this->settings->google_analytics . "']);

				_gaq.push(['_trackPageview']);

				

				(function() {

				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

				})();

			</script>";

		}

	}



	public static function Error($content, $heading = 'An Error Has Occurred', $buttons = '') {

		$pd = new Dialog_data($content);

		$pd->heading = $heading;

		if (!$buttons)

			$pd->buttons = '<div class="button close-errors">Ok</div>';

		else

			$pd->buttons = $buttons;

		System::$instance->addJquery("$('.close-errors').live('click',function(){ $('#error-dialog').RC_Dialog().close(); });");

		$pd->auto_load = true;

		$pd->id = "error-dialog";

		$dialog = new Dialog($pd);

		System::$instance->dialogs .= $dialog->render();

	}



	public static function Dialog($id, $content, $heading = '', $buttons = '', $custom_json = array()) {

		$pd = new Dialog_data($content);

		$pd->heading = $heading;

		$pd->buttons = $buttons;

		$pd->id = $id;

		foreach ($custom_json as $label => $value)

			$pd->json->Add($label, $value);



		$dialog = new Dialog($pd);



		//print print_array($pd->json);



		System::$instance->dialogs .= $dialog->render();

	}

}



/*

 * EOF

 */