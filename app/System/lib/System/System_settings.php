<?

/*
* APP PATH SETTINGS
*/

define('SETTINGS', ROOT_PATH . '/Settings/');
define('SETTINGS_PLUGINS', SETTINGS . 'Plugins/');
define('SETTINGS_MODEL', SETTINGS . 'Model/');

define('SYS_PATH', ROOT_PATH . '/System/');
define('SYS_LIB', SYS_PATH . 'lib/');
define('SYS_CSS', APP_PATH . 'System/css/');
define('SYS_JS', APP_PATH . 'System/js/');
define('SYS_IMGS', APP_PATH . 'System/images/');
define('SYS_DOCS', APP_PATH . 'System/docs/');
define('SYS_AJAX', APP_PATH . 'System/ajax/');

define('TEMP_PATH', APP_PATH . 'Templates/');
define('TEMP_CSS', TEMP_PATH . 'css/');
define('TEMP_JS', TEMP_PATH . 'js/');
define('TEMP_IMGS', TEMP_PATH . 'images/');
define('TEMP_DOCS', TEMP_PATH . 'docs/');
define('TEMP_AJAX', TEMP_PATH . 'ajax/');

define('UPLOAD_PATH', APP_PATH . 'Uploads/');

define('MODEL_PATH', APP_PATH . 'Models/');

define('PLUGIN_PATH', APP_PATH . 'Plugins/');

define('API_PATH', ROOT_PATH . 'APIs/');

abstract class System_columns {
	const Content_left = 9;
	const Content_right = 10;
	const Billboard = 3;
	const Content_faq = 15;
	const Content = 4;
}

require_once(SETTINGS . 'System_defaults.php');

/*
 * EOF
*/