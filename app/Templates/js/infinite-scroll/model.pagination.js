// Set the path to the model.ajax file folder
ASSET_AJAX = '';

consoleEnabled = !!(window.console && window.console.log);

scrollPagination = modelScrollPagination('Model_name', {
	select   : '',
	where    : "",
	order    : "",
	index    : 0, // starting index
	limit    : 5, // entries per page
	template : ''
});

function debug($msg, object) {
	if (consoleEnabled) {
		if (window.console) {
			if (typeof window.console.log === 'function') {
				if ($msg) {
					if (object)
						window.console.log($msg + " %O", object);
					else
						window.console.log($msg);
				}
			}
		}
	}
}

function consoleGroupCollapsed(label) {
	if (consoleEnabled)
		if (typeof console.groupCollapsed === 'function')
			console.groupCollapsed(label);
}

function consoleGroupEnd() {
	if (consoleEnabled)
		if (typeof console.groupEnd === 'function')
			console.groupEnd();
}

function modelAjax(options, callback) {
	var result = false,
		opts = {
			type     : options.method,
			async    : options.async,
			url      : MODEL_AJAX_PATH + 'model.ajax.php',
			dataType : 'json',
			data     : _.defaults(options, {
				pageTag    : PAGE_TAG,
				activeSite : ACTIVE_SITE
			}),
			success  : function (data, textStatus, jqXHR) {
				if (_.isFunction((callback)))
					callback(data);
				else if (options.action == 'count')
					result = data.count;
				else if (data.success) {
					if (options.action != '' && options.pagination !== true)
						result = data.entries;
					else
						result = data;
				}

				if (options.async)
					return result;
			},
			error    : function (jqXHR, textStatus, errorThrown) {
				debug(textStatus + ', ' + errorThrown);
				debug(jqXHR);
				result = false;
			}
		};

	if (_.isUndefined(options.async))
		opts.async = true;

	if (_.isUndefined(options.action))
		opts.action = '';

	if (_.isUndefined(options.method))
		opts.method = 'post';

	if ((options.action == 'update' || options.action == 'publish') && !_.isUndefined(options.fields.id)) {
		debug("modelAjax attempted failed, missing ID on update/publish");
		return false;
	}

	if (options.file === true) {
		// Set and override some of the options
		opts = jQuery.extend(opts, {
			async       : false,
			action      : 'upload',
			method      : 'post',
			processData : false,
			contentType : false,
			data        : options.data,
			url         : ASSET_AJAX + 'upload.php?model=' + options.model + '&id=' + options.id
		});
	}

	//	console.log("modelAajx options %O", options);

	options.preview = PREVIEW;

	jQuery.ajax(opts);

	if (!options.async)
		return result;
}

function modelScrollPagination(model, options) {
	(function ($) {
		var $list = $('.' + model + '__list'),
			$container = $list.find('.sortable').length ? $list.find('.sortable') : $list,
			isLoading = false,
			listFullyLoaded = false,
			query = _.defaults(options, {
				index : 0,
				limit : 20
			}),
			originalQuery = _.clone(query),
			$loader = $('' +
				'<div class="ajaxLoadingBar text-align-center hide" style="padding-top: 20px;">' +
				'   <p class="bold">Loading entries...</p>' +
				'   <div class=" progress progress-striped progress-success active span6 offset3">' +
				'		<div class="bar" style="width: 100%;"></div>' +
				'	</div>' +
				'	<div class="clearFloat"></div>' +
				'</div>');

		$list.addClass('scrollList').after($loader);

		var self = {
			load     : function () {
				if (!isLoading) {
					isLoading = true;

					if (!listFullyLoaded) {
						$loader.stop(true, true).show();
						if (!_.isUndefined(correctBoxHeight))
							if (_.isFunction(correctBoxHeight))
								correctBoxHeight();

						if (query.where)
							query.where = query.where.replace(/\'/g, '"');

						modelAjax({
							model : model,
							async : false,
							query : query
						}, function (data) {
							isLoading = false;
							$loader.fadeOut('');
							if (data) {
								if (data.entries) {
									if (_.isFunction(options.process))
										options.process(data.entries);

									_.each(data.entries, function (entry) {
										var $entry = $(entry);
										$container.append($entry);
									});

									query.index += query.limit;

									self.finalize(data.entries);
								}
								else
									self.finalize(0);
							}
						});
					}
				}
			},
			reset    : function () {
				isLoading = false;
				listFullyLoaded = false;
				$container.html('');
				$list.find('.noMore').remove();
				query = originalQuery;
				query.index = 0;
			},
			search   : function (keywords, fields) {
				self.reset();
				query.search = keywords || '';
				self.load();
			},
			finalize : function (remainingCount) {
				if (remainingCount < query.limit && !$list.find('.noMore').length) {
					var noMore = $('<p class="text-align-center noMore">There are no more entries.</p>');
					listFullyLoaded = true;
					$list.append(noMore);
				}
			},
			filter   : function ($filter) {
				setTimeout(function () {
					debug("Scroller filter");

					modelAjax({
						async  : false,
						model  : model,
						action : 'adminFilter',
						fields : $filter.val()
					}, function (data) {
						// Because the filter has changed, to ensure any search results are captured, and the list effectively
						// resets fully, run the search submit to ensure all runs in order.
						self.reset();
						self.load();
					});
				}, 500);
			}
		};

		$(".searchBox").submit(function (e) {
			var $form = $(this),
				keywords = $form.find('input[name=q]').val();

			debug("Submitting search for " + keywords);

			self.search(keywords);

			e.preventDefault();
			return false;
		});

		$(window).scroll(function (e) {
			var footerTop = ($(window).scrollTop() + $(window).height() - 200),
				fullHeight = $("#content").height();

			if (footerTop + 400 > fullHeight) {
				self.load();
			}
		});
		self.load();

		$list.data('scroller', self);
	})(jQuery);
}

