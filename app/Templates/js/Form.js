var validCapthca = false;

jQuery(document).ready(function($) {
	$('#form-clear').click(function() {
		if (confirm("Are you sure you wish to reset your information? All data you've entered will be cleared.")) {
			$("#form-form").find('input, textarea').val('').blur();
			$("#form-form").find('select').val('').change();
			//$("#form-form").find('input, textarea, select').css('border', '1px solid #666');
			$(".table-selection").each(function() {
				$(this).find('.table-selection-placeholder').text($(this).find('.table-selection-placeholder')
						                                                  .attr('title'));
			});
		}
	});

	$('#form-submit').click(function() {
		errors = false;
		errormsg = '';
		checkField("first_name", /^.+$/i, 'Please enter a valid first name.');
		checkField("last_name", /^.+$/i, 'Please enter a valid last name.');

		if (!validatePhoneNumber($("input[name=phone]").val())) {
			errors = true;
			errormsg += "- Please enter a valid phone number and area code\n";
		}

		if (!validateEmail($("input[name=email]").val())) {
			errors = true;
			errormsg += "- Please enter a valid email\n";
		}

//
//		if (!validPostalCode($("#form-postal").val())) {
//			errors = true;
//			errormsg += "- Please enter a valid postal code\n";
//		}
//
//		if ($("#heard_about-table-selection-placeholder").text() == $("#heard_about-table-selection-placeholder")
//				.attr('title')) {
//			errors = true;
//			errormsg += "- Please let us know how you heard about the contest\n";
//		}

		if (errors) {
			alert(errormsg);
			return false;
		}
//
//		var $file = $("#file");
//		var fileExt = $file.val().split(/\\/).pop().split('.').pop().toLowerCase();
//
//		if ($file.val().length < 1) {
//			errors = true;
//			errormsg += "- Please attach your contest entry to your submission\n";
//		}
//		else if (fileExt.indexOf('jpg') == -1 && fileExt.indexOf('pdf') == -1 && fileExt
//				                                                                         .indexOf('jpeg') == -1 && fileExt
//						                                                                                                   .indexOf('png') == -1 && fileExt
//				.indexOf('gif')) {
//			errors = true;
//			errormsg += "- Please ensure you are uploading a .jpg, .png, .gif, or .pdf entry file\n";
//		}


		/*if ($("#recaptcha_response_field").val().length < 1) {
			errors = true;
			errormsg += "- Please enter the correct security code\n";

			submitForm();
		}
		else {
			if (!validCapthca) {
				$.post(RC_RECAPTCHA_AJAX, $('form').serialize(), function(data) {
					debug(data);
					if (data.success) {
						validCapthca = true;
					}
					else {
						errors = true;
						errormsg += "- Please enter the correct security code\n";
						Recaptcha.reload();
					}
					submitForm();
				}, 'json');
			}
			else
				submitForm();
		}*/

		submitForm();
		return false;
	});

	function submitForm() {
		if (errors) {
			alert(errormsg);
			return false;
		}
		else {
			//$("#form-buttons").hide().next().show();
			$("#form").submit();
		}
	}

	function checkField(fieldId, regex, msg) {
		var $field = $("input[name=" + fieldId+"]");
		if ($field.val().search(regex) < 0 || $field.val() == $field.attr('title')) {
			//$("#"+fieldId).css('border','1px solid #F00');
			errors = true;
			errormsg += "- " + msg + "\n";
			
			return false;
		}
		return true;
	}

	$("#mock-submit").click(function(){
		$("#form-buttons").hide().next().show();
	});

});

function validateEmail(email) {
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
	return emailPattern.test(email);
}

function validatePhoneNumber(elementValue) {
	var phoneNumberPattern = /^\(?(\d{1})?\)?[-\. ]?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})(\s+e?x?t?[\d]*)?$/; //([\s]+[x]{1}[\d]{1,5)?
	//var phoneNumberPattern = /^1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?$/;
	return phoneNumberPattern.test(elementValue);
}

function validPostalCode(pcode) {
	var postalPattern = /^([0-9]{5})|(\s*[a-ceghj-npr-tvxy]\d[a-ceghj-npr-tv-z](\s)?\d[a-ceghj-npr-tv-z]\d\s*)$/i;
	var postalRegExp = new RegExp(postalPattern);

	debug('postal checking');

	if (postalRegExp.test(pcode) == false)
		return false;

	debug('postal is good');
	return true;
}