<!DOCTYPE html

	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"

	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>

	<title><?=$this->page->title . ' - ' . $this->settings->client_name?></title>

	<meta name="google-site-verification" content="JVheAapuwSRlKl30H0Ooi5zFaFZXDHkK6rnFRqwsky0" />

	<meta name="keywords" content="<?=$this->page->keywords?>" />

	<meta name="description" content="<?=$this->page->description?>" />

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >

	<? /* INITIATING GLOBAL JAVASCRIPT PATH VARS */ ?>

	<? include(TEMP_PATH . '_includes.php'); ?>



	<link type="text/css" rel="stylesheet" href="<?=TEMP_CSS?>System/font-face/Chaparral/Chaparral.css" />


	<link type="text/css" rel="stylesheet" href="<?=APP_PATH?>Minify/?g=main-css" />


	<!--	<link rel="stylesheet" type="text/css" href="--><?//=TEMP_CSS?><!--Template.css"/>-->


	<script type="text/javascript">

		MENU_ID = '<?= $m = $menu_id ? $menu_id : PAGE_ID; ?>';

		linkIcons =
			[

				{filter : 'pdf'},

				{filter : 'doc'},

				{filter : 'xls', icon : 'excel'},

				{filter : 'mp3', icon : 'audio'},

				{filter : 'wav', icon : 'audio'}

			];

	</script>


	<script type="text/javascript" src="<?=APP_PATH?>Minify/?g=main-js"></script>



	<? /* LOADING SYSTEM INCLUDE FILES (js/css) NOW, IN CASE THEY'RE ACTED UPON IN GENERATED CODE */ ?>

	<?= $this->includes ?>

	<? $this->printPluginIncludes(); ?>



	<? /* RENDER SYSTEM GENERATED JS/JQUERY */ ?>



	<script type="text/javascript">


			var bPopupDefaults = {
		
					amsl: 50,
		
					appendTo: "body",
		
					closeClass: "close",
		
					content: "ajax",
		
					contentContainer: null,
		
					escClose: !0,
		
					fadeSpeed: 250,
		
					follow: [!0, !0],
		
					followSpeed: 500,
		
					loadUrl: null,
		
					modal: !0,
		
					modalClose: 0,
		
					modalColor: "#000",
		
					onClose: null,
		
					onOpen: null,
		
					opacity: 0.90,
		
					position: ["auto", "auto"],
		
					scrollBar: 0,
		
					zIndex: 99999
		
				};
		
				
		
	jQuery(document).ready(function($){
		
			if($.browser.webkit || $.browser.safari) {
		
					$("#contest-dialog div").prepend('<a href="#" class="close" style="width: 72px; height: 24px; position: absolute; top: 0; left: 804px; z-index: 999;"></a>'+
		
					'<a href="http://www.homeforholidays.ca" target="_blank" style="width: 380px; height: 72px; position: absolute; top: 303px; left: 0; z-index: 999;"></a>');
		
				}
				
				
					
			$('#homeContestVideoButton').click(function(){
				document.getElementById('bModal').style.display='none';
				document.getElementById('contest-dialog').style.display='none';
				$('#contest-video-box').RC_Dialog().open();
				setTimeout("dialogFix('contest-video-box')",1);
				setTimeout("dialogFix('contest-video-box')",200);
				setTimeout("dialogFix('contest-video-box')",500);
			});

	});
	
	
		<?=$this->js?>

		<?= $this->jquery != '' ? "jQuery(document).ready(function($){ $this->jquery });" : ""?>


		
	</script>



	<? /* RENDER SYSTEM GENERATED CSS OVERRIDES */ ?>

	<style type="text/css"><?=$this->css?></style>

	<style>
		#contest-dialog {
			display: none;	
		}
	</style>

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?=TEMP_CSS?>ie7.css" />    <![endif]-->

</head>

<body>

<a id="page-top" href="#"></a>
<div id="contest-dialog" style="width: 875px; height: 401px; margin: 0 auto;">

	<div style='margin: 0 auto;'>

		<a href="#" class="close" style="width: 72px; height: 24px; position: absolute; top: 0; left: 804px; z-index: 999;" onclick="document.getElementById('bModal').style.display='none';"></a>

		<a href="http://www.homeforholidays.ca" target="_blank" style="width: 380px; height: 72px; position: absolute; top: 303px; left: 0; z-index: 999;"></a>

		<img src="splash/popup.contest2012.jpg" width="875" height="435" border="0" usemap="#Map" style="z-index: 1;"/>

        <map name="Map" id="Map">

          <area shape="rect" coords="1,377,438,432" href="http://www.homeforholidays.ca" target="_blank"  alt="Click here to enter our contest!" />

          <area shape="rect" coords="801,1,875,25" href="#" alt="Close" class="close" onclick="document.getElementById('bModal').style.display='none';" />
          
          <area shape="rect" coords="535,383,793,427" href="#" id="homeContestVideoButton"  alt="Watch a Video of Last Year's Winner" />

        </map>

		



		<a href="http://www.homeforholidays.ca" target="_blank" style="left: -55px; position: absolute; top: 348px;  z-index: 1;"><img src="splash/bow.png" width="137" height="137"/></a>



	</div>

</div>


<div id="top-menu-dummy-div"></div>

<!-- DUMMY DIV FOR TOP MENU CONTINUATION -->

<div id="header-container">

	<div id="header">

		<? $this->renderSubsiteSelector(); ?>

		<div id="header-logo">

			<a href="<?=System_defaults::Root_web_path?>"><img src="<?=TEMP_IMGS?>header/header-logo.png" alt="Logo" /></a>

		</div>

		<div id="top-menu-container">

			<div id="top-menu" class="top-menu">

				<!--<div class='headerSocialMedia' style='float:left; margin-left:17px;'>

					<a href=''><img src='<?/*=TEMP_IMGS*/?>header/header_fb.jpg'/></a></div>

				<div class='headerSocialMedia' style='float:left;'>

					<a href=''><img src='<?/*=TEMP_IMGS*/?>header/header_t.jpg'/></a></div>

				<div class='headerSocialMedia' style='float:left;'>

					<a href=''><img src='<?/*=TEMP_IMGS*/?>header/header_in.jpg'/></a></div>

				<div class='headerSocialMedia' style='float:left; margin-right:8px;'>

					<a href=''><img src='<?/*=TEMP_IMGS*/?>header/header_v.jpg'/></a></div>-->


				<div id='social_container' style="display: block;">
					<img src="/app/Templates/images/social/social-dd.png" width="35" height="34" />
					<div class="rollover" style="">
						<a href="http://www.facebook.com/BensonKearleyIFG" target="_blank" alt="Facebook Link">
							<img src="http://bensonkearleyifg.com/app/Templates/images/social/facebook.png" alt="http://bensonkearleyifg.com/app/Templates/images/social/facebook-over.png" />

						</a>
						<br />
						<a href="https://twitter.com/#!/BKIFG" target="_blank">
							<img src="http://bensonkearleyifg.com/app/Templates/images/social/twitter.png" alt="http://bensonkearleyifg.com/app/Templates/images/social/twitter-over.png" />

						</a> <br />
						<a href="http://ca.linkedin.com/company/benson-kearley-ifg?trk=ppro_cprof" target="_blank">
							<img src="http://bensonkearleyifg.com/app/Templates/images/social/linkedin.png" alt="http://bensonkearleyifg.com/app/Templates/images/social/linkedin-over.png" />
						</a>
						<br />
						<a href="https://vimeo.com/bkifg" target="_blank" style="margin-top: -7px;">
							<img src="http://bensonkearleyifg.com/app/Templates/images/social/vimeo.png" alt="http://bensonkearleyifg.com/app/Templates/images/social/vimeo-over.png" />
							</a>
						<br />
					</div>
				</div>
				<div id='radial_container' <?//$_GET['testing'] ? 'style="display:block"' : ''?>>
					<div class="radial-menu">
						<img src="http://bensonkearleyifg.com/app/Templates/images/social/social-radial.png" /></div>
					<ul class='list'>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>

						<li class='item'>
							<div class='my_class'>test</div>
						</li>


						<li class='item'>
							<div class='my_class'>
								<a href="https://vimeo.com/bkifg" target="_blank"><img src="http://bensonkearleyifg.com/app/Templates/images/social/vimeo.png" />
								</a></div>
						</li>
						<li class='item'>
							<div class='my_class'>
								<a href="http://ca.linkedin.com/company/benson-kearley-ifg?trk=ppro_cprof" target="_blank">
									<img src="http://bensonkearleyifg.com/app/Templates/images/social/linkedin.png" /></a>
							</div>
						</li>
						<li class='item'>
							<div class='my_class'>
								<a href="https://twitter.com/#!/BKIFG" target="_blank">
									<img src="http://bensonkearleyifg.com/app/Templates/images/social/twitter.png" />
								</a></div>
						</li>
						<li class='item'>
							<div class='my_class'>
								<a href="http://www.facebook.com/BensonKearleyIFG" target="_blank"><img src="http://bensonkearleyifg.com/app/Templates/images/social/facebook.png" /></a>
							</div>
						</li>

						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>
						<li class='item'>
							<div class='my_class'>test</div>
						</li>

					</ul>
				</div>


				<div class="menu-container" id="menu-2-Container">

					<div class="menu1 menutop menu1Open" id="menu-2">

						<a href="<?=System_defaults::Root_web_path?>index.php?section=home">Home</a>

					</div>

					<div class="submenu" id="menu-2-Submenu" style="display: none;">

					</div>

				</div>

				<?= $this->renderMenu('Top Menu', '', Sites::Main) ?>

			</div>

		</div>

		<div id="menu-container">

			<div id="menu" class="main-menu">


				<?= $this->renderMenu('Main Menu', '', ACTIVE_SITE) ?>

			</div>

		</div>


	</div>

</div>

<div id="billboard-container">

	<div id="billboard">

		<div id="billboard-inside">

			<?= $this->billboard ?>

		</div>

	</div>

</div>

<div id="page-container">

	<div id="content-container" class="content-container">

		<div id="content">

			<div id="contentLeft-container" class="contentLeft-container">

				<div id="contentLeft">

					<div id='scroll-btt'><a href='#'><img src='<?=TEMP_IMGS?>back2top-mini.png' /></a></div>

					<? $this->renderContent(); ?>

				</div>

			</div>

			<div id="contentRight-container">

				<div id="contentRight" class="contentRight">

					<? $this->renderContentRight(); ?>

				</div>

			</div>

			<div class="clear-float"></div>

		</div>

	</div>

</div>

<div id="footer-container">

	<div id="footer">

		<a href="#" class="back2top"> <img src="<?=TEMP_IMGS?>/back2top.png" alt="back to top" /> </a>


		<div class="footer-copy-section">

			<img src="<?=TEMP_IMGS?>bk-logo-white-footer.png" alt="Benson Kearly logo" />

			<p>17705 Leslie St. Suite 101,<br />Newmarket, ON, L3Y 3E3</p>


			<p>Phone: 905.898.3815<br />Toll Free: 800.463.6503</p>


			<p class="footer-social" style="visibility: <?=$_GET['testing'] ? 'visible' : 'visible'?>;">

				<a href="http://www.facebook.com/BensonKearleyIFG" target="_blank">
					<img src="<?=TEMP_IMGS?>/social/footer/fb-icon.png" alt="facebook" class="soc-med-icon" />
					<img src="<?=TEMP_IMGS?>/social/footer/fb-icon-over.png" alt="facebook" class="soc-med-icon" style="display: none;" />
				</a>

				<a href="https://twitter.com/#!/BKIFG" target="_blank">
					<img src="<?=TEMP_IMGS?>/social/footer/twitter-icon.png" alt="facebook" class="soc-med-icon" />
					<img src="<?=TEMP_IMGS?>/social/footer/twitter-icon-over.png" alt="facebook" class="soc-med-icon" style="display: none;" />
				</a>
				<a href="http://ca.linkedin.com/company/benson-kearley-ifg?trk=ppro_cprof" target="_blank">
					<img src="<?=TEMP_IMGS?>/social/footer/linkedin-icon.png" alt="facebook" class="soc-med-icon" />
					<img src="<?=TEMP_IMGS?>/social/footer/linkedin-icon-over.png" alt="facebook" class="soc-med-icon" style="display: none;" />
				</a>
				<a href="https://vimeo.com/bkifg" target="_blank">
					<img src="<?=TEMP_IMGS?>/social/footer/vimeo-icon.png" alt="facebook" class="soc-med-icon" />
					<img src="<?=TEMP_IMGS?>/social/footer/vimeo-icon-over.png" alt="facebook" class="soc-med-icon" style="display: none;" />
				</a>

			</p>

			<p>&copy;<?=date("Y")?> Benson Kearley IFG<br />
                <a href="http://www.rcdesign.com" title="Financial Website Design" class="no-underline">Financial website</a> design by
				RC Design<br />

				<a href="index.php?section=privacy-policy">Privacy Policy</a>
			</p>

		</div>

		<div id="footer-menu">

			<div class="menu-container" id="menu-2-Container">

				<div class="menu1 menutop menu1Open" id="footer-menu-2">

					<a href="<?=System_defaults::Root_web_path?>index.php?section=home">Home</a>

				</div>


			</div>

			<?= $this->renderMenu('Footer Menu', '', Sites::Main) ?>

		</div>

		<div id="footer-sub-menu">

			<div class="sub-box">

				<span>Personal Insurance</span>

				<?= $this->renderMenu('Footer Menu', System_defaults::Root_web_path . 'personal/',

				Sites::Personal_insurance) ?>

			</div>

			<div class="sub-box">

				<span>Commercial Insurance</span>

				<?= $this->renderMenu('Footer Menu', System_defaults::Root_web_path . 'commercial/',

				Sites::Commercial_insurance) ?>

			</div>

			<div class="sub-box">

				<span>Financial Services</span>

				<?= $this->renderMenu('Footer Menu', System_defaults::Root_web_path . 'financial/',

				Sites::Financial_services) ?>

			</div>

			<div class="clearfix"></div>

		</div>

		<div id="footer-tag"><span>It's all about our customer</span></div>

		<div class="clear-float"></div>

	</div>

</div>



<? $this->renderGoogle(); ?>

</body>

</html>
