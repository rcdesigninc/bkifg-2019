<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<title><?=$this->settings->client_name . ' ~ ' . $this->page->title?></title>

	<? /* INITIATING GLOBAL JAVASCRIPT PATH VARS */ ?>
	<? include(TEMP_PATH . '_includes.php'); ?>

	<link type="text/css" rel="stylesheet" href="<?=APP_PATH?>Minify/?g=cms-css" />

	<script type="text/javascript">
		TINYMCE_PATH = '<?=CMS_JS?>tinyMCE/';
		$tinyMCEcss = '<?=CMS_CSS?>tinymce.css';
		CMS_AJAX_PATH = '<?=CMS_AJAX?>';

		CMS_IMGS = '<?=CMS_IMGS?>';
		CMS_AJAX = '<?=CMS_AJAX?>';
		CMS_CSS = '<?=CMS_CSS?>';
		CMS_JS = '<?=CMS_JS?>';
		CMS_DOCS = '<?=CMS_DOCS?>';

		MENU_ID = '<?= MENU_ID; ?>';
		linkIcons = new Array();
	</script>

	<script type="text/javascript" src="<?=APP_PATH?>Minify/?g=cms-js"></script>

	<?
	/*
	<link rel="stylesheet" type="text/css" href="<?=TEMP_CSS?>System.css"/>
	<link rel="stylesheet" type="text/css" href="<?=CMS_CSS?>cms.css"/>
	 */
	?>

	<? /* LOADING VITAL JS/JQUERY LIBS  */ ?>
	<?
	/*
	<script type="text/javascript" src="<?=CMS_JS?>jquery.ui.datepicker.min.js"></script>
	<script type="text/javascript" src="<?=CMS_JS?>jquery.ui.sortable.min.js"></script>

	<script type="text/javascript" src="<?=CMS_JS?>cms.js"></script>

	<script type="text/javascript" src="<?=CMS_JS?>tinyMCE/jquery.tinymce.js"></script>
	<script type="text/javascript" src="<?=CMS_JS?>tiny_mce.js"></script>

	<script type="text/javascript" src="<?=CMS_JS?>tinyMCE/plugins/ezfilemanager/js/ez_tinyMCE.js"></script>
	 */
?>

	<?
	/* LOADING SYSTEM INCLUDE FILES (js/css) NOW, IN CASE THEY'RE ACTED UPON IN GENERATED CODE */
	//print $this->includes;

	$this->printPluginIncludes();
	?>



	<? /* RENDER SYSTEM GENERATED JS/JQUERY */ ?>

	<script>
		<?=$this->js?>
		jQuery(document).ready(function($) {
			$("#menu-<?=MENU_ID?>").addClass('selected');
		<?=$this->jquery?>
		});
	</script>

	<? /* RENDER SYSTEM GENERATED CSS OVERRIDES */ ?>

	<style>
		<?=$this->css?>
	<?//
//		if (!$this->user->is_logged()) print "#menu-container { display: none; }";
//		if ($this->user->is_root()) {
//		}
//		else if ($this->user->is_admin()) {
//			print "#menu-14-Container { display: none; }";
//		}
//		?>
	</style>

	<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="<?=TEMP_CSS?>ie7.css"/><![endif]-->
</head>
<body>
<div id="header-container">
	<div id="header">
		<div id="header-logo">
			<!-- <img src="<?=TEMP_IMGS?>header/header-logo.png"/> -->
			
                    <div class="header-logo-background"></div>
		</div>
		<? if ($this->user->is_admin()) {
		print "<div id='top-menu'>";
		$s = new Site();
		$s->Load($this->managing_site);
		print "Welcome back, {$this->user->first_name}<br/>";
		print "<a href='index.php'>Managing</a>: {$s->name}<br/><br/>";
		if ($this->user->is_root()) {
			print "<a href='gen.php' target='_blank'>Re-gen Model Classes</a><br />";
		}
		if ($this->user->id) print "<a href='logout.php'>Logout</a><br />";
		print "</div>";
	}?>
		<div id="menu-container">
			<div id="menu">
				<?= $this->renderMenu(); ?>
			</div>
		</div>
	</div>
</div>

<div id="page-container">
	<div id="page">
		<div id="content-container">
			<div id="content">
				<div class="" style="height:15px;"></div>
				<? $this->renderContent(); ?>
				<div class="" style="height:10px; clear: both;"></div>
			</div>
		</div>
	</div>
</div>
</body>
</html>