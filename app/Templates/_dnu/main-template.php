<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<title><?=$this->page->title . ' ~ ' . $this->settings->client_name?></title>

	<meta name="keywords" content="<?=$this->page->keywords?>"/>
	<meta name="description" content="<?=$this->page->description?>"/>

	<? /* INITIATING GLOBAL JAVASCRIPT PATH VARS */ ?>
	<? include(TEMP_PATH . '_includes.php'); ?>

	<link type="text/css" rel="stylesheet" href="<?=APP_PATH?>Minify/?g=main-css" />

<!--	<link rel="stylesheet" type="text/css" href="--><?//=TEMP_CSS?><!--Template.css"/>-->

	<script type="text/javascript">
		MENU_ID = '<?= $m = $menu_id ? $menu_id : PAGE_ID; ?>';
		linkIcons = [
			{filter:'pdf'},
			{filter:'doc'},
			{filter:'xls',icon:'excel'},
				{filter:'mp3',icon:'audio'},
				{filter:'wav',icon:'audio'}
		];
	</script>

	<script type="text/javascript" src="<?=APP_PATH?>Minify/?g=main-js"></script>

	<? /* LOADING SYSTEM INCLUDE FILES (js/css) NOW, IN CASE THEY'RE ACTED UPON IN GENERATED CODE */ ?>
	<?= $this->includes ?>
	<?
	$this->printPluginIncludes();
?>

	<? /* RENDER SYSTEM GENERATED JS/JQUERY */ ?>

	<script type="text/javascript">
		<?=$this->js?>
		<?= $this->jquery != '' ? "jQuery(document).ready(function($){ $this->jquery });" : ""?>
	</script>

	<? /* RENDER SYSTEM GENERATED CSS OVERRIDES */ ?>
	<style type="text/css"><?=$this->css?></style>

	<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="<?=TEMP_CSS?>ie7.css"/><![endif]-->
</head>
<body>
<a id="page-top" href="#"></a>
<div id="header-container">
	<div id="header">
		<div id="header-logo">
			<a href="index.php">
                            <!--<img src="<?=TEMP_IMGS?>header/header-logo.png" alt="Logo"/>-->
                            <div class="header-logo-background"></div>
                        </a>
		</div>
		<div id="top-menu-container">
			<div id="top-menu">
				<?= $this->renderMenu('Top Menu') ?>
			</div>
		</div>
	</div>
</div>
<div id="menu-container">
	<div id="menu">
		<?= $this->renderMenu('Main Menu') ?>
	</div>
</div>
<div id="billboard-container">
	<div id="billboard">
		<?= $this->billboard ?>
	</div>
</div>
<div id="page-container">
	<div id="page">
		<div id="content-container" class="content-container">
			<div id="content">
				<div id="contentLeft-container" class="contentLeft-container">
					<div id="contentLeft">
						<?= $this->renderContent(); ?>
					</div>
				</div>
				<div id="contentRight-container">
					<div id="contentRight">
						<? $this->renderContentRight(); ?>
					</div>
				</div>
				<div class="clear-float"></div>
			</div>
		</div>
	</div>
</div>
<div id="footer-container">
	<div id="footer">
		<a href="#">
			<button id="back-to-top" class="button">Back To Top</button>
		</a>
		<div id="footer-menu">
			<?= $this->renderMenu('Footer Menu') ?>
		</div>
		<? $this->renderFooterCopyright(); ?>
		<div class="clear-float"></div>
	</div>
</div>
<? $this->renderGoogle(); ?>
</body>
</html>