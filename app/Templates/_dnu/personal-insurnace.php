<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" lang="en">
<head>
	<?
		if (PAGE_TAG =='home'){
			$this->addJquery("$('.content-section a').addClass('red-over');");
			$this->addJquery("$('#contentRight .content-section:first').addClass('neg-top-margin');");
		}
	?>
	<title><?=$this->page->title . ' ~ ' . $this->settings->client_name?></title>

	<meta name="keywords" content="<?=$this->page->keywords?>"/>
	<meta name="description" content="<?=$this->page->description?>"/>
	<? 
		$this->addJquery('
			// for showing and hideing the video
			$(".play-video").click(function(){

				$("#white-screen").fadeTo("slow", 0.75);
				$(".close-vid").fadeTo("slow", 1);
				$("#video-player-container").fadeTo("slow", 1);
			});

			$("#white-screen, .close-vid").click(function(){

				$("#white-screen").hide();
				$(".close-vid").hide();
				$("#video-player-container").hide();
			});

			// THIS IS FOR THOSE TWO SMALLER BOXES IN THE CONTENT LEFT SECTION
			$(".float-box:first").addClass("floatbox-margin");

			// Adds the different first paragraph
			$(".content-section p:first").addClass("intro");

		');

//		if (PAGE_TAG == 'personal_home'){
			$this->addJquery('
				$(".float-box:eq(2)").after("<div class=\"clearfix\"></div>");
				$(".float-box:eq(3)").removeClass("float-box");

			');
//		}
	?>
	<? /* INITIATING GLOBAL JAVASCRIPT PATH VARS */ ?>
	<? include(TEMP_PATH . '_includes.php'); ?>

    <link type="text/css" rel="stylesheet" href="<?=TEMP_CSS?>System/font-face/Chaparral/Chaparral.css" />

	<link type="text/css" rel="stylesheet" href="<?=APP_PATH?>Minify/?g=main-css" />
	<link type="text/css" rel="stylesheet" href="<?=TEMP_CSS?>Homepage/Homepage.css" />

<!--	<link rel="stylesheet" type="text/css" href="--><?//=TEMP_CSS?><!--Template.css"/>-->

	<script type="text/javascript">
		MENU_ID = '<?= $m = $menu_id ? $menu_id : PAGE_ID; ?>';
		linkIcons = [
			{filter:'pdf'},
			{filter:'doc'},
			{filter:'xls',icon:'excel'},
				{filter:'mp3',icon:'audio'},
				{filter:'wav',icon:'audio'}
		];
	</script>

	<script type="text/javascript" src="<?=APP_PATH?>Minify/?g=main-js"></script>

	<? /* LOADING SYSTEM INCLUDE FILES (js/css) NOW, IN CASE THEY'RE ACTED UPON IN GENERATED CODE */ ?>
	<?= $this->includes ?>
	<?
	$this->printPluginIncludes();
?>

	<? /* RENDER SYSTEM GENERATED JS/JQUERY */ ?>

	<script type="text/javascript">
		<?=$this->js?>
		<?= $this->jquery != '' ? "jQuery(document).ready(function($){ $this->jquery });" : ""?>
	</script>

	<? /* RENDER SYSTEM GENERATED CSS OVERRIDES */ ?>
	<style type="text/css"><?=$this->css?></style>

	<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="<?=TEMP_CSS?>ie7.css"/><![endif]-->
</head>
<body id="<?= PAGE_TAG?>">
<a id="page-top" href="#"></a>
<div id="top-menu-dummy-div"></div><!-- DUMMY DIV FOR TOP MENU CONTINUATION -->

<!-- HIDDEN VIDEO PLAYER -->
<div id="white-screen"><!-- BLANK SCREEN OVERLAY --></div>
<div id="video-player-container">
    <div id="video-player">
        <iframe src="https://player.vimeo.com/video/30830412" width="714" height="402" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
    </div>
    <a href ="#" class="close-vid"></a>
</div>
<!-- END VIDEO PLAYER -->
<div id="header-container">

	<div id="header">
    <div id="insurance-mini-menu"> <img src="<?=TEMP_IMGS?>menu-down-arrow.png" alt="See All" /></div>
    	<div id="header-logo">
			<a href="index.php">
                            <!--<img src="<?=TEMP_IMGS?>header/header-logo.png" alt="Logo"/>-->
                            <div class="header-logo-background"></div>
                        </a>
		</div>
		<div id="top-menu-container">
			<div id="top-menu" class="top-menu">
				<?= $this->renderMenu('Top Menu','../',Sites::Main) ?>
			</div>
		</div>
        <div id="menu-container">
            <div id="menu" class="main-menu">
                <?= $this->renderMenu('Main Menu','',Sites::Personal_insurance) ?>
            </div>
        </div>

    </div>
</div>
<div id="billboard-container">
	<div id="billboard">
        <div id="billboard-inside">
    		<?= $this->billboard ?>
	    </div>
    </div>
</div>
<div id="page-container" style="position: relative; z-index: 9999;">
		<div id="content-container" class="content-container">
			<div id="content">
                <div id="insurances-container">
                	<div><a href=""><img src="<?=TEMP_IMGS?>/home/personal-box3.png" onmouseout="this.src='<?=TEMP_IMGS?>/home/personal-box3.png';" onmouseover="this.src='<?=TEMP_IMGS?>/home/personal-box3-over.png';"/></a></div>
                    <div><a href=""><img src="<?=TEMP_IMGS?>/home/personal-box2.png" onmouseout="this.src='<?=TEMP_IMGS?>/home/personal-box2.png';" onmouseover="this.src='<?=TEMP_IMGS?>/home/personal-box2-over.png';"/></a></div>
                    <div><a href=""><img src="<?=TEMP_IMGS?>/home/personal-box1.png" onmouseout="this.src='<?=TEMP_IMGS?>/home/personal-box1.png';" onmouseover="this.src='<?=TEMP_IMGS?>/home/personal-box1-over.png';"/></a></div>
                </div>
                <div class="push"></div>
				<div id="contentLeft-container" class="contentLeft-container">
					<div id="contentLeft">
						<?
						$first = array_shift($this->sections);
						/** @var $first Site_page_sections */
						print $first->getContent();

						$i = 0;
						$numberOfSections = count($this->sections);
							if ($numberOfSections % 2 == 0){

								foreach($this->sections as $s) { /** @var $s Site_page_sections */

									print $s->getContent('','float-box');

									$i++;
									if($i % 2 == 0)
									print "<div class='clear-float'></div>";
								}

							}
							else{
								foreach($this->sections as $s) { /** @var $s Site_page_sections */
									$i++;
									if($i == $numberOfSections)
									{
										print $s->getContent();
									}
									else{
										print $s->getContent('','float-box');
										if($i % 2 == 0){
											print "<div class='clear-float'></div>";
										}
									}
								}

							}

					?>
					</div>
				</div>
				<div id="contentRight-container" style="margin-top:-20px;">
					<div id="contentRight">
						<? $this->renderContentRight(); ?>
					</div>
                </div>
				<div class="clear-float"></div>
			</div>
		</div>
	</div>
    <div class="clear-float"></div>
</div>
<div id="footer-container">
	<div id="footer">
		<a href="#" class="back2top">
			<img src="<?=TEMP_IMGS?>/back2top.png" alt="back to top" />
		</a>
        <div class="footer-copy-section">
<!--        	<img src="<?=TEMP_IMGS?>/bk-logo-white-footer.png" alt"Benson/Kearly logo" />-->
            <div class="footer-logo-background"></div>
            <p>17705 Leslie St. Suite 101,<br />Newmarket, ON, L3Y 3E3</p>
            <p>Phone: 905.898.3815<br />Toll Free: 800.463.6503</p>
            <p>
            	<img src="<?=TEMP_IMGS?>/fb-icon.png" alt="facebook" class="soc-med-icon"/><img src="<?=TEMP_IMGS?>/twitter-icon.png" alt="twitter" class="soc-med-icon" /><img src="<?=TEMP_IMGS?>/linkedin-icon.png" alt="linkedin" class="soc-med-icon"/><img src="<?=TEMP_IMGS?>/vimeo-icon.png" alt="vimeo" class="soc-med-icon"/>
            </p>
            <p>&copy;2011 Benson Kearley IFG<br /> Site designed and developed by <a href="https://www.rcdesign.com" target="_blank" class="rc-link">RC Design</a></p>
        </div>
		<div id="footer-menu">
			<?= $this->renderMenu('Footer Menu','../',Sites::Main) ?>
		</div>
        <div id="footer-sub-menu">
        	<ul class="footer-sub">
            	<li><span>Personal Insurance</span></li>
                <li><a href="index.php?section=home_insurance">Home Insurance</a></li>
                <li><a href="index.php?section=auto_insurance">Auto Insurance</a></li>
                <li><a href="index.php?section=personal_insurance">Personal Insurance</a></li>
                <!--<li><a href="">Resources</a></li>-->
            </ul>
            <ul class="footer-sub">
            	<li><span>Commercial Insurance</span></li>
                <li><a href="index.php?section=commercial_insurance">Commercial Insurance</a></li>
                <li><a href="index.php?section=business_solutions">Business Solutions</a></li>
                <li><a href="index.php?section=claims_management">Claims Management</a></li>
<!--                <li><a href="">Staffing</a></li>
                <li><a href="">Resources</a></li>-->
            </ul>
            <ul class="footer-sub-last">
            	<li><span>Financial Services</span></li>
                <li><a href="index.php?section=financial_services">Financial Services</a></li>
                <li><a href="index.php?section=banking_services">Banking Services</a></li>
<!--                <li><a href="">Resources</a></li>-->
            </ul>
            <div class="clearfix"></div>
        </div>
		<div id="footer-tag"><span>It's all about our customer</span></div>
		<div class="clear-float"></div>
	</div>
</div>
<? $this->renderGoogle(); ?>
</body>
</html>