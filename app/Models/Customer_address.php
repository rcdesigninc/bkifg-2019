<?

class_exists('tbl_customer_address') || require_once(MODEL_PATH . '_base/' . 'tbl_customer_address' . '.php');

class Customer_address extends tbl_customer_address {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Options', Model_field_type::Hidden); // dbtype: int(10) unsigned
		$this->_funcSetField('customer_id', 'Customer_id', Model_field_type::Hidden); // dbtype: int(10) unsigned
		$this->_funcSetField('type_id', 'Type', Model_field_type::Select,
			'table="customer_address_type" show="label"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(60)
		$this->_funcSetField('first_name', 'First_name', Model_field_type::Input); // dbtype: varchar(30)
		$this->_funcSetField('last_name', 'Last_name', Model_field_type::Input); // dbtype: varchar(30)
		$this->_funcSetField('phone', 'Phone', Model_field_type::Phone); // dbtype: varchar(15)
		$this->_funcSetField('street', 'Street', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('street2', 'Street2', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('city', 'City', Model_field_type::Input); // dbtype: varchar(25)
		$this->_funcSetField('province', 'Province', Model_field_type::Select,
			'table="customer_province" show="name"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('province_other', 'Other Province',
			Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('postal', 'Postal', Model_field_type::Input); // dbtype: varchar(10)
		$this->_funcSetField('country', 'Country', Model_field_type::Select,
			'table="customer_country" show="name"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('date_added', 'date added', Model_field_type::Hidden); // dbtype: varchar(15)
		$this->_funcSetField('date_modified', 'date_modified', Model_field_type::Hidden); // dbtype: varchar(15)

		System_core::$instance->addJquery("
		// If other is selected, show else hide.
		$('select[name=province]').live('change',function(){
			$(this).parent().next().toggleClass('hidden',!($(this).find(':selected').text() == 'Other'));
		});");
	}

	public function Insert($data = null) {
		if ($data)
			$data['date_added'] = getTimestamp();
		$this->date_added = getTimestamp();
		$result = parent::Insert($data);

		Customer::addLog("Customer Address Created", $result, $this->customer_id);
	}

	public function Update($data = null) {
		if ($data)
			$data['date_modified'] = getTimestamp();
		$this->date_modified = getTimestamp();
		$result = parent::Update($data);

		Customer::addLog("Customer Address Updated", $result, $this->customer_id);
	}

	public function Delete($id = null) {
		$result = parent::Delete($id);

		Customer::addLog("Customer Address Deleted", $result, $this->customer_id);
	}

	public static function renderDefaultInputs() {
		$o = new self();
		$html = '';
		$html .= $o->getInput('street', Model_field_render::Inside_label);
		$html .= $o->getInput('street2', Model_field_render::Inside_label);
		$html .= $o->getInput('city', Model_field_render::Inside_label);
		$html .= $o->getInput('province', Model_field_render::Inside_label);
		$html .= $o->getInput('province_other', Model_field_render::Inside_label);
		$html .= $o->getInput('postal', Model_field_render::Inside_label);
		$html .= $o->getInput('country', Model_field_render::Inside_label);
		return $html;
	}
}

/*
 * EOF
 */