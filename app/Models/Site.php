<?

class_exists('tbl_site') || require_once(MODEL_PATH . '_base/' . 'tbl_site' . '.php');

class Site extends tbl_site {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('url', 'Url', Model_field_type::Url); // dbtype: varchar(50)
		$this->_funcSetField('template_id', 'Default Page Template', Model_field_type::Select,
			'table="site_template" show="name"'); // dbtype: tinyint(4) unsigned
	}
}

?>