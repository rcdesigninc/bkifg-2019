<?

class_exists('tbl_notification_from') || require_once(MODEL_PATH . '_base/' . 'tbl_notification_from' . '.php');

class Notification_from extends tbl_notification_from {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('email', 'Email', Model_field_type::Input); // dbtype: varchar(100)
	}
}

/*
 * EOF
 */