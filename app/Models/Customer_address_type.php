<?

class_exists('tbl_customer_address_type') || require_once(MODEL_PATH . '_base/' . 'tbl_customer_address_type' . '.php');

class Customer_address_type extends tbl_customer_address_type {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(25)
	}
}

/*
 * EOF
 */