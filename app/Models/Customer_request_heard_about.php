<?

class_exists('tbl_customer_request_heard_about') || require_once(MODEL_PATH.'_base/'.'tbl_customer_request_heard_about'.'.php');

class Customer_request_heard_about extends tbl_customer_request_heard_about {
	protected function _funcInit() {
			$this->_funcSetField('id', 'Options', Model_field_type::Hidden);// dbtype: tinyint(3) unsigned
			$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(50)
	}
}

/*
 * EOF
 */