<?

class_exists('tbl_customer_log') || require_once(MODEL_PATH . '_base/' . 'tbl_customer_log' . '.php');

class Customer_log extends tbl_customer_log {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Options', Model_field_type::Hidden); // dbtype: int(10) unsigned
		$this->_funcSetField('customer_id', 'Customer_id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('ip', 'Ip', Model_field_type::Input); // dbtype: varchar(20)
		$this->_funcSetField('success', 'Success', Model_field_type::Input); // dbtype: tinyint(1)
		$this->_funcSetField('action', 'Action', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('uri', 'Iri', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('date', 'Date', Model_field_type::Input); // dbtype: timestamp
	}

	public function Insert($action, $success, $customer_id) {
		$this->customer_id = $customer_id;
		$this->success = $success;
		$this->action = $action;

		$this->uri = $_SERVER['REQUEST_URI'];
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->date = getTimestamp();

		parent::Insert();
	}

	public static function Access($override = false) {

		if (!$_SESSION['access_tracked'] && !$override) {
			$_SESSION['access_tracked'] = true;

			$log = new self();

			$user_agent = 'Site Accessed by User Agent: ' . $_SERVER['HTTP_USER_AGENT'];

			$customer_id = System::$instance->customer->id;

			$log->Insert($user_agent, true, $customer_id);
		}
	}

	public static function Error($error, $customer_id = 0) {
		$log = new self();

		if (!$customer_id)
			$customer_id = System::$instance->customer->id;

		$error = str_replace("'", "", $error);

		$log->Insert($error, false, $customer_id);
	}
}

/*
 * EOF
 */