<?

class_exists('tbl_blog_comment') || require_once(MODEL_PATH . '_base/' . 'tbl_blog_comment' . '.php');

class Blog_comment extends tbl_blog_comment {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Options', Model_field_type::Hidden); // dbtype: int(10) unsigned
		$this->_funcSetField('post_id', 'Posting', Model_field_type::Select,
			'table="blog_post" show="title"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('email', 'Email', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('url', 'Url', Model_field_type::Url); // dbtype: varchar(200)
		$this->_funcSetField('comment', 'Comment', Model_field_type::Text); // dbtype: varchar(2000)
		$this->_funcSetField('security_code', 'Security_code', Model_field_type::Hidden); // dbtype: varchar(6)
		$this->_funcSetField('date', 'Date', Model_field_type::Hidden); // dbtype: timestamp
		$this->_funcSetField('is_deleted', 'Delete Entry', Model_field_type::Bool); // dbtype: int(1) unsigned
	}

	public function getContent() {
		$link = $this->name;
		$date = date("M j, Y, g:i a", $this->date);
		$content = strip_tags($this->comment);
		if ($this->url && $this->url != 'Website')
			$link = "<a href='{$this->url}' target='_blank'>$this->name</a>";

		$str = "
		<div class='comment-posting'>
			<h4>$link</h4>
			<h2>$date</h2>
			<br />
			<p>$content</p>
		</div>";
		return $str;
	}

	public static function renderComments($post_id) {
		$ps = new Site_page_sections();

		$c = new self();
		$comments = $c->Select("*", "`post_id` = '$post_id'", 'date DESC');

		if (count($comments)) {

			$content = '';
			if (count($comments)) {
				foreach ($comments as $comm) {
					$content .= $comm->getContent();
				}
			}
			else
				$content = "There are presently no comments, be the first to add one now.";

			$ps->heading = "Comments";
			$ps->content = "
		<div class='comment-block'>
			$content
		</div>";
		}

		return $ps->getContent();
	}

	public static function renderCommentForm() {
		$ps = new Site_page_sections();

		$now = getTimestamp();

		$ps->heading = "Add A Comment:";

		$content = '';
		$content .= "
		<div class='comment-form'>
			<form id='frmComments' name='frmComments' method='post'>
				<input type='hidden' name='date' value='{$now}'/>
				<input type='hidden' name='post_id' value='{$_SESSION[post_id]}'/>
				<input type='text' id='name' name='name' value='Name*' title='Name*' class='input-entry'/>
				<input type='text' id='email' name='email' value='E-mail*' title='E-mail*' class='input-entry'/>
				<input type='text' id='url' name='url' value='Website' title='Website' class='input-entry'/><br clear='all'/>
				<textarea id='comment' name='comment' title='Comment*&nbsp;&nbsp;' class='input-entry'>Comment*&nbsp;&nbsp;</textarea><br clear='all'/>";

		/*
						 * Do security
						 <input name='security_code' id='comment_security_code' class='input-entry' type='text' value='Security Code*' title='Security Code*'/>
						$_SESSION['comment_security_code'] = substr(rand(1000000, 9999999), 0, 6);
						$security_code_items = str_split($_SESSION['comment_security_code']);
						echo('<input type='hidden' id='comment_hidSecurityCode' name='hidSecurityCode' value='' . $_SESSION['comment_security_code'] . '' />');
						echo '<div style='float:left;'><img src=\'' . COMM_IMGS . 'validation/left.jpg\' alt=\'Security Code\'/>';
						foreach ($security_code_items as $item) echo '<img src=\'' . COMM_IMGS . 'validation/' . md5($item) . '.jpg\' alt=\'Security Code\'/>';
						echo '<img src=\'' . COMM_IMGS . 'validation/right.jpg\' alt=\'Security Code\'/></div>';*/
		$content .= "
				<div class='clear-float'></div>
		
				<span class='siteDefaultColor'>* Fields are required.</span> <br/> <br/>
		
				<div>
					<input type='image' src='images/button_clear.gif' width='137' height='35' id='comment-clear' style='float:left; margin-right: 10px;'/>
					<input type='image' src='images/button_submit.gif' width='137' height='35' id='comment-submit' style=''/>
				</div>
				<div class='clear-float'></div>
			</form>
		</div>
		";

		$ps->content = $content;

		return $ps->getContent();
	}
}

?>