<?

class_exists('tbl_site_log') || require_once(MODEL_PATH . '_base/' . 'tbl_site_log' . '.php');

class Site_log extends tbl_site_log {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('user_id', 'User_id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('ip', 'Ip', Model_field_type::Input); // dbtype: varchar(20)
		$this->_funcSetField('success', 'Success', Model_field_type::Input); // dbtype: tinyint(1)
		$this->_funcSetField('action', 'Action', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('uri', 'Uri', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('date', 'Date', Model_field_type::Input); // dbtype: int(11) unsigned
	}

	public function Insert($action, $success, $user_id) {
		$this->user_id = $user_id;
		$this->success = $success;
		$this->action = $action;

		$this->uri = $_SERVER['REQUEST_URI'];
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->date = getTimestamp();

		parent::Insert();
	}
}

/*
 * EOF
 */