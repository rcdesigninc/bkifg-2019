<?

class_exists('tbl_notification_vars') || require_once(MODEL_PATH . '_base/' . 'tbl_notification_vars' . '.php');

class Notification_vars extends tbl_notification_vars {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('var', 'Var', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('configureable', 'Configureable', Model_field_type::Input); // dbtype: tinyint(1)
	}
}

/*
 * EOF
 */