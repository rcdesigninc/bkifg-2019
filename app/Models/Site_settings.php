<?

class_exists('tbl_site_settings') || require_once(MODEL_PATH . '_base/' . 'tbl_site_settings' . '.php');

class Site_settings extends tbl_site_settings {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('site_id', 'Site_id', Model_field_type::Select,
			'table="site" show="name"'); // dbtype: tinyint(4) unsigned
		$this->_funcSetField('client_name', 'Client_name', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('google_analytics', 'Google_analytics', Model_field_type::Input); // dbtype: varchar(20)
		$this->_funcSetField('under_maintanace', 'Under_maintanace', Model_field_type::Bool); // dbtype: tinyint(1)
	}
}

?>