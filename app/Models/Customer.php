<?

class_exists('tbl_customer') || require_once(MODEL_PATH . '_base/' . 'tbl_customer' . '.php');

class Customer extends tbl_customer {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Hidden); // dbtype: int(10) unsigned
		$this->_funcSetField('username', 'Username', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('password', 'Password', Model_field_type::Password); // dbtype: varchar(20)
		$this->_funcSetField('first_name', 'First Name', Model_field_type::Input); // dbtype: varchar(60)
		$this->_funcSetField('last_name', 'Last Name', Model_field_type::Input); // dbtype: varchar(30)
		$this->_funcSetField('email', 'Email', Model_field_type::Email); // dbtype: varchar(100)
		$this->_funcSetField('phone', 'Phone', Model_field_type::Phone); // dbtype: varchar(15)
		$this->_funcSetField('fax', 'Fax', Model_field_type::Phone); // dbtype: varchar(15)
		$this->_funcSetField('date_joined', 'date joined', Model_field_type::Hidden); // dbtype: varchar(15)
		$this->_funcSetField('date_modified', 'date_modified', Model_field_type::Hidden); // dbtype: varchar(15)
	}

	public function Login() {
		$o = $this->Select("id",
				"`username` = \"" . mysql_real_escape_string($_POST['username']) . "\" AND `password` = \"" . mysql_real_escape_string($_POST['password']) . "\"",
			'', "", false);

		// Log user account, regardless of a valid password in the case this is a failed attempt at a valid account
		$u = $this->Select("id",
				"`username` = \"" . mysql_real_escape_string($_POST['username']) . "\"",
			'', "", false);

		self::addLog("Customer Account Login Attempt", $u->id, $u->id);

		$this->checkAccessAttempt();
		
		if ($o->id)
			$this->setSession($o->id);
	}

	private function checkAccessAttempt() {
		// Check and see if this user has just recently been accessing the site, to associate their that access log to the new account
		LoadModel('Customer_log');
		$log = new Customer_log();
		$log->LoadWhere('action LIKE "%Site Accessed%" AND date > '.strtotime('-30 minutes'));
		if($log->id) {
			$log->customer_id = $this->id;
			$log->Update();
		}
	}

	public function setSession($id) {
		parent::setSession($id);

		// todo: expand to auto-load related classes
		/*
		LoadModel('Customer_address');*/
	}

	public function Logout() {
		session_destroy();
		header('location: index.php');
	}

	public function Insert($data = null) {
		if ($data)
			$data['date_joined'] = getTimestamp();
		$this->date_joined = getTimestamp();
		$result = parent::Insert($data);

		System::$instance->customer->Load($this->id);

		$this->checkAccessAttempt();

		self::addLog("Customer Account Created", $result, $this->id);

		return $result;
	}

	public function Update($data = null) {
		if ($data)
			$data['date_modified'] = getTimestamp();
		$this->date_modified = getTimestamp();
		$result = parent::Update($data);

		self::addLog("Customer Account Updated", $result, $this->id);

		return $result;
	}

	public function Delete($id = null) {
		$result = parent::Delete($id);

		self::addLog("Customer Account Deleted", $result, $this->id);
	}

	public static function renderDefaultInputs() {
		$o = new self();

		System::$instance->addJquery(<<< EOB
		$("input[name=first_name]").attr('tabindex',1);
		$("input[name=last_name]").attr('tabindex',2);
		$("input[name=email]").attr('tabindex',3);
		$("input[name=phone]").attr('tabindex',4);
		$("textarea").attr('tabindex',5);
EOB
);

		$html = '';
		$html .= "<div class='float-box-left'>";
		$html .= $o->getInput('first_name', Model_field_render::Inside_label);
		$html .= $o->getInput('email', Model_field_render::Inside_label);
		$html .= "</div>";
		$html .= "<div class='float-box-right'>";
		$html .= $o->getInput('last_name', Model_field_render::Inside_label);
		$html .= $o->getInput('phone', Model_field_render::Inside_label);
		$html .= "</div>";
		return $html;
	}


	public static function addLog($action, $result, $customer_id = 0) {
		LoadModel('Customer_log');
		$log = new Customer_log();
		if (!$customer_id)
			$customer_id = System::$instance->customer->id;
		$log->Insert($action, ($result ? true : false), $customer_id);
	}
}

/*
 * EOF
 */