<?

class_exists('tbl_notification_to') || require_once(MODEL_PATH . '_base/' . 'tbl_notification_to' . '.php');

class Notification_to extends tbl_notification_to {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(25)
	}
}

/*
 * EOF
 */