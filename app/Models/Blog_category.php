<?

class_exists('tbl_blog_category') || require_once(MODEL_PATH . '_base/' . 'tbl_blog_category' . '.php');

class Blog_category extends tbl_blog_category {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('tag', 'tag', Model_field_type::Hidden); // dbtype: varchar(50)
	}

	public static function renderCategories($heading = 'Categories') {
		$o = new self();
		$o = $o->Select("*", "id IN (SELECT DISTINCT category_id FROM tbl_blog_post_category)", "label");

		$ps = new Site_page_sections();
		$ps->heading = $heading;

		$content = '';
		foreach ($o as $cat) {
			/** @var Blog_category $cat */
			if (hyphonate($cat->label) != $_GET['cat']) {
				$content .= '<p>' . $cat->renderLink() . '</p>';
			}
		}
		$ps->content = $content;

		return $ps;
	}

	public static function getJson() {
		$o = new self();
		$o = $o->Select("*", "", "label");

		$json =
				array
				();
		foreach ($o as $cat)
			$json[] =
					array
					("key" => $cat->label,
					 "value" => $cat->id);

		return $json;
	}

	public function getLink() {
		$url = System_defaults::Uri . Site_page::getTagById($_SESSION['page_id']) . "&cat=" . hyphonate($this->label);
		return $url;
	}

	public function renderLink() {
		return "<a href=\"" . $this->getLink() . "\" class=''>$this->label</a>";
	}
}

?>