<?



class_exists('tbl_customer_request') || require_once(MODEL_PATH . '_base/' . 'tbl_customer_request' . '.php');



class Customer_request extends tbl_customer_request {

	protected function _funcInit() {

		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: int(10) unsigned

		$this->_funcSetField('customer_id', 'Customer_id', Model_field_type::Input); // dbtype: int(10) unsigned

		$this->_funcSetField('date', 'Date', Model_field_type::Input); // dbtype: int(11)

		$this->_funcSetField('notify_by', 'I would like to be notified by...?', Model_field_type::Dropdown,

		                     'Customer_request_notify_by'); // dbtype: tinyint(3) unsigned

		$this->_funcSetField('heard_about', 'This message is regarding...?', Model_field_type::Dropdown,

		                     'Customer_request_heard_about'); // dbtype: tinyint(3) unsigned

		$this->_funcSetField('message', 'Comment/Message/Concern', Model_field_type::Textarea); // dbtype: text

	}



	public function Insert() {

		LoadModel('Customer');

		LoadModel('Customer_address');



		$c = new Customer();

		$ca = new Customer_address();



		$id = $c->Insert($_POST);

		if ($id) {

			$_POST['customer_id'] = $id;

			$ca->Insert($_POST);



			$this->customer_id = $id;

			$this->date = getTimestamp();

			$this->message = scrubDbData($_POST['message']);

			parent::Insert();



			LoadModel('Customer_request_notify_by');

			LoadModel('Customer_request_heard_about');

			$notify = new Customer_request_notify_by();

			$heard = new Customer_request_heard_about();



			$notify->Load($this->notify_by);

			$heard->Load($this->heard_about);



			$send_to = 'info@bensonkearleyifg.com';

			if($_POST['send-to'])

				$send_to = $_POST['send-to'];



			$email_name = $c->first_name . ' ' . $c->last_name;

			$email_from = $c->email;

			$email_subject = "A message from www.bensonkearleyifg.com";

			$email_content = nl2br(<<< EOB

<b>First Name:</b> $c->first_name

<b>Last Name:</b> $c->last_name

<b>Email:</b> $c->email

<b>Phone:</b> $c->phone



<b>Notify By:</b> $_POST[notify_by]

<b>Heard about:</b> $_POST[heard_about]





EOB

);



			$email_content .= "<b>Comments:</b><br/>".nl2br($_POST['message']);



			//SendMail('shomer@rcdesign.com',$email_name, $email_from, $email_subject, "<b>ATTN</b>: $send_to<br/>".$email_content);

			SendMail($send_to,$email_name, $email_from, $email_subject, $email_content);

			//SendMail('cdano@rcdesign.com',$email_name, $email_from, $email_subject, $email_content);



			Customer::addLog("Customer Requesting Contact", true, $id);



			return true;

		}

		else {

			Customer::addLog("Customer Requesting Contact Failed: " . print_array($_POST), false, 0);

			System::Error("There was an error trying to submit your request. Please try again.");

		}



		return false;

	}



	public function renderForm() {

		LoadModel('Customer');

		LoadModel('Customer_address');



		$html = "<form id='form' action='" . System_defaults::Uri . "{$_GET[section]}' method='post'>";

		$html .= '<input type="hidden" name="send-to">';

		$html .= Customer::renderDefaultInputs();

		//$html .= Customer_address::renderDefaultInputs();

		$html .= self::renderDefaultInputs();



		//$r = new Recaptcha();

		$html .= "<div class='float-box-left'>";

//		$html .= $r->render();

		$html .= "</div>";

		$html .= "<div class='float-box-right'>";

		$html .= "<div class='float-right'>";

		$html .= "<input id='form-clear' type='reset' class='button' value='Clear'/> <input id='form-submit' type='submit' class='button' value='Submit'/>";

		$html .= "<div class='please-note'>Please note: all fields are required.</div></div>";

		$html .= "</div>";

		$html .= "<div class='clear-float'></div>";

		$html .= "</form>";



		return $html;

	}



	public static function renderDefaultInputs() {

		$o = new self();

		$html = '';

		$html .= "<div class='float-box-left'>";

		$html .= $o->getInput('notify_by', Model_field_render::Inside_label);

		$html .= "</div>";

		$html .= "<div class='float-box-right'>";

		$html .= $o->getInput('heard_about', Model_field_render::Inside_label);

		$html .= "</div>";

		$html .= $o->getInput('message', Model_field_render::Inside_label);

		return $html;

	}

}



/*

 * EOF

 */