<?

class_exists('tbl_site_menu') || require_once(MODEL_PATH . '_base/' . 'tbl_site_menu' . '.php');

class Site_menu extends tbl_site_menu {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('parent_id', 'Parent Menu', Model_field_type::Select,
				'table="site_menu" show="label" filter="`site_id` = ' . System_core::$instance->managing_site . '"  data-table="site" data-show="name"'); // dbtype: tinyint(4)
		$this->_funcSetField('site_id', 'Site_id', Model_field_type::Select,
			'table="site" show="name"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('managed_on', 'Managed_on', Model_field_type::Select,
			'table="site" show="name"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('page_id', 'Page', Model_field_type::Select,
				'table="site_page" show="title" filter="`site_id` = ' . System_core::$instance->managing_site . '"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('url', 'Url', Model_field_type::Url); // dbtype: varchar(200)
		$this->_funcSetField('width', 'Width', Model_field_type::Input); // dbtype: mediumint(8) unsigned
		$this->_funcSetField('order_by', 'Order_by', Model_field_type::Input); // dbtype: tinyint(4) unsigned
	}

	function scrubFields() {
		if (System_core::$instance->user->is_root()) {
		}
		else if (System_core::$instance->user->is_admin()) {
			$this->_funcSetField('parent_id', 'Parent Menu', Model_field_type::Select,
					'table="site_menu" show="label" filter="`site_id` = ' . System_core::$instance->managing_site . ' AND `parent_id` = 0"  data-table="site" data-show="name"'); // dbtype: tinyint(4)
			$this->_funcSetField('site_id', 'Site_id', Model_field_type::Hidden); // dbtype: tinyint(4) unsigned
			$this->_funcSetField('managed_on', 'managed_on', Model_field_type::Hidden); // dbtype: varchar(100)
			$this->_funcSetField('url', 'url', Model_field_type::Hidden); // dbtype: varchar(50)
			$this->_funcSetField('width', 'width', Model_field_type::Hidden); // dbtype: varchar(50)
			$this->site_id = System_core::$instance->managing_site;
			$this->managed_on = System_core::$instance->managing_site;
		}
	}

	public function getLink() {
		$link = $this->url;
		if ($link == '') {
			$tag = Site_page::getTagById($this->page_id);
			$link = System_defaults::Uri . $tag;
		}
		return $link;
	}

	public function getClassId() {
		$mid = $this->page_id;
		if (!$mid)
			$mid = $this->id;
		return $mid;
	}

	final public static function getIdByLabel($label, $site_id = '') {
		$m = new self();
		if($site_id)
			$site_id = "AND site_id = $site_id";
		$m->LoadWhere("`label` = '$label' $site_id");
		return $m->id;
	}

	final public static function getIdByLabelActive($label) {
		$m = new self();
		$m->LoadWhere("`label` = '$label'  AND `site_id` = " . ACTIVE_SITE);
		return $m->id;
	}
}

?>