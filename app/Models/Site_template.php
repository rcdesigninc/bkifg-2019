<?

class_exists('tbl_site_template') || require_once(MODEL_PATH . '_base/' . 'tbl_site_template' . '.php');

class Site_template extends tbl_site_template {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Options', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('path', 'Path', Model_field_type::Input); // dbtype: varchar(200)
	}
}

/*
 * EOF
 */