<?

class_exists('tbl_notification_log') || require_once(MODEL_PATH . '_base/' . 'tbl_notification_log' . '.php');

class Notification_log extends tbl_notification_log {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('notification_id', 'Notification_id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('customer_id', 'Customer_id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('sent_from', 'Sent_from', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('sent_to', 'Sent_to', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('date', 'Date', Model_field_type::Input); // dbtype: timestamp
	}
}

/*
 * EOF
 */