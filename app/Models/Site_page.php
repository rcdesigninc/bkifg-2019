<?

class_exists('tbl_site_page') || require_once(MODEL_PATH . '_base/' . 'tbl_site_page' . '.php');

class Site_page extends tbl_site_page {
	private $image_dir;

	protected function _funcInit() {
		$this->_funcSetField('id', 'Options', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('site_id', 'Site Id', Model_field_type::Select,
			'table="site" show="name"'); // dbtype: tinyint(4) unsigned
		$this->_funcSetField('template_id', 'Template Id', Model_field_type::Select,
			'table="site_template" show="name"'); // dbtype: tinyint(4) unsigned
		$this->_funcSetField('tag', 'Tag', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('title', 'Title', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('script', 'Script', Model_field_type::Hidden); // dbtype: varchar(2000)
		$this->_funcSetField('css', 'Css', Model_field_type::Hidden); // dbtype: varchar(2000)
		$this->_funcSetField('description', 'Description', Model_field_type::Input); // dbtype: varchar(2000)
		$this->_funcSetField('keywords', 'Keywords', Model_field_type::Input); // dbtype: varchar(2000)

		$this->image_dir = MEDIA_PATH . "images/page_headings/";
	}

	function scrubFields() {
		if (System_core::$instance->user->is_root()) {
			if(!$this->template_id) {
				$s = new Site();
				$s->Load(CMS_System::$instance->managing_site);
				$this->template_id = $s->template_id;
			}
		}
		else if (System_core::$instance->user->is_admin()) {
			$this->_funcSetField('site_id', 'Site_id', Model_field_type::Hidden); // dbtype: tinyint(4) unsigned
			$this->_funcSetField('template_id', 'Template_id', Model_field_type::Hidden); // dbtype: tinyint(4) unsigned
			$this->_funcSetField('tag', 'Tag', Model_field_type::Hidden); // dbtype: varchar(100)
		}
	}

	function insertTag() {
		if (System_core::$instance->user->is_root()) {
		}
		if (System_core::$instance->user->is_admin()) {
			$this->_funcSetField('tag', 'Tag', Model_field_type::Input); // dbtype: varchar(100)
		}
	}

	/**
	 * @param int $id
	 * @return string
	 * When only a pages id is known, it's tag can be retrieved from here, useful for
	 * menu interactions.
	 */
	final public static function getTagById($id) {
		$p = new self();
		$p->LoadWhere("`id` = {$id}");
		return $p->tag;
	}

	final public static function getIdByTag($tag, $managed = ACTIVE_SITE) {
		$p = new self();
		$p->LoadWhere("`tag` = '$tag' AND `site_id` = '$managed'");
		return $p->id;
	}
}

?>