<?

class_exists('tbl_customer_country') || require_once(MODEL_PATH . '_base/' . 'tbl_customer_country' . '.php');

class Customer_country extends tbl_customer_country {
	protected function _funcInit() {
		$this->_humanName = 'Country';

		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(25)
		$this->_funcSetField('iso', 'Iso', Model_field_type::Input); // dbtype: varchar(3)
		$this->_funcSetField('iata', 'Iata', Model_field_type::Input); // dbtype: varchar(3)
	}
}

/*
 * EOF
 */