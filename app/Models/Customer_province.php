<?

class_exists('tbl_customer_province') || require_once(MODEL_PATH . '_base/' . 'tbl_customer_province' . '.php');

class Customer_province extends tbl_customer_province {
	protected function _funcInit() {
		$this->_humanName = 'Province';

		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(25)
	}
}

/*
 * EOF
 */