<?

class_exists('tbl_locations') || require_once(MODEL_PATH.'_base/'.'tbl_locations'.'.php');

class Locations extends tbl_locations {
	protected function _funcInit() {
			$this->_funcSetField('id', 'Options', Model_field_type::Hidden);// dbtype: int(11)
			$this->_funcSetField('address', 'Address', Model_field_type::Input); // dbtype: varchar(255)
			$this->_funcSetField('tagline', 'Tagline', Model_field_type::Input); // dbtype: varchar(255)
	}
}

/*
 * EOF
 */