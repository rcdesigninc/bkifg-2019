<?

class_exists('tbl_blog_post_category') || require_once(MODEL_PATH . '_base/' . 'tbl_blog_post_category' . '.php');

class Blog_post_category extends tbl_blog_post_category {
	protected function _funcInit() {
		$this->_funcSetField('post_id', 'Post_id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('category_id', 'Category_id', Model_field_type::Input); // dbtype: int(10) unsigned
	}

	public static function renderPostCategories($post_id) {
		$result = '';
		if ($post_id) {
			LoadModel('Blog_category');
			/** @var Blog_category $c  */
			$c = new Blog_category();
			$o = new self();

			$cats = $o->Select("category_id", "`post_id` = '$post_id'");
			$catLinks =
					array
					();
			foreach ($cats as $x) {
				/** @var Blog_post_category $x  */
				$c->Load($x->category_id);
				$catLinks[] = $c->renderLink();
			}
			$result = implode(", ", $catLinks);
		}
		return $result;
	}

	public static function getDefaultsJson($post_id) {
		$result = '';
		if ($post_id) {
			LoadModel('Blog_category');
			$o = new self();

			$json =
					array
					();

			$cats = $o->Select("category_id", "`post_id` = '$post_id'");
			foreach ($cats as $cat) {
				/** @var Blog_post_category $cat  */
				$c = new Blog_category();
				$c->Load($cat->category_id);
				/*
			 $content .= "
			 $(\"select[name=categories]\").trigger(\"addItem\",[{'title': '$c->label', 'value': '$c->id'}]);\n";
			 */
				$json[] =
						array
						('title' => $c->label,
						 'value' => $c->id);
			}

			$result = json_encode($json);
		}
		return $result;
	}
}

?>