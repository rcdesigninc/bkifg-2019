<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_menu extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $parent_id */
	//public $parent_id;
	/** @var Model_field $site_id */
	//public $site_id;
	/** @var Model_field $managed_on */
	//public $managed_on;
	/** @var Model_field $label */
	//public $label;
	/** @var Model_field $page_id */
	//public $page_id;
	/** @var Model_field $url */
	//public $url;
	/** @var Model_field $width */
	//public $width;
	/** @var Model_field $order_by */
	//public $order_by;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_menu');
		$this->_funcAddField('id','mediumint(8) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('parent_id','mediumint(9) unsigned','NO','','0','');
		$this->_funcAddField('site_id','tinyint(3) unsigned','NO','','1','');
		$this->_funcAddField('managed_on','tinyint(3)','NO','','0','');
		$this->_funcAddField('label','varchar(50)','NO','','','');
		$this->_funcAddField('page_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('url','varchar(200)','NO','','','');
		$this->_funcAddField('width','mediumint(8) unsigned','NO','','100','');
		$this->_funcAddField('order_by','tinyint(4)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */