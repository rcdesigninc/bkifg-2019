<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_blog_post_category extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $post_id */
	//public $post_id;
	/** @var Model_field $category_id */
	//public $category_id;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_blog_post_category');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('post_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('category_id','int(10) unsigned','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */