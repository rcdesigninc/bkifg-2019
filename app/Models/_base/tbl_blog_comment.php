<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_blog_comment extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $post_id */
	//public $post_id;
	/** @var Model_field $name */
	//public $name;
	/** @var Model_field $email */
	//public $email;
	/** @var Model_field $url */
	//public $url;
	/** @var Model_field $comment */
	//public $comment;
	/** @var Model_field $security_code */
	//public $security_code;
	/** @var Model_field $date */
	//public $date;
	/** @var Model_field $is_deleted */
	//public $is_deleted;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_blog_comment');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('post_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('name','varchar(50)','NO','','','');
		$this->_funcAddField('email','varchar(100)','NO','','','');
		$this->_funcAddField('url','varchar(200)','NO','','','');
		$this->_funcAddField('comment','text','NO','','','');
		$this->_funcAddField('security_code','varchar(6)','NO','','','');
		$this->_funcAddField('date','int(11)','YES','','','');
		$this->_funcAddField('is_deleted','int(1) unsigned','NO','','0','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */