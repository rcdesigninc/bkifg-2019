<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_province extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $name */
	//public $name;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_province');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('name','varchar(25)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */