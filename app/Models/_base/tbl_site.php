<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $name */
	//public $name;
	/** @var Model_field $url */
	//public $url;
	/** @var Model_field $template_id */
	//public $template_id;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('name','varchar(50)','NO','','','');
		$this->_funcAddField('url','varchar(50)','NO','','','');
		$this->_funcAddField('template_id','tinyint(3) unsigned','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */