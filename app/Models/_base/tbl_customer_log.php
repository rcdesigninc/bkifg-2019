<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_log extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $customer_id */
	//public $customer_id;
	/** @var Model_field $ip */
	//public $ip;
	/** @var Model_field $success */
	//public $success;
	/** @var Model_field $action */
	//public $action;
	/** @var Model_field $uri */
	//public $uri;
	/** @var Model_field $date */
	//public $date;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_log');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('customer_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('ip','varchar(20)','NO','','','');
		$this->_funcAddField('success','tinyint(1)','NO','','','');
		$this->_funcAddField('action','varchar(500)','YES','','','');
		$this->_funcAddField('uri','varchar(500)','NO','','','');
		$this->_funcAddField('date','int(11) unsigned','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */