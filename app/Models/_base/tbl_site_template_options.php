<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_template_options extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $template_id */
	//public $template_id;
	/** @var Model_field $template_section_id */
	//public $template_section_id;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_template_options');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('template_id','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('template_section_id','tinyint(3) unsigned','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */