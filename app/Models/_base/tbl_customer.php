<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $username */
	//public $username;
	/** @var Model_field $password */
	//public $password;
	/** @var Model_field $first_name */
	//public $first_name;
	/** @var Model_field $last_name */
	//public $last_name;
	/** @var Model_field $email */
	//public $email;
	/** @var Model_field $phone */
	//public $phone;
	/** @var Model_field $fax */
	//public $fax;
	/** @var Model_field $date_joined */
	//public $date_joined;
	/** @var Model_field $date_modified */
	//public $date_modified;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('username','varchar(30)','NO','','','');
		$this->_funcAddField('password','varchar(30)','NO','','','');
		$this->_funcAddField('first_name','varchar(60)','NO','','','');
		$this->_funcAddField('last_name','varchar(30)','YES','','','');
		$this->_funcAddField('email','varchar(100)','NO','','','');
		$this->_funcAddField('phone','varchar(20)','YES','','','');
		$this->_funcAddField('fax','varchar(20)','NO','','','');
		$this->_funcAddField('date_joined','int(11) unsigned','NO','','','');
		$this->_funcAddField('date_modified','int(11) unsigned','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */