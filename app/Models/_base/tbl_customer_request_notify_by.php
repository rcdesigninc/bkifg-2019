<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_request_notify_by extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $label */
	//public $label;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_request_notify_by');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('label','varchar(30)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */