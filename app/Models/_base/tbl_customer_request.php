<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_request extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $customer_id */
	//public $customer_id;
	/** @var Model_field $date */
	//public $date;
	/** @var Model_field $notify_by */
	//public $notify_by;
	/** @var Model_field $heard_about */
	//public $heard_about;
	/** @var Model_field $message */
	//public $message;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_request');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('customer_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('date','int(11)','NO','','','');
		$this->_funcAddField('notify_by','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('heard_about','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('message','text','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */