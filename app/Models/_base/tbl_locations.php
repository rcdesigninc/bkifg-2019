<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_locations extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $address */
	//public $address;
	/** @var Model_field $tagline */
	//public $tagline;
	/** @var Model_field $full_address */
	//public $full_address;
	/** @var Model_field $url */
	//public $url;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_locations');
		$this->_funcAddField('id','int(11)','NO','PRI','','auto_increment');
		$this->_funcAddField('address','varchar(255)','NO','','','');
		$this->_funcAddField('tagline','varchar(255)','NO','','','');
		$this->_funcAddField('full_address','varchar(255)','NO','','','');
		$this->_funcAddField('url','varchar(255)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */