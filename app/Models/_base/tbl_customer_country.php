<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_country extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $name */
	//public $name;
	/** @var Model_field $iso */
	//public $iso;
	/** @var Model_field $iata */
	//public $iata;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_country');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('name','varchar(25)','NO','','','');
		$this->_funcAddField('iso','varchar(3)','NO','','','');
		$this->_funcAddField('iata','varchar(3)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */