<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_user extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $site_id */
	//public $site_id;
	/** @var Model_field $email */
	//public $email;
	/** @var Model_field $username */
	//public $username;
	/** @var Model_field $password */
	//public $password;
	/** @var Model_field $first_name */
	//public $first_name;
	/** @var Model_field $last_name */
	//public $last_name;
	/** @var Model_field $site_permission_id */
	//public $site_permission_id;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_user');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('site_id','tinyint(4) unsigned','NO','','','');
		$this->_funcAddField('email','varchar(100)','NO','','','');
		$this->_funcAddField('username','varchar(100)','NO','','','');
		$this->_funcAddField('password','varchar(50)','NO','','','');
		$this->_funcAddField('first_name','varchar(25)','NO','','','');
		$this->_funcAddField('last_name','varchar(25)','NO','','','');
		$this->_funcAddField('site_permission_id','tinyint(4)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */