<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_address extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $customer_id */
	//public $customer_id;
	/** @var Model_field $type_id */
	//public $type_id;
	/** @var Model_field $label */
	//public $label;
	/** @var Model_field $first_name */
	//public $first_name;
	/** @var Model_field $last_name */
	//public $last_name;
	/** @var Model_field $phone */
	//public $phone;
	/** @var Model_field $street */
	//public $street;
	/** @var Model_field $street2 */
	//public $street2;
	/** @var Model_field $city */
	//public $city;
	/** @var Model_field $province */
	//public $province;
	/** @var Model_field $province_other */
	//public $province_other;
	/** @var Model_field $postal */
	//public $postal;
	/** @var Model_field $country */
	//public $country;
	/** @var Model_field $date_added */
	//public $date_added;
	/** @var Model_field $date_modified */
	//public $date_modified;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_address');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('customer_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('type_id','tinyint(3) unsigned','NO','','3','');
		$this->_funcAddField('label','varchar(60)','NO','','Home','');
		$this->_funcAddField('first_name','varchar(30)','NO','','','');
		$this->_funcAddField('last_name','varchar(30)','NO','','','');
		$this->_funcAddField('phone','varchar(20)','NO','','','');
		$this->_funcAddField('street','varchar(100)','NO','','','');
		$this->_funcAddField('street2','varchar(100)','YES','','','');
		$this->_funcAddField('city','varchar(25)','NO','','','');
		$this->_funcAddField('province','tinyint(3) unsigned','YES','','','');
		$this->_funcAddField('province_other','varchar(30)','YES','','','');
		$this->_funcAddField('postal','varchar(10)','NO','','','');
		$this->_funcAddField('country','tinyint(3) unsigned','YES','','1','');
		$this->_funcAddField('date_added','int(11) unsigned','NO','','','');
		$this->_funcAddField('date_modified','int(11) unsigned','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */