<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_template_sections extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $parent_id */
	//public $parent_id;
	/** @var Model_field $label */
	//public $label;
	/** @var Model_field $tag_id */
	//public $tag_id;
	/** @var Model_field $container_tag_id */
	//public $container_tag_id;
	/** @var Model_field $is_horizontal */
	//public $is_horizontal;
	/** @var Model_field $is_configurable */
	//public $is_configurable;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_template_sections');
		$this->_funcAddField('id','tinyint(4) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('parent_id','tinyint(4) unsigned','NO','','','');
		$this->_funcAddField('label','varchar(100)','NO','','','');
		$this->_funcAddField('tag_id','varchar(50)','NO','','','');
		$this->_funcAddField('container_tag_id','varchar(50)','NO','','','');
		$this->_funcAddField('is_horizontal','tinyint(1)','YES','','0','');
		$this->_funcAddField('is_configurable','tinyint(1)','NO','','0','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */