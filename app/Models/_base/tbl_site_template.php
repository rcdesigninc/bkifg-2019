<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_template extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $name */
	//public $name;
	/** @var Model_field $path */
	//public $path;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_template');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('name','varchar(200)','NO','','','');
		$this->_funcAddField('path','varchar(500)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */