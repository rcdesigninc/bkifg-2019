<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_settings extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $site_id */
	//public $site_id;
	/** @var Model_field $client_name */
	//public $client_name;
	/** @var Model_field $google_analytics */
	//public $google_analytics;
	/** @var Model_field $under_maintanace */
	//public $under_maintanace;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_settings');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('site_id','tinyint(4) unsigned','NO','','','');
		$this->_funcAddField('client_name','varchar(100)','NO','','','');
		$this->_funcAddField('google_analytics','varchar(20)','YES','','','');
		$this->_funcAddField('under_maintanace','tinyint(1)','YES','','0','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */