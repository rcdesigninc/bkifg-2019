<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_notification_log extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $notification_id */
	//public $notification_id;
	/** @var Model_field $customer_id */
	//public $customer_id;
	/** @var Model_field $sent_from */
	//public $sent_from;
	/** @var Model_field $sent_to */
	//public $sent_to;
	/** @var Model_field $date */
	//public $date;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_notification_log');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('notification_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('customer_id','int(10) unsigned','NO','','','');
		$this->_funcAddField('sent_from','tinyint(3) unsigned','NO','','1','');
		$this->_funcAddField('sent_to','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('date','timestamp','NO','','CURRENT_TIMESTAMP','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */