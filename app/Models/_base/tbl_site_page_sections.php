<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_page_sections extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $page_id */
	//public $page_id;
	/** @var Model_field $template_section_id */
	//public $template_section_id;
	/** @var Model_field $heading */
	//public $heading;
	/** @var Model_field $billboard_button */
	//public $billboard_button;
	/** @var Model_field $billboard_url */
	//public $billboard_url;
	/** @var Model_field $billboard_color_id */
	//public $billboard_color_id;
	/** @var Model_field $heading_image */
	//public $heading_image;
	/** @var Model_field $content */
	//public $content;
	/** @var Model_field $order_by */
	//public $order_by;
	/** @var Model_field $date */
	//public $date;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_page_sections');
		$this->_funcAddField('id','smallint(5) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('page_id','tinyint(3) unsigned','NO','','1','');
		$this->_funcAddField('template_section_id','tinyint(3) unsigned','NO','','9','');
		$this->_funcAddField('heading','varchar(200)','NO','','','');
		$this->_funcAddField('billboard_button','varchar(100)','YES','','','');
		$this->_funcAddField('billboard_url','varchar(200)','YES','','','');
		$this->_funcAddField('billboard_color_id','tinyint(3) unsigned','YES','','','');
		$this->_funcAddField('heading_image','varchar(200)','YES','','','');
		$this->_funcAddField('content','text','NO','','','');
		$this->_funcAddField('order_by','tinyint(3) unsigned','YES','','1','');
		$this->_funcAddField('date','date','YES','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */