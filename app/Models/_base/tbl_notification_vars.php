<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_notification_vars extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $label */
	//public $label;
	/** @var Model_field $var */
	//public $var;
	/** @var Model_field $configureable */
	//public $configureable;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_notification_vars');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('label','varchar(100)','NO','','','');
		$this->_funcAddField('var','varchar(100)','NO','','','');
		$this->_funcAddField('configureable','tinyint(1)','NO','','0','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */