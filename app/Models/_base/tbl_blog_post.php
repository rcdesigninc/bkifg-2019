<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_blog_post extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $tag */
	//public $tag;
	/** @var Model_field $title */
	//public $title;
	/** @var Model_field $author_id */
	//public $author_id;
	/** @var Model_field $intro_content */
	//public $intro_content;
	/** @var Model_field $content */
	//public $content;
	/** @var Model_field $date_created */
	//public $date_created;
	/** @var Model_field $date_modified */
	//public $date_modified;
	/** @var Model_field $date_published */
	//public $date_published;
	/** @var Model_field $is_active */
	//public $is_active;
	/** @var Model_field $categories */
	//public $categories;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_blog_post');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('tag','varchar(500)','NO','','','');
		$this->_funcAddField('title','varchar(500)','NO','','','');
		$this->_funcAddField('author_id','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('intro_content','varchar(2000)','NO','','','');
		$this->_funcAddField('content','text','NO','','','');
		$this->_funcAddField('date_created','int(11)','YES','','','');
		$this->_funcAddField('date_modified','int(11)','YES','','','');
		$this->_funcAddField('date_published','int(11)','YES','','','');
		$this->_funcAddField('is_active','int(1) unsigned','YES','','0','');
		$this->_funcAddField('categories','tinyint(1)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */