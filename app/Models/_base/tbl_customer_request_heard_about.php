<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_customer_request_heard_about extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $label */
	//public $label;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_customer_request_heard_about');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('label','varchar(50)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */