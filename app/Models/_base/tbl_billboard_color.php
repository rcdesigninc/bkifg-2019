<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_billboard_color extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $name */
	//public $name;
	/** @var Model_field $hex */
	//public $hex;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_billboard_color');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('name','varchar(25)','NO','','','');
		$this->_funcAddField('hex','varchar(6)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */