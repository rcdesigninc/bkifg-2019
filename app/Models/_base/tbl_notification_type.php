<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_notification_type extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $label */
	//public $label;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_notification_type');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('label','varchar(35)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */