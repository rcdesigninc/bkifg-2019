<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_notification_from extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $name */
	//public $name;
	/** @var Model_field $email */
	//public $email;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_notification_from');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('name','varchar(100)','NO','','','');
		$this->_funcAddField('email','varchar(100)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */