<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_blog_category extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $label */
	//public $label;
	/** @var Model_field $tag */
	//public $tag;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_blog_category');
		$this->_funcAddField('id','int(10) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('label','varchar(50)','NO','','','');
		$this->_funcAddField('tag','varchar(50)','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */