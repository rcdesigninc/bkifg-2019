<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_notification extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $type_id */
	//public $type_id;
	/** @var Model_field $days_until */
	//public $days_until;
	/** @var Model_field $sent_from */
	//public $sent_from;
	/** @var Model_field $sent_to */
	//public $sent_to;
	/** @var Model_field $change_renewal */
	//public $change_renewal;
	/** @var Model_field $change_level */
	//public $change_level;
	/** @var Model_field $change_status */
	//public $change_status;
	/** @var Model_field $archive */
	//public $archive;
	/** @var Model_field $subject */
	//public $subject;
	/** @var Model_field $content */
	//public $content;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_notification');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('type_id','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('days_until','mediumint(8) unsigned','NO','','','');
		$this->_funcAddField('sent_from','tinyint(3) unsigned','NO','','1','');
		$this->_funcAddField('sent_to','tinyint(3) unsigned','NO','','4','');
		$this->_funcAddField('change_renewal','tinyint(1)','NO','','0','');
		$this->_funcAddField('change_level','tinyint(3) unsigned','NO','','0','');
		$this->_funcAddField('change_status','tinyint(3) unsigned','NO','','0','');
		$this->_funcAddField('archive','tinyint(1)','NO','','0','');
		$this->_funcAddField('subject','varchar(500)','NO','','','');
		$this->_funcAddField('content','text','NO','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */