<?

require_once(SYS_LIB . '/' .'Model.php');

class tbl_site_page extends Model
{
	/** @var Model_field $id */
	//public $id;
	/** @var Model_field $site_id */
	//public $site_id;
	/** @var Model_field $template_id */
	//public $template_id;
	/** @var Model_field $tag */
	//public $tag;
	/** @var Model_field $title */
	//public $title;
	/** @var Model_field $description */
	//public $description;
	/** @var Model_field $keywords */
	//public $keywords;

	public function __construct($arrData = NULL) {
		parent::__construct('tbl_site_page');
		$this->_funcAddField('id','tinyint(3) unsigned','NO','PRI','','auto_increment');
		$this->_funcAddField('site_id','tinyint(4) unsigned','NO','','1','');
		$this->_funcAddField('template_id','tinyint(3) unsigned','NO','','','');
		$this->_funcAddField('tag','varchar(100)','NO','','','');
		$this->_funcAddField('title','varchar(100)','NO','','','');
		$this->_funcAddField('description','varchar(2000)','YES','','','');
		$this->_funcAddField('keywords','varchar(2000)','YES','','','');


		$this->_funcPopulateFields($arrData);
		
		$this->_funcInit();
	}
}

/*
 * EOF
 */