<?

class_exists('Site_page_sections') || require_once(MODEL_PATH . '' . 'Site_page_sections' . '.php');

class News_and_events extends Site_page_sections {
	protected function _funcInit() {
		parent::_funcInit();

		$this->_class_prefix = "news";

		// Readjust field properties
		$this->_funcSetField('page_id', 'Page', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('template_section_id', 'Section', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('date', 'Date', Model_field_type::Date); // dbtype: tinyint(3) unsigned

		// Enforce the proper section and page is selected
		$this->template_section_id = System_columns::Content_left;
		$this->page_id = $_SESSION['page_id'];
	}

	public static function renderRecentEntries($heading = 'Recent Entries', $limit = 2, $order_by = 'date, heading') {
		$o = new self();

		if ($limit)
			$order_by .= " LIMIT 0, $limit";

		$entries = $o->Select("*",
				"`page_id` = '" . Site_page::getIdByTag('news-and-events') . "' AND `template_section_id` IN(" . System_columns::Content_left . ")",
			$order_by);

		$ps = new Site_page_sections();
		$ps->heading = $heading;

		$content = '';
		foreach ($entries as $entry) {
			/** @var News_and_events $entry */
			$content .= $entry->renderLink();
		}
		$ps->content = $content;

		return $ps;
	}
}

/*
 * EOF
 */