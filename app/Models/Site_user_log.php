<?

class_exists('tbl_site_user_log') || require_once(MODEL_PATH . '_base/' . 'tbl_site_user_log' . '.php');

class Site_user_log extends tbl_site_user_log {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('user_id', 'User_id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('ip', 'Ip', Model_field_type::Input); // dbtype: varchar(20)
		$this->_funcSetField('success', 'Success', Model_field_type::Input); // dbtype: tinyint(1)
		$this->_funcSetField('action', 'Action', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('uri', 'Uri', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('date', 'Date', Model_field_type::Input); // dbtype: int(11) unsigned
	}

	public function Insert($action, $success, $user_id) {
		$this->user_id = $user_id;
		$this->success = $success;
		$this->action = $action;

		$this->uri = $_SERVER['REQUEST_URI'];
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->date = getTimestamp();

		parent::Insert();
	}

	public static function Access($override = false) {

		if (!$_SESSION['access_tracked'] && !$override) {
			$_SESSION['access_tracked'] = true;

			$log = new self();

			$user_agent = 'Site Accessed by User Agent: ' . $_SERVER['HTTP_USER_AGENT'];

			$user_id = System_core::$instance->user->id;

			$log->Insert($user_agent, true, $user_id);
		}
	}

	public static function Error($error, $user_id = 0) {
		$log = new self();

		if (!$user_id)
			$user_id = System_core::$instance->user->id;

		$error = str_replace("'", "", $error);

		$log->Insert($error, false, $user_id);
	}
}

/*
 * EOF
 */