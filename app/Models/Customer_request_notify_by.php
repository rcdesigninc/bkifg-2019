<?

class_exists('tbl_customer_request_notify_by') || require_once(MODEL_PATH.'_base/'.'tbl_customer_request_notify_by'.'.php');

class Customer_request_notify_by extends tbl_customer_request_notify_by {
	protected function _funcInit() {
			$this->_funcSetField('id', 'Options', Model_field_type::Hidden);// dbtype: tinyint(3) unsigned
			$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(30)
	}
}

/*
 * EOF
 */