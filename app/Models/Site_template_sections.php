<?

class_exists('tbl_site_template_sections') || require_once(MODEL_PATH . '_base/' . 'tbl_site_template_sections' . '.php');

class Site_template_sections extends tbl_site_template_sections {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Hidden); // dbtype: tinyint(4) unsigned
		$this->_funcSetField('parent_id', 'Parent_id', Model_field_type::Select,
			'table="site_template_sections" show="label"'); // dbtype: tinyint(4) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('tag_id', 'Tag_id', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('container_tag_id', 'Container_tag_id', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('is_horizontal', 'Is_horizontal', Model_field_type::Bool); // dbtype: tinyint(1)
		$this->_funcSetField('is_configurable', 'Is_configurable', Model_field_type::Bool); // dbtype: tinyint(1)
	}
}

?>