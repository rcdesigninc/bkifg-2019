<?

class_exists('tbl_site_page_sections') || require_once(MODEL_PATH . '_base/' . 'tbl_site_page_sections' . '.php');

//LoadApiClass('Pagination');

interface iSitePageSections {
	public function getContent($content = NULL, $section_classes = '');
}

class Site_page_sections extends tbl_site_page_sections implements iSitePageSections, iPagination {
	/*protected $_paginate_column = Columns::Content_left;*/

	protected $image_dir;

	protected function _funcInit() {
		$this->_class_prefix = "section";

		$this->_funcSetField('id', 'Id', Model_field_type::Hidden); // dbtype: smallint(5) unsigned
		$this->_funcSetField('page_id', 'Page', Model_field_type::Select,
		                     'table="site_page" show="title" filter="`site_id` = ' . System_core::$instance->managing_site . '"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('template_section_id', 'Section', Model_field_type::Select,
		                     'table="site_template_sections" show="label"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('date', 'Date', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('heading', 'Heading', Model_field_type::Input); // dbtype: varchar(50)
		$this->_funcSetField('billboard_button', 'Billboard Button', Model_field_type::Input); // dbtype: text
		$this->_funcSetField('billboard_url', 'Billboard Url', Model_field_type::Input); // dbtype: text
		$this->_funcSetField('billboard_color_id', 'Billboard Color', Model_field_type::Select,
		                     'table="billboard_color" show="name"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('heading_image', 'Image', Model_field_type::File); // dbtype: varchar(50)
		$this->_funcSetField('content', 'Content', Model_field_type::Text); // dbtype: text

		$this->_funcSetField('order_by', 'Order By', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned

		$this->image_dir = $this->_uploadDir . "site_page_sections/";
	}

	/**********************************************************************************************
	 * INPUT AND ENTRY ADJUSTMENT
	 * To hide and show appropriate fields when user and or admins are interacting with this model
	 ***********************************************************************************************/
	public function scrubFields() {

		if (!(($_SESSION['page_id'] == 2 || $_SESSION['page_id'] == 3 || $_SESSION['page_id'] == 33 || $_SESSION['page_id'] == 4) && $this->template_section_id == 3)) {
			$this->_funcSetField('billboard_button', 'Billboard Button', Model_field_type::Hidden); // dbtype: text
			$this->_funcSetField('billboard_url', 'Billboard Url', Model_field_type::Hidden); // dbtype: text
			$this->_funcSetField('billboard_color_id', 'Billboard Color', Model_field_type::Hidden,
			                     'table="billboard_color" show="name"'); // dbtype: tinyint(3) unsigned
		}

		if (!$_GET['id']) {
			$this->page_id = $_SESSION['page_id'];
			$this->template_section_id = System_columns::Content_left;
		}

		if ($this->template_section_id == 3) {
			$this->_funcSetField('content', 'Billboard Content', Model_field_type::Text); // dbtype: text
		}
		//else{
		//            $this->_funcSetField('billboard_button', 'Billboard Button', Model_field_type::Hidden); // dbtype: text
		//        }

		if ($_SESSION['page_id'] == 7) {
			$this->template_section_id = 0;
			$this->_funcSetField('template_section_id', 'Section', Model_field_type::Select,
			                     'table="site_template_sections" show="label" filter="id NOT IN(' . System_columns::Content_left . ')"'); // dbtype: tinyint(3) unsigned
		}

		if (System_core::$instance->user->is_root()) {
		}
		else if (System_core::$instance->user->is_admin()) {
			$this->_funcSetField('page_id', 'Page', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
			$this->page_id = $this->page_id ? $this->page_id : $_SESSION['page_id'];

			$this->_funcSetField('template_section_id', 'Section', Model_field_type::Select,
			                     "table='site_template_sections' show='label' filter='`id` IN (3, 9,10)' p_override='true'"); // dbtype: tinyint(3) unsigned

			/*$this->_funcSetField('template_section_id', 'Section', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
			$this->template_section_id = $this->template_section_id ? $this->template_section_id : Columns::Content_left;*/
		}
	}

	/**********************************************************************************************
	 * DB INTERACTION OVERRIDES
	 ***********************************************************************************************/
	public function Insert() {
		$this->page_id = $_POST['page_id'];
		$this->template_section_id = $_POST['template_section_id'];
		$this->heading = $_POST['heading'];
		$this->content = urldecode($_POST['content']);
		$this->billboard_button = $_POST['billboard_button'];
		$this->billboard_url = $_POST['billboard_url'];
		$this->billboard_color_id = $_POST['billboard_color_id'];

		$o = new self();
		$order_by = 1;
		$order_list = $o->Select("id",
		                         "`page_id` = '$this->page_id' AND `template_section_id` = '$this->template_section_id'");
		if ($order_list)
			$order_by += count($order_list);
		$this->order_by = $order_by;

		//die(print_array($this));
		parent::Insert();
		//die(System_core::$DB->lastQuery().print_array($this));

		return $this->updateImage();
	}

	public function Update() {
		// Ensure this isn't be caught by the sorttable ajax!!
		if (!(isset($_POST['table']) || isset($_POST['orderBy']) || $_POST['rowId'])) {
			$this->page_id = $_POST['page_id'];
			$this->template_section_id = $_POST['template_section_id'];
			$this->heading = $_POST['heading'];
			$this->content = urldecode($_POST['content']);
			$this->date = $_POST['date'];
			$this->billboard_button = $_POST['billboard_button'];
			$this->billboard_url = $_POST['billboard_url'];
			$this->billboard_color_id = $_POST['billboard_color_id'];
		}

		parent::Update();
		//die(System_core::$DB->lastQuery().print_array($this));

		return $this->updateImage();
	}

	public function updateImage() {
		if (isset($_FILES['heading_image'])) { // image was entered
			try {
				if (is_uploaded_file($_FILES['heading_image']['tmp_name'])) {
					if ($_FILES['heading_image']['size'] < $this->maxsize) {
						$this->heading_image = "heading" . $this->id . substr($_FILES['heading_image']['name'],
						                                                      strrpos($_FILES['heading_image']['name'],
						                                                              "."));
					}
					else {
						// if the file is not less than the maximum allowed, print an error
						echo
								'<div>File exceeds the Maximum File limit</div>
						  <div>Maximum File limit is ' . $this->maxsize . '</div>
						  <div>File ' . $_FILES['heading_image']['name'] . ' is ' . $_FILES['heading_image']['size'] . ' bytes</div>
						  <hr />';
					}
				}
				parent::Update();

				if (!is_dir($this->image_dir))
					mkdir($this->image_dir);

				//printf("<pre>%s</pre>",print_r($this->,true));
				if ($this->Upload($_FILES["heading_image"], $this->image_dir, $this->heading_image)) {
					//$this->ResizeImage($this->image_dir.$this->heading_image, 432,288);
				}
			} catch (Exception $e) {
				Site_user::addLog("Upload Page Section Image failed: " . $e->getMessage(), false);
				return false;
			}
		}

		if (isset($_FILES['billboard_button'])) { // image was entered
			try {
				if (is_uploaded_file($_FILES['billboard_button']['tmp_name'])) {
					if ($_FILES['billboard_button']['size'] < $this->maxsize) {
						$this->billboard_button = "btn" . $this->id . substr($_FILES['billboard_button']['name'],
						                                                     strrpos($_FILES['billboard_button']['name'],
						                                                             "."));
					}
					else {
						// if the file is not less than the maximum allowed, print an error
						echo
								'<div>File exceeds the Maximum File limit</div>
						  <div>Maximum File limit is ' . $this->maxsize . '</div>
						  <div>File ' . $_FILES['billboard_button']['name'] . ' is ' . $_FILES['billboard_button']['size'] . ' bytes</div>
						  <hr />';
					}
				}
				parent::Update();

				if (!is_dir($this->image_dir))
					mkdir($this->image_dir);

				//printf("<pre>%s</pre>",print_r($this->,true));
				if ($this->Upload($_FILES["billboard_button"], $this->image_dir, $this->billboard_button)) {
					//$this->ResizeImage($this->image_dir.$this->heading_image, 432,288);
				}
			} catch (Exception $e) {
				Site_user::addLog("Upload Billboard Button Image failed: " . $e->getMessage(), false);
				return false;
			}
		}
		return true;
	}

	/**********************************************************************************************
	 * OUTPUT AND RENDING OF DATA
	 ***********************************************************************************************/

	/**
	 * @param null $content
	 * @param string $section_classes
	 * @return string
	 */
	public function getContent($content = NULL, $section_classes = '') {
		//<a id='{$this->id}-{$this->_class_prefix}' class='float-right'>&nbsp;</a>
		$str = "

		<div class='content-section $section_classes'>";
		$str .= "
			<h2 class='content-heading'>$this->heading</h2>";
		$str .= "
			<div class='content-body'>";
		if ($this->heading_image)
			$str .= $this->renderImage();

		if ($this->billboard_button)
			$str .= $this->renderButtonImage();

		$str .= $content ? $content : $this->content;
		$str .= "
			</div>";

		$str .= "
		</div>";
		return $str;
	}

	public function renderBillboard($classes = '') {
		LoadModel('Billboard_color');
		$color = new Billboard_color();
		$color->Load($this->billboard_color_id);

		//print($this->heading.' = '.strlen(trim($this->heading)));

		$short_title = '';
		if (strlen(trim($this->heading)) <= 26) {
			$short_title = "short-title";
		}

		$link = $this->billboard_button;
		if (strlen($this->billboard_url))
			$link = "<a  style='color: #{$color->hex};' href='{$this->billboard_url}'>{$this->billboard_button}</a>";

		$button = '';
		if ($this->billboard_button)
			$button = "<div class='billboard-button' style='color: #{$color->hex};'>$link</div>";

		$html = <<< EOB
		<div class='$classes '>
			{$this->renderImage()}
			<div class='billboard-content-container' style='background-color: #$color->hex'>
				<div class='billboard-content'>
					<div class='billboard-title $short_title'>$this->heading</div>
					<div class='billboard-copy'>$this->content</div>
				</div>
				$button
            </div>
		</div>
EOB;
		return $html;
	}

	/**********************************************************************************************
	 * PAGINATION METHODS
	 ***********************************************************************************************/

	public static function paginationRender() {
		$p = self::paginationProcess();
		return $p->render();
	}

	public static function paginationJson() {
		$p = self::paginationProcess();
		return $p->data->json->Encode();
	}

	public static function paginationProcess() {
		//$cname = get_called_class();
		$p = new Pagination(new Pagination_data(new self(), "`page_id` = '" . $_SESSION['page_id'] . "' AND `template_section_id` = '" . System_columns::Content_left . "'", 'date, heading', 2));
		$html = '';
		if (count($p->data->entries)) {
			foreach ($p->data->entries as $entry)
				/** @var Site_page_sections $entry */
				$html .= $entry->getContent();
		}
		else
			$html = "<div class='paginate-placeholder'>There are currently no entries. Please check back soon!</div>";

		$p->data->json->Add('content', $html);
		return $p;
	}

	/**********************************************************************************************
	 * HELPER METHODS
	 ***********************************************************************************************/

	public function renderImage() {
		return $this->heading_image ? "<img src='" . $this->image_dir . $this->heading_image . "' />" : '';
	}

	public function renderButtonImage() {
		return $this->billboard_button ? "<img src='" . $this->image_dir . $this->billboard_button . "' />" : '';
	}

	public function renderPath() {
		return $this->heading_image ? $this->image_dir . $this->heading_image : '';
	}

	public function renderLink() {
		return "<p><a href='#{$this->id}-{$this->_class_prefix}' class='" . ($this->_class_prefix
				? $this->_class_prefix . '-' : '') . "scroll-link scroll-link'>{$this->heading}</a></p>";
	}
}

/*
 * EOF
 */