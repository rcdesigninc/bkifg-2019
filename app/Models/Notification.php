<?

class_exists('tbl_notification') || require_once(MODEL_PATH . '_base/' . 'tbl_notification' . '.php');

class Notification extends tbl_notification {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('type_id', 'Type_id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('days_until', 'Days_until', Model_field_type::Input); // dbtype: mediumint(8) unsigned
		$this->_funcSetField('sent_from', 'Sent_from', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('sent_to', 'Sent_to', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('change_renewal', 'Change_renewal', Model_field_type::Input); // dbtype: tinyint(1)
		$this->_funcSetField('change_level', 'Change_level', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('change_status', 'Change_status', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('archive', 'Archive', Model_field_type::Input); // dbtype: tinyint(1)
		$this->_funcSetField('subject', 'Subject', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('content', 'Content', Model_field_type::Input); // dbtype: text
	}
}

/*
 * EOF
 */