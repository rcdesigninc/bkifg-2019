<?

class_exists('tbl_site_user') || require_once(MODEL_PATH . '_base/' . 'tbl_site_user' . '.php');

class Site_user extends tbl_site_user {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('site_id', 'Site_id', Model_field_type::Select,
			'table="site" show="name"'); // dbtype: tinyint(4) unsigned
		$this->_funcSetField('email', 'Email', Model_field_type::Email); // dbtype: varchar(100)
		$this->_funcSetField('username', 'Username', Model_field_type::Input); // dbtype: varchar(100)
		$this->_funcSetField('password', 'Password', Model_field_type::Password); // dbtype: varchar(20)
		$this->_funcSetField('first_name', 'First_name', Model_field_type::Input); // dbtype: varchar(25)
		$this->_funcSetField('last_name', 'Last_name', Model_field_type::Input); // dbtype: varchar(25)
		$this->_funcSetField('site_permission_id', 'Site_permission_id', Model_field_type::Select,
			'table="site_permissions" show="label"'); // dbtype: tinyint(4)
	}

	public function Login() {
		$json['success'] = false;

		$u = new self();
		if (sizeof($_POST)) {
			$u->LoadWhere("`username` = '" . scrubDbData($_POST['username']) . "' AND `password` = '" . scrubDbData($_POST['password']) . "'");
			//die('Trying login, db ok'.print_array($u));

			$this->checkAccessAttempt();

			if ($u->id)
				$this->setSession($u->id);
		}

		if ($u->id)
			$json['success'] = true;

		$u->LoadWhere("`username` = '" . scrubDbData($_POST['username']) . "'");
		self::addLog("User Login Attempt", $u->id, $u->id);

		print json_encode($json);
	}

	private function checkAccessAttempt() {
		// Check and see if this user has just recently been accessing the site, to associate their that access log to the new account
		LoadModel('Site_user_log');
		$log = new Site_user_log();
		$log->LoadWhere('action LIKE "%Site Accessed%" AND date > ' . strtotime('-30 minutes'));
		if ($log->id) {
			$log->customer_id = $this->id;
			$log->Update();
		}
	}

	public function Logout() {
		self::addLog("User Logging Out", true);
		session_destroy();
		header('location: index.php');
	}

	public function is_root() {
		return $this->site_permission_id == 100 ? true : false;
	}

	public function is_admin() {
		return $this->site_permission_id >= 75 ? true : false;
	}

	public function is_mod() {
		return $this->site_permission_id >= 50 ? true : false;
	}

	public function is_logged() {
		return $this->site_permission_id >= 1 ? true : false;
	}


	final public static function addLog($action, $result, $user_id = 0) {
		LoadModel('Site_user_log');
		$log = new Site_user_log();
		if (!$user_id)
			$user_id = System_core::$instance->user->id;
		$log->Insert($action, ($result ? true : false), $user_id);
	}
}

