<?

class_exists('tbl_site_template_options') || require_once(MODEL_PATH . '_base/' . 'tbl_site_template_options' . '.php');

class Site_template_options extends tbl_site_template_options {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: int(10) unsigned
		$this->_funcSetField('template_id', 'Template_id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('template_section_id', 'Template_section_id',
			Model_field_type::Input); // dbtype: tinyint(3) unsigned
	}
}

/*
 * EOF
 */