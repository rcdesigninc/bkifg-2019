<?

class_exists('tbl_billboard_color') || require_once(MODEL_PATH.'_base/'.'tbl_billboard_color'.'.php');

class Billboard_color extends tbl_billboard_color {
	protected function _funcInit() {
			$this->_funcSetField('id', 'Options', Model_field_type::Hidden);// dbtype: tinyint(3) unsigned
			$this->_funcSetField('name', 'Name', Model_field_type::Input); // dbtype: varchar(25)
			$this->_funcSetField('hex', 'Hex', Model_field_type::Input); // dbtype: varchar(6)
	}
}

/*
 * EOF
 */