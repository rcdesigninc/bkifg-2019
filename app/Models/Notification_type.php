<?

class_exists('tbl_notification_type') || require_once(MODEL_PATH . '_base/' . 'tbl_notification_type' . '.php');

class Notification_type extends tbl_notification_type {
	protected function _funcInit() {
		$this->_funcSetField('id', 'Id', Model_field_type::Input); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('label', 'Label', Model_field_type::Input); // dbtype: varchar(35)
	}
}

/*
 * EOF
 */