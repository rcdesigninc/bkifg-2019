<?

System::$instance->LoadPlugin('Faqs');

class_exists('Site_page_sections') || require_once(MODEL_PATH . '' . 'Site_page_sections' . '.php');

class Faqs extends Site_page_sections {
	protected function _funcInit() {
		parent::_funcInit();

		$this->_class_prefix = 'faq';

		// Readjust field properties
		$this->_funcSetField('page_id', 'Page', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('template_section_id', 'Section', Model_field_type::Hidden); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('date', 'Date', Model_field_type::Date); // dbtype: tinyint(3) unsigned

		// Enforce the proper section and page is selected
		$this->template_section_id = System_columns::Content_faq;
	}

	public function getContent($content = NULL, $section_classes = '') {
		// Drop a anchor to pin down where this will sit on the page, so we can auto-scroll to it from jquery
		$html = "<div id='{$this->id}-{$this->_class_prefix}' class='float-right'>&nbsp;</div>";

		// Format the content
		$html .= "
		<div class='faq-link' >
			 $this->heading
		</div>
		<div class='faq-content'>
			$this->content
		</div>
		";

		return $html;
	}

	public static function renderFaqs($heading = '', $limit = 0, $page_id = PAGE_ID) {
		// Filter and retrieve the filtered faqs for this page
		$o = new self();
		$order_by = 'order_by ';
		if ($limit)
			$order_by .= "LIMIT 0, $limit";

		$entries = $o->Select("id,heading,content",
				"`page_id` = '$page_id' AND `template_section_id` = " . System_columns::Content_faq, $order_by);

		// Build the content, of all the specified faqs together
		$ps = new Site_page_sections();
		$ps->heading = $heading;

		// Wrap the content in a container
		$content = '';
		$content .= '<div class="faqs">';
		foreach ($entries as $entry)
			/** @var Faqs $entry */
			$content .= $entry->getContent();
		$content .= '</div>';

		$ps->content = $content;

		// Return the page section object
		return $ps;
	}

	public static function renderFaqsList($heading = '', $limit = 2, $ul = false, $page_id = PAGE_ID) {
		// Filter and retrieve the filtered faqs heading links for this page
		$o = new self();
		$order_by = 'order_by ';
		if ($limit)
			$order_by .= "LIMIT 0, $limit";

		$entries = $o->Select("id,heading",
				"`page_id` = '" . $page_id . "' AND `template_section_id` = " . System_columns::Content_faq, $order_by);

		// Build the content, of all the specified faqs together
		$ps = new Site_page_sections();
		$ps->heading = $heading;

		// Wrap the content in a container
		$content = '';
		$content .= '<div class="faq-links">';
		if ($ul)
			$content .= "<ul>";
		foreach ($entries as $entry) {
			/** @var Faqs $entry */
			if ($ul)
				$content .= '<li>' . $entry->renderLink() . '</li>';
			else
				$content .= $entry->renderLink();
		}
		if ($ul)
			$content .= "</ul>";
		$content .= '</div>';

		$ps->content = $content;

		// Return the page section object
		return $ps;
	}
}

/*
 * EOF
 */