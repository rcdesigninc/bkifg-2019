<?

class_exists('tbl_blog_post') || require_once(MODEL_PATH . '_base/' . 'tbl_blog_post' . '.php');

class Blog_post extends tbl_blog_post implements iPagination {
	protected function _funcInit() {
		// override pagination defaults
		$this->_class_prefix = "blog";
		$this->_paginate_entries_per_page = 2;
		$this->_paginate_order_by = 'date_published DESC';
		//$this->_paginate_column = Columns::Content_left;

		$this->_funcSetField('id', 'Options', Model_field_type::Hidden); // dbtype: int(10) unsigned
		$this->_funcSetField('tag', 'Tag', Model_field_type::Hidden); // dbtype: varchar(500)
		$this->_funcSetField('title', 'Title', Model_field_type::Input); // dbtype: varchar(500)
		$this->_funcSetField('author_id', 'Author', Model_field_type::Select,
			'table="site_user" show="first_name"'); // dbtype: tinyint(3) unsigned
		$this->_funcSetField('intro_content', 'Intro Content', Model_field_type::Text); // dbtype: varchar(2000)
		$this->_funcSetField('content', 'Content', Model_field_type::Text); // dbtype: varchar(2000)
		$this->_funcSetField('date_created', 'Date_created', Model_field_type::Hidden); // dbtype: timestamp
		$this->_funcSetField('date_modified', 'Date_modified', Model_field_type::Hidden); // dbtype: timestamp
		$this->_funcSetField('date_published', 'Date_published', Model_field_type::Hidden); // dbtype: timestamp
		$this->_funcSetField('is_active', 'Is_active', Model_field_type::Bool); // dbtype: int(1) unsigned
		$this->_funcSetField('categories', 'Categories', Model_field_type::Select); // dbtype: varchar(2000)
	}

	public static function LoadByTitle($title) {
		$o = new self();
		$tag = unhyphonate($title);
		return $o->LoadWhere("`tag` = '$tag'");
	}

	public static function LoadByPublishDate($date) {
		$o = new self();
		return $o->LoadWhere("`date_published` = '$date'");
	}

	/**********************************************************************************************
	 * INPUT AND ENTRY ADJUSTMENT
	 * To hide and show appropriate fields when user and or admins are interacting with this model
	 ***********************************************************************************************/

	function scrubFields() {
		if (System_core::$instance->user->is_root()) {
		}
		else if (System_core::$instance->user->is_admin()) {
			$this->_funcSetField('author_id', 'Author', Model_field_type::Select,
				'table="site_user" show="first_name" filter="`id` > 1"'); // dbtype: tinyint(3) unsigned
		}
	}

	/**********************************************************************************************
	 * OUTPUT AND RENDING OF DATA
	 ***********************************************************************************************/

	public function getContent() {
        class_exists('Blog_post_category') || require_once(MODEL_PATH . 'Blog_post_category' . '.php');

		$ps = new Site_page_sections();

        $bc = new Blog_category();

        $categories = $bc->Select();

        $blog_categories = array();

        foreach($categories as $category){
            $blog_categories[$category->id] = array(
                'tag' => $category->tag,
                'label' => $category->label
            );
        }

        $publishedOn = $this->getPublishDate();
		$author = $this->getAuthor();
		//print $System->lastQuery;

		// Add share this button, or other social media links
		//social = '<a href="http://www.addthis.com/bookmark.php" class="addthis_button_compact"><img src="images/blog_share.gif" width="137" height="35" style="float: left; vertical-align: middle; margin-right: 20px;"/></a>';

		// Get the list and links of categories for this posting
		$bpc = new Blog_post_category();
		//$cats = Blog_post_category::renderPostCategories($this->id);
        $cats = $bpc->Select("*","post_id='{$this->id}'");

        foreach($cats as $cat){
            $tag = $blog_categories[$cat->category_id]['tag'];
            $label = $blog_categories[$cat->category_id]['label'];

            if($tag && $label) {
                $blog_category .= "<li title='$label' class='category-icon category-icon-$tag'>&nbsp;</li>";
            }
        }

        $ps->content = "
		<div class='blog-posting'>
			<h1>$this->title</h1>
            <ul class='blog-posting-category-listing'>
                $blog_category
                <li>
                    $publishedOn
                </li>
            </ul>
			<div class='intro_content'>$this->intro_content</div>
			<div class='this_content'>$this->content</div>
			$social
		</div>";

		return $ps->getContent();
	}

	public function getContentIntro() {
		$ps = new Site_page_sections();

		$publishedOn = $this->getPublishDate();
		$author = $this->getAuthor();
		$this_content = $this->intro_content;

		$ps->heading = $this->renderLink();

		$ps->content = "
		<div class='blog-posting'>
		<h1></h1>
		<h3>Posted $publishedOn by $author</h3>
		$this_content
		<div class='blog-button'><a href='" . $this->getLink() . "' class='read-more'>" . 'Read' . "</a></div>
		</div>";

		return $ps->getContent();
	}

	public static function renderRecentEntries($heading = 'Recent Entries', $entries_per_page = 10, $order_by = 'date_published') {
		$o = new self();

		if ($entries_per_page)
			$order_by .= " LIMIT 0, $entries_per_page";

		$entries = $o->Select("*", "`date_published` != '$_GET[published]' AND `is_active` = '1'", $order_by);
		//die($System->lastQuery.print_array($entires));

		$ps = new Site_page_sections();
		$ps->heading = $heading;

		$content = '';
		foreach ($entries as $entry) {
			/** @var Blog_post $entry */
			$content .= '<p>' . $entry->renderLink() . '</p>';
		}
		$ps->content = $content;

		return $ps;
	}

	/**********************************************************************************************
	 * PAGINATION METHODS
	 ***********************************************************************************************/

	public static function paginationRender() {
		$p = self::paginationProcess();
		return $p->render();
	}

    function categoryFilterRender() {
        $filter_name = "category-filter";
        $o = new Blog_category();
        $categories = $o->Select("*", "id IN (SELECT DISTINCT category_id FROM tbl_blog_post_category)", "label");
        $content = "<ul class='$filter_name horizontal radio-filter-container'>";

        $content .= "<li class='radio-filter-selected'><input type='radio' id='$filter_name-radio-all' name='$filter_name' value='*' />".
            "<label for='$filter_name-radio-all'>All</label></li>";

        foreach($categories as $category){
            $content .= "<li><input type='radio' id='$filter_name-radio-{$category->tag}' name='$filter_name' value='{$category->tag}' />".
                "<label for='$filter_name-radio-{$category->tag}'>".$category->label."</label></li>";
        }

        $content .= "</ul>";

        return $content;
    }

    public static function paginationJson() {
		$p = self::paginationProcess();
		return $p->data->json->Encode();
	}

    function filterListProcess($current = false,$limit = 1) {
        class_exists('Blog_post_category') || require_once(MODEL_PATH . 'Blog_post_category' . '.php');

        $blog_post = new self();
        $ps = new Site_page_sections();
        $bpc = new Blog_post_category();

        $ps->heading = '';//$blog_post->renderLink($post);

        $o = new self();
        $bc = new Blog_category();

        $categories = $bc->Select();

        $blog_categories = array();

        foreach($categories as $category){
            $blog_categories[$category->id] = array(
                'tag' => $category->tag,
                'label' => $category->label
            );
        }

        $where = array();

        if($current){
           $where[] = "id < $current";
        }
        else{
            $where[] = "1";
        }

        $posts = $o->Select("*",implode(' AND ',$where),"date_published DESC",$limit);

        $ps->content = "<span id='autoscroll-container'></span>";

        $last_post = 0;

        foreach($posts as $post){
            $last_post = ($post->id > $last_post) ? $post->id : $last_post;

            $cats = $bpc->Select('*','post_id='.$post->id);

            $class = array('category-filter-item');
//            $blog_category = false;
            $blog_category = '';

            foreach($cats as $cat){
                $tag = $blog_categories[$cat->category_id]['tag'];
                $label = $blog_categories[$cat->category_id]['label'];

                if($tag && $label) {
                    $blog_category .= "<li title='$label' class='category-icon category-icon-$tag'>&nbsp;</li>";
                    $class[] = "category-filter-$tag";
                }
            }
            $class = implode(' ',$class);

            $publishedOn = $blog_post->getPublishDate($post->date_published);
            $author = $blog_post->getAuthor($post->author_id);
            $this_content = $post->intro_content;
            $this_title = $blog_post->renderLink($post);

            $ps->content .= "
                <div class='blog-posting $class'>
                <h2>$this_title</h2>
                <ul class='blog-posting-category-listing'>
                    $blog_category
                    <li>
                        $publishedOn
                    </li>
                </ul>
                $this_content
                <div class='blog-button'><a href='" . $blog_post->getLink($post) . "' class='read-more'>" . 'Read' . "</a></div>
                </div>";

        }

        if($current === false){
            $ps->content .= "<div class='blog-posting category-filter-item' id='no-posts-found' style='display:none;'>There are no posts in this category.</div>";
        }
        if(count($posts) == $limit){
            $ps->content .= "<a href='/index.php?section=blog&ajax&limit={$limit}&last={$last_post}'>&nbsp;</a>";
        }
        return $ps;
    }
	public static function paginationProcess() {
		$where = "`date_published` > 0 AND `is_active` = '1' ";
		if ($_SESSION['cat'])
			$where .= " AND id IN (SELECT a.post_id FROM tbl_blog_post_category a INNER JOIN tbl_blog_category b ON b.id = a.category_id  WHERE b.tag = '{$_SESSION[cat]}')";

		$p = new Pagination(new Pagination_data(new self(), $where, 'date_published DESC', 10));
		$html = '';
		if (count($p->data->entries)) {
			/** @var Blog_post $entry */
			foreach ($p->data->entries as $entry)
				$html .= $entry->getContentIntro();
		}
		else
			$html = "<div class='paginate-placeholder'>There are currently no blog entries. Please check back soon!</div>";

		$p->data->json->Add('content', $html);
		return $p;
	}

    public function getAjaxPagingData(){
        if(!(($limit = $_GET['limit']) && ($last = $_GET['last']))){
            echo "No posts found";
        }

        $o = new self();
        $content = $o->filterListProcess($last,$limit)->content;

        echo $content;
    }

	/**********************************************************************************************
	 * HELPER METHODS
	 ***********************************************************************************************/

	public function getPublishDate($date = false) {
        if($date === false) $date = (int)$this->date_published;
		return date("F j, Y, g:i a", $date);
	}

	public function getAuthor($auth_id = false) {
		$u = new Site_user();

        if($auth_id === false) $auth_id = $this->author_id;

		$author = $u->LoadWhere("`id` = '$auth_id'");
		return $u->first_name;
	}

	public function getLink($post = false) {
        if($post === false) $post = $this;

        $url = System_defaults::Uri . Site_page::getTagById($_SESSION['page_id']) . "&post=" . hyphonate($post->title) . "&published=$post->date_published";

        return $url;
	}

	public function renderLink($post = false) {
        if($post === false) $ttl = $this; else $ttl = $post;
        return "<a href='" . $this->getLink($post) . "' class=''>$ttl->title</a>";
	}

}

?>