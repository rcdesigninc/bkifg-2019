<?

/*
 * Document Path
 */
define('ROOT_PATH', dirname(__FILE__));
if (ROOT_PATH != dirname(__FILE__)) die('Root path differs, please update to: ' . dirname(__FILE__));

require_once(ROOT_PATH . '/System/lib/System.php');

$System = new System();