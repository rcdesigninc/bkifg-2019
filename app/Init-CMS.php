<?

/*
 * Document Path
 */
define('ROOT_PATH', dirname(__FILE__));
if (ROOT_PATH != dirname(__FILE__)) die('Root path differs, please update to: ' . dirname(__FILE__));

require_once(ROOT_PATH . '/System/lib/System.php');
require_once(ROOT_PATH . '/CMS/lib/CMS_System.php');

// Override client $System, using CMS_System class
$System = new CMS_System();