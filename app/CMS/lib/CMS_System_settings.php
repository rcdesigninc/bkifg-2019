<?

define('CMS_PATH', ROOT_PATH . '/CMS/');
define('CMS_LIB', CMS_PATH . 'lib/');
define('CMS_CSS', APP_PATH . 'CMS/css/');
define('CMS_JS', APP_PATH . 'CMS/js/');
define('CMS_IMGS', APP_PATH . 'CMS/images/');
define('CMS_DOCS', APP_PATH . 'CMS/docs/');
define('CMS_AJAX', APP_PATH . 'CMS/ajax/');
