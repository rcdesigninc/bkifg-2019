<?



require_once(dirname(__FILE__) . '/' . 'CMS_System_settings.php');

class_exists('System_core') || require_once(SYS_LIB . '/System/' . 'System_core.php');



final class CMS_System extends System_core {

	/** @var int $manging_site

	 * A int specifying the current site which is being modified within the CMS.

	 */

	public $managing_site = 2;



	public $dataTableStrip = 200;



	public function __construct() {

		parent::__construct($this);

		self::$instance =& $this; // must be called from child



		$this->managing_site = $_SESSION['managing_site'];



		$this->LoadPlugin('Blog');

	}



	public function renderMenu() {

		if (ACTIVE_SITE == 1 && $this->managing_site && !$this->disableMenu) {



			$parentId = 0;

			$m = new Site_menu();

			$menu_list = $m->Select("*", "`managed_on` = 0 OR (`managed_on` = '" . $this->managing_site . "' AND site_id = 1)");


			if(!$this->user->is_root())
				array_shift($menu_list);

			$menu = $submenu = array();

			foreach ($menu_list as $entry) {

				/** @var Site_menu $entry */



				$pid = $entry->parent_id;



				/*die("parent_id = $pid".print_array($entry));*/



				if ($pid != $parentId)

					$submenu[$pid][] = $entry;

				else

					$menu[] = $entry;

			}



			/*die(print_array($submenu));*/



			$padding = 24; // 44

			$left = 0;

			$str = '';

			foreach ($menu as $entry) {

				$sub_id = $mid = $entry->getClassId();



				if (!count($submenu[$sub_id])) { // doesn't have a submenu



					$str .= "

				<div id='menu-$mid-Container'>

					<div id='menu-$mid' class='menu1 menutop'>

						<a href='" . $entry->getLink() . "' style='width: " . $entry->width . "px;'>$entry->label</a>

					</div>

					<div id='menu-$mid-Submenu' class='submenu'>

					</div>

				</div>";

				}

				else { // has a submenu

					$str .= "

				<div id='menu-$mid-Container'>

					<div id='menu-$mid' class='menu1 menutop'>

						<a href='" . $entry->getLink() . "' style='width: " . $entry->width . "px;'>$entry->label</a>

					</div>

					<div id='menu-$mid-Submenu' class='submenu' style='width: " . 'auto' . "px; left: " . $left . "px;'>"; //($entry->width + 75)



					foreach ($submenu[$sub_id] as $sub) {

						$str .= "

						<div class='menu2'><a href='" . $sub->getLink() . "'>$sub->label</a></div>";

					}

					$str .= "

					</div>

				</div>";

				}

				$left += $entry->width + $padding;

				//print "left is: $left<br/>";

			}

			//die();

			return $str;

		}

	}



	public function getDataTable($data, $columns, $sortable = false, $page = '?', $override_del = false, $fullFields = false) {

		$table = get_class($data[0]);

		/*

			   $str = "<form method='get' action='".$page."'>";

			   $str .= "<input type='submit' name='add' value='Add Entry'>";

			   $str .= "</form>";

			   */

		$str = '';

		$classes = "datatable ";

		if ($sortable)

			$classes .= "sorttable ";

		$str .= "<table class='$classes'>";

		$str .= "<thead>";

		foreach ($columns as $dbcol => $col) {

			if ($dbcol == 'site_id' || $dbcol == 'order_by')

				continue;

			else

				$str .= "<td dbsort='$dbcol'>&nbsp;$col</td>";

		}

		$str .= "</thead>";

		$str .= "<tbody>";

		foreach ($data as $entry) {

			$str .= "<tr table='$table' row-id='$entry->id'>";

			foreach ($columns as $dbcol => $col) {

				if ($dbcol == 'site_id' || $dbcol == 'order_by')

					continue;

				else if ($dbcol == 'id') {

					$str .= "<td>";

					$str .= "&nbsp;<a href='" . $page . "id=$entry->id'>Edit</a>";

					if ($override_del || $this->user->is_root())

						$str .= " <a  class='del-entry' href='" . $page . "id=$entry->id&delete=true'>Del</a>&nbsp;&nbsp;";

					$str .= "</td>";

				}

				else {

					$text = $entry->$dbcol;

					$text = strip_tags($text);

//					if (!$fullFields)

//						$text = printHtml($this->dataTableStrip, $text);

					$str .= "<td>$text</td>";

				}

			}

			$str .= "</tr>";

		}

		$str .= "</tbody>";

		$str .= "</table>";

		return $str;

	}



	public function getInputTable($data, $columns, $is_multipart = true) {

		$submit_label = 'Save';

		if ($data->id)

			$submit_label = 'Update';



		$str = "<div class='form-buttons'>";

		$str .= "<input id='inputTableSubmit' type='submit' name='submit' value='$submit_label'><br/>";

		$str .= "<input id='inputTableCancel' type='submit' name='submit' value='Cancel' class='cancel'>";

		$str .= "</div>";



		$str .= "<table class='entry-table'>";

		$str .= "<thead>";

		$str .= "</thead>";

		$str .= "<tbody>";

		foreach ($columns as $dbcol => $col) {

			$str .= "<tr>";

			$str .= "<td>" . $data->getInput($dbcol) . "</td>";

			$str .= "</tr>";

		}

		$str .= "</tbody>";

		$str .= "</table>";



		return $this->buildForm($str);

	}



	public function buildForm($content, $is_multipart = true, $method = 'POST') {

		if ($is_multipart)

			$multipart = "enctype='multipart/form-data'";



		$action = $_SERVER['REQUEST_URI'];

		$str = "<form action='$action' method='$method' $multipart class='entry-form' >";

		$str .= $content;

		$str .= "</form>";



		return $str;

	}





	final function prepPagination($class, $where, $order_by, $entries_per_page = 100) {

		$from = 1;

		if ($_GET['from'])

			$from = (int)$_GET['from'];

		else if ($_POST['from'])

			$from = (int)$_POST['from'];

		define("PAGINATE_FROM", $from);



		LoadModel($class);



		$c = new $class();

		$count = ceil(count($c->Count($where)) / $entries_per_page);

		define("PAGINATE_COUNT", $count);



		$order_by = $order_by . " LIMIT " . (($from - 1) * $entries_per_page) . ", " . $entries_per_page;

		$entries = $c->Select("*", $where, $order_by);



		return $entries;

	}



	final function renderPagination($useJq = true) {

		$content = '<div class="spacer"></div>';

		if (PAGINATE_COUNT > 1) {

			$page_links = array();

			for ($i = 1; $i <= PAGINATE_COUNT; $i++) {

				$page_links[] = "<a id='posting-page-$i' href='?from=$i' " . (PAGINATE_FROM == $i ? 'class="selected"'

						: '') . ">$i</a>";

			}

			$content .= '

			<div id="posting-nav-container">

				<div id="posting-nav">

						<div id="prev-postings">

							<b>PREV</b>

						</div>';

			$content .= "<div id='page-nav'>" . join(", ", $page_links) . "</div>";

			$content .= '

						<div id="next-postings">

							<b>NEXT</b>

						</div>

				</div>

			</div>

			<div style="clear: both;"></div>';

		}

		if ($useJq) {

			$this->addJquery('

			' . (PAGINATE_FROM > 1 ? '

			$("#prev-postings").click(function(){

				debug("#prev-postings:click");

				window.location = $("#posting-page-' . (PAGINATE_FROM - 1) . '").attr("href");

			});' : '

			$("#prev-postings").css("visibility","hidden");

			') . '



			' . (PAGINATE_FROM < PAGINATE_COUNT ? '

			$("#next-postings").click(function(){

				debug("#next-postings:click");

				window.location = $("#posting-page-' . (PAGINATE_FROM + 1) . '").attr("href");

			});' : '

			$("#next-postings").css("visibility","hidden");

			') . '



			navWidth = $("#page-nav").width()+40+90;

			newLeft = $("#posting-nav").width() / 2 - navWidth / 2;



			$("#prev-postings").css("margin-right",newLeft);

			$("#next-postings").css({"float":"right","margin-right":"10px"});

			$("#posting-nav").css("visibility","visible");



			$("#page-nav a").hover(function(){

				$(this).addClass("hover");

			},function(){

				$(this).removeClass("hover");

			});

		');

		}

		return $content;

	}

}



$System = new CMS_System();



/*

 * EOF

 */