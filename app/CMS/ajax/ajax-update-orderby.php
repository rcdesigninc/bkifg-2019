<?
define('PAGE_ID',1);
require_once('_config.php');

if(!$System->user->is_admin())
	die();

$class = ucwords($_POST['table']);
$orderby = $_POST['orderBy'];
$id = $_POST['rowId'];

LoadModel($class);
$c = new $class();
$c->Load($id);

$c->order_by = $orderby;
//print (print_array($c));

print $c->Update();

?>