// JavaScript Document
jQuery(document).ready(function($) {
	$("#logout").click(function(){ window.location = 'logout.php'; });

	$(".entry-form .cancel").click(function(){
		history.go(-1);
		return false;
	});

	// TODO Verify the delete entry functionality
	var DeleteConfirm = function() {
		if(confirm("Are you sure you wish to delete this entry?  Please note that the data will not be recoverable!"))
			return true;
		return false;
	};

	$("a.del-entry").each(function(i){
		$(this).click(DeleteConfirm);
	});
	
	$("input[type=submit]").addClass('button').css('width','200px');
	
	$("label").wrap("<div style='float: left; width: 100px; text-align: right; margin-right: 10px;'></div>");

	$.initTables = function() {	
		$('.datatable tbody tr:odd').css({'background-color': '#ccc'});
		$('.datatable tbody tr:even').css({'background-color': '#eee'});
		
		if($('.sorttable').exists()) {
			$.getScript(CMS_JS+'jquery.ui.sortable.min.js', function() {
				debug("Processing SortTables");
				$('.sorttable').addClass('ui-state-default');
				$('.sorttable tbody').sortable({
					forcePlaceholderSize: true,
					forceHelperSize: true,
					items: 'tr',
					opacity: 0.6,
					placeholder: "ui-state-highlight",
					distance: 20,
					update: function(event, ui){
						$('.datatable tbody tr:odd').css({'background-color': '#ccc'});
						$('.datatable tbody tr:even').css({'background-color': '#eee'});
				
						$datatable = $(this).attr('db-table');
						$(this).find('tr').each(function(indexOrder){
							//if($(this).hasClass('moveable'))
							{
								//alert("Index at ("+indexOrder+") as row-id = "+$(this).attr('row-id'));
								
								// Use ajax, go through the list of elements and update the order appropriately
								$.post(CMS_AJAX+'ajax-update-orderby.php', {
									rowId: $(this).attr('row-id'), 
									orderBy: indexOrder+1, // pad to ensure no 0 indexes are used
									table: $(this).attr('table')
								}, 					 
								function(data){
									// do something
									//alert('Success = '+data);
								},'json');
							}
						});
					}
				}).find('td').addClass('ui-state-default');
			});
		}
	
		if($('.datefield').exists()) {
			$.getScript(CMS_JS+'jquery.ui.datepicker.min.js', function() {
				debug("Processing DateField");
				$('.datefield').css('cursor','pointer').datepicker({ 
					minDate: $(this).attr('min-date'), 
					maxDate: $(this).attr('max-date'),
					changeMonth: false,
					changeYear: false,
					showOtherMonths: true,
					selectOtherMonths: false,
					showOn: "both",
					buttonImage: $datepickerImage,
					buttonImageOnly: true,
					buttonText: 'datepickerImage',
					autoSize: true,
					dateFormat: 'yy-mm-dd'
				});
			});
	
			$('img[title=datepickerImage]').css({'cursor':'pointer', 'margin':'3px 0px 0px 3px'});
		}
	}

	
	
	$.initTables();
});