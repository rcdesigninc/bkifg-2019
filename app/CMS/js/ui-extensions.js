// JavaScript Document
jQuery(document).ready(function($){
	$('li').addClass('ui-state-default');
								
	$dbEdit = '<div class="sort-edit">'+jQueryUiIcon('scissors')+'Edit</div>';
	$dbDelete = '<div class="sort-delete">'+jQueryUiIcon('close')+'Delete</div>';				
	/*
	* JQUERY UI SORTABLE
	*/
		   
	$( ".sortable" ).sortable({
		items: 'li.moveable',
		placeholder: "ui-state-highlight",
		distance: 30,
		update: function(event, ui){
			$datatable = $(this).attr('db-table');
			$(this).find('li').each(function(indexOrder){
				if($(this).hasClass('moveable'))
				{
					$dbid = $(this).attr('db-id');
					//alert("Index at ("+i+") as DB-ID = "+$(this).attr('db-id'));
					
					// Use ajax, go through the list of elements and update the order appropriately
					$.post('lib/web/js/ajax/update_sotr_components.php', { 
						id: $dbid, 
						order: indexOrder
					}, 					 
					function(data){
						// do something
					},'json');
				}
			});
		}
	});
	
	$(".sortable").find('li').each(function(i){
		$(this).addClass('ui-state-default');
		if($(this).hasClass('moveable'))
		{
			$(this).prepend(jQueryUiIcon('arrowthick-2-n-s')).append($dbDelete);
		}
		else
			$(this).prepend(jQueryUiIcon('locked'))
		$(this).append($dbEdit);
	});
	
	
	$(".sortable").append('<li class="ui-state-highlight new">'+jQueryUiIcon('document')+'Add A New Component</li>');
	
	$(".sort-edit").click(function(){
		alert("Edit for "+$(this).parent().attr('db-id')+' clicked');
	});
	$(".sort-delete").click(function(){
		alert("Delete for "+$(this).parent().attr('db-id')+' clicked');
	});
});