// JavaScript Document

jQuery(document).ready(function($) {
	if ($('textarea').exists()) {
		$("textarea.mceLight").tinymce({
			script_url: TINYMCE_PATH + 'tiny_mce.js',
			// General options
			/*
			 mode : "specific_textareas",
			 editor_selector : "mceLight",
			 */

			//theme : "simple",
			theme : "advanced",
			plugins : "ezfilemanager,iespell,spellchecker,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

			// Theme options
			//theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,undo,redo,|,link,unlink,image,|,iespell,spellchecker,|,preview",
			//theme_advanced_buttons2 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,formatselect,|,charmap,sub,sup",
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,charmap,sub,sup,|,tablecontrols",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,br,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",

			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",

			file_browser_callback: "CustomFileBrowser",

			paste_auto_cleanup_on_paste : true,
			paste_remove_styles: true,
			paste_remove_styles_if_webkit: true,
			paste_strip_class_attributes: 'all',

			//relative_urls : false,
			//file_browser_callback: "CustomFileBrowser",
			//extended_valid_elements : "iframe[src|width|height|name|align]",
			force_br_newlines : false,
			force_p_newlines : true,
			forced_root_block : '', // Needed for 3.x
			entity_encoding : "named",
			//entities : '224,agrave,225,aacute,226,acirc,227,atilde,231,ccedil,232,egrave,233,eacute,234,ecric,236,igrave,237,iacute,238,icirc',


			//theme_advanced_buttons3 : "|,hr,removeformat,visualaid,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			//theme_advanced_buttons4 : "moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			theme_advanced_resizing : false,

			// Example content CSS (should be your site CSS)
			content_css : $tinyMCEcss,

			languages : 'en',
			disk_cache : true,


			relative_urls : true, // Default value
			remove_script_host : false,
			document_base_url : '',

			height: '400px',
			width: '400px',
			oninit: fixMceLight
		});

		$("textarea.mceFull").tinymce({
			script_url: TINYMCE_PATH + 'tiny_mce.js',
			// General options
			/*
			 mode : "specific_textareas",
			 editor_selector : "mceFull",
			 */

			//theme : "simple",
			theme : "advanced",
			plugins : "iespell,spellchecker,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

			// Theme options
			theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,tablecontrols",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,iespell,spellchecker,|,undo,redo,|,bullist,numlist,|,outdent,indent,|,charmap,sub,sup,|,link,unlink,|,preview",
			theme_advanced_buttons3 : "|,hr,br,removeformat,visualaid,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",

			paste_auto_cleanup_on_paste : true,
			paste_remove_styles: true,
			paste_remove_styles_if_webkit: true,
			paste_strip_class_attributes: 'all',

			//relative_urls : false,
			//file_browser_callback: "CustomFileBrowser",
			//extended_valid_elements : "iframe[src|width|height|name|align]",
			force_br_newlines : false,
			force_p_newlines : true,
			forced_root_block : '', // Needed for 3.x
			entity_encoding : "named",
			//entities : '224,agrave,225,aacute,226,acirc,227,atilde,231,ccedil,232,egrave,233,eacute,234,ecric,236,igrave,237,iacute,238,icirc',

			//theme_advanced_buttons4 : "moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : false,

			// Example content CSS (should be your site CSS)
			content_css : $tinyMCEcss,

			languages : 'en',
			disk_cache : true,

			height: '400px',
			width: '400px'

		});

		$("textarea.mceSuperLight").tinymce({
			script_url: TINYMCE_PATH + 'tiny_mce.js',

			// General options
			/*
			 mode : "specific_textareas",
			 editor_selector : "mceSuperLight",
			 */

			nowarp: true,
			//theme : "simple",
			theme : "advanced",
			plugins : "ezfilemanager,iespell,spellchecker,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
			// Theme options
			theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,iespell,spellchecker,|,undo,redo,|,charmap,|,link,unlink",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",

			paste_auto_cleanup_on_paste : true,
			paste_remove_styles: true,
			paste_remove_styles_if_webkit: true,
			paste_strip_class_attributes: 'all',

			force_br_newlines : true,
			force_p_newlines : false,
			forced_root_block : '', // Needed for 3.x
			entity_encoding : "named",

			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			theme_advanced_resizing : false,

			content_css : $tinyMCEcss,

			languages : 'en',
			disk_cache : true,

			height: '40px',
			width: '400px',
			oninit: fixMceSuperLight,
			handle_event_callback : "myHandleEvent"
		});

		$("textarea.mceCoder").tinymce({
			script_url: TINYMCE_PATH + 'tiny_mce.js',

			theme : "advanced",
			plugins : "paste,xhtmlxtras",

			theme_advanced_buttons1 : "code",//"bold,italic,underline,|,cut,copy,paste,pastetext,|,undo,redo,|,charmap,sub,sup,|,link,unlink,|,preview",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",

			paste_auto_cleanup_on_paste : true,
			paste_remove_styles: true,
			paste_remove_styles_if_webkit: true,
			paste_strip_class_attributes: 'all',

			force_br_newlines : false,
			force_p_newlines : false,
			convert_newlines_to_brs: false,
			apply_source_formatting : false,
			forced_root_block : '', // Needed for 3.x
			entity_encoding : "raw",

			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			theme_advanced_resizing : false,

			content_css : $tinyMCEcss,

			languages : 'en',
			disk_cache : true,

			height: '40px',
			width: '400px',
			oninit: fixMceLight,
			handle_event_callback : "myHandleEvent"
		});
	}
});

function fixMceLight() {
//	debug("fixMceLight :: running()");
//	jQuery(document).ready(function($) {
//		$(".mceLight").siblings().find('iframe').css({height:'100px'}).find(".mceContentBody").css({background: 'none'});
//	});
}

function fixMceSuperLight() {
//	debug("fixMceSuperLight :: running()");
//	jQuery(document).ready(function($) {
//		$(".mceSuperLight").siblings().find('iframe').css({height:'50px'}).find(".mceContentBody").css({background: 'none'});
//	});
}

function myHandleEvent(e) {
	debug("myHandleEvent :: running(); e.type = " + e.type + ", e.which = " + e.which);

	if (e.type == 'keydown' && e.which == 13)
		return false;

	return true; // Continue handling
}