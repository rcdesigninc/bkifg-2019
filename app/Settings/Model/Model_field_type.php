<?

abstract class Model_field_type {
	const Input = 'text';
	const Password = 'password';
	const Text = 'mceLight';
	const Textarea = 'textarea';
	const Text_limited = 'mceSuperLight';
	const Html = 'html';
	const Bool = 'checkbox';
	const Checkbox = 'checkbox';
	const Radio = 'radio';
	const Select = 'select';
	const Button = 'button';
	const Hidden = 'hidden';
	const Date = 'date';
	const Email = 'email';
	const Phone = 'text';
	const Url = 'input';
	const File = 'file';
	const Dropdown = 'dropdown';
}

/*
 * EOF
 */