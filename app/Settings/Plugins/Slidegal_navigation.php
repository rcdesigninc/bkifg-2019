<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sean
 * Date: 07/09/11
 * Time: 9:11 AM
 * To change this template use File | Settings | File Templates.
 */

abstract class Slidegal_navigation {
	const None = 0;
	const All = 1;
	const Prev_next = 2;
	const Page_nav = 3;
}
