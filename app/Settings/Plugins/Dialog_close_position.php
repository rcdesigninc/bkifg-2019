<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sean
 * Date: 08/09/11
 * Time: 9:27 AM
 * To change this template use File | Settings | File Templates.
 */
 
abstract class Dialog_close_position {
	const Top_right = 1; // offset to the top right of the dialog
	const Top_left = 2; // offset to the top left of the dialog
	const Bottom_right = 3; // offset to the top right of the dialog
	const Bottom_left = 4; // offset to the top left of the dialog
}