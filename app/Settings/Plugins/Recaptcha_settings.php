<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sean
 * Date: 14/11/11
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * The reCAPTCHA server URL's
 */
define("RECAPTCHA_API_SERVER", "http://www.google.com/recaptcha/api");
define("RECAPTCHA_API_SECURE_SERVER", "https://www.google.com/recaptcha/api");
define("RECAPTCHA_VERIFY_SERVER", "www.google.com");

abstract class Recaptcha_settings {
	const Public_key = '6Lc4_MsSAAAAAGivylC2AbhEfN1gjWCSAR49W0Or';
	const Private_key = '6Lc4_MsSAAAAAKuGnI_cjLurCTOCGvraDg9l43x1';
	const Theme = 'white';
}