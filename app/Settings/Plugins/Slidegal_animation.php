<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sean
 * Date: 07/09/11
 * Time: 9:10 AM
 * To change this template use File | Settings | File Templates.
 */
 
abstract class Slidegal_animation {
	const blindX = 'blindX';
	const blindY = 'blindY';
	const blindZ = 'blindZ';
	const cover = 'cover';
	const curtainX = 'curtainX';
	const curtainY = 'curtainY';
	const fade = 'fade';
	const fadeZoom = 'fadeZoom';
	const growX = 'growX';
	const growY = 'growY';
	const none = 'none';
	const scrollUp = 'scrollUp';
	const scrollDown = 'scrollDown';
	const scrollLeft = 'scrollLeft';
	const scrollRight = 'scrollRight';
	const scrollHorz = 'scrollHorz';
	const scrollVert = 'scrollVert';
	const shuffle = 'shuffle';
	const slideX = 'slideX';
	const slideY = 'slideY';
	const toss = 'toss';
	const turnUp = 'turnUp';
	const turnDown = 'turnDown';
	const turnLeft = 'turnLeft';
	const turnRight = 'turnRight';
	const uncover = 'uncover';
	const wipe = 'wipe';
	const zoom = 'zoom';
}
