<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sean
 * Date: 08/09/11
 * Time: 9:16 AM
 * To change this template use File | Settings | File Templates.
 */

require_once(SETTINGS_PLUGINS . 'Dialog_close_position.php');
require_once(SETTINGS_PLUGINS . 'Recaptcha_settings.php');
require_once(SETTINGS_PLUGINS . 'Slidegal_animation.php');
require_once(SETTINGS_PLUGINS . 'Slidegal_navigation.php');

abstract class Plugin_defaults {
	const Dialog_close_pos = Dialog_close_position::Top_right;
	const Dialog_close_offset = '0, 0';

	const Slidegal_animation = Slidegal_animation::fade;
	const Slidegal_show_nav = Slidegal_navigation::Page_nav;
}