<?

require_once(SETTINGS_MODEL . 'Model_field_render.php');
require_once(SETTINGS_MODEL . 'Model_field_type.php');

abstract class System_defaults {
	const Debugging = false;

	const Page = 'home'; /* default page tag */
	const Uri = 'index.php?section=';
	const Plugins = 'Dialog, Pagination, Recaptcha, Slidegal, SmoothDropdown';
	const Models = 'Site_user, Site, Site_template, Site_page, Site_page_sections, Site_settings, Site_menu';

	const Root_web_path = '/';
	const Minify_groupConfig_path = '/var/www/bensonkearleyifg.com/web/app/'; // requires are double slash, using the root folder the project runs off the server; must also have trailing slash
	const Minify_run_path = 'app'; // using the root folder the project runs off the server;

	const Model_field_render = Model_field_render::Outside_label;
}

abstract class Sites {
	const Main = 2;
	const Personal_insurance = 3;
	const Commercial_insurance = 4;
	const Financial_services = 5;

}

/*
 * EOF
*/
