
function validateEmail(email) {
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
	return emailPattern.test(email);
}

function validatePhoneNumber(elementValue) {
	var phoneNumberPattern = /^\(?(\d{1})?\)?[-\. ]?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/;
	return phoneNumberPattern.test(elementValue);
}

function validPostalCode(pcode) {
	var postalPattern = /^([0-9]{5})|(\s*[a-ceghj-npr-tvxy]\d[a-ceghj-npr-tv-z](\s)?\d[a-ceghj-npr-tv-z]\d\s*)$/i;
	var postalRegExp = new RegExp(postalPattern);

	debug('postal checking');

	if (postalRegExp.test(pcode) == false)
		return false;

	debug('postal is good');
	return true;
}