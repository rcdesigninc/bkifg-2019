// JavaScript Document
jQuery(document).ready(function($) {
	$(".faq-link").each(function(i){
		$(this).next().append('<div class="faq-close">CLOSE</div>');

		$(this).click(function(){
			if($.browser.msie && $.browser.version < 8) {
				$(".faq-content").hide();
				$(this).next().show(0,function(){
					$o = $(this).prev();
					setTimeout('startScroll($o);',1);
				});
			}
			else {
				$(".faq-content").slideUp();
				$(this).next().slideDown("fast",function(){
					$o = $(this).prev();
					setTimeout('startScroll($o);',1);
				});
			}
		});
	});

	$(".faq-close").live('click',function(){
		$(this).parent().slideUp();
	});

	$(".faq-scroll-link").unbind().live('click',function(){
		// Must removed the default .scroll-link control
		// Force the scroll to start before the faqs open/close to make is smoother
		startScroll($($(this).attr('href')).next('.faq-link'));
		$($(this).attr('href')).next('.faq-link').click();
		return false;
	});
});