// JavaScript Document
var $focus = null;
var errors = new Array();

var $errorDialog = null;

function processForm($form, heading) {
	if (errors.length)
		displayErrors(heading);
	else
		$form.submit();
}

function displayErrors(heading, content) {
	if (heading)
		if (heading.length > 0)
			$errorDialog.find('.heading h1').text(heading);

	// By default display any errors
	$errorDialog.find(".errors").html("&bull; " + errors.join("<br/>&bull; "));

	if (content)
		if (content.length > 0)
			$errorDialog.find('.content').html(content);

	$errorDialog.bPopup();

	$errorDialog.find(".close").click(function() {
		if ($focus)
			$focus.focus();
		jQuery('body').unbind('keydown');
	});

	jQuery('body').keydown(function(event) {
		if (event.keyCode == '13')
			$errorDialog.find('.close').click();
	});
}

function addError($o, msg) {
	if ($o) {
		if ($o == 'undefined')
			$o = null;
		debug("addError('null', '" + msg + "')");
		debug("addError(" + $o.attr('name') + ", '" + msg + "')");
	}

	errors.push(msg);
	$focus = $focus == null ? $o : $focus;
}

function resetErrors() {
	$focus = null;
	errors = new Array();
}