<?

class MapBox {
	public static function render($name, $lat, $lon, $address) {
		return "<div class='img map-box-container' data-name='$name' data-address='$address' data-icon='' data-lat='$lat' data-lon='$lon'></div>";
	}
}
