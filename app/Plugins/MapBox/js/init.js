RC_MapBox_AJAX = PLUGIN_PATH + 'MapBox/ajax/MapBox.php';

document.write('<' + 'script src="' + 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCrNfMwUacTECf5iYQgt8OTloWDCWdxUnQ&sensor=true' + '"' +
	' type="text/javascript"><' + '/script>');

jQuery(document).ready(function ($) {
	var $mapBoxes = $('.map-box-container');

	$mapBoxes.each(function () {
		var $this = $(this);
		var lat = $this.attr('data-lat');
		var lon = $this.attr('data-lon');
		var address = $this.attr('data-address');
		var latlng;

		$this.attr('id', $.Guid.New());

		$this.find("*").css('width', 'auto');

		if(lat != "" && lon != ""){
			latlng = new google.maps.LatLng(lat, lon);
			render_map(latlng, $this);
		}else{
			var data_string = "address="+address;
 			$.ajax({
				type: "POST",
				url: RC_MapBox_AJAX,
				data: data_string,
				success: function(return_data){
					var latlng_array = return_data.split("@");
					lat = latlng_array[0];
					lon = latlng_array[1];
					latlng = new google.maps.LatLng(lat, lon);
					render_map(latlng, $this);
				}
			});
		}

		function render_map(latlng_para, map_obj){
			var myOptions = {
				zoom              : 15,
				center            : latlng_para,
				mapTypeControl    : false,
				mapTypeId         : google.maps.MapTypeId.ROADMAP,
				keyboardShortcuts : false,
				scrollwheel: false,
				navigationControl: false,
				scaleControl: false
			};
			var map = new google.maps.Map(document.getElementById(map_obj.attr('id')), myOptions);
			var marker = new google.maps.Marker({
				position  : latlng_para,
				map       : map,
				animation : google.maps.Animation.DROP,
				title     : map_obj.attr('data-name'),
				icon      : 'https://www.bkifg.com/app/Templates/images/map-icon.png',
				url       : 'https://www.google.ca/maps/place/17705+Leslie+St+%23101,+Newmarket,+ON+L3Y+3E3/@44.0708963,-79.4326128,17z/data=!3m1!4b1!4m2!3m1!1s0x882acd9b15874609:0x25bfc3e8e92f69f0'
			});

			google.maps.event.addListener(marker, 'click', function() {
				window.open(marker.url, '_blank');
			});
		}
	});

	jQuery(".map-link").live('click',function(e){
		$mapBoxes.hide();
		$mapBoxes.filter('.'+$(this).attr('data-map')).show();
		e.preventDefault();
		return false;
	});
});
