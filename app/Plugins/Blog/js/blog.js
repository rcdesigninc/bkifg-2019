// JavaScript Document
jQuery(document).ready(function($) {

	navWidth = $("#page-nav").width() + 40 + 90;
	newLeft = $("#posting-nav").width() / 2 - navWidth / 2;

	//$("#prev-postings").css('margin-left',newLeft);
	$("#posting-nav").css('visibility', 'visible');


	$("#prev-postings, #next-postings").hover(function() {
		$(this).find('img').toggle();
	}, function() {
		$(this).find('img').toggle();
	});
	$("#page-nav a").hover(function() {
		$(this).addClass('hover');
	}, function() {
		$(this).removeClass('hover');
	});

	$("object").width(500).height(420).wrap("<div align='center'></div>");
	$(".blog-posting img").aeImageResize({width: 500,height:380}).wrap("<div align='center'></div>");

	$("#comment-clear").live('click', function() {
		$(this).parent().parent().find('input[type=text], textarea').each(function(i) {
			$(this).val('').blur();
		});
		$(this).parent().parent().find('select').each(function() {
			document.getElementById(this.id).selectedIndex = 0;
		});
	});

	$("#comment-submit").click(function() {
		if (checkCommentForm()) {
			$.post("ajax-comment.php", $("#frmComments").serialize(), function(data) {
						if (data.success) {
							alert("Thank you for your comment!");
							$("#clear").click();
						}
						else
							alert("There was an error submitting your request, please try again.");
					}, "json");
		}
	});
});