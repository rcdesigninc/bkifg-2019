<?

require_once("_config.php");

LoadModel('Recaptcha');
$r = new Recaptcha();

$r->check($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

print $r->data->json->Encode();

/*
 * EOF
 */