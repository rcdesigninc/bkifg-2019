<?

include(dirname(__FILE__) . '/Recaptcha.lib.php');
include(dirname(__FILE__) . '/Recaptcha_data.php');

class Recaptcha extends Plugin {
	public $data = null;

	/**
	 * @param Slidegal_data $data
	 */
	public function __construct(Recaptcha_data $data = null) {
		if(!$data)
			$this->data = new Recaptcha_data();
		else
			$this->data = $data;

		$this->process();
		//die(print_array($this));
	}

	public function process() {
		System::$instance->addJs("
		var RecaptchaOptions = {
		   theme : '".Recaptcha_settings::Theme."'
		};
		");

		System::$instance->addCSS("
        #recaptcha_widget_div {
            margin-top: 10px;
        }
		");

	}

	/**
	 * @return string
	 */
	public function render() {
		return recaptcha_get_html(Recaptcha_settings::Public_key);
	}

	public function check($challange, $response) {
		$resp = recaptcha_check_answer(Recaptcha_settings::Private_key, $_SERVER["REMOTE_ADDR"],
		                               $challange, $response);
		if (!$resp->is_valid) {
			$this->data->json->Add('success',false);
			$this->data->json->Add('error_message',$resp->error);
		}
		else {
			$this->data->json->Add('success',true);
		}
	}
}

/*
 * EOF
 */