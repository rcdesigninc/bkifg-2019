<?

class Slidegal_data extends Plugin_data {
	public $width = 960;
	public $height = 407;
	public $speed = 9000;

	public $animation = Plugin_defaults::Slidegal_animation;
	public $show_nav = Plugin_defaults::Slidegal_show_nav;

	public $navigation = '';

	function __construct() {
		parent::__construct();
		$a = func_get_args();
		$i = func_num_args();

		$f = '__construct_';

		switch ($i) {
			case 1:
				$f .= 'slideshow';
				break;
			case 3:
				$f .= 'slideshow_basic';
				break;
			case 5:
				$f .= 'slideshow_animate';
				break;
			case 7:
				$f .= 'slideshow_full';
				break;

			case 2:
				$f .= 'gallery';
				break;
			case 4:
				$f .= 'gallery_basic';
				break;
			case 6:
				$f .= 'gallery_animate';
				break;
			case 8:
				$f .= 'gallery_full';
				break;
		}

		//die('trying to call '.$f);

		if (method_exists($this, $f)) {
			call_user_func_array(
				array
				($this,
				$f), $a);
		}

//		if (!$this->id) // Assigned generic ID, if no id was selected
//			$this->id = uniqid() . '-slidegal';
	}

	public function __construct_slideshow($entries) {
		$this->entries = $entries;
	}

	public function __construct_slideshow_basic($entries, $width, $height) {
		$this->entries = $entries;

		$this->width = $width;
		$this->height = $height;
	}

	public function __construct_slideshow_animate($entries, $width, $height, $speed, $animation) {
		$this->entries = $entries;

		$this->width = $width;
		$this->height = $height;
		$this->speed = $speed;
		$this->animation = $animation;
	}

	public function __construct_slideshow_full($id, $entries, $width, $height, $speed, $animation, $show_nav) {
		$this->entries = $entries;

		$this->id = $id;
		$this->width = $width;
		$this->height = $height;
		$this->speed = $speed;
		$this->animation = $animation;
		$this->show_nav = $show_nav;
	}

	public function __construct_gallery($entries, $navigation) {
		$this->entries = $entries;
		$this->navigation = $navigation;
	}

	public function __construct_gallery_basic($entries, $navigation, $width, $height) {
		$this->entries = $entries;
		$this->navigation = $navigation;

		$this->width = $width;
		$this->height = $height;
	}

	public function __construct_gallery_animate($id, $entries, $navigation, $width, $height, $speed, $animation) {

		$this->entries = $entries;
		$this->navigation = $navigation;

		$this->id = $id;
		$this->width = $width;
		$this->height = $height;
		$this->speed = $speed;
	}

	public function __construct_gallery_full($id, $entries, $navigation, $width, $height, $speed, $animation, $show_nav) {
		parent::__construct();

		$this->entries = $entries;
		$this->navigation = $navigation;

		$this->id = $id;
		$this->width = $width;
		$this->height = $height;
		$this->speed = $speed;
		$this->animation = $animation;
		$this->show_nav = $show_nav;
	}

	public function setJson() {
		$this->json->AddArray(
			array
			('id' => $this->id,
			'width' => $this->width,
			'height' => $this->height,
			'speed' => $this->speed,
			'animate' => $this->animation,
			'show_nav' => $this->show_nav,
			'customer_pager' => strlen($this->navigation) ? true : false));

//		$this->json->Add('id', $this->id);
//		$this->json->Add('width', $this->width);
//		$this->json->Add('height', $this->height);
//		$this->json->Add('speed', $this->speed);
//		$this->json->Add('animate', $this->animation);
//		$this->json->Add('show_nav', $this->show_nav);
//
//		if ($this->navigation)
//			$this->json->Add('custom_pager', true);

		return $this->json->Encode();
	}
}
