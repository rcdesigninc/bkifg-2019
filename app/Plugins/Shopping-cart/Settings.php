<?


/*
 * Canadian Taxes
 */
abstract class Taxes {
	const TAX_DEFAULT = 0.05;
	const TAX_BC = 0.12;
	const TAX_NB = 0.13;
	const TAX_NF = 0.13;
	const TAX_NS = 0.15;
	const TAX_ON = 0.13;
}

/*
 * EOF
 */