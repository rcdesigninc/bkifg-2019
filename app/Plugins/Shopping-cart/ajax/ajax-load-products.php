<?
require_once("_config.php");

$json = array();

LoadModel('Product');
LoadModel('Product_category');

$p = new Product();
$pc = new Product_category();

$pc = $pc->Select();

$where = '';
switch ($_POST['range']) {
	case 'UP TO $75':
		$where = 'price <= 75';
		break;
	case '$75 - $125':
		$where = 'price BETWEEN 75 AND 125';
		break;
	case '$125 - $175':
		$where = 'price BETWEEN 125 AND 175';
		break;
	case '$175 AND UP':
		$where = 'price >= 175';
		break;
}

$category = "All Products";
if($_POST['category']) {
	$category = $_POST['category'];

	if(strlen($where))
		$where .= " AND ";
	foreach($pc as $c)
		if($c->label == $category)
			$where .= "category_id = '$c->id'";
}
$content .= "<h2>$category</h2>";
$content .= "<a id='shop-top''>&nbsp;</a>";


// array("(SHOW ALL)" => "", "UP TO $75" => "<= 75", "$75 - $125" => "BETWEEN 75 AND 125", "$125 - $175" => "BETWEEN 125 AND 175", "$175 AND UP" => ">= 175",)) . '
$sort = 'order_by';
switch ($_POST['sort']) {
	case 'PRICE (HIGH TO LOW)':
		$sort = 'price DESC';
		break;
	case 'PRICE (LOW TO HIGH)':
		$sort = 'price ASC';
		break;
}
//array("(BEST SELLING)" => "", "PRICE (HIGH TO LOW)" => "DESC", "PRICE (LOW TO HIGH)" => "ASC")) . '

$products = $System->prepPagination("Product", $sort, $where, $_POST['per_page']);
//die($System->lastQuery);

$list = array();
foreach ($products as $x) {
	$list[] = $x;
}

if(count($list)) {
	foreach ($list as $product)
		$content .= $product->renderShopBox();
}
else
	$content = "No products where found to match the requested parameters, please try again.";



//$content .= $System->renderPagination();

$json['PAGINATE_COUNT'] = PAGINATE_COUNT;
$json['PAGINATE_FROM'] = PAGINATE_FROM;
$json['content'] = $content;

print json_encode($json);

/*
 *
 * EOF
 */