<?
include("_config.php");

$json = array();
$json['success'] = false;

if ($System->cart) {

	// Add or update the cart customers personal information
	//die(print_array($System->cart));
	$System->customer->Update($_POST);

	$_SESSION['cart'] = $System->cart->session;

	$System->cart->delivery_date = $_POST['delivery_date'];
	$System->cart->delivery_time = $_POST['delivery_time'];
	$System->cart->in_memory_of = $_POST['in_memory_of'];
	$System->cart->special_instructions = $_POST['special_instructions'];
	$System->cart->terms_confirmed = $_POST['terms_confirmed'] == 'yes' ? 1 : 0;
	$System->cart->Update();


	//die($System->lastQuery);

	$json['success'] = true;
}

// Push the json vars to back to the browser
print json_encode($json);

/*
 *
 * EOF
 */