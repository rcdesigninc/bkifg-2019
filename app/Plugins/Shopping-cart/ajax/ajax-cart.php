<?
include("_config.php");
LoadModel("Cart");
LoadModel("Cart_item");
LoadModel("Product");

$ci = new Cart_item();
$p = new Product();

$json = array();

// Retrieve the form vars
$action = $_POST['action'];
$product_id = $_POST['product_id'];
$cart_item_id = $_POST['cart_item_id'];
$quantity = $_POST['quantity'];

if ($action == "add" && isset($product_id) && isset($quantity)) {
	$p->Load($product_id);
	if ($p->id) {

		// Use this, if each cart item is it's own unique entry

		for($i = 0; $i < $quantity; $i++) {
			$ci = new Cart_item();
			$ci->cart_id = $System->cart->id;
			$ci->product_id = $product_id;
			$ci->quantity = 1;
			$ci->Insert();

			// Updated the calculated costs, to save real time calculations
			$ci->cost = $ci->quantity * $p->price;
			$ci->Update();
		}

		/*// Use this, if each cart item stacks on top of each other
		$ci = $ci->LoadWhere("`id` = '$product_id' AND `cart_id` = '".$System->cart->id."'");
		if (!$ci) {
			$ci = new Cart_item();
			$ci->cart_id = $System->cart->id;
			$ci->product_id = $product_id;
			$ci->quantity = $quantity;
			$ci->Insert();
		} else {
			$ci->quantity += $quantity;
		}

		// Updated the calculated costs, to save real time calculations
		$ci->cost = $ci->quantity * $p->price;
		$ci->Update();
		*/


	}
} else if ($action == "update" && isset($cart_item_id)) { //  && isset($quantity)
	$ci->Load($cart_item_id);
	//die(print_array($ci));
	if ($ci) {
		$p->Load($ci->product_id);

		// If quantity has been reduced to zero, remove the item completely
		/*if ($quantity == 0) {
			$ci->Delete();
		} else*/
		{
			$ci->giftcard = $_POST['giftcard'];
			$ci->from = $_POST['from'];

			/*// Updated the calculated costs, to save real time calculations
			$ci->quantity = $quantity;
			$ci->cost = $ci->quantity * $p->price;*/
			//$ci->quantity = 1;
			//$ci->cost = 1 * $p->price;

			//die(print_array($ci));
			$ci->Update();

			$json['cost'] = $ci->cost;
		}
	}
} else if ($action == "remove" && isset($cart_item_id)) {
	$json['removed'] = false;
	$ci->Load($cart_item_id);

	if ($ci) {
		$ci->Delete();
		$json['removed'] = true;
	}
} else if ($action == "empty") {
	$items = $ci->Select("*", "`cart_id` = '".$System->cart->id."'");
	foreach ($items as $i) $i->Delete();
}

// Update the current total of items within the cart
$items = $ci->Select("*", "`cart_id` = '".$System->cart->id."'");
$System->cart->quantity = $System->cart->subtotal = 0;
foreach ($items as $i) {
	$System->cart->quantity += $i->quantity;
	$System->cart->subtotal += $i->cost;
}
$System->cart->tax = round($System->cart->subtotal * 0.13,2);;
$System->cart->grand_total = round($System->cart->tax + $System->cart->subtotal,2);

// Update the cart's last accessed date, quantity, and total cost
$System->cart->last_accessed = date("Y-m-d");
$System->cart->Update();

// Add cart quantity, and total cost to the cookies & json
setcookie("cart_qty", $System->cart->quantity, 0, '/');
setcookie("cart_total", $System->cart->grand_total, 0, '/');

$json['cart_qty'] = $System->cart->quantity;
$json['subtotal'] = $System->cart->subtotal;
$json['tax'] = $System->cart->tax;
$json['grand_total'] = $System->cart->grand_total;

// Push the json vars to back to the browser
print json_encode($json);


/*
 *
 * EOF
 */