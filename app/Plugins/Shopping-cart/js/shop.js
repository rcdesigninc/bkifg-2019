// Shop functionality
jQuery(document).ready(function($) {
	$(".table-selection td").live("click", function() {
		PAGINATE_FROM = 1;
		$.showShop();
	});

	$(".add-to-cart").live("click", function() {
		$p = $(this).parent();
		$id = $p.find("input[name='id']").val();
		$quantity = $p.find("input[name='quantity']").val();
		$.post(AJAX_PATH + "ajax-cart.php", {
			action: "add"
			,product_id: $id
			,quantity: $quantity
		}, function(data) {
			$("#cart-count").val(data.cart_qty);
			
			//$p.find("input").val(1);
			
		}, "json");
	});

	/*$enlarge = $("<div class='enlarge'><img class='closed' src='/web/media/images/common/shop/enlarge.png' width='72'  height='41' alt='+'/><img class='open' src='/web/media/images/common/shop/enlarge-open.png' width='72'  height='41' alt='+'/></div>");

	$(".thumbnail").after($enlarge);*/

});

PAGINATE_FROM = 1;
PAGINATE_COUNT = 0;
PAGINATE_LOADING = false;

// showShop definition
(function($) {
	$(".page-nav-link").live('click',function(){
		PAGINATE_FROM = $(this).attr('id');
		$.showShop();
		return false;
	});
	$(".page-nav-link, #prev-postings, #next-postings").live('click',function(){
		scrollFrom($("#shop-top"));
	});

	$.showShop = function() {
		PAGINATE_LOADING = true;
		$(".preview-pane").remove();
		$.post(AJAX_PATH + "ajax-load-products.php", {
			per_page: 6
			,from: PAGINATE_FROM
			,range: $("#filter").val()
			,sort: $("#sort").val()
			,category: PAGINATE_CATEGORY
		}, function(data) {
			$(".preview-pane").remove();
			PAGINATE_FROM = data.PAGINATE_FROM;
			PAGINATE_COUNT = data.PAGINATE_COUNT;
			$("#shop-products").html(data.content);

			PAGINATE_LOADING = false;

			if (PAGINATE_COUNT > 1) {
				$content = "<div id='posting-nav'>";
				$content += "    <div id='prev-postings' class='page-buttons'><img src='/web/media/images/common/shop/arrow-right.png' alt='<' /> <b>PREV</b></div>";
				$content += "    <div id='page-nav'>";
				for (i = 1; i <= PAGINATE_COUNT; i++)
					$content += "    <a class='page-nav-link " + (PAGINATE_FROM == i ? "selected" : "") + "' id='" + i + "'>" + i + "</a>";
				$content += "    </div>";
				$content += "    <div id='next-postings' class='page-buttons'><b>NEXT</b> <img src='/web/media/images/common/shop/arrow-left.png' alt='>' /></div>";
				$content += "</div>";
				$("#posting-nav-container").html($content);

				navWidth = $("#page-nav").width()+120;
				newLeft = $("#posting-nav").width() / 2 - navWidth / 2;

				$("#prev-postings").css("margin-right",newLeft);
				$("#next-postings").css({"float":"right","margin-right":"10px"});
				$("#posting-nav").css("visibility","visible");
			}



			if (PAGINATE_FROM > 1) {
				$("#prev-postings").click(function() {
					if(!PAGINATE_LOADING) {
						debug("#prev-postings:click");
						//window.location = $("#posting-page-"+(PAGINATE_FROM - 1)).attr("href");
						PAGINATE_FROM--;
						$.showShop();
					}
				});
				$("#prev-postings").css("visibility", "visible");
			} else {
				$("#prev-postings").unbind("click");
				$("#prev-postings").css("visibility", "hidden");
			}

			if (PAGINATE_FROM < PAGINATE_COUNT) {
				$("#next-postings").click(function() {
					if(!PAGINATE_LOADING) {
						debug("#next-postings:click");
						//window.location = $("#posting-page-"+(PAGINATE_FROM + 1)).attr("href");
						PAGINATE_FROM++;
						$.showShop();
					}
				});
				$("#next-postings").css("visibility", "visible");
			} else {
				$("#next-postings").unbind("click");
				$("#next-postings").css("visibility", "hidden");
			}

			$("#page-nav a").hover(function() {
				$(this).addClass("hover");
			}, function() {
				$(this).removeClass("hover");
			});
			//imagePreview();
		}, "json");
	};
})(jQuery);