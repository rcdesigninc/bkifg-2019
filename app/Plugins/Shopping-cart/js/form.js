jQuery(document).ready(function($) {
	$(".submit").live("click", function() {

		debug("Submitting form for validation");
		//scrollFrom($("#form-top"));
		$form = $(this).closest('form');
		if ($form.validateForm()) {
			//$("#notice").fadeUpdate("<h4 class='p-callout'>Processing your request...</h4>");

			$.post(AJAX_PATH + "ajax-checkout.php", $form.serialize(), function(data) {
				if (data.success) {
					debug("Subitting form now!");
					// todo: see why traditional form submission isn't working
					//$form.submit();
					//return true;
					//alert("Go to paypal now");
					window.location = 'process_order.php';
					
				}
			}, "json");
		}
		return false;
	});


	$("input, textarea").focus(
			function() {
				if ($(this).val() == $(this).attr('title'))
					$(this).val('');
			}).blur(function() {
				if ($(this).val() == '')
					$(this).val($(this).attr('title'));
			});

	$("input").keypress(function (e) {
		if (e.which == 13)
			$(".submit").click();
	});

	function completeForm(success) {
		if (success == true) {
			$("form").slideUp();
			$(".clear-form").click();
			$("#notice").fadeUpdate("<h4 class='p-callout'>Your message has been received, thank you!</h4>").css({'margin-top':'0px'});
			scrollFrom($("#page-top"));
		} else
			$("#notice").fadeUpdate("<h4 class='p-callout'>There was an error submitting your response, please try again.</h4>");
	}

	var defaultBorder = '2px solid #C4B7E6';
	var errorBorder = '2px solid #ff0000';

	$("input[type!='image'][type!='checkbox'], select, textarea").live('focus blur',function(){
		$(this).css({border:defaultBorder});
	})

	$.fn.validateForm = function() {
		var $this = $(this[0]);
		var errors = false;
		var $focus = null;

		$('.billing-info .required').each(function() {
			//debug("Checking validity of: "+$(this).attr('name'));
			var $x = $(this);
			$x.css({'border':defaultBorder});

			if ($x.attr('name') == 'email') {
				if (!validateEmail($x.val())) {
					$x.css({'border':errorBorder});
					errors = true;
				}
			} else if ($x.attr('name') == 'phone') {
				if (!validatePhoneNumber($x.val())) {
					$x.css({'border':errorBorder});
					errors = true;
				}
			} else if ($x.is("input[name*='postal']")) {
				if (!validPostalCode($x.val())) {
					$x.css({'border':errorBorder});
					errors = true;
				}
			} else if ($x.is('.required')) {
				if ($x.val() == $x.attr('title') || $x.val() == '') {
					$x.css({'border':errorBorder});
					errors = true;
				}
			}

			if (errors && !$focus)
				$focus = $x;
		});
		$x = $('select[name="billing_province"]');
		if($x.val() == 'Select an option') {
			$x.css({'border':errorBorder});
			errors = true;
			if (errors && !$focus)
				$focus = $x;
		}
		if (!$("input[name='ship_to_rr']").is(':checked')) {
			$('.shipping-info .required').each(function() {
				//debug("Checking validity of: "+$(this).attr('name'));
				var $x = $(this);
				$x.css({'border':defaultBorder});

				if ($x.is("input[name*='postal']")) {
					if (!validPostalCode($x.val())) {
						$x.css({'border':errorBorder});
						errors = true;
					}
				} else if ($x.val() == $x.attr('title') || $x.val() == '') {
					$x.css({'border':errorBorder});
					errors = true;
				}

				if (errors && !$focus)
					$focus = $x;
			});
			$x = $('select[name="shipping_province"]');
			if($x.val() == 'Select an option') {
				$x.css({'border':errorBorder});
				errors = true;
				if (errors && !$focus)
					$focus = $x;
			}
		}
		else { // Ensure that the cart is shipping to RR to allow this field to validate
			$x = $('input[name="in_memory_of"]');
			if($x.val() == '') {
				$x.css({'border':errorBorder});
				errors = true;
				if (errors && !$focus)
					$focus = $x;
			}
		}
		$x = $('input[name="delivery_date"]');
		if($x.val() == '') {
			$x.css({'border':errorBorder});
			errors = true;
			if (errors && !$focus)
				$focus = $x;
		}
		$x = $('input[name="delivery_time"]');
		if($x.val() == '') {
			$x.css({'border':errorBorder});
			errors = true;
			if (errors && !$focus)
				$focus = $x;
		}


		$x = $('input[name="terms_consent"]');
		if (!errors &&  !$x.is(':checked')) {
			$(".terms-dialog").openPopup();
			return false;
		}

		if (errors) {
			$(".close").live('click',function(){
				scrollFrom($("h2:first"));
				return true;
			});

			$(".errors-dialog").openPopup();
			return false;
		}

		return true;
	};

	$.fn.fadeUpdate = function(html) {
		$this = $(this[0]);
		$this.stop(true, true).fadeOut("fast", function() {
			$this.html(html).fadeIn();
		});
		return $this;
	};
});

function validateEmail(email) {
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
	return emailPattern.test(email);

}

function validatePhoneNumber(elementValue) {
	var phoneNumberPattern = /^\(?(\d{1})?\)?[-\. ]?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/;
	return phoneNumberPattern.test(elementValue);
}

function validPostalCode(pcode) {
	var postalPattern = /^([0-9]{5})|(\s*[a-ceghj-npr-tvxy]\d[a-ceghj-npr-tv-z](\s)?\d[a-ceghj-npr-tv-z]\d\s*)$/i;
	var postalRegExp = new RegExp(postalPattern);

	debug('postal checking');

	if (postalRegExp.test(pcode) == false)
		return false;

	debug('postal is good');
	return true;
}