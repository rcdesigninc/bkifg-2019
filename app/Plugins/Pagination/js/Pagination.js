RC_PAGINATE_AJAX = PLUGIN_PATH + 'Pagination/ajax/Pagination.php';
(function($) {
	$.fn.RC_Pagination = function(options, callback) {
		if ($.isFunction(options)) {
			callback = options;
			options = null;
		}
		o = $.extend({}, $.fn.RC_Pagination.defaults, options);

		var $container = $(this);

		// Local global vars, so allow for effectively multiple pagination controls on a single page
		var from = o.from;
		var count = o.count;
		var PAGINATE_LOADING = false;

		// Ensure that we are at least scrolling back up to the container tag
		if (!o.scrollTo)
			o.scrollTo = $container;

		$(".page-nav-link").live('click', function() {
			from = $(this).attr('id');

			if (!PAGINATE_LOADING)
				callAjax();
			return false;
		});
		$(".page-nav-link, #prev-postings, #next-postings").live('click', function() {
			debug("pagination::Attempt to AutoScroll");
			startScroll(o.scrollTo);
			return false;
		});

		$("#prev-postings").live('click', function() {
			debug("pagination::Adding prev-posting:cilck");
			if (!PAGINATE_LOADING) {
				from--;
				callAjax();
			}
			return false;
		});

		$("#next-postings").live('click', function() {
			if (!PAGINATE_LOADING) {
				from++;
				callAjax();
			}
			return false;
		});

		$("#page-nav a").live('mouseover', function() {
			$(this).toggleClass("hover", true);
		});
		$("#page-nav a").live('mouseleave', function() {
			$(this).toggleClass("hover", false);
		});

		return this.each(function() {
			// Assume first load has taken place server side, unless enabled don't auto load
			//if (o.autoLoad)

			// pre-process
			processEntries();

			callAjax();

			return $container;
		});

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// HELP FUNCTIONS - PRIVATE
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function callAjax() {
			PAGINATE_LOADING = true;

			var postJson = {
				method: o.method
				,limit: o.limit
				,from: from
				,order_by: o.order_by
				,filter: o.filter
				/*
				 ,range: $("#filter").val()
				 ,sort: $("#sort").val()
				 ,category: PAGINATE_CATEGORY*/
			};

			debug("processEntries::callAjax - " + JSON.stringify(postJson));
			$.post(RC_PAGINATE_AJAX, postJson, function(data) {
				processEntries(data);
			}, "json");

			$.isFunction(callback) && callback();
		}

		function processEntries(data) {

			if (data) {
				from = data.from;
				count = data.count;
				$container.html(data.content);
			}

			debug("Pagination::processEntries - from = " + from + ", count = " + count);

			PAGINATE_LOADING = false;

			if (count > 1) {
				$content = "<div id='posting-nav'>";
				$content += "    <div id='prev-postings' class='page-buttons'><b>PREV</b></div>";
				$content += "    <div id='page-nav'>";
				for (i = 1; i <= count; i++)
					$content += "    <a class='page-nav-link " + (from == i ? "selected" : "") + "' id='" + i + "'>" + i + "</a>";
				$content += "    </div>";
				$content += "    <div id='next-postings' class='page-buttons'><b>NEXT</b></div>";
				$content += "</div>";
				$("#posting-nav-container").html($content);

				navWidth = $("#page-nav").width() + 120;
				newLeft = $("#posting-nav").width() / 2 - navWidth / 2;

				$("#prev-postings").css("margin-right", newLeft);
				$("#next-postings").css({"float":"right","margin-right":"10px"});
				$("#posting-nav").css("visibility", "visible");
			}

			$("#prev-postings").toggleClass("invisible", !(from > 1));
			$("#next-postings").toggleClass("invisible", !(from < count));
			$("#page-nav").toggleClass("invisible", (count < 1));

			$container.addLinkIcons(linkIcons);
		}
	};

	$.fn.RC_Pagination.defaults = {
		method: ''
		,order_by: ''
		,entries_per_page: 2
		,filter: ''
		,scrollTo : null
		,from: 1
		,count: 0
		//,autoLoad : true
	};
})(jQuery);