<?

class Pagination_data extends Plugin_data {
	public $model = null;
	public $where = '';
	public $order_by = 'id'; // safety back up, in case no ordering is specified
	public $limit = 10;

	public function __construct(Model $model, $where = '', $order_by = '', $limit = 0) {
		parent::__construct();
		$this->model = $model;

		// Check if any post variables affect
		if ($where)
			$this->where = $where;

		if ($order_by)
			$this->order_by = $order_by;
		else if (isset($_POST['order_by']))
			$this->order_by = $_POST['order_by'];

		if ($limit)
			$this->limit = $limit;
		else if (isset($_POST['limit']))
			$this->limit = $_POST['limit'];

		//die("  $this->order_by = $_POST[order_by];else if($order_by) ".print_array($this));
	}

	public function setJson() {
		$this->json->AddArray(array(
				'method'=>$this->model->_className,
				'limit'=>$this->limit,
				'order_by'=>$this->order_by
		                      ));

		return $this->json->Encode();
	}
}