<?

include(dirname(__FILE__) . '/Pagination_data.php');

interface iPagination {
	public static function paginationProcess();

	public static function paginationJson();

	public static function paginationRender();
}


/*
 * Sample calls
 *
 * Method 1
$data = new Pagination_data($this);
$data->where = "`page_id` = '" . $_SESSION['page_id'] . "' AND `template_section_id` = '".Columns::Content_left."'";
$data->order_by = 'date, heading';
$data->limit = 10;
$p = new Pagination($data);
*
* Method 2
$p = new Pagination(new Pagination_data($this, "`page_id` = '" . $_SESSION['page_id'] . "' AND `template_section_id` = '".Columns::Content_left."'",'date, heading'));
*/

class Pagination extends Plugin {
	public $data = null;

	public function __construct(Pagination_data $data) {
		$this->data = $data;
		$this->process();

		//die(print_array($this));
	}

	public function process() {
		$from = 1;
		if ($_GET['from'])
			$from = (int)$_GET['from'];
		else if ($_POST['from'])
			$from = (int)$_POST['from'];
		//define("from", $from);
		$this->data->json->Add('from', $from);

		$count = $this->data->model->Count($this->data->where);
		//$this->data->json->Add('sql',System::$DB->lastQuery());
		$count = ceil($count / $this->data->limit);
		//define("count", $count);
		$this->data->json->Add('count', $count);

		$order_by = $this->data->order_by;
		$limit = (($from - 1) * $this->data->limit) . ", " . $this->data->limit;

		$this->data->entries = $this->data->model->Select("*", $this->data->where, $order_by, $limit);

		System::$instance->addJquery('$("#' . $this->data->id . '").RC_Pagination('.$this->data->setJson().');');
	}

	public function render() {
		$html = "<div id='{$this->data->id}'>";
		$html .= $this->data->json->Val('content');
		$html .= "</div>";
		$html .= $this->renderNavigation(false);
		return $html;
	}

	private function renderNavigation($useJq = true) {
		$content = '
		<div class="spacer"></div>';
		if ($this->data->json->Val('count') > 1) {
			$page_links =
					array
					();
			for ($i = 1; $i <= $this->data->json->Val('count'); $i++) {
				$page_links[] = "<a id='posting-page-$i' href='?from=$i' " . ($this->data->json->Val('from') == $i
						? 'class="selected"' : '') . ">$i</a>";
			}
			$content .= '
			<div id="posting-nav-container">
				<div id="posting-nav">
					<div id="prev-postings">
						<b>PREV</b>
					</div>
					';
			$content .= "
					<div id='page-nav'>" . join(", ", $page_links) . "</div>
					";
			$content .= '
					<div id="next-postings">
						<b>NEXT</b>
					</div>
				</div>
			</div>
			<div style="clear: both;"></div>';
		}
		if ($useJq) {
			System::$instance->addJquery('
			' . ($this->data->json->Val('from') > 1 ? '
			$("#prev-postings").click(function(){
				debug("#prev-postings:click");
				window.location = $("#posting-page-' . ($this->data->json->Val('from') - 1) . '").attr("href");
			});' : '
			$("#prev-postings").css("visibility","hidden");
			') . '

			' . ($this->data->json->Val('from') < $this->data->json->Val('count') ? '
			$("#next-postings").click(function(){
				debug("#next-postings:click");
				window.location = $("#posting-page-' . ($this->data->json->Val('from') + 1) . '").attr("href");
			});' : '
			$("#next-postings").css("visibility","hidden");
			') . '

			navWidth = $("#page-nav").width()+40+90;
			newLeft = $("#posting-nav").width() / 2 - navWidth / 2;

			$("#prev-postings").css("margin-right",newLeft);
			$("#next-postings").css({"float":"right","margin-right":"10px"});
			$("#posting-nav").css("visibility","visible");

			$("#page-nav a").hover(function(){
				$(this).addClass("hover");
			},function(){
				$(this).removeClass("hover");
			});
		');
		}
		return $content;
	}
}

/*
 * EOF
 */