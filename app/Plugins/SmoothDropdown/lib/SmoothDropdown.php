<?

include(dirname(__FILE__) . '/SmoothDropdown_data.php');

class SmoothDropdown extends Plugin {
	public $data = null;

	/**
	 * @param Slidegal_data $data
	 */
	public function __construct(SmoothDropdown_data $data) {
		$this->data = $data;
		$this->process();
		//die(print_array($this));
	}

	public function process() {
	}

	final public function renderTableSelection($id, $message, $values) {
		return $this->render();
	}

	/**
	 * @return string
	 */
	public function render() {
		//$vals = array_keys($values);
		// $vals[0]
		$str = '
		<div style="float:left; width: 300px;">
		<label style="float:left; margin-right: 10px;">' . $message . '</label>
		<input name="' . $id . '" id="' . $id . '" type="hidden"/>
		 <div id="' . $id . '-SmoothDropdown" class="SmoothDropdown">
		  <div id="' . $id . '-SmoothDropdown-placeholder" class="SmoothDropdown-placeholder">' . $values[0] . '</div>
		  <table id="' . $id . '-SmoothDropdown-table" class="SmoothDropdown-table">';
		foreach ($values as $label) { // => $values
			$str .= '
			  <tr>
				<td data-value="' . $label . '" data-target="' . $id . '" class="">' . $label . '</td>
			  </tr>';
		}
		$str .= '
		  </table>
		</div>
		 </div>
		';
		return $str;
	}

	public static function renderDropdown($id, $message, $optionsModel) {
		global $zDex;

		LoadModel($optionsModel);
		$c = new $optionsModel();
		$vals = $c->Select();
		$options = array();
		foreach($vals as $v) {
			$options[] = $v->label;
		}

		if (!$zDex)
			$zDex = 999;

		$zDex -= 10;
		$z = " style=\"z-index: $zDex;\"";
		$html = <<< EOB
<div id="$id-SmoothDropdown" class="SmoothDropdown" $z>
	<input type="hidden" name="$id" id="$id"/>
	<div class="SmoothDropdown-image"></div>
	<div id="$id-SmoothDropdown-placeholder" class="SmoothDropdown-placeholder" $z title="$message"><i>$message</i></div>
	<table id="$id-SmoothDropdown-table" class="SmoothDropdown-table" $z>
		<thead>
			<tr>
				<td>
					<i>$message</i>
				</td>
			</tr>
		</thead>
		<tbody>
EOB;
		foreach ($options as $o) {
			$html .= <<< EOB
		<tr>
			<td data-target="$id">$o</td>
		</tr>
EOB;
		}
		$html .= <<< EOB
		</tbody>
	</table>
</div>
EOB;
		return $html;
	}

	public static function renderCheckboxes($id, $message, $options = array(), $radio = false) {
		//	global $zDex;
		//	$zDex -= 10;

		$type = 'checkbox';
		if ($radio)
			$type = 'radio';

		//	$z = " style=\"z-index: $zDex;\"";
		print <<< EOB

<div class="checkbox-list">
	<label>{$id}. $message</label><br clear="all"/>
EOB;
		foreach ($options as $o) {

			if ($o == 'Other')
				print <<< EOB
<input name="q{$id}_other_check" type="$type"/> <input name="q{$id}_other" type="text" title="Other" value="Other" maxlength="50" disabled="disabled"/>
EOB;
			else


				print <<< EOB
<div><input name="q{$id}[]" type="$type" value="$o"/>$o</div>

EOB;
		}
		print <<< EOB
<br clear="all"/>
</div>
EOB;
	}
/*
printDropdown('heard_about', 'How did you hear about the contest?', array(
	'Direct Mail Piece',
	'Snap Advertisement',
	'Era Advertisement',
	'Hempen Jewellers Website',
	'Referral'
));
printCheckboxes('1', Contest_questions::q1,
array('Yourself', 'Family Members', 'Significant Other', 'Other'));

 */
}

/*
 * EOF
 */