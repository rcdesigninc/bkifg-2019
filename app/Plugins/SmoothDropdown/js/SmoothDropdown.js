var currentTsHover = false;

jQuery(document).ready(function($) {

	var tsIndex = 9999;
	var tsCount = $(".SmoothDropdown").length;

	if (tsCount) {
		$(".SmoothDropdown").each(function(i) {
			var $this = $(this);
			var $table = $this.find('table');

			//$this.css('z-index',tsIndex);
			$this.parent().parent().css('z-index',tsIndex);
			$this.find('.SmoothDropdown-image').css('z-index',tsIndex+1);
			//$table.css('z-index',tsIndex);

			var $placeholder = $this.find('.SmoothDropdown-placeholder');
			var $input = $this.find('input');
			//$table.css('top','-'+$table.height()+'px');
			$table.css('top', '0');
			$table.find('td').width($table.innerWidth());

			$this/*			.click(function() {
			 $(".SmoothDropdown table").stop(true, true);
			 $table.fadeIn();
			 })*/.hover(function() {
				$(".SmoothDropdown table").stop(true, true);
				$table.fadeIn();
			}, function() {
				$(".SmoothDropdown table").stop(true, true);
				$table.hide();
			});
			$this.find("tbody td").click(function() {
				var value = $(this).text();

				$input.val(value);
				$placeholder.html(value);

//				if ($placeholder.text() == $placeholder.attr('title'))
//					$placeholder.wrapInner('<i></i>');

				$table.fadeOut('slow');
			});

			tsIndex--;
		});
	}
});