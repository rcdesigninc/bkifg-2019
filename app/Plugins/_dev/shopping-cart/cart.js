jQuery(document).ready(function($) {
	$(".cart tbody tr td:first-child").css({'padding-left':'20px'});
			//.find("label").after("<br/>").end().find(".tooltip").css({'background':'#fff'});

	// Use if cart items are stacking ontop of eachother
	// $(".cart tbody tr td:not(:first-child)").css({"text-align":"center"});

	// Use if cart items are displayed uniquely
	$(".cart tbody tr td:not(:first-child) p").css({"margin-left":"23px"});

	$(".cart tbody tr td:first-child, .cart tbody tr td:last-child").css({'z-index':'99'});

	$('.example').prev().css({'margin-bottom':'0'});

	$(".remove").live('mouseover mouseout',function(){
		$(this).find('img').toggle();
	}).click(function() {
		return $(this).deleteConfirm();
	});

	$(".quantity input").live("focus", function() {
		$(this).attr('title', $(this).val());
	});

	$lastInput = null;
	$(".cart input").live("change, blur", function() {
		$this = $(this);
		$parent = $this.closest("tr");
		$lastInput = $this;
		if ($this.is('[name="quantity"]') && $this.val() == "0") {
			$this.deleteConfirm();
		} else {
			$.post(AJAX_PATH + "ajax-cart.php", {
				action:"update"
				,cart_item_id: $parent.attr("data-id")
				,quantity: $parent.find('input[name="quantity"]').val()
				,giftcard: $parent.find('input[name="giftcard"]').val()
				,from: $parent.find('input[name="from"]').val()
			}, function(data) {
				$parent.find('.cost span').html(data.cost);
				$.updateTotals(data);
			}, "json");
		}
	});

	$.resetCartCells = function() {
		$(".cart tbody tr:even").css("background-color", "#fff");
		$(".cart tbody tr:odd").css("background-color", "#faf9ee");
	};
	$.resetCartCells();

	var $lastParent = null;

	$.fn.deleteConfirm = function(){
		$this = $(this[0]);
		$lastParent = $this.closest("tr");
		$('.cart').next().clone(true).insertAfter('.cart');
		$('.cart').next().openPopup();
		return false;
	};

	$('.close').live('click',function(){
		if($lastInput)
			if($lastInput.is('[name="quantity"]') && $lastInput.val() == "0")
				$lastInput.val($this.attr('title'));
	});

	$('.confirm-delete').live('click',function(){
		$(this).closest("div").find('.close').click();
		debug("deleteConfirm::running");
		{
			debug("deleteConfirm::accepted");
			$.post(AJAX_PATH + "ajax-cart.php", {
				action:"remove"
				,cart_item_id: $lastParent.attr("data-id")
			}, function(data) {
				if (data.removed)
					$lastParent.remove();
				$.resetCartCells();
				$.updateTotals(data);
			}, "json");
			return true;
		}
		debug("deleteConfirm::cancelled");
	});

	$.updateTotals = function(data) {
		$(".cart-totals .subtotal").html(data.subtotal);
		$(".cart-totals .tax").html(data.tax);
		$(".cart-totals .grand_total").html(data.grand_total);
	}
});