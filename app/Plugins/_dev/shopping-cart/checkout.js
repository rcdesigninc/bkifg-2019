jQuery(document).ready(function($) {
	$("label").wrap("<div class='wrapper'></div>");
	$("input:not(:checkbox)").prev().find("label").append(":");

	$('.ajaxSelect').each(function(i) {
		$(this).load(AJAX_PATH + 'ajax-load-select.php', {
			table: $(this).attr('table'),
			show: $(this).attr('show'),
			filter: $(this).attr('filter'),
			selected: $(this).attr('data-selected')
		});
	});

	$('.datefield').css('cursor', 'pointer').datepicker({
		minDate: new Date(),
		changeMonth: false,
		changeYear: false,
		showOtherMonths: true,
		selectOtherMonths: false,
		showOn: 'both',
		buttonImage: "/web/js/jquery/ui/calendar.gif",
		buttonImageOnly: true,
		buttonText: 'datepickerImage',
		autoSize: true,
		beforeShow: function() {
			$('#ui-datepicker-div').maxZIndex();
		},
		dateFormat: 'yy-mm-dd',
		beforeShow: function(input, inst) {
			inst.dpDiv.css({marginTop: -input.offsetHeight + 'px', marginLeft: $(input).attr('data-offset-x') + 'px'});
		}
		//,beforeShowDay: stripDays
	});
	$('img[title=datepickerImage]').css({'cursor':'pointer', 'margin-left':'10px','vertical-align':'middle'});


	$.maxZIndex = $.fn.maxZIndex = function(opt) {
		/// <summary>
		/// Returns the max zOrder in the document (no parameter)
		/// Sets max zOrder by passing a non-zero number
		/// which gets added to the highest zOrder.
		/// </summary>
		/// <param name="opt" type="object">
		/// inc: increment value,
		/// group: selector for zIndex elements to find max for
		/// </param>
		/// <returns type="jQuery" />
		var def = { inc: 10, group: "*" };
		$.extend(def, opt);
		var zmax = 0;
		$(def.group).each(function() {
			var cur = parseInt($(this).css('z-index'));
			zmax = cur > zmax ? cur : zmax;
		});
		if (!this.jquery)
			return zmax;

		return this.each(function() {
			zmax += def.inc;
			$(this).css("z-index", zmax);
		});
	}

	$("input[name='terms_consent'], input[name='ship_to_rr']").each(function() {
		$old = $(this).prev().clone();
		$(this).prev().remove();
		$(this).css({
			'width':'20px'
			,float:'left'
			,'margin-right':'10px'
			,'margin-left':'128px'
		});
		$old.css({
			float: 'left'
			,'margin-right': '0'
			,width: '422px'
			,'text-align':'left'
		});
		$(this).after($old);
	});
	$("input[name='terms_consent']").css({'margin-left':'0'});


	$("input[name='ship_to_rr']").live('click, blur, change', function() {
		//alert("Check box is "+$(this).is(':checked'))
		if ($(this).is(':checked')) {
			$('input[name="in_memory_of"]').blur().show().prev().show();
			$(".shipping-info").hide().find('input, select').blur();//.find('input').val('');
		} else {
			$('input[name="in_memory_of"]').blur().hide().val('').prev().hide();
			$(".shipping-info").show();
		}
	});
	$("input[name='ship_to_rr']").change();

	$(".continue-shopping").unbind('click').click(function() {
		$form = $(this).closest('form');
		$.post(AJAX_PATH + "ajax-checkout.php", $form.serialize(), function(data) {
			/*if (data.success) {
			 }*/
			window.location = "shop.php";
		}, "json");
	});
});

function stripDays(date) {
	var d = new Date(); // todays date
	/*if(date.getDay() == 0 || date.getDay() == 6) // Discard weekends
	 return [false, ''];*/

	if ((date.getFullYear() < d.getFullYear()) || (date.getFullYear() == d.getFullYear() && date.getMonth() < d.getMonth()) || (date.getFullYear() == d.getFullYear() && date.getMonth() == d.getMonth() && date.getDate() < d.getDate())) {
		return [false, ''];
	}

	return [true, ''];

}