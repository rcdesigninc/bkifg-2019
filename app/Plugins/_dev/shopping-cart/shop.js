// Shop functionality
jQuery(document).ready(function($) {
	$(".table-selection td").live("click", function() {
		PAGINATE_FROM = 1;
		$.showShop();
	});

	$(".add-to-cart").live("click", function() {
		$p = $(this).parent();
		$id = $p.find("input[name='id']").val();
		$quantity = $p.find("input[name='quantity']").val();
		$.post(AJAX_PATH + "ajax-cart.php", {
			action: "add"
			,product_id: $id
			,quantity: $quantity
		}, function(data) {
			$("#cart-count").val(data.cart_qty);
			
			//$p.find("input").val(1);
			
		}, "json");
	});

	/*$enlarge = $("<div class='enlarge'><img class='closed' src='/web/media/images/common/shop/enlarge.png' width='72'  height='41' alt='+'/><img class='open' src='/web/media/images/common/shop/enlarge-open.png' width='72'  height='41' alt='+'/></div>");

	$(".thumbnail").after($enlarge);*/
});