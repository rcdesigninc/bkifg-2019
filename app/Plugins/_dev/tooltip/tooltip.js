/*
 * Image preview script 
 * powered by jQuery (http://www.jquery.com)
 * 
 * written by Alen Grakalic (http://cssglobe.com)
 * 
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 *
 */

function imagePreview() {
	(function($) {
		/* CONFIG */

		xOffset = 40;
		yOffset = 0;

		// these 2 variable determine popup's distance from the cursor
		// you might want to adjust to get the right result

		/* END CONFIG */
		$(".image-preview").live('mouseenter',function(e) {
			this.t = this.title;
			this.title = "";
			var c = (this.t != "") ? "<br/>" + this.t : "";
			if(!$('.preview-pane').length)
				$(this).after("<div class='preview-pane'><img class='preview-image' src='" + $(this).attr('data-image') + "' alt='Image preview' />" + c + "</div>");
			//yOffset = -1 * $(".preview-image").height();
			yOffset = -1 * $(window).height();
			$(".preview-pane").css("top", (e.pageY + yOffset) + "px").css("left", (e.pageX + xOffset) + "px").fadeIn("fast");
		});
		$(".image-preview, .preview-pane").live('mousemove',function(e) {
			//yOffset = -1 * $(".preview-image").height();
			yOffset = -1 * $(window).height();
			$(".preview-pane").css("top", (e.pageY + yOffset) + "px").css("left", (e.pageX + xOffset) + "px");
			//debug("Preview pos =  y "+(e.pageY + yOffset)+", x "+(e.pageX + xOffset)+";  Mouse pos = "+(e.pageY)+", x "+(e.pageX))
		});
		$(".image-preview").live('mouseleave',function(e) {
			this.title = this.t;
			$(".preview-pane").remove();
		});
	})(jQuery);
};

jQuery(document).ready(function($){
	imagePreview();
});