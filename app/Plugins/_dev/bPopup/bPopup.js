/******************************************************************************************************************
 * @name: bPopup
 * @type: jQuery
 * @author: Bjoern Klinggaard (http://dinbror.dk/bpopup)
 * @version: 0.3.6
 * @requires jQuery 1.3
 *
 * DEFAULT VALUES:
 * amsl(Above Mean Sea Level): 150px // Vertical distance from the middle of the window, + = above, - = under
 * appendTo: 'body' // Which element the popup should append to (append to 'form' when ASP.net)
 * closeClass: 'bClose' // Class to bind the close event to
 * escClose: true // Close on esc
 * fadeSpeed: 250 // Animation speed on fadeIn/out
 * follow: true // Should the popup follow the screen on scroll/resize? 
 * followSpeed: 500 // Animation speed for the popup on scroll/resize
 * loadUrl: null // External page or selection to load in popup
 * modal: true // Modal overlay
 * modalColor: #000 // Modal overlay color
 * overlay: 0.5 // Transparency, from 0.1 to 1.0 (filled)
 * scrollBar: true // Scrollbars visible
 * vStart: null // Vertical start position for popup
 * xLink: false // Xlink popup
 * zIndex: 9999 // Popup z-index, modal overlay = popup z-index - 1
 *
 * TOD: REFACTOR CODE!!! easing
 *******************************************************************************************************************/ 
modali = 1;
;(function($) {
  $.fn.openPopup = function(options, callback) {
    if($.isFunction(options)) {
        callback = options;
        options = null;
    }
    o = $.extend({}, $.fn.openPopup.defaults, options); 
    
    //HIDE SCROLLBAR?  
    if(!o.scrollBar)  {
        $('html').css('overflow', 'hidden');
    }

    var $selector = $(this),
		modalId = "#bModal"+modali,
        $modal = $('<div id="bModal'+modali+'" class="bModal"></div>'),
        d = $(document),
        w = $(window),
        cp = getCenterPosition($selector, o.amsl),
        vPos = cp[0],
        hPos = cp[1],
        isIE6 = $.browser.msie && parseInt($.browser.version) == 6 && typeof window['XMLHttpRequest'] != 'object'; // browser sniffing is bad
		modali++;
        
    return this.each(function() {  
          // MODAL OVERLAY
		  
		  // safty check, to ensure not other modal overlays are still present...
		  $(".bModal").remove();
		  
          if(o.modal) {
             $modal
                .css(getModalStyle())
                .appendTo(o.appendTo)   
                .animate({'opacity': o.opacity}, o.fadeSpeed);
          }   
          // OPEN POPUP  
          create();
    }); 
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // HELP FUNCTIONS - PRIVATE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function create() {
        var hasInputField = $('input[type=text]', $selector).length != 0;
        var t = o.vStart != null ? o.vStart : d.scrollTop() + vPos;
        if(o.xLink) {
            //Not generic yet
            $('a#bContinue')
                .attr({'href': $('a.xlink').attr('href')});
            $('a#bContinue .btnLink') 
                .text($('a.xlink').attr('title'))
        }  
        $selector
            .css( {'left': d.scrollLeft() + hPos, 'position': 'absolute', 'top': t, 'z-index': o.zIndex } )
            .appendTo(o.appendTo)
            .hide(function(){
                if(hasInputField) {
                    // Resets input fields
                    $selector.each(function() {
                        //$selector.find('input[type=text]').val('');    
                    });
                } 
                if(o.loadUrl != null) 
                    $selector.load(o.loadUrl);                             
            })
            .fadeIn(o.fadeSpeed, function(){
                if(hasInputField) {
                    //$selector.find('input[type=text]:first').focus();
                } 
                // Triggering the callback if set    
                $.isFunction(callback) && callback();          
            });
	    $selector.css("opacity","1.0");
        //BIND EVENTS
        bindEvents(); 
    }
    function close() { 
        if(o.modal) {
            $(modalId)   
                .fadeOut(o.fadeSpeed, function(){
                    $(modalId)
                        .remove();
                });  
        }
        $selector.fadeOut(o.fadeSpeed, function(){
            if(o.loadUrl != null) 
                $selector.empty();
        });  
        unbindEvents();
    }
    function getModalStyle() {
        if(isIE6) {
            var dd = getDocumentDimensions();
            return {'background-color': o.modalColor,'height': dd[0], 'left': getDistanceToBodyFromLeft(), 'opacity': 0, 'position': 'absolute', 'top': 0, 'width': dd[1], 'z-index': o.zIndex - 1};
        }
        else
            return {'background-color': o.modalColor,'height': '100%', 'left': 0, 'opacity': 0, 'position': 'fixed', 'top': 0, 'width': '100%', 'z-index': o.zIndex - 1};     
    }
    function bindEvents() {
       $('.' + o.closeClass).live('click', function(){
            close();
            return false;
       });
       if(o.follow) {
           w.bind('scroll.bPopup', function() { 
                $selector
                   .stop()
                   .animate({'left': d.scrollLeft() + hPos, 'top': d.scrollTop() + vPos }, o.followSpeed);
           })
           .bind('resize.bPopup', function() {
                // MODAL OVERLAY IE6
                if(o.modal && isIE6) {
                    var dd = getDocumentDimensions(); 
                    $modal
                        .css({ 'height': dd[0], 'width': dd[1], 'left': getDistanceToBodyFromLeft() });
                }
                // POPUP
                var pos = getCenterPosition($selector, o.amsl);
                vPos = pos[0];
                hPos = pos[1];
                $selector
                    .stop()
                    .animate({'left': d.scrollLeft() + hPos, 'top': d.scrollTop() + vPos }, o.followSpeed);               
           });
       } 
       if(o.escClose) {
           d.bind('keydown.bPopup', function(e) {
                if(e.which == 27) {  //escape
                    close();
                }
           });  
       }   
    }
    function unbindEvents() {
        if(!o.scrollBar)  {
            $('html').css('overflow', 'auto');
        }
        $('.' + o.closeClass).die('click');
        d.unbind('keydown.bPopup');
        w.unbind('.bPopup');
    }
    function getDocumentDimensions() {
        return [d.height(), d.width()];
    }	
    function getDistanceToBodyFromLeft() {
        return (w.width() < $('body').width()) ? 0 : ($('body').width() - w.width()) / 2;
    }
    function getCenterPosition(s, a) {
        var vertical = ((w.height() - s.height()) / 2) - a;
        var horizontal = ((w.width() - s.width()) / 2) + getDistanceToBodyFromLeft(); 
        return [vertical < 20 ? 20 : vertical, horizontal];
    } 
  };
  $.fn.openPopup.defaults = {
        appendTo: 'body',
        followSpeed: 500,
        loadUrl: null,
        modal: true,
        modalColor: '#000',
		scrollBar: true,
        xLink: false,
        
		// Customized
		closeClass:'close',
		zIndex: '999',
        escClose: true,
        follow: true,
		opacity: 0.85,
		fadeSpeed: 500,
		amsl: 0
  };
  
  $.fn.closePopup = function() {
      $(this).trigger('click'); 
  }
})(jQuery);
