jQuery(document).ready(function($) {

	$(".table-selection").hover(
			function() {
				$(this).find(".table-selection-table").show();
			},
			function() {
				$(this).find(".table-selection-table").hide();
			}).each(function(i) {
				$ts = $(this);
				$ts.prepend("<div class='arrow'></div> ");
				$ts.css('z-index', '999').find(".table-selection-table, .arrow").css('z-index', '9999');
			});
	$(".table-selection-table td").click(function() {
		var val = $(this).attr('data-value');
		var target = "#" + $(this).attr('data-target');
		debug("TS TD CLICK::target = " + target + ", id = " + val);
		$(target).val(val);
		$(target + "-table-selection-placeholder").html(val);
		$(target + "-table-selection-table").hide();
	});

	$.fn.tsSetWidth = function(width) {
		$ts = $(this[0]);
		$ts.width(width).find('.table-selection-table').width(width);
		$ts.find('.arrow').css('left',(width-25)+"px");
	}

	$(".table-selection:first").tsSetWidth(130);
	$(".table-selection:last").tsSetWidth(180);
});