<?

class Dialog_data extends Plugin_data {
	public $id = '';
	public $auto_load = false;
	public $close_pos = '';
	public $close_offset = '';
	public $heading = '';
	public $content = '';
	public $buttons = '';

	public function __construct($content, $heading = '', $buttons = '', $auto_load = false, $close_pos = Plugin_defaults::Dialog_close_pos, $close_offset = Plugin_defaults::Dialog_close_offset) {
		parent::__construct();

		if (!$this->id) // Assigned generic ID, if no id was selected
			$this->id = uniqid() . '-dialog';

		$this->content = $content;
		$this->heading = $heading;
		$this->buttons = $buttons;
		$this->auto_load = $auto_load;
		$this->close_pos = $close_pos;
		$this->close_offset = $close_offset;
	}

	public function setJson() {
		$x = $y = '';
		switch ($this->close_pos) {
			case Dialog_close_position::Top_right:
				$x = 'left';
				$y = 'top';
				break;
			case Dialog_close_position::Top_left:
				$x = 'right';
				$y = 'top';
				break;
			case Dialog_close_position::Bottom_right:
				$x = 'left';
				$y = 'bottom';
				break;
			case Dialog_close_position::Bottom_left:
				$x = 'right';
				$y = 'bottom';
				break;
		}

		$this->json->Add('auto_load', $this->auto_load);
		$this->json->Add('close_pos_x', $x);
		$this->json->Add('close_pos_y', $y);
		$this->json->Add('close_offset', $this->close_offset);
	}
}
