<?

include(dirname(__FILE__) . '/Dialog_data.php');

class Dialog extends Plugin {
	/** @var Dialog_data $data */
	public $data = null;

	public function __construct(Dialog_data $data) {
		$this->data = $data;
		$this->process();

		//die(print_array($this));
	}

	public function process() {
		$this->data->setJson();

		System::$instance->addJquery('$("#' . $this->data->id . '").RC_Dialog(' . $this->data->json->Encode() . ');' . PHP_EOL);
		
		if ($this->data->auto_load)
			System::$instance->addJquery('$("#' . $this->data->id . '").RC_Dialog().open();' . PHP_EOL);
	}

	public function render() {
		// check and see if there's a button that's been added, which should instead be the main method for closing
		if (strpos($this->data->buttons, 'close') !== false)
			$closeHide = 'hidden';

		$html = "<div id='{$this->data->id}' class='dialog'>
			<div class='close $closeHide'></div>";
		//if($this->data->heading)
		$html .= "<div class='heading'><h3>{$this->data->heading}</h3></div>";
		$html .= "<div class='content'>";
		$html .= $this->data->content;
		if ($this->data->buttons)
			$html .= "<div class='options'>{$this->data->buttons}</div>";
		$html .= "</div>";
		$html .= "</div>";

		//die(htmlspecialchars($html));
		return $html;
	}
}

/*
 * EOF
 */