/**
 * Created by JetBrains PhpStorm.
 * User: Sean
 * Date: 25/08/11
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */
;
var RC_Dialog_options = new Array();
(function($) {
	$.fn.RC_Dialog = function(options, callback) {
		if ($.isFunction(options)) {
			callback = options;
			options = null;
		}

		var $container = $(this);

		o = $.extend({}, $.fn.RC_Dialog.defaults, options);
		if(!RC_Dialog_options[$container.attr('id')]) {
			debug("adding options");
			RC_Dialog_options[$container.attr('id')] = o;
		}
		else
			o = RC_Dialog_options[$container.attr('id')];

		//debug("o.offset = "+o.close_offset+', options.offset'+options.close_offset);

		// Select the auto-format/generated close button, ignore any custom ones
		var $close = $container.find('.close');

		this.open = function() {
			openPopup();
		};

		this.close = function() {
			closePopup();
		};

		return this.each(function() {


//			if (o.auto_load)
//				openPopup();
		});

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// HELP FUNCTIONS - PRIVATE
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function setup() {
			var offset = o.close_offset.split(',');

			var horizontal = ($container.width() - ($close.width() / 2)) - parseInt(offset[0]);
			var vertical = (-1 * $close.height() / 2) - parseInt(offset[1]);

			debug("RC_Dialog::setup(offset, horz, vert, pos_x, pos_y) = "+offset+", "+horizontal+", "+vertical+", "+o.close_pos_x+", "+o.close_pos_y+", ");

			// Ensure the close is above the dialog content/header
			$close.css({'z-index':$container.css('z-index') + 1});
			$close.css(o.close_pos_x, horizontal + 'px');
			$close.css(o.close_pos_y, vertical + 'px');
		}

		function openPopup() {
			setup();

			if(PAGE_TAG != 'home')
				o.position = ['auto','auto'];

			var bPopupDefaults = {
				amsl: 50,
				appendTo: "body",
				closeClass: "close",
				content: "ajax",
				contentContainer: null,
				escClose: !0,
				fadeSpeed: 250,
				follow: [0, 0],
				followSpeed: 500,
				loadUrl: null,
				modal: !0,
				modalClose: 0,
				modalColor: "#fff",
				onClose: function() {
                    $container.pause();
					$('.Slidegal').cycle('resume');
				},
				onOpen: function() {
					$('.Slidegal').cycle('pause');
				},
				opacity: 0.7,
				//position: [100, 0],
				position: ["auto", "auto"],
				scrollBar: !0,
				zIndex: 9999
			};
			var d = $.extend({}, bPopupDefaults, o);
			debug("popup options, appendTo = "+d.appendTo);

			$container.bPopup(d);

			setTimeout(function () {
				$container.fadeTo('slow', 1);
			}, 200);

		}

		function closePopup() {
			$container.bPopup().close();
		}
	};

	$.fn.RC_Dialog.open = function() {
		
	}

	$.fn.RC_Dialog.defaults = {
		loadUrl: null,
		zIndex: 9999,

		auto_load: false,
		close_pos_x: 'left',
		close_pos_y: 'top,',
		close_offset: '-20, 20'
	};
})(jQuery);

jQuery.fn.vimeo = function(action,value){
    var _iframe = jQuery(this);
    if(!_iframe.is("iframe")){
        _iframe = _iframe.find("iframe:first");
    }
var _url = _iframe.attr('src').split('?')[0];

data = {method:action};

if(value){
    data.value = value;
    }

_iframe[0].contentWindow.postMessage(JSON.stringify(data),_url);
}
jQuery.fn.play = function(){
    jQuery(this).vimeo('play');
}
jQuery.fn.pause = function(){
    jQuery(this.vimeo('pause'));
}

