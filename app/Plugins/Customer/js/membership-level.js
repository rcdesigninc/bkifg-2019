jQuery(document).ready(function($) {

	$("#save").click(function() {

		$focus = null;

		errors = new Array();



		var $holder = $(".membership-level");

		var $form = $("#membership-level");



		var $name = $holder.find('input[name=name]');

		var $renewal_period = $holder.find('select[name=renewal_period]');

		var $fee = $holder.find('input[name=fee]');

		var $currency = $holder.find('select[name=currency]');



		if ($name.val() == '')

			addError($name, "Please enter a valid name.");



		//debug("debugError("+$renewal_period.attr('name')+", '"+$renewal_period.val()+"')");

		if ($renewal_period.val() == 'Select an option' || $renewal_period.val() < 1)

			addError($renewal_period, "Please select a renewal period.");



		if ($fee.val() < 1)

			addError($fee, "Please enter a valid fee.");



		if ($currency.val() == 'Select an option' || $currency.val() < 1)

			addError($currency, "Please select a currency.");



		processForm($form);

	});



	$("#.membership-level input").onEnterClick("#save");



	$("#cancel").unbind().click(function() {

		window.location = "membership-levels.php";

	});

});

