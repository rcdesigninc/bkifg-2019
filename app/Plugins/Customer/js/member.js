var lastMember = 0;



jQuery(document).ready(function($) {

	$("#export-all").bind('click',function(){

		window.location = "report-member-summary.php";

	});



	/*$(".mdialog .edit").live('click', function() {

		$(".mdialog .input-value, .mdialog .action-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field, .mdialog .edit-buttons").removeClass('hidden').addClass('visible');

	});



	$(".mdialog .cancel").live('click', function() {

		$(".mdialog .input-value, .mdialog .action-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field, .mdialog .edit-buttons").removeClass('visible').addClass('hidden');

	});*/



	$(".mdialog .edit").live('click', function() {

		$(".mdialog .input-field ~ .input-value, .action-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field, .edit-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field").parent().addClass('editing-margins');

	});



	$(".mdialog .cancel").live('click', function() {

		$(".mdialog .input-field ~ .input-value, .action-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field, .edit-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field").parent().removeClass('editing-margins');

	});



	$(".mdialog .renew").live('click', function() {

		$(".mdialog").bPopup().close();

		$("#renewal-dialog").bPopup();

		$("#renewal-dialog .save").live('click', function() {

			$.post(AJAX_PATH + "ajax-renew-member.php", {

				id: $('.mdialog input[name=id]').val()

				,date_renewal: $('#renewal-dialog input[name=date_renewal]').val()

				,send_notification: $('#renewal-dialog input[name=renewal_notification]').is(":checked")

			}, function(data) {

				if (data.success) {

					$('.mdialog #date_renewal-label').next().next().html(data.date_renewal);

					$(".mdialog #membership_status-label").next().next().html(data.membership_status);

					$(".mdialog textarea[name=notes]").html(data.notes).next().next().html(data.notes);



					$("#renewal-dialog").bPopup().close();

					setTimeout('openMemberDialog()', 25);

				}

			}, 'json');

		});

		$("#renewal-dialog .close").live('click', function() {

			setTimeout('openMemberDialog()', 25);

		});

	});



	$(".mdialog .account-statement").live('click', function() {

		$(".mdialog").bPopup().close();

		$("#invoice-dialog").bPopup();

		$("#invoice-dialog .close").live('click', function() {

			setTimeout('openMemberDialog()', 25);

		});

	});



	$(".mdialog .save").live('click', function() {

		$.post(AJAX_PATH + "ajax-update-member.php", $('#member-form').serialize(), function(data) {

			if (data.success) {

				$('#prof_type-label').next().next().html(data.prof_type);

				$('#heard_about-label').next().next().html(data.heard_about);

				$("#checkmark").attr('class',data.agree_to_terms);



				$('.mdialog input[type=text]').each(function(i) {

					$(this).next().html($(this).val());

				});

				$(".mdialog .cancel").click();

			}

		}, 'json');

	});



	$(".mdialog .suspend").live('click', function() {

		$(".mdialog").bPopup().close();

		$('#confirm-dialog .heading h1').html('Membership Suspension');

		$('#confirm-dialog .content').html("<p><b>Are you sure you with to suspend this member?</b></p>" +

				"<p>Note this account's membership will be set inactive, renewal<br/>date cleared, and account set to archived if you proceed.</p>");

		$('#confirm-dialog').bPopup();

		$("#confirm-dialog .approve").unbind().bind('click', function() {

			$.post(AJAX_PATH + "ajax-suspend-member.php", {

				id: $('.mdialog input[name=id]').val()

			}, function(data) {

				if (data.success) {

					$('.mdialog #date_renewal-label').next().next().html(data.date_renewal);

					$(".mdialog #membership_status-label").next().next().html(data.membership_status);

					

					$('#confirm-dialog').bPopup().close;



					setTimeout('openMemberDialog()', 25);

				}

			}, 'json');

		});

		$("#confirm-dialog .close").live('click', function() {

			setTimeout('openMemberDialog()', 25);

		});

	});



	$(".mdialog .archive").live('click', function() {

		$(".mdialog").bPopup().close();

		$('#confirm-dialog .heading h1').html('Archive Member Account');

		$('#confirm-dialog .content').html("<p><b>Are you sure you with to archive this member?</b></p>" + "<p>Note the account will be unaffected and fully usable,<br/>but hidden from standard report.</p>");

		$('#confirm-dialog').bPopup();

		$("#confirm-dialog .approve").unbind().bind('click', function() {

			$.post(AJAX_PATH + "ajax-archive-member.php", {

				id: $('.mdialog input[name=id]').val()

			}, function(data) {

				if (data.success) {

					$('#confirm-dialog').bPopup().close;

					setTimeout('openMemberDialog()', 25);

				}

			}, 'json');

		});

		$("#confirm-dialog .close").live('click', function() {

			setTimeout('openMemberDialog()', 25);

		});

	});





	$(".mdialog .delete-member").live('click', function() {

		$(".mdialog").bPopup().close();

		$('#confirm-dialog .heading h1').html('Delete Member Account');

		$('#confirm-dialog .content').html("<p><b>Are you sure you with to delete this member?</b></p>" + "<p>Note the account will be permanently removed from the system.</p>");

		$('#confirm-dialog').bPopup();

		$("#confirm-dialog .approve").unbind().bind('click', function() {

			$.post(AJAX_PATH + "ajax-delete-member.php", {

				id: $('.mdialog input[name=id]').val()

			}, function(data) {

				if (data.success) {

					history.go(-1);

				}

			}, 'json');

		});

		$("#confirm-dialog .close").live('click', function() {

			setTimeout('openMemberDialog()', 25);

		});

	});



	//$( "input[name=date_renewal]" ).datepicker( "option", "minDate", "+1" ); // Renewal days must be set in the future stating today

});



function openMemberDialog(id) {

	if(id)

		lastMember = id;

	//jQuery("#popup-container").bPopup({loadUrl: AJAX_PATH+'ajax-load-member.php?get='+lastMember});



	jQuery("#loading-dialog").bPopup();



	jQuery("#popup-container").load(AJAX_PATH+'ajax-load-member.php?get='+lastMember,function(date) {

		jQuery("#loading-dialog").bPopup().close();

		runAjaxSelect();

		jQuery("input[type=checkbox]").css({width:'auto'});

		jQuery("input[type=hidden]").css({display:'none',width:'0'});

		jQuery("#agree_to_terms-label").text("I agree to the terms as being legally binding").siblings().css({float:'left','margin-right':'10px'});

		jQuery(".mdialog").bPopup();

		jQuery(".mdialog .scroll-box").jScrollPane();

		/*jQuery(".mdialog .close").live('click',function(){

			jQuery(".bModal").remove();

		});*/

		jQuery('.datefield').css('cursor', 'pointer').datepicker({

			minDate: jQuery(this).attr('min-date'),

			maxDate: jQuery(this).attr('max-date'),

			changeMonth: false,

			changeYear: false,

			showOtherMonths: true,

			selectOtherMonths: false,

			//showOn: "both",

			//buttonImage: $datepickerImage,

			//buttonImageOnly: true,

			//buttonText: 'datepickerImage',

			autoSize: true,

			dateFormat: 'yy-mm-dd'

		});

	});



}