var typing = false;

var $keywords = null;

var currentLevel = null;



jQuery(document).ready(function($) {

	$keywords = $("input[name=keywords]");



	if ($("#keyword-view").children().length == 0) {

		var $loader = $("#loader").clone().removeClass('hidden');

		$("#keyword-view").html($loader);

		$("#keyword-view").load(AJAX_PATH + "ajax-load-member-summary.php", function(response, status, xhr) {

			/*if (status == "error") {

			 var msg = "Sorry but there was an error: ";

			 alert(msg + xhr.status + " " + xhr.statusText);

			 }

			 if($.browser.msie)

			 $("#keyword-view").html(response);

			 */



			refreshTable();

			$keywords = $("input[name=keywords]");



			//processSort();

		});

	}



	$(".summery-selection p").live('click', function() {

		refreshTable();

		$(this).addClass('selected').siblings().removeClass('selected');

		$("#summary-view, #keyword-view").addClass("hidden");

		$("#" + $(this).attr('data-select')).removeClass('hidden');



	});



	$('#summary-view table:first-child').find('tbody, tfoot').find('td')

			.not(':first-child')

			.not(':last-child')

			.not(':nth-child(9)')

			.each(function() {

		if ($.trim($(this).text()) != '0') {

			$(this).click(function() {

				$(".summery-selection p:last-child").addClass('selected').siblings().removeClass('selected');

				$("#summary-view, #keyword-view").addClass("hidden");

				$("#" + $(".summery-selection p:last-child").attr('data-select')).removeClass('hidden');



				typing = false;

				debug("Option select should be: " + $(this).parent().attr('data-level'));

				$("select[name=membership_level] option:contains(" + $(this).parent().attr('data-level') + ")").attr("selected",

						"selected").parent().change();

				$keywords.val($(this).attr('data-status'));



				jQuery("#keywords-loader").removeClass("invisible");

				jQuery(".record-count").text('-');



				setTimeout('filterWords()', 25);



			});

			$(this).css({'text-decoration':'underline','cursor':'pointer'});

		}

	});





	$("select[name=membership_level]").live('change', function() {

		currentLevel = $(this).find('option:selected').text();

		if (currentLevel == 'Select an option')

			currentLevel = null;

		typing = false;

		jQuery("#keywords-loader").removeClass("invisible");

		filterWords();

	});



	$(".member-link").live('click', function() {

		jQuery("#popup-container").html('');

		$(".mdialog").remove();

		openMemberDialog($(this).attr('data-id'));

	});





	$keywords.live('keydown', function() {

		typing = true;

		jQuery("#keywords-loader").removeClass("invisible");

	});



	$keywords.live('keyup', function() {

		typing = false;



		setTimeout('filterWords()', 250);

	});



	$(".clear-keywords").live('click', function() {

		$(".keywords").val('');

		typing = false;

		filterWords();

	});



	$("#add-level").live('click', function() {

		window.location = "membership-levels-manage.php";

	});



	$("table tr td:first-child").css({"text-align":"left"});

	refreshTable();

	$("table tfoot td").addClass('t-border-red');



	//$(".table-scroll-pane").jScrollPane();



	//grab all header rows

	function processSort() {

		$("*").not('#summary-view)').not('#keyword-view').find('thead tr:last-child td').each(function(column) {



			$(this).addClass('sortable').click(function() {

				var $parent = $(this).closest('table');

				jQuery("#keywords-loader").removeClass("invisible");



				var findSortKey = function($cell) {

					return $cell.find('.sort-key').text().toUpperCase() + ' ' + $cell.text().toUpperCase();

				};

				var sortDirection = $(this).is('.sorted-asc') ? -1 : 1;

				if(sortDirection == 0)

					sortDirection = 1;



				//step back up the tree and get the rows with data

				//for sorting

				var $rows = $parent.find('tbody tr').get();



				//loop through all the rows and find

				$.each($rows, function(index, row) {

					row.sortKey = findSortKey($(row).children('td').eq(column));

				});



				//compare and sort the rows alphabetically

				$rows.sort(function(a, b) {

					if (a.sortKey < b.sortKey) return -sortDirection;

					if (a.sortKey > b.sortKey) return sortDirection;

					return 0;

				});



				//add the rows in the correct order to the bottom of the table

				$.each($rows, function(index, row) {

					$parent.find('tbody').append(row);

					row.sortKey = null;

				});



				//identify the column sort order

				$parent.find('thead tr:last-child td').removeClass('sorted-asc sorted-desc');

				var $sortHead = $parent.find('thead tr:last-child td').filter(':nth-child(' + (column + 1) + ')');

				sortDirection == 1 ? $(this).addClass('sorted-asc') : $(this).addClass('sorted-desc');



				//identify the column to be sorted by

				$parent.find('tbody td').removeClass('sorted').filter(':nth-child(' + (column + 1) + ')').addClass('sorted');



				//$parent.find('.visible td').removeClass('odd');

				//zebraRows('.visible:even td', 'odd');

				jQuery("#keywords-loader").addClass("invisible");



				setTimeout('refreshTable()',25);

			});

		});

	}



	processSort();



});



function filterWords() {





	if (!typing) {

		jQuery("#keywords-loader").removeClass("invisible");

		if ($keywords.val().length) {

			debug("Filtering keyword: " + $keywords.val());

			jQuery("#keywords-table tbody tr").hide();



			jQuery("#keywords-table tbody tr td:containsi('" + $keywords.val() + "')").parent().show();

		}

		else

			jQuery("#keywords-table tbody tr").show();



		if (currentLevel) { // if level selected, filter further to only show those of that level

			debug("Filtering level: " + currentLevel);

			jQuery("#keywords-table tbody tr td:nth-child(3):not(:contains('" + currentLevel + "'))").parent().hide();

		}



		refreshTable();



		jQuery(".record-count").text(jQuery("#keywords-table tbody tr:visible").length);

		jQuery("#keywords-loader").addClass("invisible");

	}

	else

		setTimeout('filterWords()', 250);

}



function refreshTable() {

	jQuery("table tbody tr:visible:even").removeClass("bg-light-grey");

	jQuery("table tbody tr:visible:odd").addClass("bg-light-grey");

}