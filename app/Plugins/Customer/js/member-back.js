var lastMember = 0;



jQuery(document).ready(function($) {

	$("#agree_to_terms-label").text("I agree to the terms as being legally binding").siblings().css({float:'left','margin-right':'10px'});



	$("#interac-checkout").live('click', function() {

		$("input[name=payment_method]").val('2');

		$("#checkout").submit();

	});



	$("#paypal-checkout").live('click', function() {

		$("input[name=payment_method]").val('1');

		$("#checkout").submit();

	});



	// todo: setup memberhsip change and dialog

	$("#change-membership-level").live('click', function() {

		$("#change-membership-level-dialog").bPopup();

	});



	$("#change-membership-level-dialog .save").live('click', function() {

		if ($("#change-membership-level-dialog  input:checked").length == 1) {

			//$("#change-membership-level-dialog form").submit();

			window.location = 'membership-level-change.php?change=' + $("#change-membership-level-dialog  input:checked").val();

		}

		else {

			resetErrors();

			addError(null, "You must select a new membership level to modify your account.");

			$("#change-membership-level-dialog").bPopup().close();

			displayErrors('Membership Level Change');

		}

	});



	$("#edit-profile").live('click', function() {

		$(".mdialog .input-field ~ .input-value, .action-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field, .edit-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field").parent().addClass('editing-margins');

	});



	$("#cancel-profile").live('click', function() {

		history.go(-1);

	});



	// todo: setup memberhsip self renewal change and dialog

	$("#renew-membership").live('click', function() {

		window.location = "membership-renewal.php";

	});



	$("#save-profile").live('click', function() {

		resetErrors();



		var $form = $("#member-form");



		$x = $("input[name=first_name]");

		if ($x.val() == "")

			addError($x, "You must enter a first name.");

		$x = $("input[name=last_name]");

		if ($x.val() == "")

			addError($x, "You must enter a last name.");

		$x = $("input[name=email]");

		if (!validateEmail($x.val()))

			addError($x, "You must enter a valid email.");

		$x = $("input[name=password]");

		if ($x.val() == "")

			addError($x, "You must enter a password.");



		$x = $("select[name=membership_level] option:selected");

		if ($x.html() == "Select an option" || $x.val() < 1)

			addError($x, "You must select a membership level.");



		processForm($form);

	});

});

