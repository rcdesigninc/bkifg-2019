jQuery(document).ready(function($) {

	$(".mdialog .edit").click(function() {

		$(".mdialog .input-value, .mdialog .action-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field, .mdialog .edit-buttons").removeClass('hidden').addClass('visible');

	});



	$(".mdialog .cancel").click(function() {

		$(".mdialog .input-value, .mdialog .action-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field, .mdialog .edit-buttons").removeClass('visible').addClass('hidden');

	});



	$(".mdialog .renew").click(function() {

		$(".mdialog").bPopup().close();

		$("#renewal-dialog").bPopup();

		$("#renewal-dialog .save").click(function() {

			$.post(AJAX_PATH + "ajax-renew-member.php", {

				id: $('.mdialog input[name=id]').val(),

				date_renewal: $('#renewal-dialog input[name=date_renewal]').val()

			}, function(data) {

				if (data.success) {

					$('.mdialog #date_renewal-label').next().next().html(data.date_renewal);

					$(".mdialog #membership_status-label").next().next().html(data.membership_status);

					$("#renewal-dialog").bPopup().close();

					setTimeout('openMemberDialog()', 1);

				}

			}, 'json');

		});

		$("#renewal-dialog .close").click(function() {

			setTimeout('openMemberDialog()', 1);

		});

	});



	$(".mdialog .save").click(function() {

		$.post(AJAX_PATH + "ajax-update-member.php", $('#member-form').serialize(), function(data) {

			if (data.success) {

				$('#prof_type-label').next().next().html(data.prof_type);

				$('#heard_about-label').next().next().html(data.heard_about);



				$("#agree_to_terms-label").next().next().html()



				$('.mdialog input').each(function(i) {

					$(this).next().html($(this).val());

				});

				$(".mdialog .cancel").click();

			}

		}, 'json');

	});



	$(".mdialog .suspend").click(function() {

		$(".mdialog").bPopup().close();

		$('#confirm-dialog .heading h1').html('Membership Suspension');

		$('#confirm-dialog .content').html("<p><b>Are you sure you with to suspend this member?</b></p>" + "<p>Note this account's membership will be set inactive,<br/>and renewal date switched to today if you proceed.</p>");

		$('#confirm-dialog').bPopup();

		$("#confirm-dialog .approve").unbind().bind('click', function() {

			$.post(AJAX_PATH + "ajax-suspend-member.php", {

				id: $('.mdialog input[name=id]').val()

			}, function(data) {

				if (data.success) {

					$('.mdialog #date_renewal-label').next().next().html(data.date_renewal);

					$(".mdialog #membership_status-label").next().next().html(data.membership_status);

					$('#confirm-dialog').bPopup().close;

					setTimeout('openMemberDialog()', 1);

				}

			}, 'json');

		});

		$("#confirm-dialog .close").click(function() {

			setTimeout('openMemberDialog()', 1);

		});

	});



	$(".mdialog .archive").click(function() {

		$(".mdialog").bPopup().close();

		$('#confirm-dialog .heading h1').html('Archive Member Account');

		$('#confirm-dialog .content').html("<p><b>Are you sure you with to archive this member?</b></p>" + "<p>Note the account will be unaffected and fully usable,<br/>but hidden from standard report.</p>");

		$('#confirm-dialog').bPopup();

		$("#confirm-dialog .approve").unbind().bind('click', function() {

			$.post(AJAX_PATH + "ajax-archive-member.php", {

				id: $('.mdialog input[name=id]').val()

			}, function(data) {

				if (data.success) {

					$('#confirm-dialog').bPopup().close;

					setTimeout('openMemberDialog()', 1);

				}

			}, 'json');

		});

		$("#confirm-dialog .close").click(function() {

			setTimeout('openMemberDialog()', 1);

		});

	});





	$(".mdialog .delete-member").click(function() {

		$(".mdialog").bPopup().close();

		$('#confirm-dialog .heading h1').html('Delete Member Account');

		$('#confirm-dialog .content').html("<p><b>Are you sure you with to delete this member?</b></p>" + "<p>Note the account will be permanently removed from the system.</p>");

		$('#confirm-dialog').bPopup();

		$("#confirm-dialog .approve").unbind().bind('click', function() {

			$.post(AJAX_PATH + "ajax-delete-member.php", {

				id: $('.mdialog input[name=id]').val()

			}, function(data) {

				if (data.success) {

					history.go(-1);

				}

			}, 'json');

		});

		$("#confirm-dialog .close").click(function() {

			setTimeout('openMemberDialog()', 1);

		});

	});



	$(".scroll-box").jScrollPane();

	openMemberDialog();



	//$( "input[name=date_renewal]" ).datepicker( "option", "minDate", "+1" ); // Renewal days must be set in the future stating today

});



function openMemberDialog() {

	//jQuery(".mdialog").bPopup();

	jQuery(".mdialog").show();

}