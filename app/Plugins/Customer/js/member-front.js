jQuery(document).ready(function($) {

	$("#agree_to_terms-label").text("I agree to the terms as being legally binding").siblings().css({'float':'left','margin-right':'10px'});



	$("#interac-checkout").live('click', function() {

		$("input[name=payment_method]").val('2');

		$("#checkout").submit();

	});



	$("#paypal-checkout").live('click', function() {

		$("input[name=payment_method]").val('1');

		$("#checkout").submit();

	});



	// todo: setup membership change and dialog

	$("#change-membership-level").live('click', function() {

		$("#change-membership-level-dialog").bPopup();

	});



	$("#change-membership-level-dialog .save").live('click', function() {

		if ($("#change-membership-level-dialog  input:checked").length == 1) {

			//$("#change-membership-level-dialog form").submit();

			window.location = 'membership-level-change.php?change=' + $("#change-membership-level-dialog  input:checked").val();

		}

		else {

			resetErrors();

			addError(null, "You must select a new membership level to modify your account.");

			$("#change-membership-level-dialog").bPopup().close();

			displayErrors('Membership Level Change');

			$("#error-dialog .close").bind('click',function(){

				setTimeout('openLevelDialog()',25);

			});

		}

	});



	$("#edit-profile").live('click', function() {



		$(".mdialog .input-field ~ .input-value, .action-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field, .edit-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field").parent().addClass('editing-margins');

	});



	$("#cancel-profile").live('click', function() {

		$(".mdialog .input-field ~ .input-value, .action-buttons").removeClass('hidden').addClass('visible');

		$(".mdialog .input-field, .edit-buttons").removeClass('visible').addClass('hidden');

		$(".mdialog .input-field").parent().removeClass('editing-margins');

	});



	// todo: setup membership self renewal change and dialog

	$("#renew-membership").live('click', function() {

		window.location = "membership-renewal.php";

	});



	$("#save-profile").live('click', function() {

		$.post(AJAX_PATH + "ajax-member-updating.php", $('#member-form').serialize(), function(data) {

			if (data.success) {

				$('#prof_type-label').next().next().html(data.prof_type);

				$('#heard_about-label').next().next().html(data.heard_about);

				$("#checkmark").attr('class', data.agree_to_terms);



				$('.mdialog input[type=text]').each(function() {

					$(this).next().html($(this).val());

				});

				$("#cancel-profile").click();

			}

		}, 'json');

	});

});



function openLevelDialog() {

	jQuery("#change-membership-level-dialog").bPopup();

}