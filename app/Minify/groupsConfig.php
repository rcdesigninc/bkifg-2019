<?php

/**

 * Groups configuration for default Minify implementation

 * @package Minify

 */



/**

 * You may wish to use the Minify URI Builder app to suggest

 * changes. http://yourdomain/min/builder/

 *

 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas

 **/



define("SETTINGS_MODEL",'../Settings/Model/');



include('../Settings/System_defaults.php');





return

		array

		( // 'js' => array('//js/file1.js', '//js/file2.js'),

			// 'css' => array('//css/file1.css', '//css/file2.css'),

			'cms-js' =>

			array

			(System_defaults::Minify_groupConfig_path.'System/js/jquery.common.min.js',

			 System_defaults::Minify_groupConfig_path.'System/js/global.js',

			 System_defaults::Minify_groupConfig_path.'System/js/menu.js',

			 System_defaults::Minify_groupConfig_path.'Templates/js/Template.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/jquery.ui.mouse.min.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/jquery.ui.widget.min.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/jquery.ui.datepicker.min.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/jquery.ui.sortable.min.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/jquery.ui.draggable.min.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/jquery.ui.droppable.min.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/cms.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/tinyMCE/jquery.tinymce.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/tiny_mce.js',

			 System_defaults::Minify_groupConfig_path.'CMS/js/tinyMCE/plugins/ezfilemanager/js/ez_tinyMCE.js'),



			'cms-css' =>

			array

			(System_defaults::Minify_groupConfig_path.'System/css/_UniversalReset.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/colours.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/elements.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/layout.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/positioning.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/type.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/typeography.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/font-face/Cantarell/Cantarell.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/html.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/form.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/import/jquery-ui-themes/humanity/jquery-ui-1.8.6.custom.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/import/common.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/import/datatable.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/import/form.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/import/header.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/import/cms-menu.css',

			 System_defaults::Minify_groupConfig_path.'CMS/css/tinymce.css'),



			'main-js' =>

			array

			(System_defaults::Minify_groupConfig_path.'System/js/jquery.common.min.js',

			 System_defaults::Minify_groupConfig_path.'System/js/global.js',

			 System_defaults::Minify_groupConfig_path.'System/js/menu.js',

			 System_defaults::Minify_groupConfig_path.'Templates/js/Template.js',

			 System_defaults::Minify_groupConfig_path.'Templates/js/Form.js',

			 System_defaults::Minify_groupConfig_path.'Templates/js/jquery.radial.js',

			),



			'main-css' =>

			array

			(System_defaults::Minify_groupConfig_path.'System/css/_UniversalReset.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/colours.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/elements.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/layout.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/positioning.css',

			 System_defaults::Minify_groupConfig_path.'System/css/Common/type.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/typeography.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/font-face/Chaparral/Chaparral.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/html.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/System/form.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/Template/page.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/Template/header.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/Template/menu.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/Template/billboard.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/Template/content.css',

			 System_defaults::Minify_groupConfig_path.'Templates/css/Template/footer.css',

			System_defaults::Minify_groupConfig_path.'Templates/css/Template.css',),);