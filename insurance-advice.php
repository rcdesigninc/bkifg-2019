<?

$ps = new Site_page_sections();


LoadModel('Faqs');


// Get the faq content, and append it too the left column

$auto = Faqs::renderFaqs('Auto', 0, 58);

$home = Faqs::renderFaqs('Home', 0, 60);

$business = Faqs::renderFaqs('Business', 0, 59);


array_push($System->sections, $auto);

array_push($System->sections, $business);

array_push($System->sections, $home);

$System->addCSS(<<< EOB
ul {
	margin-bottom: 0;
}
EOB
);

if(ACTIVE_SITE > 3)
	include("_strip-quote.php");
else
	$System->renderDefaultPage($content);