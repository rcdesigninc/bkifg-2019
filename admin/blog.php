<?
define('PAGE_ID', 1);
define('MENU_ID', 46);

require_once('_config.php');
LoadModel('Blog_post');

$db_section = new Blog_post();
if ($_GET['id'])
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: ?");
}

if (sizeof($_POST)) {
	$_POST['tag'] = hyphonate($_POST['title']);
	$_POST['categories'] = '';

	//die(print_array($_POST).print_array($db_section));

	if ($_GET['add']) {
		if ($_POST['is_active'])
			$db_section->date_published = getTimestamp();
		if ($db_section->Insert($_POST))
			$added = true;
	}
	else {
		if ($_POST['is_active'])
			$db_section->date_published = getTimestamp();
		$db_section->Update($_POST);
	}
	header("location: ?");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {
	if (!$_GET['id'])
		$db_section->date_created = getTimestamp();

	$_SESSION['posting_id'] = $db_section->id;

	$defaults =
			array
			();

	$defaults = '';
	if ($db_section->id) {
		LoadModel('Blog_post_category');
		$defaults = Blog_post_category::getDefaultsJson($db_section->id);
	}

	//	$System->addInclude('
	//	<script src="' . CMS_JS . 'APIs/fcbkcomplete/jquery.fcbkcomplete.js" type="text/javascript" charset="utf-8"></script>
	//    <link rel="stylesheet" href="' . CMS_JS . 'APIs/fcbkcomplete/style.css" type="text/css" media="screen" charset="utf-8" />');
	$System->addJquery('
	BLOG_AJAX = PLUGIN_PATH+"blog/ajax/"+"Blog_post_categories.php"
	$("select[name=categories]").fcbkcomplete({
			json_url: BLOG_AJAX,
			defaults: ' . ($defaults ? $defaults : '{}') . '
		});
	');

	$db_section->date_modified = getTimestamp();
	//die(print_array($db_section));
	//print $System->lastQuery;

	$db_section->scrubFields();
	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);
	$content .= "<br />
	<br />
	<br />
	";
}
else {
	$db_section_list = $db_section->Select("*", "", "date_modified DESC");


	for ($i = 0; $i < count($db_section_list); $i++) {
		$db_section_list[$i]->date_created = date("M j, Y, g:i a", $db_section_list[$i]->date_created);
		$db_section_list[$i]->date_modified = date("M j, Y, g:i a", $db_section_list[$i]->date_modified);
		$db_section_list[$i]->date_published = $db_section_list[$i]->date_published ? date("M j, Y, g:i a",
			$db_section_list[$i]->date_published) : '-';
		$u = new Site_user();
		$u->Load($db_section_list[$i]->author_id);
		//die(print_array($u));
		$db_section_list[$i]->author_id = $u->first_name;
		$db_section_list[$i]->is_active = $db_section_list[$i]->is_active ? "Yes" : "No";
	}

	//die(print_array($db_section_list));

	$columns =
			array
			('id' => 'Options',
			 'title' => 'Title',
			 'author_id' => 'Author',
			 'date_modified' => 'Date Last Modified',
			 'date_published' => 'Date Published',
			 'is_active' => 'Published');

	$content .= "<form method=\"get\" action=\"?\">";
	$content .= "<input type=\"submit\" name=\"add\" value=\"Add Entry\">";
	$content .= "</form>";

	$content .= $System->getDataTable($db_section_list, $columns) . "<br />";
}

$System->renderPage($content);
?>