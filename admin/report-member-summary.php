<?
include("_config.php");
define("PHPXLS_PATH", APP_PATH . 'APIs/phpxls/');
require_once(PHPXLS_PATH . 'Writer.php');

/*
 *  
 */
$report_title = 'Member Summary';
$report_filename = $report_title . ' - ' . date("Y-m-d");

/*
 * 
 */

LoadModel('Country');
LoadModel('Province');
LoadModel('Sp_member');
LoadModel('Sp_member_log');
LoadModel('Sp_member_prof_type');
LoadModel('Sp_membership');
LoadModel('Sp_membership_obtained');
LoadModel('Sp_member_heard_about');
LoadModel('Sp_membership_status');
$m = new Sp_member();
$members = $m->Select("*", "archived = 0");

$columns = $m->_funcGetColumns();

array_shift($columns);

$sheet1 =
		array
		();
foreach ($columns as $index => $label)
	if ($label != 'Password')
		$sheet1[0][] = $label;

foreach ($members as $m) {
	$p = new Province();
	$p->Load($m->prov_id);

	$c = new Country();
	$c->Load($m->country_id);

	$level = new Sp_membership();
	$level->Load($m->membership_level);

	$status = new Sp_membership_status();
	$status->Load($m->membership_status);

	$prof_type = new Sp_member_prof_type();
	$prof_type->Load($m->prof_type);

	$heard = new Sp_member_heard_about();
	$heard->Load($m->heard_about);

	$obtained = new Sp_membership_obtained();
	$obtained->Load($m->obtained_by);

	$sheet1[] =
			array
			($m->old_id,
			 $m->first_name,
			 $m->last_name,
			 $m->city_dept,
			 $m->email,
			 $m->phone_home,
			 $m->agree_to_terms ? 'Yes' : 'No',
			 $prof_type->label,
			 $m->job_title,
			 $m->employee_id,
			 $m->phone_work,
			 $m->address,
			 $m->city,
			 $p->name,
			 $c->label,
			 $m->postal,
			 $obtained->$label,
			 $m->squadpass_num,
			 $m->squadpass_expires,
			 $heard->label,
			 $m->persons_in_household,
			 $m->directory_listing,
			 $m->archived ? 'Yes' : 'No',
			 $m->membership_active ? 'Yes' : 'No',
			 $level->name,
			 $status->label,
			 $m->date_joined,
			 $m->date_renewal,
			 $m->date_last_renewal,
			 $m->date_level_change,
			 $m->newsletters_active,
			 $m->newsletters_events,
			 $m->newsletters_members,
			 $m->directory_access,
			 $m->photo_albums,
			 $m->balance,
			 $m->donations,
			 $m->notes);
}


/*
 * 
 * 
 */

$workbook = new Spreadsheet_Excel_Writer();

$format_und =& $workbook->addFormat();
$format_und->setBottom(2); //thick
$format_und->setBold();
$format_und->setColor('black');
$format_und->setFontFamily('Arial');
$format_und->setSize(8);

$format_reg =& $workbook->addFormat();
$format_reg->setColor('black');
$format_reg->setFontFamily('Arial');
$format_reg->setSize(8);

$arr =
		array
		($report_title => $sheet1,);

foreach ($arr as $wbname => $rows) {
	$rowcount = count($rows);
	$colcount = count($rows[0]);

	$worksheet =& $workbook->addWorksheet($wbname);

	/*
    $worksheet->setColumn(0,0, 6.14);//setColumn(startcol,endcol,float)
    $worksheet->setColumn(1,3,15.00);
    $worksheet->setColumn(4,4, 8.00);
	*/
	$worksheet->setColumn(0, 0, 30);
	$worksheet->setColumn(0, 3, 15);
	$worksheet->setColumn(0, 8, 10);

	for ($j = 0; $j < $rowcount; $j++) {
		for ($i = 0; $i < $colcount; $i++) {
			$fmt =& $format_reg;
			if ($j == 0)
				$fmt =& $format_und;

			if (isset($rows[$j][$i])) {
				$data = $rows[$j][$i];
				$worksheet->write($j, $i, $data, $fmt);
			}
		}
	}
}
$name = '';
if ($to && $from)
	$name = "--$from-$to";
$workbook->send("$report_filename.xls");
$workbook->close();


?>