<?
define('PAGE_ID', 1);
define('MENU_ID', 1);
require_once('_config.php');


if (!$System->user->id)
	$System->user->Logout();

$db_section = new Site_menu();
if ($_GET['id'])
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: ?");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert($_POST))
			$added = true;
	}
	else
		$db_section->Update($_POST);
	header("location: ?");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {

	if (!$_GET['id']) {
		$db_section->managed_on = $System->managing_site;

		if ($System->managing_site != 1)
			$db_section->site_id = $System->managing_site;
	}


	$db_section->scrubFields();

	//$db_section->parent_id->attrs .= ' filter=\'`site_id` = "$System->managing_site"\'';
	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);
}
else {
	$db_section_list = $db_section->Select("*", "`site_id` = '$System->managing_site'", "`parent_id`, `order_by`");
	$list =
			array
			();
	foreach ($db_section_list as $x) {
		/** @var Site_menu $x */
		$list[$x->managed_on][$x->parent_id][] = $x;
	}

	$columns =
			array
			('id' => 'Options',
			 'label' => 'Label');

	$content .= "<h1>Menus</h1>";

	$content .= "<form method='get' action='?'>";
	$content .= "<input type='submit' name='add' value='Add Entry'>";
	$content .= "<input type='hidden' name='page_id' value='$id'>";
	$content .= "</form>";

	foreach ($list as $managed_on) {
		foreach ($managed_on as $parent_id => $list) {
			if (!$System->user->is_root() && !$parent_id)
				continue;

			if ($parent_id) {
				$parent = $db_section->Load($parent_id);
				$content .= "<h2>$parent->label Menu</h2>";
			}
			else
				$content .= "<h2>Main Menu</h2>";

			$content .= $System->getDataTable($list, $columns, true) . "<br />";
		}
	}
}

$System->renderPage($content);
?>