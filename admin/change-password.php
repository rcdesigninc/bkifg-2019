<?

define('PAGE_ID', 1);

require_once('_config.php');

$System->addInclude('<script type="text/javascript" src="' . JS_PATH . 'Core/' . 'change-password.js"></script>');


if (!$System->user->id && !($System->user->is_admin() || $System->user->is_mod()))

	$System->user->Logout();


$ps = new Site_page_sections();


$pass_changed = false;


if (sizeof($_POST)) {

	//die(print_array($_POST));

	if ($_POST['password_current'] && $_POST['password_current'] == $System->user->password && $_POST['password_new'] != $System->user->password && $_POST['password_new'] == $_POST['password_confirm']) {

		$pass_changed = true;


		$_SESSION['password'] = $System->user->password = $_POST['password_new'];

		$System->user->Update();

		$System->addJquery('

		displayErrors("Change Password Successful","<p>You have successful updated your password to: <b>' . $System->user->password . '</b>.</p>"+

		"<p>A reminder email has been sent to you as well for your records.</p>");

		$errorDialog.find(".close").unbind().click(function() {

			window.location = "member-profile.php";

		});

		');

	}

	else {

		$System->addJquery('

		addError($("input[name=password_current]"),"There was an error processing your request. Please verify you have entered everything correctly and try again.");

		displayErrors();

		');

	}

	// todo: add server side verification

	//$db_section->Update($_POST);

}


if (!$pass_changed) {

	/*$System->addTopMenu("<div id='cancel' class='button'>Cancel</div>");

	$System->addTopMenu("<div id='save' class='button'>Save</div>");*/


	$ps->content .= "<form id='password-form' method='post'>";

	$ps->content .= "<div id='cancel' class='button'>Cancel</div>";

	$ps->content .= "<div id='save' class='button'>Save</div>";

	$ps->content .= "<p><b>Change password for: </b>" . $System->user->email . "</p>" . '<div class="spacer"></div>' . '<div class="spacer"></div>';

	$ps->content .= "<div style='display:none;'>" . $System->user->getInput('password') . '</div>';

	$ps->content .= Model_field::buildInput('password_current', 'Current Password', Model_field_type::Password,

		20) . '<div class="spacer"></div>';

	$ps->content .= Model_field::buildInput('password_new', 'New Password', Model_field_type::Password,

		20) . '<div class="spacer"></div>';

	$ps->content .= Model_field::buildInput('password_confirm', 'Confirm New Password', Model_field_type::Password,

		20) . '<div class="spacer"></div>' . '<div class="spacer"></div>';

	$ps->content .= "</form>";


	$content = $ps->getContent();

}

else {

	$System->addTopMenu(" ");

}


$System->renderPage($content);

?>