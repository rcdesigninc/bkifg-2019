<?
define('PAGE_ID', 1);
define('MENU_ID', 2);

require_once('_config.php');

if (!$System->user->id)
	$System->user->Logout();

$id = 0;
if ($_GET['id']) {
	$id = $_SESSION['page_id'] = $_GET['id'];
}

$db_section = new Site_page();

if ($id) {
	$db_section->Load($id);
	//die(System::$DB->lastQuery() . print_array($db_section));
}

if ($_GET['delete']) {
	$db_section->Delete();
	header("location: pages.php");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert($_POST))
			$added = true;
		header("location: pages.php?id=" . $db_section->id);
	}
	else
		$db_section->Update($_POST);
	header("location: pages.php");
}

if ($id || ($_GET['add'] && !$added)) {
	$content .= "<h1>$db_section->title Page Info</h1>";
	$db_section->scrubFields();

	if (!$id) {
		$db_section->insertTag();
		$db_section->site_id = $_SESSION['managing_site'];
	}

	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);
	//$content .= $db_section->renderImage();

	if ($_GET['id'] != 31 && $_GET['id'] != 57) {
		$columns = array(
			'id' => 'Options', 'heading' => 'Heading', 'content' => 'Content'
		);

		$content .= "<br />";

		LoadModel('Site_template_sections');
		$ts = new Site_template_sections();
		$ps = new Site_page_sections();

		if (!$_GET['add']) {
			$content .= "<h2>$db_section->title Page Sections</h2>";
			$ps_list = $ps->Select("*, SUBSTR(content,1,100) as 'content'", "`page_id` = \"$db_section->id\"",
			                       'template_section_id, order_by');

			$section_list = array();
			foreach ($ps_list as $s)
				$section_list[$s->template_section_id][] = $s;

			//die(print_array($section_list));
			//if($System->user->is_root()) 
			{
				$content .= "<form method=\"get\" action=\"page_sections.php\">";
				$content .= "<input type=\"submit\" name=\"add\" value=\"Add Page Section\">";
				$content .= "<input type=\"hidden\" name=\"page_id\" value=\"$id\">";
				$content .= "</form>";
			}

			foreach ($section_list as $section_id => $list) {
				if ($db_section->id == 7 && $section_id == 9)
					continue;

				$ts->Load($section_id);
				$content .= "<h2>$ts->label Section</h2>";

				$sortable = true;
				if ($id != 2 && $section_id == 3)
					$sortable = false;


				$content .= $System->getDataTable($list, $columns, $sortable,
				                                  "page_sections.php?page_id=$db_section->id&");
			}
			$content .= "<br />";
		}
	}
}
else {
	$_SESSION['page_id'] = 0;

	$content .= "<h1>Pages</h1>";

	if ($System->user->is_root()) {
		$content .= "<form method=\"get\" action=\"?\">";
		$content .= "<input type=\"submit\" name=\"add\" value=\"Add Page\">";
		$content .= "<input type=\"hidden\" name=\"page_id\" value=\"$id\">";
		$content .= "</form>";
	}

	/*$db_section_list = $db_section->Select("*","`site_id` = \"$System->managing_site\"",'title');
	$columns =
	array(
		  "id"=>"Options",
		  "title"=>"Title"
	);
	$content .= $System->getDataTable($db_section_list,$columns);*/
	$entries = $System->prepPagination("Site_page", "`site_id` = '" . $System->managing_site . "'", "title", 10000);

	$section_list = array();
	foreach ($entries as $x) {
		$section_list[] = $x;
	}
	$columns = array(
		"id" => "Options", "title" => "Title"
	);
	//$columns = $db_section->_funcGetColumns();
	$content .= $System->getDataTable($section_list, $columns) . "<br />";
	$content .= $System->renderPagination($from, $page_count);
}

$System->renderPage($content);
?>