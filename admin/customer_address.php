<?
define('PAGE_ID', 1);
define('MENU_ID', 54);

require_once('_config.php');
$cname = 'Customer_address';

$cid = $_SESSION['customer_id'];

$backUrl = "customers.php?id=" . $cid;

LoadModel($cname);
$db_section = new $cname();

if ($_GET['id'])
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: $backUrl");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert($_POST))
			$added = true;
	}
	else
		$db_section->Update($_POST);
	header("location: $backUrl");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {
	$content .= "<h2>$cname Address Info</h2>";

	$db_section->customer_id = $cid;

	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);
}
else {
	$db_section_list = $db_section->Select();

	$section_list =
			array
			();
	foreach ($db_section_list as $x) {
		$x->date_added = printTimestamp($x->date_added);
		if ($x->province_other)
			$x->province = $x->province_other;
		else {
			$prov->Load($x->province);
			$x->province = $prov->name;
		}
		$country->Load($x->country);
		$x->country = $country->iso;
		$x->bill_and_ship = $x->bill_and_ship ? 'Both' : 'Ship';
		$section_list[] = $x;
	}

	$content .= "<h2>$cname Address List</h2>";

	$columns = $db_section->_funcGetColumns();
	$columns =
			array
			('id' => 'Options',
			 'first_name' => 'First Name',
			 'last_name' => 'Last Name',
			 'phone' => 'Phone',
			 'street' => 'Street',
			 'street2' => 'Street 2',
			 'city' => 'City',
			 'province' => 'Prov',
			 'postal' => 'Postal',
			 'country' => 'Country',
			 'bill_and_ship' => 'Bill/Ship',
			 'date_added' => 'Date Added');


	//if(!count($section_list)) {
	$content .= "<form method='get'>";
	$content .= "<input type='submit' name='add' value='Add Entry'>";
	$content .= "</form>";
	//}

	$content .= $System->getDataTable($section_list, $columns, false, '?', false) . "<br />";
}

$System->renderPage($content);
?>