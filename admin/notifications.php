<?

define('PAGE_TAG', 'notification-templates');

$menu_id = '158';

require_once('_config.php');

$System->addInclude('<script type="text/javascript" src="' . JS_PATH . 'table.js"></script>');

$System->addInclude('<script type="text/javascript" src="' . JS_PATH . 'notifications.js"></script>');


LoadModel('Sp_member');


LoadModel('Sp_notification');

LoadModel('Sp_notification_from');

LoadModel('Sp_notification_to');

LoadModel('Sp_notification_type');


$notifications = new Sp_notification();

$from = new Sp_notification_from();

$to = new Sp_notification_to();

$types = new Sp_notification_type();


$form = $from->Select();

$to = $to->Select();

$types = $types->Select();


$ps = new Site_page_sections();


$System->addTopMenu('<div class="button" id="export-all">Export All</div>');


$notifications = $notifications->Select();

$ps->content .= "

<h2>Notification Templates</h2>";


$System->addCSS('

#notes {

	width: 930px;

}

');


$System->addJquery('

$("#notes thead td:nth-child(1)").css({width:"22%"});

$("#notes thead td:nth-child(2)").css({width:"38%"});

$("#notes thead td:nth-child(3)").css({width:"15%"});

$("#notes thead td:nth-child(4)").css({width:"15%"});

$("#notes thead td:nth-child(5)").css({width:"10%"});

');


$searchTable = "

<table id='notes'>

	<thead>

		<tr class='bg-dark-blue'>

			<td class='r-border-light'>

				Type

			</td>

			<td class='r-border-light'>

				Subject

			</td>

			<td class='r-border-light'>

				Will Sent To

			</td>

			<td class='r-border-light'>

				Sent On

			</td>

			<td>

				Test Email

			</td>

		</tr>

	</thead>

	<tbody>";

foreach ($notifications as $n) {

	$t = new Sp_notification_type();

	$t->Load($n->type_id);


	$to = new Sp_notification_to();

	$to->Load($n->sent_to);


	/*$from = new Sp_notification_from();

	$from->Load($n->sent_from);*/


	$searchTable .= "

	<tr>

		<td class='r-border-light'>

			<a href='notifications-manage.php?id=$n->id'>$t->label</a>

		</td>

		<td class='r-border-light'>

			$n->subject

		</td>

		<td class='r-border-light'>

			$to->label

		</td>

		<td class='r-border-light'>

			";

	switch ($n->type_id) {

		case 1:

		case 2:

			$searchTable .= $n->days_until . " days before";

			break;

		case 3:

			$searchTable .= "Day of renewal";

			break;

		case 4:

		case 5:

			$searchTable .= $n->days_until . " days after";

			break;

		default:

			$searchTable .= "Action taken";

	}

	$searchTable .= "

		</td>

		<td>

			<a data-id='$n->id' class='test-notification' href='#'>Send Test</a>

		</td>

	</tr>

	";

}


$searchTable .= '

	</tbody>

</table>

<div class="spacer"></div>

<div class="spacer"></div>

<div class="spacer"></div>';

$ps->content .= $searchTable;


$content .= $ps->getContent();


$System->renderPage($content);







?>





