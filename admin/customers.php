<?
define('PAGE_ID', 1);
define('MENU_ID', 54);

$cname = 'Customer';

require_once('_config.php');
LoadModel($cname);
$db_section = new $cname();

$id = $_GET['id'];
$_SESSION['customer_id'] = $_GET['id'];

if ($id)
	$db_section->Load($id);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: ?");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert($_POST))
			$added = true;
	}
	else
		$db_section->Update($_POST);
	header("location: ?");
}

if ($id || ($_GET['add'] && !$added)) {
	$content .= "<h2>$cname Info</h2>";

	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);

	if ($id) {
		//		$content .= "<h2>$cname Addresses</h2>";
		//		//if(!count($section_list)) {
		//		$content .= "<form method='get' action='customer_address.php?add=true'>";
		//		$content .= "<input type='submit' name='add' value='Add Entry'>";
		//		$content .= "</form>";
		//		//}
		//
		//		LoadModel('Customer_address');
		//		$model = new Customer_address();
		//		$list = $model->Select("*", "customer_id = '{$db_section->id}'");
		//		$section_list =
		//				array
		//				();
		//		if (count($list)) {
		//
		//			LoadModel('Customer_province');
		//			$prov = new Customer_province();
		//			LoadModel('Customer_country');
		//			$country = new Customer_country();
		//			LoadModel('Customer_address_type');
		//			$type = new Customer_address_type();
		//
		//
		//			foreach ($list as $x) {
		//				$x->date_added = printTimestamp($x->date_added);
		//				if ($x->province_other)
		//					$x->province = $x->province_other;
		//				else {
		//					$prov->Load($x->province);
		//					$x->province = $prov->name;
		//				}
		//				$country->Load($x->country);
		//				$x->country = $country->iso;
		//				$type->Load($x->type_id);
		//				$x->type_id = $type->label;
		//				$section_list[] = $x;
		//			}
		//
		//			$columns = $model->_funcGetColumns();
		//			$columns =
		//					array
		//					('id' => 'Options',
		//					 'type_id' => 'Type',
		//					 'label' => 'Label',
		//					 'first_name' => 'First Name',
		//					 'last_name' => 'Last Name',
		//					 'phone' => 'Phone',
		//					 'street' => 'Street',
		//					 'street2' => 'Street 2',
		//					 'city' => 'City',
		//					 'province' => 'Prov',
		//					 'postal' => 'Postal',
		//					 'country' => 'Country',
		//					 'date_added' => 'Date Added');
		//			$content .= $System->getDataTable($section_list, $columns, false, 'customer_address.php?', true) . "<br />";
		//		}
		//		else
		//			$content .= "<p>There are currently no addresses listed for this customer</p>";

		$content .= "<h2>$cname Log</h2>";
		LoadModel('Customer_log');
		$model = new Customer_log();
		$list = $model->Select("*", "customer_id = '{$db_section->id}'");
		//die(print_array($list));
		$section_list = array();
		if (count($list)) {
			foreach ($list as $x) {
				$x->date = printTimestamp($x->date);
				$section_list[] = $x;
			}
			$columns = $model->_funcGetColumns();
			$columns = array(
				'action' => 'Action', 'uri' => 'URI', "ip" => 'IP', 'date' => 'Date'
			);
			$content .= $System->getDataTable($section_list, $columns, false, '', false) . "<br />";
		}
		else
			$content .= "<p>There are currently no access logs for this customer</p>";

		$content .= "<h2>$cname Request Contact</h2>";
		LoadModel('Customer_request');
		$model = new Customer_request();
		$list = $model->Select("*", "customer_id = '{$db_section->id}'");
		//die(print_array($list));
		$section_list = array();
		if (count($list)) {
			foreach ($list as $x) {
				$x->date = printTimestamp($x->date);
				$section_list[] = $x;
			}
			$columns = $model->_funcGetColumns();
			$columns = array(
				'message' => 'Message', 'date' => 'Date'
			);
			$content .= $System->getDataTable($section_list, $columns, false, '', false, true) . "<br />";
		}
		else
			$content .= "<p>There are currently no contact request for this customer</p>";
	}
}
else {
	$db_section_list = $db_section->Select();

	$section_list = array();
	foreach ($db_section_list as $x) {
		/** @var Customer $x */

		$x->date_joined = printTimestamp($x->date_joined);

		$section_list[] = $x;
	}

	$content .= "<h2>$cname List</h2>";

	$columns = $db_section->_funcGetColumns();
	$columns = array(
		'id' => 'Options', 'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'Email',
		'phone' => 'Phone', 'date_joined' => 'Date Joined'
	);

	if ($System->user->is_root()) {
		$content .= "<form method='get'>";
		$content .= "<input type='submit' name='add' value='Add Entry'>";
		$content .= "</form>";
	}

	$content .= $System->getDataTable($section_list, $columns, false, '?', false) . "<br />";
}

$System->renderPage($content);
?>