<?

define('PAGE_TAG', 'manage-notification-templates');

$menu_id = '158';

require_once('_config.php');


$backUrl = "notifications.php";


$System->addInclude('<script type="text/javascript" src="' . JS_PATH . 'notifications.js"></script>');


LoadModel('Sp_notification');

LoadModel('Sp_notification_from');

LoadModel('Sp_notification_log');

LoadModel('Sp_notification_to');

LoadModel('Sp_notification_type');


$entry = new Sp_notification();


$id = $_GET['id'];

if ($id) {

	$entry->Load($id);

}


if (sizeof($_POST)) {

	if (!$_POST['change_renewal'])
		$_POST['change_renewal'] = 0;

	if (!$_POST['change_level'])
		$_POST['change_level'] = 0;

	if (!$_POST['change_status'])
		$_POST['change_status'] = 0;

	if (!$_POST['archive'])
		$_POST['archive'] = 0;


	if ($id) {

		$entry->sent_to = $_POST['sent_to'];


		switch ($entry->type_id) {

			case 1:

			case 2:

				$entry->days_until = $_POST['days_until'];

				break;

			case 3:

				$c .= "<div class='label-wrapper'><label>Sent On:</label></div> Day of renewal";

				break;

			case 4:

			case 5:

				$entry->days_until = $_POST['days_until'];


				break;

		}

		$entry->change_renewal = $_POST['change_renewal'];

		$entry->change_level = $_POST['change_level'];

		$entry->change_status = $_POST['change_status'];

		$entry->archive = $_POST['archive'];


		$entry->subject = $_POST['subject'];

		$entry->content = $_POST['content'];


		$entry->Update();

	}

	else {

		$entry->Insert($_POST);

	}


	header("location: $backUrl");

}


$System->addTopMenu("<div id='cancel' class='button'>Cancel</div>");

$System->addTopMenu("<div id='save' class='button'>Save</div>");


$ps = new Site_page_sections();


$ps->content .= "

<h2>Manage Notifications</h2>";


$t = new Sp_notification_type();

$t->Load($entry->type_id);


$c = "<div class='spacer'></div>";

$c .= "<div class='membership-level'>";

$c .= "<form id='manage-notification' method='post'>";

$c .= $entry->getInput('id');

$c .= "     <div class='inner-section'>";

$c .= "         <div class='inner-heading'>";

$c .= "             <h2>Notification Details</h2>";

$c .= "         </div>";

$c .= "         <div class='inner-subheading inner-subheading-trimmed r-border-light'>";

$c .= "             Notification Details";

$c .= "         </div>";

$c .= "         <div class='inner-subheading'>";

$c .= "             Notification Options";

$c .= "         </div>";

$c .= "         <div class='inner-content'>";

$c .= "             <div class='left r-border-light'>";

$c .= "<div class='label-wrapper'><label>Notification Type:</label></div> $t->label";


$c .= $entry->getInput('sent_to');


switch ($entry->type_id) {

	case 1:

	case 2:

		$c .= $entry->getInput('days_until'); // . " days before";

		break;

	case 3:

		$c .= "<div class='label-wrapper'><label>Sent On:</label></div> Day of renewal";

		break;

	case 4:

	case 5:

		$c .= $entry->getInput('days_until'); // . " days after";

		break;

	default:

		$c .= "<div class='label-wrapper'><label>Sent On:</label></div> Action taken";

}

$c .= "             </div>";

$c .= "             <div class='right'>";


switch ($entry->type_id) {

	case 4:

	case 5:

		$c .= $entry->getInput('change_renewal');

		$c .= $entry->getInput('change_level');

		$c .= $entry->getInput('change_status');

		$c .= $entry->getInput('archive');

		break;

	default:

		$c .= $entry->getInput('change_level');

		$c .= $entry->getInput('change_status');

		break;

}


$c .= "             </div>";

$c .= "         </div>";

$c .= "     </div>";

$c .= "<div class='spacer'></div>";

$c .= "     <div class='inner-section'>";

$c .= "         <div class='inner-heading'>";

$c .= "             <h2>Notification Content</h2>";

$c .= "         </div>";

$c .= $entry->getInput('subject');

$c .= $entry->getInput('content');

$c .= "     </div>";

$c .= "<div class='spacer'></div>";

$c .= "</form>";

$c .= "</div>";


$ps->content .= $c;


$content .= $ps->getContent();


$jquery = '

tinymce.create("tinymce.plugins.ExamplePlugin", {

		createControl: function(n, cm) {

			switch (n) {

				case "mymenubutton":

					var c = cm.createListBox("mymenubutton", {

						title : "+ Custom Value",

						onselect: function(v) {

							tinyMCE.activeEditor.execCommand("mceInsertContent", false, v);

						}

					});';


LoadModel('Sp_custom_var');

$vars = new Sp_custom_var();

$vars = $vars->Select();


foreach ($vars as $var)
	$jquery .= "\nc.add(\"$var->label\", \"$var->var\");\n";


// todo: add customer vars

/*c.add("Custom Var Label 1", "{$custom_var_1}");

c.add("Custom Var Label 2", "{$custom_var_2}");

c.add("Custom Var Label 3", "{$custom_var_3}");

c.add("Custom Var Label 4", "{$custom_var_4}");*/


$jquery .= '

					// Return the new menu button instance

					return c;

			}



			return null;

		}

	});



	// Register plugin with a short name

	tinymce.PluginManager.add("example", tinymce.plugins.ExamplePlugin);



	// Initialize TinyMCE with the new plugin and menu button

	$(".mceLight").tinymce({

		theme : "advanced",



		// Location of TinyMCE script

		script_url : JS_PATH+"cms/tinymce/jscripts/tiny_mce/tiny_mce.js",



		// General options

		plugins : "-example,autolink,lists,pagebreak,table,inlinepopups,contextmenu,paste,nonbreaking",



		// Theme options

		theme_advanced_buttons1 : "undo,redo,|,cut,copy,paste,|,bold,italic,underline,strikethrough,|,bullist,numlist,|,outdent,indent,blockquote,hr",

		theme_advanced_buttons2 : "mymenubutton,link,unlink,anchor,|,sub,sup,charmap,|,tablecontrols",

		theme_advanced_buttons3 : "", //save,newdocument,,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect

		theme_advanced_buttons4 : "",

		theme_advanced_toolbar_location : "top",

		theme_advanced_toolbar_align : "left",

		theme_advanced_statusbar_location : "none",

		theme_advanced_resizing : false,



		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",



		paste_auto_cleanup_on_paste : true,

		paste_remove_styles: true,

		paste_remove_styles_if_webkit: true,

		paste_strip_class_attributes: "all",



		relative_urls : false,

		//file_browser_callback: "CustomFileBrowser",

		//extended_valid_elements : "iframe[src|width|height|name|align]",

		force_br_newlines : true,

		force_p_newlines : false,

		convert_newlines_to_brs: true,

		apply_source_formatting : false,

		forced_root_block : "", // Needed for 3.x

		entity_encoding : "named",



		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js",



		// Replace values for the template plugin

		template_replace_values : {

			username : "Some User",

			staffid : "991234"

		},



		languages : "en",

		disk_cache : true,



		height: "300px",

		width: "738px"

	});



	$(".mceSuperLight").tinymce({

		theme : "advanced",



		// Location of TinyMCE script

		script_url : JS_PATH+"cms/tinymce/jscripts/tiny_mce/tiny_mce.js",



		// General options

		plugins : "-example,autolink,lists,pagebreak,table,inlinepopups,contextmenu,paste,nonbreaking",



		// Theme options

		theme_advanced_buttons1 : "mymenubutton",

		theme_advanced_buttons2 : "",

		theme_advanced_buttons3 : "", //save,newdocument,,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect

		theme_advanced_buttons4 : "",

		theme_advanced_toolbar_location : "top",

		theme_advanced_toolbar_align : "left",

		theme_advanced_statusbar_location : "none",

		theme_advanced_resizing : false,



		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",



		paste_auto_cleanup_on_paste : true,

		paste_remove_styles: true,

		paste_remove_styles_if_webkit: true,

		paste_strip_class_attributes: "all",



		relative_urls : false,

		//file_browser_callback: "CustomFileBrowser",

		//extended_valid_elements : "iframe[src|width|height|name|align]",

		force_br_newlines : true,

		force_p_newlines : false,

		convert_newlines_to_brs: true,

		apply_source_formatting : false,

		forced_root_block : "", // Needed for 3.x

		entity_encoding : "named",



		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js",



		// Replace values for the template plugin

		template_replace_values : {

			username : "Some User",

			staffid : "991234"

		},



		languages : "en",

		disk_cache : true,



		height: "50px",

		width: "738px"

	});';


$System->addJquery($jquery);


$System->addCSS('

.inner-section, .inner-heading {

	width: 900px;

}

.inner-content {

	width: 860px;

}



.inner-subheading, .inner-subheading-trimmed {

width: 410px;

}

.inner-subheading-trimmed, .inner-content .left, .inner-content .right {

width: 409px;

}

.label-wrapper {

width: 220px;

}

');


$System->renderPage($content);







?>





