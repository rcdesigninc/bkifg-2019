<?
define('PAGE_ID', 1);
define('MENU_ID', 52);

require_once('_config.php');
LoadModel('Site_user_log');

if (!$System->user->id)
	$System->user->Logout();

$db_section = new Site_user_log();

if ($id)
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: ?");
}

$content .= "<h1>User Logs</h1>";

$System->dataTableStrip = 1000;

$entries = $System->prepPagination("Site_user_log", '', "date DESC");
$section_list =
		array
		();
$u = new Site_user();
foreach ($entries as $x) {
	$u->Load($x->user_id);
	$x->user_id = $u->first_name;
	$x->success = $x->success ? 'Y' : 'N';
	//$x->action = nl2br($x->action);
	$x->date = printTimestamp($x->date);
	$section_list[] = $x;
}
$columns =
		array
		('action' => 'Action',
		 "success" => 'Success',
		 'uri' => 'URI',
		 "user_id" => 'User',
		 "ip" => 'IP',
		 'date' => 'Date');

/*$columns = $db_section->_funcGetColumns();
array_shift($columns);
array_unshift($columns,array(
			 "custom" => "Id",
		 ));*/


$content .= $System->getDataTable($section_list, $columns) . "<br />";
$content .= $System->renderPagination();


$System->renderPage($content);
?>