<?
define('PAGE_ID', 1);
define('MENU_ID', 45);

require_once('_config.php');
LoadModel('News_and_events');

if (!$System->user->id)
	$System->user->Logout();

$_SESSION['page_id'] = Site_page::getIdByTag('news-and-events', $System->managing_site);

$backUrl = 'news-and-events.php';

$db_section = new News_and_events();
if ($_GET['id'])
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: $backUrl");
}
if ($_GET['delete-image']) {
	$db_section->heading_image = '';
	$db_section->Update();
	header("location: $backUrl");
}


if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert())
			$added = true;
	}
	else
		$db_section->Update();
	header("location: $backUrl");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {
	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);

	if ($db_section->heading_image)
		$preview = "<a href='" . $db_section->renderLink() . "' target='_blank'>Image Prview</a> <a href='#' class='delete-image'>Remove Image</a>";

	$System->addJquery('
	$(".delete-image").live("click",function(){
		if(confirm("Are you sure you wish to remove this sections header image?")) {
			window.location = "?id=' . $_GET['id'] . '&delete-image=true";
		}
	});
	$("input[name=heading_image]").after("' . $preview . '");
	');
}
else {

	$content .= "<h2>News & Events</h2>";

	$content .= "<form method=\"get\" action=\"?\">";
	$content .= "<input type=\"submit\" name=\"add\" value=\"Add Entry\">";
	$content .= "</form>";

	$entries = $System->prepPagination("Site_page_sections",
			"`page_id` = '" . $_SESSION['page_id'] . "' AND `template_section_id` IN(" . System_columns::Content_left . ")",
		"date, heading", 10);
	$section_list =
			array
			();
	foreach ($entries as $x) {
		/** @var News_and_events $x */
		$x->date = printTimestamp($x->date);
		$section_list[] = $x;
	}
	$columns =
			array
			('id' => 'Options',
			 'heading' => 'Heading',
			 'content' => 'Content',
			 'date' => 'Date',);
	//$columns = $db_section->_funcGetColumns();
	$content .= $System->getDataTable($section_list, $columns) . "<br />";
	$content .= $System->renderPagination($from, $page_count);

}

$System->renderPage($content);
?>