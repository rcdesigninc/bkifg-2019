<?
define('PAGE_ID', 1);
require_once('_config.php');

$System->addCSS('
.button {
	width: 200px;
}
');

$System->disableMenu = true;

/*if(!$System->user->is_logged()) {
	if(sizeof($_POST)) {
		$System->user->Login();
	}
}*/

//die(print_array($System->user));
//die(print_array($System->user));
if ($System->user->is_logged()) {
	if ($System->user->is_root() || $System->user->is_admin()) {
		$sites = $System->site->Select();
		if (sizeof($_POST)) {
			foreach ($sites as $site) {
				/** @var Site $site  */
				if ($_POST[$site->id]) {
					$_SESSION['managing_site'] = $site->id;
					header('location: pages.php');
				}
			}
		}

		$content = "<h1>Choose the site you wish to manage:</h1>";
		$content .= "<form method=\"post\">";
		if($System->user->is_admin())
			array_shift($sites);

		foreach ($sites as $site)
			$content .= "<input type=\"submit\" name=\"$site->id\" value=\"$site->name\"> ";
		$content .= "</form>";
	}
	else if ($System->user->is_mod()) {
		$_SESSION['managing_site'] = 2;
		header('location: pages.php');
	}
	$System->addJquery('
	$(".button").css("width","215px");
	');
}
else {
	$System->addJquery('
	$("#login-form #login").click(function(){
		$.post(CMS_AJAX_PATH+"ajax-user-login.php",
			$("#login-form").serialize(), 
			function(data){
				if(data.success)
					window.location = "index.php";
				else
					alert("There was an error logging in, please check your credentials and try again.");
			},"json"
		);
		
	});
	$("input").keypress(function(e){
		if(e.which == 13)
			$("#login-form #login").click();
	});
	$("input[name=username]").focus();
	');

	//die(print_array($System->user));

	$content = "<form id=\"login-form\" method=\"post\">";
	$content .= $System->user->getInput('username') . "<br />";
	$content .= $System->user->getInput('password') . "<br />";
	$content .= "<div class=\"button\" id=\"login\" style=\"width: 75px; margin-left: 110px;\">Login</div>";
	$content .= "</form>";

}
$System->renderPage($content);
?>