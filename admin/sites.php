<?
define('PAGE_ID', 1);
define('MENU_ID', 4);

require_once('_config.php');


$db_section = new Site();
if ($_GET['id'])
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: ?");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert($_POST))
			$added = true;
	}
	else
		$db_section->Update($_POST);
	header("location: ?");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {
	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);
}
else {
	$db_section_list = $db_section->Select();

	$section_list =
			array
			();
	foreach ($db_section_list as $x) {
		$section_list[] = $x;
	}

	$content .= "<h2>Site Settings</h2>";

	$columns =
			array
			('id' => 'Options',
			 'heading' => 'Heading',
			 'date' => 'Date',);
	$columns = $db_section->_funcGetColumns();

	//if(!count($section_list)) {
	$content .= "<form method=\"get\">";
	$content .= "<input type=\"submit\" name=\"add\" value=\"Add Entry\">";
	$content .= "</form>";
	//}

	$content .= $System->getDataTable($section_list, $columns, false, '?', false) . "<br />";
}

$System->renderPage($content);
?>