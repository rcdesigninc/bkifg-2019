<?
define('PAGE_ID', 1);
define('MENU_ID', 2);

require_once('_config.php');

if (!$System->user->id)
	$System->user->Logout();

$backUrl = 'pages.php?id=' . $_SESSION['page_id'];

$db_section = new Site_page_sections();
if ($_GET['id'])
	$db_section->Load($_GET['id']);

if ($_GET['delete']) {
	$db_section->Delete();
	header("location: $backUrl");
}
if ($_GET['delete-image']) {
	$db_section->heading_image = '';
	$db_section->Update();
	header("location: $backUrl");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		if ($db_section->Insert())
			$added = true;
	}
	else
		$db_section->Update();
	header("location: $backUrl");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {
	$db_section->scrubFields();

	//die(print_array($db_section));

	if (($_SESSION['page_id'] >= 58 && $_SESSION['page_id'] <= 60) || $_SESSION['page_id'] == 23)
		$db_section->template_section_id = System_columns::Content_faq;

	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);

	if ($db_section->heading_image)
		$preview = "<a href='" . $db_section->renderPath() . "' target='_blank'>Image Preview</a> <a href='#' class='delete-image'>Remove Image</a>";

	$System->addJquery('
	$(".delete-image").live("click",function(){
		if(confirm("Are you sure you wish to remove this sections header image?")) {
			window.location = "?id=' . $_GET['id'] . '&delete-image=true";
		}
	});
	$("input[name=heading_image]").after("' . $preview . '");
	');
}
else {
	$_SESSION['page_id'] = '';

	$p = new Site_page();
	$page_list = $p->Select("*", "`site_id` = $_SESSION[managing_site]", 'site_id, title'); // exclude the what's new section

	$valid_pages = array();
	foreach ($page_list as $p) {
		$s = new Site();
		$s->Load($p->site_id);
		$p->site_id = $s->name;
		$valid_pages[$p->id] = $p;
	}

	$columns = array(
		'id' => 'Options', 'heading' => 'Heading', 'content' => 'Content'
	);

	foreach ($valid_pages as $page_id => $page) {

		$db_section_list = $db_section->Select("id, heading, SUBSTR(content,1,100) as 'content'",
		                                       "`page_id` = $page_id  AND template_section_id = 3",
		                                       'template_section_id, order_by');

		$section_list = array();
		foreach ($db_section_list as $s)
			$section_list[$s->template_section_id][] = $s;

		if (count($section_list))
			$content .= "<h2>$page->site_id &gt; $page->title</h2>";

		foreach ($section_list as $list) {
			$content .= "<h2>" . $list[0]->template_section_id . "</h2>";
			$content .= $System->getDataTable($list, $columns, true);
		}

		if (count($section_list))
			$content .= "<br />";
	}
}

$System->renderPage($content);
?>