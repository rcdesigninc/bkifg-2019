<?
define('PAGE_ID', 1);
define('MENU_ID', 46);

require_once('_config.php');
LoadModel('Blog_comment');

$db_section = new Blog_comment();
if ($_GET['id'])
	$db_section->Load($_GET['id']);
if ($_GET['delete']) {
	$db_section->Delete();
	header("location: ?");
}

if (sizeof($_POST)) {
	if ($_GET['add']) {
		$_POST['date'] = getTimestamp();
		if ($_POST['is_active'])
			$db_section->date_published = getTimestamp();
		if ($db_section->Insert($_POST))
			$added = true;
	}
	else {
		if ($_POST['is_active'])
			$db_section->date_published = getTimestamp();
		$db_section->Update($_POST);
	}
	header("location: ?");
}

if ($_GET['id'] || ($_GET['add'] && !$added)) {
	if (!$_GET['id'])
		$db_section->date_created = getTimestamp();
	$db_section->date_modified = getTimestamp();
	//die(print_array($db_section));
	//print $System->lastQuery;
	$content .= $System->getInputTable($db_section, $db_section->_funcGetColumns(), true);
}
else {
	$db_section_list = $db_section->Select("*", 'is_deleted = 0', 'date DESC');
	LoadModel('Blog_post');
	$p = new Blog_post();

	for ($i = 0; $i < count($db_section_list); $i++) {
		$d = $db_section_list[$i];

		$d->date = date("M j, Y", $d->date);
		$d->url = $d->url && $d->url != 'Website' ? "<a href='$d->url' target='_blank'>Link</a>" : '-';
		$d->email = "<a href='mailto:$d->email'>Email</a>";
		$p->Load($d->post_id);
		$d->post_id = "<a href='blog.php?id=$d->post_id'>$p->title</a>";
		$d->author_id = $u->first_name;
		$d->is_deleted = $d->is_deleted ? "Yes" : "No";

		$db_section_list[$i] = $d;
	}

	//die(print_array($db_section_list));

	$columns =
			array
			('id' => 'Options',
			 'post_id' => 'Posting',
			 'name' => 'Name',
			 'email' => 'Email',
			 'url' => 'Site',
			 'comment' => 'Comment',
			 'date' => 'Date',
			 'is_deleted' => 'Removed');

	$content .= "<form method=\"get\" action=\"?\">";
	$content .= "<input type=\"submit\" name=\"add\" value=\"Add Entry\">";
	$content .= "</form>";

	$content .= $System->getDataTable($db_section_list, $columns) . "<br />";
}

$System->renderPage($content);
?>