<?


LoadModel('Customer_request');
$customer_request = new Customer_request();

if(sizeof($_POST)) {
	//die(printArray($_POST));
	if($customer_request->Insert()) {
		//die('inserted');
		$System->Error("<p>Thank you for your message!</p>","Your message has been received");
	}
	else
		$System->Error("<p>There was an error attempting to submit your request, please try again.</p>");
	//die('wtf');
}

System::Dialog('popup',$customer_request->renderForm(),'Please fill in the fields below to send your message.<br/>
<span class="name"></span> will get back to you as soon as possible.','',array("scrollBar"=>1,"follow"=>"[!0, !0]"));

$System->addCSS('
.dialog {
	width: 650px;
}
');

$System->addJquery(<<< EOB
$("a[href^=mailto]").each(function(){

	var email = $(this).attr('href').split(':');
	$(this).attr('data-email',email[1]);
	$(this).attr('href','#');

	$(this).click(function(){
		validCapthca = false;
		Recaptcha.reload();
		$("#popup .name").text($(this).text());
		$("#popup input[name=send-to]").val($(this).attr('data-email'));
		
		$("#popup").RC_Dialog().open();

		return false;
	});
});
EOB
);

if(ACTIVE_SITE > 3)
	include("_strip-quote.php");
else
	$System->renderDefaultPage();

/*
 * EOF
 */