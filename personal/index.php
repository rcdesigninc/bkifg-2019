<?php
require_once('_config.php');
$_SESSION['page_id'] = PAGE_ID;

$System->addInclude('<link type="text/css" rel="stylesheet" href="' . TEMP_CSS . 'Personal.css" />');

if (!Site_page::getIdByTag(PAGE_TAG)) {
	$orig_page_id = Site_page::getIdByTag(PAGE_TAG, Sites::Main);
	$System->reloadSections($orig_page_id);

	if (file_exists("../" . PAGE_TAG . ".php"))
		include_once("../" . PAGE_TAG . ".php");
	else
		$System->renderDefaultPage();
}
else if (file_exists(PAGE_TAG . ".php"))
	include_once(PAGE_TAG . ".php");
else {
	switch (PAGE_TAG) {
		case 'home-insurance':
		case 'mortgage-loan-insurance':
		case 'tenant-insurance':
		case 'condominium-insurance':
		case 'cottage-and-seasonal-home':
		case 'home-based-business-insurance':
			include("../_home-quote.php");
			break;
		case 'auto-insurance':
		case 'watercraft-insurance':
			include("../_auto-quote.php");  
			break;
		case 'combined-home-and-auto-coverage':
		case 'newmarket-chamber-insurance-program':
		case 'life-insurance-and-estate-planning':
		case 'group-benefits':
		case 'disability-insurance':
		case 'travel-insurance':
			include("../_strip-quote.php");
			break;
		default:
			$System->renderDefaultPage();
	}
}

/*
 * EOF
 */