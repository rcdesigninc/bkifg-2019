<?

$ps = new Site_page_sections();

$careers = array();



$c = new Site_page_sections();

$careers = $c->Select("*","page_id = " . Site_page::getIdByTag(PAGE_TAG, Sites::Main) . " AND template_section_id = ".System_columns::Content_faq);



$content = '';

if(count($careers)) {

	$content .= $System->sections[0]->getContent();



	$ps->heading = "Current Positions";

	$ps->content = "<p>We ware currently looking for:</p>";

	$System->addCSS('
	.apply-note {
		font-size: 11px;
		font-style: italic;
		line-height: 15px;
	}
	');

	foreach($careers as $c) {

		$ps->content .= "<p><a href='#' class='career-link'>$c->heading</a></p>";



		$c->content .= "<p class='button'>Apply</p>
		<p class='apply-note'>
		Interested candidates will be asked to complete a 12 minute survey 
		at the time of applying to determine their fit for this position.
		</p>";

		$content .= $c->getContent('','careers');

	}



	$System->prependToSectionRight($ps);

}   

else {

	$careers = $c->Select("*","page_id = " . Site_page::getIdByTag(PAGE_TAG, Sites::Main) . " AND template_section_id = ".System_columns::Content_left);

	$ps->heading = 'Careers';

	$c = $careers[0]->content;

	//$ps->content = '<h3>There are currently no positions available at Benson Kearley IFG. Please check back soon!</h3>';
	$ps->content = "$c";

	$content = $ps->getContent();

	$content = '';
}



$System->addJquery(<<< EOB

$(".career-link").click(function(){

	startScroll($(".content-section .content-heading:contains('"+$(this).text()+"')"));

	return false;

});

$(".careers:last").removeClass('careers');

EOB

);

$System->addCSS('
.careers {
  border-bottom: 1px solid #303030;
  padding-bottom: 40px;
}
');



if(ACTIVE_SITE > 3)
	include("_strip-quote.php");
else
	$System->renderDefaultPage($content,'',$billboard);