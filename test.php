<?php die( 'tests' ); ?>
<!DOCTYPE html

        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"

        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>

    <title>Home - Benson Kearley IFG</title>

    <meta name="google-site-verification" content="JVheAapuwSRlKl30H0Ooi5zFaFZXDHkK6rnFRqwsky0"/>

    <meta name="keywords"
          content="Insurance, Insurance Ontario, Insurance Auto Ontario, Insurance Auto Quote, Insurance Auto Quote Ontario, Insurance Newmarket, Insurance Auto Newmarket, Insurance Auto Quote, Insurance Auto Quote Newmarket, Insurance Newmarket Ontario, Car Insurance Newmarket Ontario, Auto Insurance Newmarket Ontario, Home Insurance Newmarket, Home Insurance, Home Insurance Ontario, Auto Insurance, Insurance Services Newmarket, Car Insurance Quotes Newmarket, Insurance Agent Newmarket, Insurance Brokerage Newmarket, Auto Insurance Quotes, Landlord House Insurance,  House Insurance, Property Insurance, Commercial Insurance, Commercial Property Insurance, Commercial Insurance Brokers, Commercial Insurance Quotes, Life Insurance"/>

    <meta name="description"
          content="Decisions are easy when you understand your values. At Benson Kearley IFG, serving you is our privilege. Our brokers are highly functioning, empathetic people who never compromise their pride or integrity in offering you the best insurance on the market with old-fashioned name-to-name service."/>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta http-equiv="Content-Script-Type" content="text/javascript"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>


    <meta name="Author" content="RC Design Inc."/>

    <meta name="language" content="en-us"/>


    <link rel="Shortcut Icon" href="app/Templates/images/favicon.ico"/>

    <link rel="icon" href="app/Templates/images/favicon.ico" type="image/x-icon"/>


    <script type="text/javascript">

		/* GLOBAL VARS */

		PAGE_TAG = 'home';
		PAGE_ID = '2';

		JS_PATH = 'app/System/js/';

		PLUGIN_PATH = 'app/Plugins/';

		AJAX_PATH = 'app/System/ajax/';


		$datepickerImage = "app/System/images/icons/calendar.png";


		CORE_IMGS = 'app/System/images/';

		SYS_AJAX = 'app/System/ajax/';


		TEMP_AJAX = 'app/Templates/ajax/';

		TEMP_CSS = 'app/Templates/css/';

		TEMP_DOCS = 'app/Templates/docs/';

		TEMP_IMGS = 'app/Templates/images/';

		TEMP_JS = 'app/Templates/js/';

    </script>


    <script type="text/javascript" src="app/System/js/_init.js"></script>


    <!--<script type="text/javascript" src="--><!--jquery-ui-common.min.js"></script>-->


    <script type="text/javascript" src="app/Templates/js/vimeo-player/player.js"></script>

    <script type="text/javascript" src="https://use.typekit.com/clg5rpo.js"></script>
    <script type="text/javascript">try {
			Typekit.load();
		} catch (e) {
		}</script>


    <link type="text/css" rel="stylesheet" href="app/Templates/css/System/font-face/Chaparral/Chaparral.css"/>


    <link type="text/css" rel="stylesheet" href="app/Minify/?g=main-css"/>


    <!--	<link rel="stylesheet" type="text/css" href="--><!--Template.css"/>-->


    <script type="text/javascript">

		MENU_ID = '2';

		linkIcons =
			[

				{filter: 'pdf'},

				{filter: 'doc'},

				{filter: 'xls', icon: 'excel'},

				{filter: 'mp3', icon: 'audio'},

				{filter: 'wav', icon: 'audio'}

			];

    </script>


    <script type="text/javascript" src="app/Minify/?g=main-js"></script>


    <script type="text/javascript" src="app/Templates/js/Main.js"></script>
    <link type="text/css" rel="stylesheet" href="app/Templates/css/Homepage.css"/>

    <link type="text/css" rel="stylesheet"
          href="app/Minify/?b=BENSONKEARLEYIFG.COM/app&amp;f=Plugins/Dialog/css/Dialog.css,Plugins/Pagination/css/Pagination.css,Plugins/Slidegal/css/Slidegal.css,Plugins/SmoothDropdown/css/SmoothDropdown.css"/>
    <script type="text/javascript"
            src="app//Minify/?b=BENSONKEARLEYIFG.COM/app&amp;f=Plugins/Dialog/js/Dialog.js,Plugins/Dialog/js/jquery.bpopup.min.js,Plugins/Pagination/js/Pagination.js,Plugins/Recaptcha/js/Recaptcha.js,Plugins/Slidegal/js/jquery.cycle.all.min.js,Plugins/Slidegal/js/Slidegal.js,Plugins/SmoothDropdown/js/SmoothDropdown.js"></script>


    <script type="text/javascript">


		function dialogFix(id) {
			debug("fixing id of.." + id);
			jQuery("#" + id).css({"left": "5px"});
		}

		jQuery(document).ready(function ($) {

			var bPopupDefaults = {

				amsl: 50,

				appendTo: "body",

				closeClass: "close",

				content: "ajax",

				contentContainer: null,

				escClose: !0,

				fadeSpeed: 250,

				follow: [!0, !0],

				followSpeed: 500,

				loadUrl: null,

				modal: !0,

				modalClose: 0,

				modalColor: "#000",

				onClose: null,

				onOpen: null,

				opacity: 0.90,

				position: ["auto", "auto"],

				scrollBar: 0,

				zIndex: 99999

			};

			//$("#contest-dialog").bPopup(bPopupDefaults);


			if ($.browser.webkit || $.browser.safari) {

				$("#contest-dialog div").prepend('<a href="#" class="close" style="width: 72px; height: 24px; position: absolute; top: 0; left: 804px; z-index: 999;"></a>' +

					'<a href="http://www.homeforholidays.ca" target="_blank" style="width: 380px; height: 72px; position: absolute; top: 303px; left: 0; z-index: 999;"></a>');

			}


			$(".subsite-selector").hover(function () {
				$(".subsite-options").show();
			}, function () {
				$(".subsite-options").hide();
			});
			$(".subsite-options div").click(function () {
				window.location = "/" + $(this).attr('data-target');
			});
			var daHomeVid = null;
			$("#watch-video").click(function () {
				$("#home-video-box").RC_Dialog().open();
				setTimeout("dialogFix('home-video-box')", 1);
				setTimeout("dialogFix('home-video-box')", 200);
				setTimeout("dialogFix('home-video-box')", 500);

				$(".close").click(function () {
					if (!daHomeVid)
						daHomeVid = $f("home-vid");
					daHomeVid.api("unload");
				});
			});
			$("#home-video-box").RC_Dialog({
				"appendTo": "#header",
				"auto_load": false,
				"close_pos_x": "left",
				"close_pos_y": "top",
				"close_offset": "0, 0"
			});
			$('#505c9840afbcb-Slidegal_data').RC_Slidegal({
				"id": "505c9840afbcb-Slidegal_data",
				"width": 960,
				"height": 407,
				"speed": 9000,
				"animate": "fade",
				"show_nav": 3,
				"customer_pager": false
			});
			$(".billboard-button:contains('Video')").click(function () {
				$("#video-box").RC_Dialog().open();
			});
			$.preLoadImages(['app/Uploads/site_page_sections/heading27.jpg', 'app/Uploads/site_page_sections/heading12.jpg', 'app/Uploads/site_page_sections/heading26.jpg', 'app/Uploads/site_page_sections/heading50.jpg', 'app/Uploads/site_page_sections/heading49.jpg', 'app/Uploads/site_page_sections/heading343.jpg']);
			$("#video-box").RC_Dialog({
				"appendTo": "#header",
				"auto_load": false,
				"close_pos_x": "left",
				"close_pos_y": "top",
				"close_offset": "0, 0"
			});


			buildRelated();

		});
    </script>
    <style>
        #contest-dialog {
            display: none;
        }
    </style>


    <style type="text/css">
        #home-video-box {
            border: 0;
        }

        #home-video-box .heading {
            display: none;
        }

        #home-video-box .content {
            padding: 0;
        }

        #video-box {
            border: 0;
        }

        #video-box .heading {
            display: none;
        }

        #video-box .content {

            padding: 0;
        }

        #billboard-container, #billboard-inside {
            height: 645px;
        }

        #contentRight .content-section {

            padding: 0 17px 20px;

            margin-bottom: 35px;

        }

        .short-title {
            margin-top: 30px;
        }

        .billboard-content-container {
            display: block;
            padding-left: 0;
        }

        .billboard-content {
            display: block;
            margin-left: 15px;
            margin-top: 20px;
        }
    </style>


    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="app/Templates/css/ie7.css"/>    <![endif]-->

</head>

<body>

<a id="page-top" href="#"></a>
<div id="contest-dialog" style="width: 875px; height: 401px; margin: 0 auto;">

    <div style='margin: 0 auto;'>

        <a href="#" class="close"
           style="width: 72px; height: 24px; position: absolute; top: 0; left: 804px; z-index: 999;"
           onclick="document.getElementById('bModal').style.display='none';"></a>

        <a href="http://www.homeforholidays.ca" target="_blank"
           style="width: 380px; height: 72px; position: absolute; top: 303px; left: 0; z-index: 999;"></a>

        <img src="splash/popup.contest.jpg" width="875" height="401" border="0" usemap="#Map" style="z-index: 1;"/>

        <map name="Map" id="Map">

            <area shape="rect" coords="1,303,380,373" href="http://www.homeforholidays.ca" target="_blank"
                  alt="Click here to enter our contest!"/>

            <area shape="rect" coords="805,4,871,25" href="#" alt="Close" class="close"/>

        </map>


        <a href="http://www.homeforholidays.ca" target="_blank"
           style="left: -55px; position: absolute; top: 278px;  z-index: 1;"><img src="splash/bow.png" width="137"
                                                                                  height="137"/></a>


    </div>

</div>

<div id="top-menu-dummy-div"></div>

<!-- DUMMY DIV FOR TOP MENU CONTINUATION -->

<div id="header-container">

    <div id="header">


        <div id="header-logo">

            <a href="/"><img src="app/Templates/images/header/header-logo.png" alt="Logo"/></a>

        </div>

        <div id="top-menu-container">

            <div id="top-menu" class="top-menu">

                <!--<div class='headerSocialMedia' style='float:left; margin-left:17px;'>

					<a href=''><img src='header/header_fb.jpg'/></a></div>

				<div class='headerSocialMedia' style='float:left;'>

					<a href=''><img src='header/header_t.jpg'/></a></div>

				<div class='headerSocialMedia' style='float:left;'>

					<a href=''><img src='header/header_in.jpg'/></a></div>

				<div class='headerSocialMedia' style='float:left; margin-right:8px;'>

					<a href=''><img src='header/header_v.jpg'/></a></div>-->


                <div id='social_container' style="display: block;">
                    <img src="/app/Templates/images/social/social-dd.png" width="35" height="34"/>
                    <div class="rollover" style="">
                        <a href="http://www.facebook.com/BensonKearleyIFG" target="_blank">
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/facebook.png"/>
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/facebook-over.png"
                                 style="display: none;"/>
                        </a>
                        <br/>
                        <a href="https://twitter.com/#!/BKIFG" target="_blank">
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/twitter.png"/>
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/twitter-over.png"
                                 style="display: none;"/>
                        </a> <br/>
                        <a href="http://ca.linkedin.com/company/benson-kearley-ifg?trk=ppro_cprof" target="_blank">
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/linkedin.png"/>
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/linkedin-over.png"
                                 style="display: none;"/>
                        </a>
                        <br/>
                        <a href="https://vimeo.com/bkifg" target="_blank" style="margin-top: -7px;">
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/vimeo.png"/>
                            <img src="http://bensonkearleyifg.com/app/Templates/images/social/vimeo-over.png"
                                 style="display: none;"/>
                        </a>
                        <br/>
                    </div>
                </div>
                <div id='radial_container'>
                    <div class="radial-menu">
                        <img src="http://bensonkearleyifg.com/app/Templates/images/social/social-radial.png"/></div>
                    <ul class='list'>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>

                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>


                        <li class='item'>
                            <div class='my_class'>
                                <a href="https://vimeo.com/bkifg" target="_blank"><img
                                            src="http://bensonkearleyifg.com/app/Templates/images/social/vimeo.png"/>
                                </a></div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>
                                <a href="http://ca.linkedin.com/company/benson-kearley-ifg?trk=ppro_cprof"
                                   target="_blank">
                                    <img src="http://bensonkearleyifg.com/app/Templates/images/social/linkedin.png"/></a>
                            </div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>
                                <a href="https://twitter.com/#!/BKIFG" target="_blank">
                                    <img src="http://bensonkearleyifg.com/app/Templates/images/social/twitter.png"/>
                                </a></div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>
                                <a href="http://www.facebook.com/BensonKearleyIFG" target="_blank"><img
                                            src="http://bensonkearleyifg.com/app/Templates/images/social/facebook.png"/></a>
                            </div>
                        </li>

                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>
                        <li class='item'>
                            <div class='my_class'>test</div>
                        </li>

                    </ul>
                </div>


                <div class="menu-container" id="menu-2-Container">

                    <div class="menu1 menutop menu1Open" id="menu-2">

                        <a href="/index.php?section=home">Home</a>

                    </div>

                    <div class="submenu" id="menu-2-Submenu" style="display: none;">

                    </div>

                </div>


                <div id='menu-13-Container' class='menu-container'>

                    <div id='menu-13' class='menu1 menutop'>

                        <a href='index.php?section=about'>About</a>

                    </div>

                    <div id='menu-13-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-14-Container' class='menu-container'>

                    <div id='menu-14' class='menu1 menutop'>

                        <a href='index.php?section=team'>Team</a>

                    </div>

                    <div id='menu-14-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-15-Container' class='menu-container'>

                    <div id='menu-15' class='menu1 menutop'>

                        <a href='index.php?section=community'>Community</a>

                    </div>

                    <div id='menu-15-Submenu' class='submenu'>

                        <div id='menu-15' class='menu2'>
                            <div><a href='index.php?section=community'>Our Community</a></div>
                        </div>

                        <div id='menu-68' class='menu2'>
                            <div><a href='index.php?section=home-for-holidays'>Home For Holidays</a></div>
                        </div>

                    </div>

                </div>

                <div id='menu-16-Container' class='menu-container'>

                    <div id='menu-16' class='menu1 menutop'>

                        <a href='index.php?section=careers'>Careers</a>

                    </div>

                    <div id='menu-16-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-17-Container' class='menu-container'>

                    <div id='menu-17' class='menu1 menutop'>

                        <a href='index.php?section=claims'>Claims</a>

                    </div>

                    <div id='menu-17-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-23-Container' class='menu-container'>

                    <div id='menu-23' class='menu1 menutop'>

                        <a href='index.php?section=insurance-glossary'>Resources</a>

                    </div>

                    <div id='menu-23-Submenu' class='submenu'>

                        <div id='menu-23' class='menu2'>
                            <div><a href='index.php?section=insurance-glossary'>Insurance Glossary</a></div>
                        </div>

                        <div id='menu-24' class='menu2'>
                            <div><a href='index.php?section=insurance-advice'>Insurance Advice</a></div>
                        </div>

                        <div id='menu-25' class='menu2'>
                            <div><a href='index.php?section=insurance_links'>Insurance Links</a></div>
                        </div>

                    </div>

                </div>

                <div id='menu-5-Container' class='menu-container'>

                    <div id='menu-5' class='menu1 menutop'>

                        <a href='index.php?section=contact'>Contact</a>

                    </div>

                    <div id='menu-5-Submenu' class='submenu'>

                    </div>

                </div>
            </div>

        </div>

        <div id="menu-container">

            <div id="menu" class="main-menu">


                <div id='menu-main-15-Container' class='menu-container'>

                    <div id='menu-main-15' class='menu1 menutop'>

                        <a href='/personal/'>Personal Insurance</a>

                    </div>

                    <div id='menu-main-15-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-main-16-Container' class='menu-container'>

                    <div id='menu-main-16' class='menu1 menutop'>

                        <a href='commercial/'>Commercial Insurance</a>

                    </div>

                    <div id='menu-main-16-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-main-66-Container' class='menu-container'>

                    <div id='menu-main-66' class='menu1 menutop'>

                        <a href='financial/'>Financial Services</a>

                    </div>

                    <div id='menu-main-66-Submenu' class='submenu'>

                    </div>

                </div>
            </div>

        </div>


    </div>

</div>

<div id="billboard-container">

    <div id="billboard">

        <div id="billboard-inside">


            <div id="505c9840afbcb-Slidegal_data-controls" class="slidegal-controls hidden">
                <div id="505c9840afbcb-Slidegal_data-next" class="next"></div>
                <div id="505c9840afbcb-Slidegal_data-prev" class="prev"></div>
            </div>
            <div id='505c9840afbcb-Slidegal_data' class='Slidegal'>
                <div class='hidden '>
                    <img src='app/Uploads/site_page_sections/heading27.jpg'/>
                    <div class='billboard-content-container' style='background-color: #717174'>
                        <div class='billboard-content'>
                            <div class='billboard-title '>We welcome you to a fresh experience...</div>
                            <div class='billboard-copy'><p>Watch our video to learn more about the Benson Kearley IFG
                                    experience and learn about how we put you first!</p></div>
                        </div>
                        <div class='billboard-button' style='color: #717174;'>Watch The Video</div>
                    </div>
                </div>
                <div class='hidden '>
                    <img src='app/Uploads/site_page_sections/heading12.jpg'/>
                    <div class='billboard-content-container' style='background-color: #8c0c04'>
                        <div class='billboard-content'>
                            <div class='billboard-title '>We&#039;ve got you covered...</div>
                            <div class='billboard-copy'><p>With Benson Kearley IFG, you&rsquo;ll be able to breathe easy
                                    with our affordable home insurance. See for yourself!</p></div>
                        </div>
                        <div class='billboard-button' style='color: #8c0c04;'><a style='color: #8c0c04;'
                                                                                 href='https://www2.compu-quote.com/ezleadsplus/introhab.asp?ORGSITE=ezLeadsplus&BRKRCDE=BKAI'>Start
                                your quote!</a></div>
                    </div>
                </div>
                <div class='hidden '>
                    <img src='app/Uploads/site_page_sections/heading26.jpg'/>
                    <div class='billboard-content-container' style='background-color: #8c0c04'>
                        <div class='billboard-content'>
                            <div class='billboard-title short-title'>It&#039;s all about you...</div>
                            <div class='billboard-copy'><p>Our pride and integrity is in offering you the best insurance
                                    on the market with old-fashioned name-to-name service.</p></div>
                        </div>
                        <div class='billboard-button' style='color: #8c0c04;'><a style='color: #8c0c04;'
                                                                                 href='/personal/'>Learn more</a></div>
                    </div>
                </div>
                <div class='hidden '>
                    <img src='app/Uploads/site_page_sections/heading50.jpg'/>
                    <div class='billboard-content-container' style='background-color: #d05f14'>
                        <div class='billboard-content'>
                            <div class='billboard-title '>One to one business approach...</div>
                            <div class='billboard-copy'><p>At Benson Kearley IFG, our model is simple; we offer
                                    one-to-one insurance solutions for your business.</p></div>
                        </div>
                        <div class='billboard-button' style='color: #d05f14;'><a style='color: #d05f14;'
                                                                                 href='commercial/'>Learn more</a></div>
                    </div>
                </div>
                <div class='hidden '>
                    <img src='app/Uploads/site_page_sections/heading49.jpg'/>
                    <div class='billboard-content-container' style='background-color: #003479'>
                        <div class='billboard-content'>
                            <div class='billboard-title '>Premiere financial service...</div>
                            <div class='billboard-copy'><p>Our professional financial planners are lauded for being the
                                    best in the business. Whatever your goals are, we&#039;ll help you reach them.</p>
                            </div>
                        </div>
                        <div class='billboard-button' style='color: #003479;'><a style='color: #003479;'
                                                                                 href='financial/'>Learn more</a></div>
                    </div>
                </div>
                <div class='hidden '>
                    <img src='app/Uploads/site_page_sections/heading343.jpg'/>
                    <div class='billboard-content-container' style='background-color: #8c0c04'>
                        <div class='billboard-content'>
                            <div class='billboard-title short-title'>Home For Holidays!</div>
                            <div class='billboard-copy'><p>Sometimes it takes more than desire to bridge distance. In
                                    2011 we launched our first community contest&mdash;to bring home a family member for
                                    the holidays!</p></div>
                        </div>
                        <div class='billboard-button' style='color: #8c0c04;'><a style='color: #8c0c04;'
                                                                                 href='index.php?section=home-for-holidays'>Learn
                                More</a></div>
                    </div>
                </div>
            </div>
            <div id="505c9840afbcb-Slidegal_data-nav" class="slidegal-nav hidden">
                <div class="slidegal-inner-nav" id="505c9840afbcb-Slidegal_data-inner-nav">
                </div>
            </div>
            <!--[if lte IE 8]>
            <style type="text/css">
                #billboard-container {
                    height: 613px;
                }

                #billboard-inside {
                    height: 643px;
                }

                .grad-box {
                    bottom: 25px;
                }
            </style>
            <![endif]-->

            <div id="home-box-container" class="clear-float">

                <div><a href="/financial"><img src="/app/Templates/images//home/home-box3.png"/><img
                                src="app/Templates/images//home/home-box3-over.png" class="hidden"/></a></div>

                <div><a href="/commercial"><img src="/app/Templates/images//home/home-box2.png"/><img
                                src="app/Templates/images//home/home-box2-over.png" class="hidden"/></a></div>

                <div><a href="/personal"><img src="/app/Templates/images//home/home-box1.png"/><img
                                src="/app/Templates/images//home/home-box1-over.png" class="hidden"/></a></div>

            </div>

            <div id="grad-box" class="grad-box">

                <div id="callout-copy">

                    <div class="callout-title">Get your FREE online quote NOW!</div>

                    <div class="callout-content"><p>Old-fashioned one-to-one service right at your fingertips!</p>
                        <p>Click on an icon for an <span class="related-links">immediate online quote today</span>!</p>
                    </div>

                </div>

                <div class="callout-buttons">

                    <ul class="quote-b-list">

                        <li>
                            <a href="https://www2.compu-quote.com/ezleadsplus/introhab.asp?ORGSITE=ezLeadsplus&BRKRCDE=BKAI"
                               target="_blank" class="home-quote"></a></li>

                        <li>
                            <a href="https://www2.compu-quote.com/ezleadsplus/intro.asp?ORGSITE=ezLeadsplus&BRKRCDE=BKAI"
                               target="_blank" class="auto-quote"></a></li>

                    </ul>

                </div>

            </div>

            <div class='clear-float'></div>

        </div>

    </div>

</div>

<div id="page-container">

    <div id="content-container" class="content-container">

        <div id="content">

            <div id="contentLeft-container" class="contentLeft-container">

                <div id="contentLeft">

                    <div id='scroll-btt'><a href='#'><img src='app/Templates/images/back2top-mini.png'/></a></div>


                    <div class='content-section clear-float'>
                        <h2 class='content-heading'>It&#039;s all about our customer...</h2>
                        <div class='content-body'><h3 class="intro">Decisions are easy when you understand your values.
                                At Benson Kearley IFG, serving you is our privilege. Our brokers are highly functioning,
                                empathetic people who never compromise their pride or integrity in offering you the best
                                insurance on the market with old-fashioned name-to-name service. Though our scope is
                                global, our touch is local. <a href="index.php?section=about">READ MORE</a></h3>
                        </div>
                    </div>

                    <div class='content-section float-box-left'>
                        <h2 class='content-heading'>The Community</h2>
                        <div class='content-body'><p>Business is an essential, embedded part of this community, and we
                                are conscious of that every day. We represent Benson Kearley IFG outside the office as
                                well as in it, which is why each year we Participate in a number of community programs.
                                It&rsquo;s about give and take.</p>
                            <p><strong><a href="index.php?section=community">LEARN MORE ABOUT OUR INVOLVEMENT<br/>WITHIN
                                        THE COMMUNITY</a></strong></p>
                        </div>
                    </div>

                    <div class='content-section float-box-right'>
                        <h2 class='content-heading'>Insurance Simplified</h2>
                        <div class='content-body'><p>Insurance terms can be quite complex if not explained properly.
                                Click out these helpful definitions that we&rsquo;ve put together to make understanding
                                insurance easy.<strong><a href="#"></a></strong></p>
                            <p><strong><a href="index.php?section=insurance-glossary">VIEW OUR RESOURCES</a></strong>
                            </p>
                        </div>
                    </div>

                    <div class='content-section clear-float'>
                        <h2 class='content-heading'>Newmarket Chamber of Commerce Insurance Program</h2>
                        <div class='content-body'><p>Chamber of Commerce members and employees work hard for our
                                community. If you work for&nbsp;a member of the Newmarket Chamber of Commerce you may
                                qualify for&nbsp;our preferred&nbsp;home and auto insurance rates.&nbsp;</p>
                            <p><strong><a href="/personal/index.php?section=newmarket-chamber-insurance-program">LEARN
                                        MORE ABOUT THE PROGRAM</a></strong></p>
                        </div>
                    </div>
                    <div id='home-video-box' class='dialog'>
                        <div class='close '></div>
                        <div class='heading'><h3></h3></div>
                        <div class='content'>
                            <iframe id="home-vid"
                                    src="https://player.vimeo.com/video/37256927?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=home-vid"
                                    width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen
                                    allowFullScreen></iframe>
                        </div>
                    </div>
                    <div id='video-box' class='dialog'>
                        <div class='close '></div>
                        <div class='heading'><h3></h3></div>
                        <div class='content'>
                            <iframe id="home-vid"
                                    src="https://player.vimeo.com/video/37256927?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=home-vid"
                                    width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen
                                    allowFullScreen></iframe>
                        </div>
                    </div>
                </div>

            </div>

            <div id="contentRight-container">

                <div id="contentRight" class="contentRight">


                    <div class='content-section hidden related-links'>
                        <h2 class='content-heading'></h2>
                        <div class='content-body'>
                        </div>
                    </div>
                    <div class="content-section " id="save-and-combine" style="">
                        <h2 class="content-heading">Watch a message from our President...</h2>
                        <div class="content-body">
                            <p>We are independent insurance agency leaders at the top of our game, with our feet planted
                                firmly in our community. Click on the icon below to watch a video from our President,
                                Stephen Kearley.</p>
                            <div id="watch-video" style=""></div>
                        </div>
                    </div>

                    <div class='content-section '>
                        <h2 class='content-heading'>Save when you combine!</h2>
                        <div class='content-body'><h3>Combine your home and auto insurance to get the best deal! <a
                                        href="/personal/index.php?section=combined-home-and-auto-coverage">Click here</a>
                                to start today!</h3>
                            <p style="text-align: center;"><a
                                        href="/personal/index.php?section=combined-home-and-auto-coverage"><img
                                            style="margin-bottom: 10px;" title="Home &amp; Auto Combind"
                                            src="../app/Uploads/admin_uploads/main/home-auto-combo-image.png"
                                            alt="Home &amp; Auto Combind" width="149" height="80"/></a></p>
                            <p>Prefer to speak to someone over the phone? Contact us at 1.800.463.6503, Monday-Friday
                                8:30 a.m. to 5:00 p.m.</p>
                        </div>
                    </div>

                    <div class='content-section '>
                        <h2 class='content-heading'>Report a Claim</h2>
                        <div class='content-body'><h3>Benson Kearley IFG is always here for you. To report a claim,
                                please <a href="index.php?section=claims">click here</a>.</h3>
                            <p>Prefer to speak to someone over the phone? Contact us at 1.800.463.6503, Monday-Friday
                                8:30 a.m. to 5:00 p.m.</p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="clear-float"></div>

        </div>

    </div>

</div>

<div id="footer-container">

    <div id="footer">

        <a href="#" class="back2top"> <img src="app/Templates/images//back2top.png" alt="back to top"/> </a>


        <div class="footer-copy-section">

            <img src="app/Templates/images/bk-logo-white-footer.png" alt="Benson Kearly logo"/>

            <p>17705 Leslie St. Suite 101,<br/>Newmarket, ON, L3Y 3E3</p>


            <p>Phone: 905.898.3815<br/>Toll Free: 800.463.6503</p>


            <p class="footer-social" style="visibility: visible;">

                <a href="http://www.facebook.com/BensonKearleyIFG" target="_blank">
                    <img src="app/Templates/images//social/footer/fb-icon.png" alt="facebook" class="soc-med-icon"/>
                    <img src="app/Templates/images//social/footer/fb-icon-over.png" alt="facebook" class="soc-med-icon"
                         style="display: none;"/>
                </a>

                <a href="https://twitter.com/#!/BKIFG" target="_blank">
                    <img src="app/Templates/images//social/footer/twitter-icon.png" alt="facebook"
                         class="soc-med-icon"/>
                    <img src="app/Templates/images//social/footer/twitter-icon-over.png" alt="facebook"
                         class="soc-med-icon" style="display: none;"/>
                </a>
                <a href="http://ca.linkedin.com/company/benson-kearley-ifg?trk=ppro_cprof" target="_blank">
                    <img src="app/Templates/images//social/footer/linkedin-icon.png" alt="facebook"
                         class="soc-med-icon"/>
                    <img src="app/Templates/images//social/footer/linkedin-icon-over.png" alt="facebook"
                         class="soc-med-icon" style="display: none;"/>
                </a>
                <a href="https://vimeo.com/bkifg" target="_blank">
                    <img src="app/Templates/images//social/footer/vimeo-icon.png" alt="facebook" class="soc-med-icon"/>
                    <img src="app/Templates/images//social/footer/vimeo-icon-over.png" alt="facebook"
                         class="soc-med-icon" style="display: none;"/>
                </a>

            </p>


            <p>&copy;2012 Benson Kearley IFG<br/>

                Site designed and developed by
                <a href="http://www.rcdesign.com" target="_blank" class="rc-link">RC Design</a><br/>

                <a href="index.php?section=privacy-policy">Privacy Policy</a>

            </p>


        </div>

        <div id="footer-menu">

            <div class="menu-container" id="menu-2-Container">

                <div class="menu1 menutop menu1Open" id="footer-menu-2">

                    <a href="/index.php?section=home">Home</a>

                </div>


            </div>


            <div id='menu-13-Container' class='menu-container'>

                <div id='menu-13' class='menu1 menutop'>

                    <a href='index.php?section=about'>About</a>

                </div>

                <div id='menu-13-Submenu' class='submenu'>

                </div>

            </div>

            <div id='menu-14-Container' class='menu-container'>

                <div id='menu-14' class='menu1 menutop'>

                    <a href='index.php?section=team'>Team</a>

                </div>

                <div id='menu-14-Submenu' class='submenu'>

                </div>

            </div>

            <div id='menu-15-Container' class='menu-container'>

                <div id='menu-15' class='menu1 menutop'>

                    <a href='index.php?section=community'>Community</a>

                </div>

                <div id='menu-15-Submenu' class='submenu'>

                </div>

            </div>

            <div id='menu-16-Container' class='menu-container'>

                <div id='menu-16' class='menu1 menutop'>

                    <a href='index.php?section=careers'>Careers</a>

                </div>

                <div id='menu-16-Submenu' class='submenu'>

                </div>

            </div>

            <div id='menu-17-Container' class='menu-container'>

                <div id='menu-17' class='menu1 menutop'>

                    <a href='index.php?section=claims'>Claims</a>

                </div>

                <div id='menu-17-Submenu' class='submenu'>

                </div>

            </div>

            <div id='menu-23-Container' class='menu-container'>

                <div id='menu-23' class='menu1 menutop'>

                    <a href='index.php?section=insurance-glossary'>Resources</a>

                </div>

                <div id='menu-23-Submenu' class='submenu'>

                </div>

            </div>

            <div id='menu-5-Container' class='menu-container'>

                <div id='menu-5' class='menu1 menutop'>

                    <a href='index.php?section=contact'>Contact</a>

                </div>

                <div id='menu-5-Submenu' class='submenu'>

                </div>

            </div>
        </div>

        <div id="footer-sub-menu">

            <div class="sub-box">

                <span>Personal Insurance</span>


                <div id='menu-26-Container' class='menu-container'>

                    <div id='menu-26' class='menu1 menutop'>

                        <a href='/personal/index.php?section=home-insurance'>Home Insurance</a>

                    </div>

                    <div id='menu-26-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-27-Container' class='menu-container'>

                    <div id='menu-27' class='menu1 menutop'>

                        <a href='/personal/index.php?section=auto-insurance'>Auto Insurance</a>

                    </div>

                    <div id='menu-27-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-28-Container' class='menu-container'>

                    <div id='menu-28' class='menu1 menutop'>

                        <a href='/personal/index.php?section='>Personal Insurance</a>

                    </div>

                    <div id='menu-28-Submenu' class='submenu'>

                    </div>

                </div>
            </div>

            <div class="sub-box">

                <span>Commercial Insurance</span>


                <div id='menu-29-Container' class='menu-container'>

                    <div id='menu-29' class='menu1 menutop'>

                        <a href='/commercial/index.php?section=plastics-protect'>Commercial Insurance</a>

                    </div>

                    <div id='menu-29-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-48-Container' class='menu-container'>

                    <div id='menu-48' class='menu1 menutop'>

                        <a href='/commercial/index.php?section=business-solutions'>Business Solutions</a>

                    </div>

                    <div id='menu-48-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-49-Container' class='menu-container'>

                    <div id='menu-49' class='menu1 menutop'>

                        <a href='/commercial/index.php?section=claims-management'>Claims Management</a>

                    </div>

                    <div id='menu-49-Submenu' class='submenu'>

                    </div>

                </div>
            </div>

            <div class="sub-box">

                <span>Financial Services</span>


                <div id='menu-43-Container' class='menu-container'>

                    <div id='menu-43' class='menu1 menutop'>

                        <a href='/financial/index.php?section=financial-planning'>Financial Services</a>

                    </div>

                    <div id='menu-43-Submenu' class='submenu'>

                    </div>

                </div>

                <div id='menu-35-Container' class='menu-container'>

                    <div id='menu-35' class='menu1 menutop'>

                        <a href='/financial/index.php?section=banking-services'>Banking Services</a>

                    </div>

                    <div id='menu-35-Submenu' class='submenu'>

                    </div>

                </div>
            </div>

            <div class="clearfix"></div>

        </div>

        <div id="footer-tag"><span>It's all about our customer</span></div>

        <div class="clear-float"></div>

    </div>

</div>


<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-7013831-30']);
	_gaq.push(['_trackPageview']);
	(function () {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>

<!--script type='text/javascript'>

    var _gaq = _gaq || [];

    _gaq.push(['_setAccount', 'UA-7013831-30']);

    _gaq.push(['_trackPageview']);



    (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

    })();

</script-->

</body>
</html>