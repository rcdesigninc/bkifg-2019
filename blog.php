<?

$System->addInclude('<script type="text/javascript" src="' . JS_PATH . 'core/blog.js"></script>');
$System->addInclude('<script type="text/javascript" src="/app/Templates/js/jscroll.js"></script>');

LoadModel('Blog_post');
LoadModel('Blog_category');

$_SESSION['cat'] = $_GET['cat'];

$content = '';
if(isset($_GET['ajax'])) {
    Blog_post::getAjaxPagingData();
}
else {
    if ($_GET['post']) {
        $current_post = Blog_post::LoadByPublishDate($_GET['published']);

        // Set to use for comment form
        $_SESSION['post_id'] = $current_post->id;

        $content .= '<div class="clear-float"></div>';
        $content .= "<a class='back-to-blog' href='/index.php?section=blog'>&lt; Back to Blog</a>";
        $content .= '<div class="clear-float bottom-border"></div>';

        $content .= $current_post->getContent();

        // comments not supported - commenting out
        //LoadModel('Blog_comment');
        //$content .= Blog_comment::renderComments($current_post->id);
        //$content .= Blog_comment::renderCommentForm($current_post->id);

        $posts = $current_post->Select("id,title,date_published", $_SESSION['where'], 'date_published DESC');

        $nextPost = $lastPost = 0;
        foreach ($posts as $post) {
            if ($nextPost) {
                $nextPost = "?post=" . hyphonate($post->title) . "&published=$post->date_published";
                break;
            }
            else if ($current_post->id == $post->id) {
                $nextPost = 1;
            }
            else {
                $lastPost = "?post=" . hyphonate($post->title) . "&published=$post->date_published";
            }
        }

/*        if ($lastPost) $lastPost = 'history.go(-1);';
        if ($nextPost) $nextPost = "window.location = '$nextPost'";

        $System->addJquery('
        $("#prev-postings").click(function() {
            ' . $lastPost . '
        });
        $("#next-postings").click(function() {
            ' . $nextPost . '
        });');*/

        $content .= '<div class="clear-float"></div>';
        $content .= "<a class='back-to-blog' href='/index.php?section=blog'>&lt; Back to Blog</a>";
        $content .= '<div class="clear-float"></div>';
    }
    else {
        $filters = Blog_post::categoryFilterRender();
        $content = Blog_post::filterListProcess()->getContent();

        $content = $filters.$content;
    }
    $recent = Blog_post::renderRecentEntries();
    $contentRight = $recent->getContent();

    //$cats = Blog_category::renderCategories();
    //$contentRight .= $cats->getContent();

    $System->renderDefaultPage($content, $contentRight);

    ?>
    <script>
        jQuery(document).ready(function(){
            jQuery(".radio-filter-container").find("input[type='radio']").unbind().click(function(e){
                updateFilterDisplay(this);
            });
            jQuery("#autoscroll-container").closest(".content-body").jscroll({loadingHtml:"<div class='autoscroll-loading-more'>Loading more stories...</div>"});
        });

        function updateFilterDisplay(obj){
            var _this = jQuery(obj);
            var _n = _this.attr("name");
            var _v = _this.attr("value");
            var _allItem = "."+_n+"-item";

            if(_v == '*'){
                _toShow = jQuery(_allItem).not(":visible,#no-posts-found");

                jQuery("#no-posts-found").slideUp();
                _toShow.slideDown();
            }
            else{
                var _toHide = "."+_n+"-"+_v;
                var _toShow = "."+_n+"-"+_v;

                _allItem = jQuery(_allItem);
                _toHide = _allItem.not(_toHide);
                _toShow = _allItem.filter(_toShow);

                if(_toShow.length > 0){
                    _toShow.slideDown();
                    _toHide.slideUp();
                }
                else{
                    _toHide.not("#no-posts-found").slideUp();
                    jQuery("#no-posts-found").not(":visible").slideDown();
                }
            }

            jQuery("input[name='"+_n+"']").closest("li").removeClass('radio-filter-selected');
            _this.closest("li").addClass("radio-filter-selected");
        }
    </script>
<?php } ?>