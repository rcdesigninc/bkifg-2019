<?
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="keywords" content="Benson Kearley Insurance Brokers, Benson, Kearley, Insurance, insurance broker, Ontario, Newmarket, insurance newmarket, Plastics Protect, personal insurance, commercial insurance, financial services, investments, claims, insurance claims, auto insurance, home insurance, travel insurance">
	<meta name="description" content="Benson Kearley is an Insurance broker that provides full spectrum of commercial, home and auto and other personal insurance services for over 40 years.">

	<title>Benson Kearley IFG</title>
	<link rel="stylesheet" type="text/css" href="splash/css/UniversalReset.css"/>
	<style>
		#page-container, #header-container, #content-container, #overlay-container {
			width: 100%;
			margin: 0 auto;
		}

		#header-container {
			height: 393px;
			background: url(splash/header-bg.png) repeat-x top center;
		}

		#page-container, #header-container {
			margin-top: 20px;
		}

		#header {

		}

		#page, #header, #content {
			width: 1006px;
			margin: 0 auto;
		}

		#logo {
			position: absolute;
			left: 79px;
			top: 10px;
		}

		#intro {
			font-size: 12px;
			left: 457px;
			position: absolute;
			top: 63px;
			width: 475px;
		}

		body {
			font-family: Verdana, Geneva, sans-serif;
			font-size: 12px;
			line-height: 20px;
			color: #333;
		}

		.inputStyle {
			width: 300px;
			padding: 12px 10px;
			font: 12px Verdana, Geneva, sans-serif;
			margin-bottom: 10px;
			vertical-align: middle;
			border: 1px solid #8c0c04;
			color: #8c0c04;
		}

		form img {
			vertical-align: middle;
		}

		#dialog, #dialog * {
			z-index: 999999;
		}

		td {
			vertical-align: top;
		}

		.input-label {
			margin-left: 10px;
			margin-bottom: 10px;
		}

		h1 {
			font-family: georgia, helvetica, arial, sans-serif;
			color: #8c0c04;
			font-size: 25px;
			line-height: 38px;
		}

		.red {
			color: #8c0c04;
		}

		em {
			font-style: italic;
		}

		#dialog-contact {
			border-bottom: 1px solid #8c0c04;
			color: #8c0c04;
			cursor: pointer;
		}

		textarea {
			resize: none;
		}

		.button {
			font-family: georgia, helvetica, arial, sans-serif;
			background: none repeat scroll 0 0 #8C0C04;
			color: #FFFFFF;
			cursor: pointer;
			font-size: 20px;
			padding: 7px 0;
			text-align: center;
			text-transform: uppercase;
			width: 130px;
		}
	</style>
	<script src="http://www.google.com/jsapi"></script>
	<script type="text/javascript">
		/* GLOBAL VARS */
		if (typeof(google) != "undefined") {
			// Load jQuery
			google.load("jquery", "1.4.3");

			google.setOnLoadCallback(function() {
				// Your code goes here.
			});
		}
		else
			document.write(unescape("%3Cscript src='js/jquery-1.4.3.min.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript" src="splash/js/formCheck.js"></script>
	<script type="text/javascript" src="splash/js/contact-us.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			heightPad = 100;
			if ($.browser.msie)
				heightPad += 100;

			$(window).resize(function() {
				$('#dialog-overlay').width($(document).width()).height($(document).height() + heightPad);
			});
			$('#dialog-overlay').width($(document).width()).height($(document).height() + heightPad);

			$("input[type=text], textarea").blur(
					function() {
						if ($(this).val() == '')
							$(this).val($(this).attr('title'));
					}).focus(function() {
						if ($(this).val() == $(this).attr('title'))
							$(this).val('');
					});

			$("#dialog-submit").click(function() {
				debug("checkContactForm::running");

				if (checkContactForm()) {
					debug("checkContactForm::success");
					$.post("splash/mailer.php", $('#frmContactUs').serialize(), function(data) {
						debug("checkContactForm::ajax-success");
						//debug("data = "+data);
						if (data.success) {
							alert('Thank you for your message.');
							$("#dialog-reset").click();
							$("#dialog-close").click();
						}
						else
							alert('There was an error submitting your request, please try again.');
					}, "json");

					debug("checkContactForm::ajax-finished");
				}
			});
			$("#dialog-reset").click(function() {
				clearAllBoxesContactForm();
			});

			$("#dialog-contact").click(function() {
				$('#dialog, #dialog-overlay').show();
			});
			$("#dialog-close").click(function() {
				$('#dialog, #dialog-overlay').hide();
			});

		});

		function debug($msg) {
			if (window.console && window.console.log)
				window.console.log($msg);
		}
	</script>
</head>
<body>


<div id="overlay-container" style="z-index:99;">
	<div id="dialog-overlay" style="z-index: 99; width: 100%; height: 100%; background: #ebebeb url(splash/dialog-overlay.png); opacity:0.9; filter:alpha(opacity=90); position: absolute; top: -40px; left: 0px; display: none;"></div>
	<div style="width: 1006px;; margin: 0 auto; z-index:999;">
		<div id="dialog" style="position: absolute; display:  none; top: 40px; left: 160px; ">
			<div id="dialog-form" style="background: #fff; opacity:1.0; filter:alpha(opacity=100); width: 700px; height: 440px; padding-top: 12px; padding-left: 20px;">
				<h1 style="margin-bottom: 30px;">Leave us a Message</h1>

				<div id="dialog-close" style="position: absolute; top: 16px; left: 666px; cursor: pointer; background: url(splash/close.png) no-repeat; width: 37px; height: 37px;"></div>
				<form id="frmContactUs" name="frmContactUs" method="post">
					<?
					$_SESSION["security_code"] = substr(rand(1000000, 9999999), 0, 6);
					$security_code_items = str_split($_SESSION["security_code"]);
					echo("<input type='hidden' id='hidSecurityCode' name='hidSecurityCode' value='" . $_SESSION["security_code"] . "' />");
					?>
					<table>
						<tr>
							<td>
								<input type="text" id="txtName" name="txtName" class="inputStyle" value="Name" title="Name" style="margin-right: 20px; vertical-align: bottom;  float:;"/>

								<!--								<div class="input-label">Name</div>-->
							</td>
							<td>
								<input type="text" id="txtEmail" name="txtEmail" class="inputStyle" value="E-mail" title="E-mail" style="float:; vertical-align: bottom;  "/>

								<!--								<div class="input-label">E-mail</div>-->
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="txtPhone" maxlength="10" name="txtPhone" class="inputStyle" value="Phone" title="Phone" style="margin-right: 20px; vertical-align: bottom; float:; "/>

								<!--								<div class="input-label">Phone</div>-->
							</td>
							<td>
								<input type="text" id="txtWebsite" name="txtWebsite" class="inputStyle" value="Website" title="Website" style="vertical-align: bottom; border: 1px solid #333; color: #333; "/>

								<!--								<div class="input-label" style="">Website</div>-->
							</td>
						</tr>
						<tr>
							<td>
								<textarea id="txtComments" name="txtComments" class="inputStyle" style="width: ; height: 150px; font: Verdana, Geneva, sans-serif; font-family: Verdana, Geneva, sans-serif; font-size: 12px;  "
										title="Comments/Questions">Comments/Questions</textarea>

								<!--								<div class="input-label">Comments/Questions</div>-->
							</td>
							<td>
								<table>
									<tr>
										<td>

											<div class="input-label" style="margin-left: 0px;">How did you hear
												<br/> about us?
											</div>

										</td>
										<td>
											<select name="txtHeard" id="txtHeard" class="inputStyle" style="padding: 0; width: 170px;">
												<option value="">Select an option</option>
												<option>Online</option>
												<option>Print / Paper Ad</option>
												<option>Referral</option>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<?="<input name='security_code' id='security_code' class='inputStyle' type='text' value='Security Code' title='Security Code' style='' />"?>
											<!--											<div class="input-label">Security Code</div>-->
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<?
											echo "<div style='float:left; margin: 10px 0px;'><img src=\"splash/validation/left.jpg\" alt=\"Security Code\"/>";
											foreach ($security_code_items as $item)
												echo "<img src=\"splash/validation/" . md5($item) . ".jpg\" alt=\"Security Code\"/>";
											echo "<img src=\"splash/validation/right.jpg\" alt=\"Security Code\"/></div>";
											?>

										</td>
									</tr>
									<tr>
										<td colspan="2">
											<span style=" font: 12px Verdana, Geneva, sans-serif;">Please note: fields in red are required.</span>
											<br/> <br/>

											<div id="dialog-submit" class="button" style="margin-right: 20px; float: left;">Submit</div>
											<div id="dialog-reset" class="button" style="float: left;">Reset</div>
											<div style='clear: both;'></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
				<div style='clear: both;'></div>
			</div>
		</div>
	</div>
</div>


<div id="header-container">
	<div id="header">
		<img src="splash/header.jpg" style="margin-left: 55px;"/>
	</div>
</div>
<div id="page-container">

	<div id="page">

		<div id="content-container">
			<div id="content" style="height: 250px;">
				<div id="logo">
					<img src="splash/logo.png" alt="Experience Renewal Solutions"/>

					<div style="text-align: right; width: 340px; margin-top: 20px;">
						17705 Leslie St. Suite 101, Newmarket, ON, L3Y 3E3<br/>
						<span class="red">Phone:</span> 905-898-3815 <span class="red">Toll Free:</span> 800-463-6503
					</div>

				</div>
				<div id="intro">
					<h1>A <em>brand new</em> look is coming your way...</h1>
					Benson Kearley IFG is an independent insurance agency, leaders at the top of our game. We provide all lines of personal, commercial and specialty insurance programs, as well as wealth management and financial services.
					<br/> <br/>While our new site is being built, please feel free to
					<span id="dialog-contact">leave us a message</span>.
				</div>
				<div style="clear: both; height: 1%"></div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
