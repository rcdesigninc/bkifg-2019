

/*********************************************************************************************
 	RADIO BUTTON VALIDATION BELOW
**********************************************************************************************/
	// copyright Stephen Chapman, 15th Nov 2004,14th Sep 2005
	// you may copy this function but please keep the copyright notice with it
	function valButton(btn, alerttxt) {
		var cnt = -1;

		for (var i=btn.length-1; i > -1; i--) {
			if (btn[i].checked) {cnt = i;}
		}
		if (cnt > -1) return true;
		else {alert(alerttxt); return false;}
	}
/*********************************************************************************************/



/*********************************************************************************************
 	CHECK BOX VALIDATION BELOW
**********************************************************************************************/
	function valCheckboxes(chkName, alerttxt) {
		var count = 0;

		for(var i=1; i <=100; i++){				
			if(document.getElementById(chkName+i)){
				curChk = document.getElementById(chkName+i);
				if(curChk.checked){
					count=count+1;	
				}
			}else{
				break;	
			}
		}
		if(count>0){
			return true;	
		}else{
			alert(alerttxt); return false;
		}
	}
/*********************************************************************************************/



/*********************************************************************************************
 	POSTAL CODE VALIDATION BELOW
**********************************************************************************************/
	//FORMATS the postal code and then checks it.
	function postalCheck(pcode){
		var postalPattern = /^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/;
		var postalRegExp = new RegExp(postalPattern);
		pcode.value= pcode.value.toUpperCase();
		
		if(pcode.value.substring(3,4)!=" "){
			pcode.value=pcode.value.substring(0,3)+" "+pcode.value.substring(3,6);	
		}
		if (pcode.value ==""){
			alert("Please enter your Postal Code.");
			return false;
		}
		if (postalRegExp.test(pcode.value) == false ){
			alert( "This Postal Code is invalid, please enter a valid Postal Code.");
			return false;
		}else{
			return true;	
		}
	}

/*********************************************************************************************/

/*********************************************************************************************
 	EMAIL VALIDATION BELOW
**********************************************************************************************/
		
	function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		
		if (str.indexOf(at)==-1){
		   alert("Please use a valid email address.");
		   return false;
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Please use a valid email address.");
		   return false;
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
			alert("Please use a valid email address.");
		   return false;
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Please use a valid email address.");
		   return false;
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Please use a valid email address.");
		   return false;
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Please use a valid email address.");
		   return false;
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Please use a valid email address.");
		   return false;
		 }

 		 return true					
	}

/*********************************************************************************************/


/*********************************************************************************************
 	DATE VALIDATION BELOW
**********************************************************************************************/
	var dtCh= "/";
	var minYear=1900;
	var maxYear=2100;
	
	function isInteger(s){
		var i;
		for (i = 0; i < s.length; i++){   
			// Check that current character is number.
			var c = s.charAt(i);
			if (((c < "0") || (c > "9"))) return false;
		}
		// All characters are numbers.
		return true;
	}
	
	function stripCharsInBag(s, bag){
		var i;
		var returnString = "";
		// Search through string's characters one by one.
		// If character is not in bag, append to returnString.
		for (i = 0; i < s.length; i++){   
			var c = s.charAt(i);
			if (bag.indexOf(c) == -1) returnString += c;
		}
		return returnString;
	}
	
	function daysInFebruary (year){
		// February has 29 days in any year evenly divisible by four,
		// EXCEPT for centurial years which are not also divisible by 400.
		return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
	}
	function DaysArray(n) {
		for (var i = 1; i <= n; i++) {
			this[i] = 31
			if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
			if (i==2) {this[i] = 29}
	   } 
	   return this
	}
	
	function isDate(dtStr){
		var daysInMonth = DaysArray(12)
		var pos1=dtStr.indexOf(dtCh)
		var pos2=dtStr.indexOf(dtCh,pos1+1)
		var strMonth=dtStr.substring(0,pos1)
		var strDay=dtStr.substring(pos1+1,pos2)
		var strYear=dtStr.substring(pos2+1)
		
		strYr=strYear
		if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
		if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
		for (var i = 1; i <= 3; i++) {
			if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
		}
		month=parseInt(strMonth)
		day=parseInt(strDay)
		year=parseInt(strYr)
		if (pos1==-1 || pos2==-1){
			alert("The date format must be : mm/dd/yyyy")
			return false
		}
		if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
			alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
			return false
		}
		if (strMonth.length<1 || month<1 || month>12){
			alert("Please enter a valid month")
			return false
		}
		if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
			alert("Please enter a valid day")
			return false
		}
		
		if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
			alert("Please enter a valid date")
			return false
		}
	return true
	}

/*********************************************************************************************/


/*********************************************************************************************
 	DATE COMPARISON BELOW
**********************************************************************************************/
var serializedDate = function (dateObject) {
    if (dateObject.constructor == Date) return (Date.parse(new Date(dateObject.getFullYear(), dateObject.getMonth(), dateObject.getDate())));
	return false;
}

function compareDates(firstdate, seconddate){
	//start date must be BEFORE end date
	var days = 0;
	
	var difference = 0;
		
	fdate = new Date(firstdate);	
	sdate = new Date(seconddate);

	fdate=serializedDate(fdate);
	sdate=serializedDate(sdate);
	
	difference = sdate - fdate;
	//alert(difference);
	days = Math.round(difference/(1000*60*60*24));

	if(days>0){ //good
		return true;
	}else{
		return false;	
	}
}


/*********************************************************************************************/


/*********************************************************************************************
 	AGE VALIDATION BELOW
**********************************************************************************************/
function checkAge(dateString,dateType) {
/*
   function getAge
   parameters: dateString dateType
   returns: boolean

   dateString is a date passed as a string in the following
   formats:

   type 1 : 19970529
   type 2 : 970529
   type 3 : 29/05/1997
   type 4 : 29/05/97

   dateType is a numeric integer from 1 to 4, representing
   the type of dateString passed, as defined above.

   Returns string containing the age in years, months and days
   in the format yyy years mm months dd days.
   Returns empty string if dateType is not one of the expected
   values.
*/

    var now = new Date();
    var today = new Date(now.getYear(),now.getMonth(),now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    if (dateType == 1)
        var dob = new Date(dateString.substring(0,4),
                            dateString.substring(4,6)-1,
                            dateString.substring(6,8));
    else if (dateType == 2)
        var dob = new Date(dateString.substring(0,2),
                            dateString.substring(2,4)-1,
                            dateString.substring(4,6));
    else if (dateType == 3)
        var dob = new Date(dateString.substring(6,10),
                            dateString.substring(3,5)-1,
                            dateString.substring(0,2));
    else if (dateType == 4)
        var dob = new Date(dateString.substring(6,8),
                            dateString.substring(3,5)-1,
                            dateString.substring(0,2));
    else
        return '';

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();

    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow -monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--; 
        }
    }

    //return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days';
	if(yearAge<12 || yearAge>80){
		alert('You must be between 12 and 80 years of age to volunteer. Please call us for futher assistance.');
		return false;
	}else{
		return true;
	}
}


/*********************************************************************************************/



/*********************************************************************************************/



/*********************************************************************************************
 	FORM VALIDATION BELOW
**********************************************************************************************/
	function validate_required(field, alerttxt)
	{
		with(field)
		{
			if (value==null||value=="")
			  {alert(alerttxt);return false;}
			else {return true}
		}
	}
	
	//+ Jonas Raoni Soares Silva
	//@ http://jsfromhell.com/string/capitalize [v1.0]
	//usage: [string].capitalize()
	String.prototype.capitalize = function(){
		return this.replace(/\w+/g, function(a){
			return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase();
		});
	};

	

	
/*********************************************************************************************/

