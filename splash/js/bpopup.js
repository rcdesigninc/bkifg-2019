/******************************************************************************************************************
 * @name: bPopup
 * @type: jQuery
 * @author: Bjoern Klinggaard (http://dinbror.dk/bpopup)
 * @version: 0.5.0
 * @requires jQuery 1.3
 *
 * DEFAULT VALUES:
 * amsl(Above Mean Sea Level): 50px // Vertical distance from the middle of the window, + = above, - = under
 * appendTo: 'body' // Which element the popup should append to (append to 'form' when ASP.net)
 * closeClass: 'bClose' // Class to bind the close event to
 * content: 'ajax' // [iframe, ajax]
 * contentContainer: null //if null, contentContainer == $(this)
 * escClose: true // Close on esc
 * fadeSpeed: 250 // Animation speed on fadeIn/out
 * follow: [true, true] // Should the popup follow the screen on scroll/resize? [vertical, horizontal]
 * followSpeed: 500 // Animation speed for the popup on scroll/resize
 * loadUrl: null // External page or selection to load in popup
 * modal: true // Modal overlay
 * modalClose: true // Shold popup close on click on modal overlay?
 * modalColor: #000 // Modal overlay color
 * opacity: 0.7 // Transparency, from 0.1 to 1.0 (filled)
 * position: ['auto','auto'] // Vertical/horizontal start position for popup
 * scrollBar: true // Scrollbars visible
 * zIndex: 9999 // Popup z-index, modal overlay = popup z-index - 1
 *
 * TODO: REFACTOR CODE!!! start position[auto, 10](V), iphone height (modal), onClose/onOpen (V), input reset? (V),
 * center vertical - amsl -> 50px (V), remove xlink? (V), removed input reset/focus/xlink, removed vStart, follow changed to [,]
 *******************************************************************************************************************/
/*;(function($) {
    $.fn.bPopup = function(options, callback) {
        if ($.isFunction(options)) {
            callback = options;
            options = null;
        }
        o = $.extend({}, $.fn.bPopup.defaults, options);
        //HIDE SCROLLBAR?
        if (!o.scrollBar)
            $('html').css('overflow', 'hidden');

        var $selector = $(this),
        $modal = $('<div id="bModal"></div>'),
        d = $(document),
        w = $(window),
        cp = getCenterPosition($selector, o.amsl),
        fixedVPos = o.position[0] != 'auto',
        fixedHPos = o.position[1] != 'auto',
        vPos = fixedVPos ? o.position[0] : cp[0],
        hPos = fixedHPos ? o.position[1] : cp[1],
        agent = navigator.userAgent.toLowerCase(),
        isIphone = agent.indexOf('iphone') != -1,
        isIE6 = (/msie 6/i.test(agent)) && typeof window['XMLHttpRequest'] != 'object'; // browser sniffing is bad!

        //PUBLIC FUNCTION - call it: $(element).bPopup().close();
        this.close = function() {
            o = $selector.data('bPopup');
            close();
        }

        return this.each(function() {
            if ($selector.data('bPopup')) return; //POPUP already exists?
            // MODAL OVERLAY
            if (o.modal) {
                $modal
                .css(getModalStyle())
                .appendTo(o.appendTo)
                .animate({ 'opacity': o.opacity }, o.fadeSpeed);
            }
            $selector.data('bPopup', o);
            // CREATE POPUP
            create();
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // HELP FUNCTIONS - PRIVATE
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        function create() {
            $selector
            .css({ 'left': (!o.follow[1] && fixedHPos) ? hPos : d.scrollLeft() + hPos, 'position': 'absolute', 'top': (!o.follow[0] && fixedVPos) ? vPos : d.scrollTop() + vPos, 'z-index': o.zIndex })
            .appendTo(o.appendTo)
            .hide(1, function() {
                $.isFunction(o.onOpen) && o.onOpen.call($selector);
                if (o.loadUrl != null)
                    createContent();
            })
            .fadeIn(o.fadeSpeed, function() {
                // Triggering the callback if set
                $.isFunction(callback) && callback();
            });
            //BIND EVENTS
            bindEvents();
        }
        function close() {
            if (o.modal) {
                $('#bModal')
                .fadeOut(o.fadeSpeed, function() {
                    $('#bModal').remove();
                });
            }
            $selector.fadeOut(o.fadeSpeed, function() {
                if (o.loadUrl != null) {
                    o.contentContainer.empty();
                }
            });
            unbindEvents();
            if ($.isFunction(o.onClose)) {
                setTimeout(function() {
                    o.onClose.call($selector);
                }, o.fadeSpeed);
            }
            return false;
        }
        function getModalStyle() {
            if (isIE6 || isIphone) {
                var dd = getDocumentDimensions();
                return { 'background-color': o.modalColor, 'height': dd[0], 'left': getDistanceToBodyFromLeft(), 'opacity': 0, 'position': 'absolute', 'top': 0, 'width': dd[1], 'z-index': o.zIndex - 1 };
            }
            else
                return { 'background-color': o.modalColor, 'height': '100%', 'left': 0, 'opacity': 0, 'position': 'fixed', 'top': 0, 'width': '100%', 'z-index': o.zIndex - 1 };
        }
        function createContent() {
            o.contentContainer = o.contentContainer == null ? $selector : $(o.contentContainer);
            switch (o.content) {
                case ('iframe'):
                    $('<iframe scrolling="no" frameborder="0"></iframe>').attr('src', o.loadUrl).appendTo(o.contentContainer);
                    break;
                default:
                    o.contentContainer.load(o.loadUrl);
                    break;
            }
        }
        function bindEvents() {
            $('.' + o.closeClass).live('click', close);
            if (o.modalClose) {
                $('#bModal').live('click', close).css('cursor', 'pointer');
            }
            if (o.follow[0] || o.follow[1]) {
                w.bind('scroll.bPopup', function() {
                    $selector
                   .stop()
                   .animate({ 'left': o.follow[1] ? d.scrollLeft() + hPos : hPos, 'top': o.follow[0] ? d.scrollTop() + vPos : vPos }, o.followSpeed);
                })
           .bind('resize.bPopup', function() {
               // MODAL OVERLAY IE6
               if (o.modal && isIE6) {
                   var dd = getDocumentDimensions();
                   $modal
                        .css({ 'height': dd[0], 'width': dd[1], 'left': getDistanceToBodyFromLeft() });
               }
               // POPUP
               cp = getCenterPosition($selector, o.amsl);
               if (o.follow[0]) { vPos = fixedVPos ? vPos : d.scrollTop() + cp[0]; }
               if (o.follow[1]) { hPos = fixedHPos ? hPos : d.scrollLeft() + cp[1]; }
               $selector
                    .stop()
                    .animate({ 'left': hPos, 'top': vPos }, o.followSpeed);
           });
            }
            if (o.escClose) {
                d.bind('keydown.bPopup', function(e) {
                    if (e.which == 27) {  //escape
                        close();
                    }
                });
            }
        }
        function unbindEvents() {
            if (!o.scrollBar) {
                $('html').css('overflow', 'auto');
            }
            $('.' + o.closeClass).die('click');
            $('#bModal').die('click');
            d.unbind('keydown.bPopup');
            w.unbind('.bPopup');
            $selector.data('bPopup', null);
        }
        function getDocumentDimensions() {
            return [d.height(), d.width()];
        }
        function getDistanceToBodyFromLeft() {
            return (w.width() < $('body').width()) ? 0 : ($('body').width() - w.width()) / 2;
        }
        function getCenterPosition(s, a) {
            var vertical = ((w.height() - s.outerHeight(true)) / 2) - a;
            var horizontal = ((w.width() - s.outerWidth(true)) / 2) + getDistanceToBodyFromLeft();
            return [vertical < 20 ? 20 : vertical, horizontal];
        }
    };
    $.fn.bPopup.defaults = {
        amsl: 50,
        appendTo: 'body',
        closeClass: 'bClose',
        content: 'ajax',
        contentContainer: null,
        escClose: true,
        fadeSpeed: 250,
        follow: [true, true],
        followSpeed: 500,
        loadUrl: null,
        modal: true,
        modalClose: true,
        modalColor: '#000',
        onClose: null,
        onOpen: null,
        opacity: 0.7,
        position: ['auto', 'auto'],
        scrollBar: true,
        zIndex: 9999
    };
})(jQuery);*/


/************************************************************************
 * @name: bPopup
 * @author: (c) Bjoern Klinggaard (http://dinbror.dk/bpopup)
 * @version: 0.5.1.min
 ************************************************************************/
(function(a){a.fn.bPopup=function(h,j){function s(){b.css({left:!o.follow[1]&&k?d:c.scrollLeft()+d,position:"absolute",top:!o.follow[0]&&l?e:c.scrollTop()+e,"z-index":o.zIndex}).appendTo(o.appendTo).hide(1,function(){a.isFunction(o.onOpen)&&o.onOpen.call(b);if(o.loadUrl!=null)switch(o.contentContainer=o.contentContainer==null?b:a(o.contentContainer),o.content){case "iframe":a('<iframe scrolling="no" frameborder="0"></iframe>').attr("src",o.loadUrl).appendTo(o.contentContainer);break;default:o.contentContainer.load(o.loadUrl)}}).fadeIn(o.fadeSpeed,function(){a.isFunction(j)&&j()});t()}function i(){o.modal&&a("#bModal").fadeOut(o.fadeSpeed,function(){a("#bModal").remove()});b.fadeOut(o.fadeSpeed,function(){o.loadUrl!=null&&o.contentContainer.empty()});o.scrollBar||a("html").css("overflow","auto");a("."+o.closeClass).die("click");a("#bModal").die("click");c.unbind("keydown.bPopup");f.unbind(".bPopup");b.data("bPopup",null);a.isFunction(o.onClose)&&setTimeout(function(){o.onClose.call(b)},o.fadeSpeed);return!1}function u(){if(n||v){var a=[c.height(),c.width()];return{"background-color":o.modalColor,height:a[0],left:m(),opacity:0,position:"absolute",top:0,width:a[1],"z-index":o.zIndex-1}}else return{"background-color":o.modalColor,height:"100%",left:0,opacity:0,position:"fixed",top:0,width:"100%","z-index":o.zIndex-1}}function t(){a("."+o.closeClass).live("click",i);o.modalClose&&a("#bModal").live("click",i).css("cursor","pointer");(o.follow[0]||o.follow[1])&&f.bind("scroll.bPopup",function(){b.stop().animate({left:o.follow[1]?c.scrollLeft()+d:d,top:o.follow[0]?c.scrollTop()+e:e},o.followSpeed)}).bind("resize.bPopup",function(){if(o.modal&&n){var a=[c.height(),c.width()];p.css({height:a[0],width:a[1],left:m()})}g=q(b,o.amsl);o.follow[0]&&(e=l?e:c.scrollTop()+g[0]);o.follow[1]&&(d=k?d:c.scrollLeft()+g[1]);b.stop().animate({left:d,top:e},o.followSpeed)});o.escClose&&c.bind("keydown.bPopup",function(a){a.which==27&&i()})}function m(){return f.width()<a("body").width()?0:(a("body").width()-f.width())/2}function q(a,b){var c=(f.height()-a.outerHeight(!0))/2-b,d=(f.width()-a.outerWidth(!0))/2+m();return[c<20?20:c,d]}a.isFunction(h)&&(j=h,h=null);o=a.extend({},a.fn.bPopup.defaults,h);o.scrollBar||a("html").css("overflow","hidden");var b=a(this),p=a('<div id="bModal"></div>'),c=a(document),f=a(window),g=q(b,o.amsl),l=o.position[0]!="auto",k=o.position[1]!="auto",e=l?o.position[0]:g[0],d=k?o.position[1]:g[1],r=navigator.userAgent.toLowerCase(),v=r.indexOf("iphone")!=-1,n=/msie 6/i.test(r)&&typeof window.XMLHttpRequest!="object";this.close=function(){o=b.data("bPopup");i()};return this.each(function(){b.data("bPopup")||(o.modal&&p.css(u()).appendTo(o.appendTo).animate({opacity:o.opacity},o.fadeSpeed),b.data("bPopup",o),s())})};a.fn.bPopup.defaults={amsl:50,appendTo:"body",closeClass:"bClose",content:"ajax",contentContainer:null,escClose:!0,fadeSpeed:250,follow:[!0,!0],followSpeed:500,loadUrl:null,modal:!0,modalClose:!0,modalColor:"#000",onClose:null,onOpen:null,opacity:0.7,position:["auto","auto"],scrollBar:!0,zIndex:9999}})(jQuery);


