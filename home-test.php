<?


$homeVideo = '<iframe id="contest-vid" src="https://player.vimeo.com/video/37266957?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=contest-vid" width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';



$System->addInclude('<link type="text/css" rel="stylesheet" href="'.TEMP_CSS.'Homepage.css" />');



$callout = new Site_page_sections();

$callout->LoadWhere('page_id = ' . Site_page::getIdByTag('home') . ' AND template_section_id = ' . System_columns::Content);



$billboard = $System->renderBillboard();


$preloading = '';
foreach($System->billboard as $b)
    $preloading[] = "'{$b->renderPath()}'";
$preloading = implode(",",$preloading);


$video = <<< EOB
<iframe id="home-vid" src="https://player.vimeo.com/video/37256927?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=home-vid" width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
EOB;

$teamVideo = <<< EOB
<iframe id="home-team-vid" src="https://player.vimeo.com/video/78854760?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=home-team-vid" width="712" height="407" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
EOB;

$System->addJquery(<<< EOB
$(".billboard-button:contains('Watch The Video')").click(function(){
	$("#video-box").RC_Dialog().open();
});
$(".billboard-button:contains('Watch This Video')").click(function(){
	$("#team-video-box").RC_Dialog().open();
});

$.preLoadImages([$preloading]);


var daContestVid = null;

	$(".close").click(function() {
		if(!daContestVid)
	        daContestVid = $f("contest-vid");
		if(!daContestVid){
			daContestVid.api("pause");
			daContestVid.api("seekTo",0);
		}
	});



EOB
);
$System->addCSS("
#video-box {
	border: 0;
	left : 5px !important;
	top: 108px !important;
}
#video-box .heading {
	display: none;
}
#video-box .content {

	padding: 0;
}

#team-video-box {
	border: 0;
	left : 5px !important;
	top: 108px !important;
}
#team-video-box .heading {
	display: none;
}
#team-video-box .content {

	padding: 0;
}


#contest-video-box {
	border: 0;
left: 5px;
position: absolute;
top: 107px;

}
#contest-video-box .heading {
	display: none;
}
#contest-video-box .content {
	padding: 0;
}
.dialog-fix {
	left: 5px;
	top: 107px;
}

p .italic {
font-size:25px;
}

.red {
font-size: 40px;
display:inline-block;
position:relative;
padding-top:4px;
padding-bottom:4px;
}

.content p {
margin-bottom: 6px;
}

.content .smaller {
font-size: 24px !important;
}

.sans-serif {
font-family: Verdana !important;
font-size: 23px !important;
color: #4c4c4e !important;
line-height: 30px;
}

");

//if($_GET['test'])
//{
$System->addJquery("
	setTimeout(function () {
		jQuery('#auto-popup').RC_Dialog().open();
	}, 200);
");
	$autoPopup = '

	<img src="app/Templates/images/header/header-logo.png" alt="Logo">
	<hr />

	<p>
	<span class="italic">For the second consecutive year</span>
	<img src="app/Templates/images/modal-profit.png" alt="Logo" style="position:relative; top:13px;">
	<img src="app/Templates/images/modal-profit2014.png" style="position:relative; top:14px;" width="118" height="175" alt="2014 Logo" />
	<span class="red">Benson Kearley IFG</span>
	<p class="smaller">has been named to</p>
	<p class="sans-serif">Profit Magazine Top 500 Fastest-<br/>Growing Companies in Canada!</p>

';
System_core::Dialog('auto-popup',$autoPopup, '','',array('appendTo'=>'#header'));//,'position'=>"new Array(10,0)"
//}
System_core::Dialog('video-box',$video, '','',array('appendTo'=>'#header'));//,'position'=>"new Array(10,0)"

System_core::Dialog('team-video-box',$teamVideo, '','',array('appendTo'=>'#header'));//,'position'=>"new Array(10,0)"

System_core::Dialog('contest-video-box',$homeVideo, '','',array('appendTo'=>'#header-container'));//,'position'=>"new Array(10,0)"

$System->addCSS(<<< EOB

#billboard-container, #billboard-inside {
	height: 645px;
}

#contentRight .content-section {

	padding: 0 17px 20px;

	margin-bottom: 35px;

}

.short-title {
	margin-top: 30px;
}

.billboard-content-container {
	display: block;
	padding-left: 0;
}


.billboard-content {
	display: block;
	margin-left: 15px;
	margin-top: 20px;
}

EOB

);

$billboard .= '
<!--[if lte IE 8]>
<style type="text/css">
#billboard-container {
	height: 613px;
}
#billboard-inside {
	height: 643px;
}
.grad-box {
	bottom: 25px;
}
</style>
<![endif]-->
';

$billboard .= <<< EOB

<div id="home-box-container" class="clear-float">

	<div><a href="{ROOT_PATH}financial"><img src="{TEMP_IMGS}/home/home-box3.png"/><img src="{TEMP_IMGS}/home/home-box3-over.png" class="hidden"/></a></div>

	<div><a href="{ROOT_PATH}commercial"><img src="{TEMP_IMGS}/home/home-box2.png"/><img src="{TEMP_IMGS}/home/home-box2-over.png" class="hidden"/></a></div>

	<div><a href="{ROOT_PATH}personal"><img src="{TEMP_IMGS}/home/home-box1.png"/><img src="{TEMP_IMGS}/home/home-box1-over.png" class="hidden"/></a></div>

</div>

<div id="grad-box" class="grad-box">

	<div id="callout-copy">

		<div class="callout-title">$callout->heading</div>

		<div class="callout-content">$callout->content</div>

	</div>

	<div class="callout-buttons">

		<ul class="quote-b-list">

			<li><a href="https://www2.compu-quote.com/ezleadsplus/introhab.asp?ORGSITE=ezLeadsplus&BRKRCDE=BKAI" target="_blank" class="home-quote"></a></li>

			<li><a href="https://www2.compu-quote.com/ezleadsplus/intro.asp?ORGSITE=ezLeadsplus&BRKRCDE=BKAI" target="_blank" class="auto-quote"></a></li>

		</ul>

	</div>

</div>

<div class='clear-float'></div>

EOB;



$billboard = str_replace('{ROOT_PATH}', System_defaults::Root_web_path, $billboard);

$billboard = str_replace('{TEMP_IMGS}', TEMP_IMGS, $billboard);







$toggle = false;

$i = 0;

foreach ($System->sections as $s) {

    /** @var $s Site_page_sections */

    $float = 'clear-float';

    $i++;

    if ($i == 2 || $i == 3)

        $float = 'float-box-' . ($i % 2 == 0 ? 'left' : 'right');



    $content .= $s->getContent('', $float);



}



$System->renderDefaultPage($content, '', $billboard);
