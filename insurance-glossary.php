<?

$ps = new Site_page_sections();

$ps->heading = 'Insurance Glossary';


$faq_list = new Site_page_sections();


$faq_list = $faq_list->Select("heading, length(content) as `content`",

		"page_id = " . Site_page::getIdByTag(PAGE_TAG,
			Sites::Main) . " AND template_section_id = " . System_columns::Content_faq,

	"heading");


$letter = 'A';

if ($_GET['letter'])

	$letter = strtoupper($_GET['letter']);


foreach ($faq_list as $f) {

	$selected = '';

	if ($letter == $f->heading)

		$selected = 'glossary-selected';

	$link = "index.php?section=insurance-glossary&letter={$f->heading}";

	if ($f->content == 0) {

		$link = "#";

		$selected = 'no-link';

	}


	$ps->content .= "<a class='glossary-selection {$selected}' href='$link'>{$f->heading}</a>";

}


$System->addCSS('

.content-section a.glossary-selection {

	font-weight: bold;

	margin-right: 10px;

	text-decoration: none;

	font-size: 14px;

}

.content-section a.glossary-selected, .content-section a.glossary-selection:hover {

	text-decoration: underline;

}

.content-section a.no-link, .content-section a.no-link:hover {

	font-weight: normal;

	text-decoration: none;

	cursor: default;

}

');


$faq_content = new Site_page_sections();

$faq_content->LoadWhere("heading = '$letter' AND page_id = " . Site_page::getIdByTag(PAGE_TAG,
		Sites::Main) . " AND template_section_id = " . System_columns::Content_faq);


$System->addJquery('

//$(".glossary-selection:contains(Y)").attr("href","#").disable();

$("#contentLeft p strong").parent().css("margin-bottom","0");

');


$System->addCSS('



');


//$content = "<div id='scroll-btt'><a href='#'><img src='".TEMP_IMGS."back2top-mini.png'/></a></div>";

$content .= $ps->getContent();

$content .= $faq_content->getContent();

$ps->heading = '';
$content .= $ps->getContent();


if(ACTIVE_SITE > 3)
	include("_strip-quote.php");
else
	$System->renderDefaultPage($content);



/*

 * EOF

 */